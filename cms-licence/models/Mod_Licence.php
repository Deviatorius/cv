<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Advert Model
	 */
	class Mod_Licence extends CI_Model {


		/** ***************************************************************************************
		 * 	BUILDS A MODEL
		 *  ***************************************************************************************
		 */	


        /**
         * Defines a Package model
         *
         * @param array $import     The Data to Import (Usually a Database Row)
         * @param model $existing   A Existing Model (Or null to Create New)
         * @return void
         */
		public function package_model( $import = null, $existing = null ) {

            // Create Submission
			if ( empty($existing) || ! is_array($existing) ) {
				$existing = [
                    "id"		        =>	NULL,
                    "description"       =>  "",
                    "items"             =>  [

                    ]
                ];
            }

            // Populate Model
			if ( ! empty($import) ) {

                // Read Model Information
                if ( array_key_exists('id', $import) )              $existing['id']                     =	empty($import['id'])                ?	NULL	    : $import['id'];
                if ( array_key_exists('description', $import) )     $existing['description']            =	empty($import['description'])       ?	NULL	    : $import['description'];
                
            }

			// Return
            return $existing;
            
        }
        

        /**
         * Defines a Package Item Model
         *
         * @return void
         */
        public function package_item_model( $import = null, $existing = null ) {

            // Create Submission
			if ( empty($existing) || ! is_array($existing) ) {
				$existing = [
                    "package"           =>      null,
                    "product"           =>      null,
                    "credit_cost"       =>      0.00,
                    "credit_duration"   =>      0 
                ];
            }

            // Populate Model
			if ( ! empty($import) ) {
                if ( array_key_exists('package', $import) )         $existing['package']            =	empty($import['package'])           ?	NULL	    : $import['package'];
                if ( array_key_exists('product', $import) )         $existing['product']            =	empty($import['product'])           ?	NULL	    : $import['product'];
                if ( array_key_exists('credit_cost', $import) )     $existing['credit_cost']        =	empty($import['credit_cost'])       ?	0	        : $import['credit_cost'];
                if ( array_key_exists('credit_duration', $import) ) $existing['credit_duration']    =	empty($import['credit_duration'])   ?	0	        : $import['credit_duration'];
                if ( array_key_exists('cc', $import) )              $existing['credit_cost']        =	empty($import['cc'])                ?	0	        : $import['cc'];
                if ( array_key_exists('cd', $import) )              $existing['credit_duration']    =	empty($import['cd'])                ?	0	        : $import['cd'];
            }

			// Return
            return $existing;

        }        
        
        
        /**
        * Defines a Player Product model
        *
        * @param array $import     The Data to Import (Usually a Database Row)
        * @param model $existing   A Existing Model (Or null to Create New)
        * @return void
        */
       public function player_product_model( $import = null, $existing = null ) {

           // Create Submission
           if ( empty($existing) || ! is_array($existing) ) {
               $existing = [
                   "id"		            =>	NULL,
                   "player"             =>  NULL,
                   "product"            =>  NULL,
                   "description"        =>  NULL,
                   "start"              =>  NULL,
                   "end"                =>  NULL,
                   "credit_cost"        =>  0.00,
                   "credit_duration"    =>  0
               ];
           }

           // Populate Model
           if ( ! empty($import) ) {

               // Read Model Information
               if ( array_key_exists('id', $import) )               $existing['id']                   =	empty($import['id'])                ?	NULL	    : $import['id'];
               if ( array_key_exists('player', $import) )           $existing['player']               =	empty($import['player'])            ?	NULL	    : $import['player'];
               if ( array_key_exists('product', $import) )          $existing['product']              =	empty($import['product'])           ?	NULL	    : $import['product'];
               if ( array_key_exists('description', $import) )      $existing['description']          =	empty($import['description'])       ?	NULL	    : $import['description'];
               if ( array_key_exists('start', $import) )            $existing['start']                =	empty($import['start'])             ?	NULL	    : $import['start'];
               if ( array_key_exists('end', $import) )              $existing['end']                  =	empty($import['end'])               ?	NULL	    : $import['end'];
               if ( array_key_exists('credit_cost', $import) )      $existing['credit_cost']          =	empty($import['credit_cost'])       ?	0	        : $import['credit_cost'];
               if ( array_key_exists('credit_duration', $import) )  $existing['credit_duration']      =	empty($import['credit_duration'])   ?	0	        : $import['credit_duration'];
               if ( array_key_exists('cc', $import) )               $existing['credit_cost']          =	empty($import['cc'])                ?	0	        : $import['cc'];
               if ( array_key_exists('cd', $import) )               $existing['credit_duration']      =	empty($import['cd'])                ?	0	        : $import['cd'];
               
           }

           // Return
           return $existing;
           
       }





        /**
         * Gets All Products from the Database
         *
         * @return void
         */
        public function get_products() {

            // Grab Cash Control Entity
            $q  =   $this->db->query("  SELECT  *
                                        FROM    `licence_product`
                                    ")->result_array();

            // Process Items
            if ( empty($q) )    return [];

            // Array
            $r = [];

            // Loop Items
            foreach ( $q as $k => $v ) {
                $r[$v['id']] = $v['description'];
            }

            // Return
            return $r;

        }


        /**
         * Gets All Packages from the Database
         *
         * @return void
         */
        public function get_packages() {

            // Grab Cash Control Entity
            $q  =   $this->db->query("  SELECT  *
                                        FROM    `licence_package`
                                    ")->result_array();

            // Process Items
            if ( empty($q) )    return [];

            // Hold Items
            $r = [];

            // Loop Items
            foreach ( $q as $k => $v ) {
                $r[$v['id']] = $this->package_model( $v, NULL);
            }

            // Return
            return $r;

        }


        /**
         * Gets a Package from the Database
         *
         * @param [type] $id    The Id to Pull
         * @return void
         */
        public function get_package( $id ) {

            // Grab Cash Control Entity
            $q  =   $this->db->query("  SELECT  *
                                        FROM    `licence_package`
                                        WHERE   `id` = ? 
                                    ", [$id])->result_array();

            // Process Items
            if ( empty($q) )    return NULL;

            // Get Item
            $item     =   $this->package_model( $q[0], NULL);
            
            // Add Items
            $q2  =  $this->db->query("  SELECT *
                                        FROM    `licence_package_item`
                                        WHERE   `package` = ?
                                    ", [ $item['id'] ])->result_array();

            // Loop Items
            foreach ( $q2 as $k => $v ) {
                $item["items"][$v['product']]    =   $this->package_item_model( $v, NULL);
            }

            // Return
            return $item;

        }


        /**
         * Obtains Licences Assign to Player
         *
         * @param string $id
         * @return array
         */
        public function get_licences( $id ) {
          
            // Grab Products
            $q  =   $this->db->query("  SELECT  *
                                        FROM    `licence`
                                        WHERE   `player` = ?
                                    ", [$id])->result_array();

            // Process Items
            if ( empty($q) )    return [];
            return $q;

        }


        /**
         * Deletes a Package
         *
         * @param [type] $id
         * @return void
         */
        public function delete_package( $id ) {

            if (! empty($id) ){

                $this->db->query("  DELETE
                                    FROM    `licence_package_item`
                                    WHERE   `package` = ?
                                ",  [$id]);

                $this->db->query("  DELETE
                                    FROM    `licence_package`
                                    WHERE   `id` = ?
                                ",  [$id]);
                return true;
            }
            return false;
        }


        /**
         * Delete Product from Licenced Player
         *
         * @param string $id
         * @return bool
         */
        public function delete_licence( $id ) {
            
            if (! empty($id) ){

                $this->db->query("  DELETE
                                    FROM    `licence`
                                    WHERE   `id` = ?
                                ",  [$id]);

                return true;
            }
            return false;
        }


        /**
         * Save a Package
         *
         * @param [type] $model
         * @return void
         */
        public function save_package( &$model ) {

			// Check Playlist
			if ( empty($model) || ! is_array($model) )	        return FALSE;
		
			// Start Transaction
			$this->db->trans_start();

			// Create Playlist
			if ( empty($model['id']) ) {

				// Create Playlist
				$this->db->query("	INSERT INTO `licence_package`
										(`id`, `description`)
									VALUES
										(NULL, ?)
								",
									Array (
										$model['description']
									)
								);

				// Set ID
				$model['id'] = $this->db->insert_id();

			}
			else {

				// Update Playlist
				$this->db->query("	UPDATE `licence_package`
									SET	
                                        `description`   =   ?
									WHERE
										`id`		    =   ?
								",
									Array (
										$model['description'] , $model['id']
									)
								);

			}

            // Delete All from the Package
            $this->db->query("  DELETE FROM `licence_package_item` WHERE `package` = ? ", [ $model['id'] ]);

            // Re-add All Where Toggle = True
            foreach ($model['items'] as $product => $details) {

                // Inject Item
                $this->db->query("  INSERT INTO `licence_package_item` 
                                        (`package`, `product`, `credit_cost`, `credit_duration`)
                                    VALUES  
                                        (?, ?, ?, ?)
                                ",  
                                    [ $model['id'], $product, $details['credit_cost'], $details['credit_duration'] ]
                                );

                
            }

			// Commit
			$this->db->trans_complete();

			// Return
			return $model['id'];

        }


        /**
         * Assigns a Package to the Given Player
         *
         * @param array $data
         * @return void
         */
        public function assign_package( $data ) {

			// Check if Model is Set
			if ( empty($data) || ! is_array($data) )	        return FALSE;

            // Handle Dates
            $data['start']		=	gmdate("Y-m-d H:i:s", $data['start']) . " UTC";
            $data['end']		=	gmdate("Y-m-d H:i:s", $data['end']) . " UTC";
            		
            // Retrieve package details
            $package = [];

            $package = $this->db->query(" SELECT * FROM `licence_package_item` WHERE `package` = ?", [$data['package']])->result_array();
            //Start Transaction
			$this->db->trans_start();

            foreach ($package as $key => $value) {
                
                // Add Each Product
				$this->db->query("	INSERT INTO `licence`
										(`id`, `player`, `product`, `description`, `start`, `end`, `credit_cost`, `credit_duration`)
									VALUES
										(NULL, ?, ?, ?, ?, ?, ?, ?)
								",
									Array (
										$data['player'],
										$value['product'],
										$data['description'],
										$data['start'],
										$data['end'],
										$value['credit_cost'],
										$value['credit_duration']
									)
								);
            }

			// Commit
			$this->db->trans_complete();

        }


        /**
         * Assigns Products to the Given Player
         *
         * @param array $model
         * @return void
         */
        public function assign_products( &$model, $description = "" ) {

			// Check if Model is Set
			if ( empty($model) || ! is_array($model) )	        return FALSE;

            // Start Transaction
			$this->db->trans_start();
            

            foreach ($model as $key => $value) {
                
				// Handle Dates
				$value['start']		=	gmdate("Y-m-d H:i:s", $value['start']) . " UTC";
				$value['end']		=	gmdate("Y-m-d H:i:s", $value['end']) . " UTC";

				// Add New Licence
				$this->db->query("	INSERT INTO `licence`
										(`id`, `player`, `product`, `description`, `start`, `end`, `credit_cost`, `credit_duration`)
									VALUES
										(NULL, ?, ?, ?, ?, ?, ?, ?)
								",
									Array (
										$value['player'],
										$value['product'],
										$description,
										$value['start'],
										$value['end'],
										$value['credit_cost'],
										$value['credit_duration']
									)
								);
                
            }

			// Commit
			$this->db->trans_complete();

        }
    }