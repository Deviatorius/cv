<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
	<div class="option">
		<a href="<?php echo base_url("licence"); ?>"><img src="<?php echo base_url("images/licence/licence.png"); ?>"></a><br />
		[ LICENCE ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("setup"); ?>"><img src="<?php echo base_url("images/setup/logo.png"); ?>"></a><br />
		[ SETUP ]
	</div>
</div>

<img class="section_logo" src="<?php echo base_url("images/licence/licence_delete.png"); ?>">

<h1>Licensed Products</h1>
<p>View All Licensed Products</p>
<hr style="clear:both; margin-top: 35px" />

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>


<h2>Licensed Products</h2>
<p>Use the below table to edit desired packages</p>


<table class="data_table">
	<col style="width: 400px" />
	<col />
	<thead>
		<tr class="first blue">
			<td colspan="2">Current Products</td>
		</tr>
		<tr class="second blue">
			<td>Name</td>
			<td>Description</td>
		</tr>
	</thead>
	<tbody>
	<?php
		if (isset($products) && count($products) > 0) {
            
			foreach ($products as $key => $product) {
				?>
					<tr>
						<td><?php echo html_escape($key); ?></td>
						<td><?php echo html_escape($product); ?></td>
					</tr>
				<?php				
			}
		}
		else {
			?>
				<tr>
					<td colspan="3" style="padding: 10px; text-align: center">
						No products have been setup
					</td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>