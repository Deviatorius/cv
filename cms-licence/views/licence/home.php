<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<img class="section_logo" src="<?php echo base_url("images/licence/licence.png"); ?>">

<h1>Licence Options</h1>
<p>Setup Licensing</p>
<hr style="clear:both; margin-top: 35px" />

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>

<h2>Licensing</h2>
<p>To change licensing settings select an applicable option</p>

<div class="grid-container grid-container--fit">
	<div class="grid-element">
  		<a href="<?php echo base_url("setup"); ?>">
		  	<img src="<?php echo base_url("images/setup/logo.png"); ?>">
		</a>
		<p>Back to Setup</p>
	</div>
  	<div class="grid-element">
  		<a href="<?php echo base_url("licence/products"); ?>">
		  	<img src="<?php echo base_url("images/licence/licence.png"); ?>">
		</a>
		<p>View licencensable products</p>
	</div>
	<div class="grid-element">
  		<a href="<?php echo base_url("licence/packages"); ?>">
		  	<img src="<?php echo base_url("images/licence/package.png"); ?>">
		</a>
		<p>Control and edit licence packages</p>
	</div>
</div>
