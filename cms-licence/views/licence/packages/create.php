<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
	<div class="option">
		<a href="<?php echo base_url("licence/packages"); ?>"><img src="<?php echo base_url("images/licence/package.png"); ?>"></a><br />
		[ PACKAGES ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("licence"); ?>"><img src="<?php echo base_url("images/licence/licence.png"); ?>"></a><br />
		[ LICENCE ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("setup"); ?>"><img src="<?php echo base_url("images/setup/logo.png"); ?>"></a><br />
		[ SETUP ]
	</div>
</div>

<img class="section_logo" src="<?php echo base_url("images/licence/package_create.png"); ?>">

<h1>Create Package</h1>
<p>Creating a New Package</p>
<hr style="clear:both; margin-top: 35px" />

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>

<h2>Create Package</h2>
<p>Use the below form to create a new package.</p>


<?php	echo form_open( $this->router->fetch_class() . "/create"); ?>
<table class="info_table form" style="width: 100%">
	<col style="width: 400px" />
	<col style="width: 70px" />
	<col style="width: 140px" />
	<col style="width: 140px" />
	<col />
	<thead>
        <tr class="first blue">
			<td colspan="5">Package</td>
		</tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5" class="section blue">Package Details</td>
        </tr>
        <tr>
            <td class="second blue">Description</td>
            <td colspan="4"><?php   echo form_input(['name' => 'data[description]', 'placeholder' => 'Enter Description'], set_value('data[description]', '', FALSE), ['style' => 'width: 100%']); ?></td>
        </tr>
    </tbody>
    <thead>
		<tr>
			<td class="section blue">Product</td>
			<td class="section blue">Enabled</td>
            <td class="section blue">Cost Per Use(£)</td>
			<td class="section blue">Duration(hours)</td>
			<td class="section blue">Description</td>
		</tr>
	</thead>
	<tbody>
	<?php
    
		if (isset($package) && count($package) > 0 && isset($products)) {                      
			foreach ($products as $product => $description) {
                ?>
					<tr>
						<td  class="second blue"><?php   echo html_escape($product); ?></td>
                        <td><?php   echo form_checkbox('data[' . $product . '][toggle]', 'TRUE', set_checkbox('data[' . $product . '][toggle]', 'TRUE', isset($package['items'][$product])), ["style" => "width: 15px;", "class" => "cb", "id" => "cb" . $product]); ?>
                            <?php	echo form_label('', "cb" . $product, ["class" => "cb-label"]); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][cc]', 'placeholder' => 'Enter Amount'], set_value('data[' . $product . '][cc]', '', FALSE), ['style' => 'width: 120px']); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][cd]', 'placeholder' => 'Enter Amount'], set_value('data[' . $product . '][cd]', '', FALSE), ['style' => 'width: 120px']); ?></td>
                        <td><?php   echo html_escape($description); ?></td>
                    </tr>
				<?php
			}
		}
		else {
			?>
				<tr>
					<td colspan="5" style="padding: 10px; text-align: center">
						No products have been setup
					</td>
				</tr>
			<?php
		}
	?>
	</tbody>
	<thead>
        <tr class="first blue">
			<td colspan="5">Create Package</td>
		</tr>
    </thead>
    <tbody>
        <tr>
            <td class="second blue">Create Package</td>
            <td colspan="4"><?php echo form_submit('data[submit]', 'Create'); ?></td>
        </tr>
    </tbody>
</table>
<?php	echo form_close(""); ?>