<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
	<div class="option">
		<a href="<?php echo base_url("licence/create"); ?>"><img src="<?php echo base_url("images/licence/package_create.png"); ?>"></a><br />
		[ CREATE ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("licence"); ?>"><img src="<?php echo base_url("images/licence/licence.png"); ?>"></a><br />
		[ LICENCE ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("setup"); ?>"><img src="<?php echo base_url("images/setup/logo.png"); ?>"></a><br />
		[ SETUP ]
	</div>
</div>

<img class="section_logo" src="<?php echo base_url("images/licence/package.png"); ?>">

<h1>Licensed Packages</h1>
<p>View and Edit All Available Licensed Packages</p>
<hr style="clear:both; margin-top: 35px" />

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>

<h2>Licensed Packages</h2>
<p>Use the below table to edit desired packages</p>


<table class="data_table">
	<col style="width: 100px" />
	<col />
	<col style="width: 70px" />
	<thead>
		<tr class="first blue">
			<td colspan="3">Current Packages</td>
		</tr>
		<tr class="second blue">
			<td>ID</td>
			<td>Description</td>
			<td align="center">Edit</td>
		</tr>
	</thead>
	<tbody>
	<?php
		if (isset($packages) && count($packages) > 0) {
            
			foreach ($packages as $key => $package) {
				?>
					<tr>
						<td><?php echo html_escape($package['id']); ?></td>
						<td><?php echo html_escape($package['description']); ?></td>
						<td align="center"><a href="<?php echo base_url("licence/package/{$package['id']}"); ?>"><img src="<?php echo base_url("images/edit_icon.png"); ?>"></a></td>
					</tr>
				<?php				
			}
		}
		else {
			?>
				<tr>
					<td colspan="3" style="padding: 10px; text-align: center">
						No packages have been setup
					</td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>