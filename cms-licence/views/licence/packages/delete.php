<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
	<div class="option">
		<a href="<?php echo base_url("licence/package/{$package['id']}"); ?>"><img src="<?php echo base_url("images/licence/package.png"); ?>"></a><br />
		[ RETURN ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("licence/packages"); ?>"><img src="<?php echo base_url("images/licence/package.png"); ?>"></a><br />
		[ PACKAGES ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("licence"); ?>"><img src="<?php echo base_url("images/licence/licence.png"); ?>"></a><br />
		[ LICENCE ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("setup"); ?>"><img src="<?php echo base_url("images/setup/logo.png"); ?>"></a><br />
		[ SETUP ]
	</div>
</div>

<img class="section_logo" src="<?php echo base_url("images/licence/package.png"); ?>">

<h1>Delete Package</h1>
<p>Use this section to delete this package</p>
<b>Deleted package cannot be restored!</b>
<hr style="clear:both; margin-top: 35px" />

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>

<h2>Delete Package</h2>
<p>Use the below form to delete package - <?php echo html_escape($package['description']) ?></p>

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>

<?php	echo form_open( $this->router->fetch_class() . "/delete_package/{$package['id']}"); ?>
<table class="info_table form" style="max-width: 420px;">
	<col />
	<thead>
        <tr class="first red">
			<td colspan="1">Are you sure you want to delete the package?</td>
		</tr>
	</thead>
	<tbody>
        <tr class="second red">
            <td><?php	echo form_submit('data[submit]', 'DELETE'); ?></td>
        </tr>
	</tbody>
</table>
<?php	echo form_close(""); ?>