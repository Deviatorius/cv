<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
	<div class="option">
		<a href="<?php echo base_url("status/view/{$player['id']}"); ?>"><img src="<?php echo base_url("images/licence/licence.png"); ?>"></a><br />
		[ BACK ]
	</div>
	<div class="option">
		<a href="<?php echo base_url("licence/playerview/{$player['id']}/assign"); ?>"><img src="<?php echo base_url("images/licence/licence.png"); ?>"></a><br />
		[ ASSIGN ]
	</div>
</div>

<img class="section_logo" src="<?php echo base_url("images/licence/licence.png"); ?>">
<h1>Current Player Licences</h1>
<p>View Currently Assigned Licences</p>
<hr style="clear:both" />

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);	
?>

<h2>Active Licences</h2>
<p>Licences currently assigned to this Player</p>

<table class="info_table form" style="width: 100%">
	<col style="width: 400px" />
	<col style="width: 160px" />
	<col style="width: 160px" />
	<col style="width: 140px" />
	<col style="width: 140px" />
	<col />
	<col style="width: 70px" />
	<thead>
        <tr class="first blue">
			<td colspan="7">Product Assignment Form</td>
		</tr>
    </thead>
    <thead>
		<tr>
			<td class="section blue">Product</td>
			<td class="section blue">Start Time</td>
			<td class="section blue">End Time</td>
            <td class="section blue">Cost Per Use(£)</td>
			<td class="section blue">Duration(hours)</td>
			<td class="section blue">Description</td>
			<td class="section blue">Delete</td>
		</tr>
	</thead>
	<tbody>
	<?php
    
		if ( isset($licences) && count($licences) > 0 ) {                      
			foreach ($licences as $key => $value) {
                ?>
				<tr>
					<td class="second blue"><?php echo $value['product'] ?></td>
					<td><?php echo $value['start'] ?></td>
					<td><?php echo $value['end'] ?></td>
					<td><?php echo $value['credit_cost'] ?></td>
					<td><?php echo $value['credit_duration'] ?></td>
					<td><?php echo $value['description'] ?></td>
					<td align="center"><a href="<?php echo base_url("licence/deletelicence/{$id}/{$value['id']}"); ?>"><img src="<?php echo base_url("images/delete.png"); ?>"></a></td>
				</tr>
				
				<?php
			}
		}
		else {
			?>
				<tr>
					<td colspan="7" style="padding: 10px; text-align: center">
						No products have been setup
					</td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>


<h2>Active Subscriptions</h2>
<p>Subscriptions currently assigned to this Player</p>
