<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
	<div class="option">
		<a href="<?php echo base_url("licence/playerview/{$player['id']}"); ?>"><img src="<?php echo base_url("images/licence/licence.png"); ?>"></a><br />
		[ BACK ]
	</div>
</div>

<img class="section_logo" src="<?php echo base_url("images/licence/licence.png"); ?>">
<h1>Current Player Licences</h1>
<p>View Currently Assigned Licences</p>
<hr style="clear:both" />
<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);

    $packageDropdown    =   [];

    foreach ($packages as $key => $value) {
        $packageDropdown[$key] = $value['description'];
        
    }
?>
<h2>Package</h2>
<p>Use the form below to assign a new package to the player.</p>


<?php	echo form_open( $this->router->fetch_class() . "/playerview/{$player['id']}/assign"); ?>

<table class="info_table form">
	<col style="width: 200px" />
	<col style="width: 140px" />
	<col style="width: 80px" />
	<col style="width: 140px" />
	<col />
	<thead>
		<tr class="first green">
			<td colspan="6">Packages Assignment Form</td>
		</tr>
		<tr class="second green">
			<td class="section green">Package</td>
			<td class="section green">Start Date</td>
			<td class="section green">Start Time</td>
			<td class="section green">End Date</td>
			<td class="section green">End Time</td>
			<td class="section green">Description</td>
		</tr>
	</thead>
	<tbody>
        <tr>
			<?php   echo form_input(['type' => 'hidden', 'name' => 'data[player]'], $player['id']); ?>
            <td  class="second green"> <?php echo form_dropdown(['name' => 'data[package]'], $packageDropdown, set_value('data[package]', "", FALSE), ['style' => 'width: 180px', 'class' => 'form_dropdown']); ?></td>
        
			<td><?php   echo form_input(['name' => 'data[start_date]', 'autocomplete' => 'off', 'placeholder' => 'Enter Start Date'], set_value('data[start_date]', '', FALSE), ['class' => 'dp', 'style' => 'width: 120px']); ?></td>
			<td><?php   echo form_input(['name' => 'data[start_time]', 'autocomplete' => 'off',  'placeholder' => 'Enter Start Time'], set_value('data[start_time]', '00:00', FALSE), ['class' => 'tp', 'style' => 'width: 60px']); ?></td>
			<td><?php   echo form_input(['name' => 'data[end_date]', 'autocomplete' => 'off',  'placeholder' => 'Enter End Date'], set_value('data[end_date]', '', FALSE), ['class' => 'dp', 'style' => 'width: 120px']); ?></td>
			<td><?php   echo form_input(['name' => 'data[end_time]', 'autocomplete' => 'off',  'placeholder' => 'Enter End Time'], set_value('data[end_time]', '23:59', FALSE), ['class' => 'tp', 'style' => 'width: 60px']); ?></td>
			<td><?php   echo form_input(['name' => 'data[description]', 'placeholder' => 'Enter Description'], set_value('data[description]', '', FALSE), ['style' => 'width: 240px']); ?></td>

		</tr>
	</tbody>
	<thead>
        <tr class="first green">
			<td colspan="9">Form Submission</td>
		</tr>
    </thead>
    <tbody>
        <tr>
            <td class="second green">Add Package</td>
            <td colspan="6"><?php echo form_submit('data[submit]', 'Add Package'); ?></td>
        </tr>
    </tbody>
</table>

<?php	echo form_close(""); ?>


<h2>Products</h2>
<p>Use the form below to assign a new product to the player.</p>

<?php	echo form_open( $this->router->fetch_class() . "/playerview/{$player['id']}/assign"); ?>


<table class="info_table form" style="width: 100%">
	<col style="width: 400px" />
	<col style="width: 70px" />
	<col style="width: 140px" />
	<col style="width: 80px" />
	<col style="width: 140px" />
	<col style="width: 80px" />
	<col style="width: 140px" />
	<col style="width: 140px" />
	<col />
	<thead>
        <tr class="first green">
			<td colspan="9">Product Assignment Form</td>
		</tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="9" class="section green">Licensing Description</td>
        </tr>
        <tr>
            <td class="second green">Description</td>
            <td colspan="8"><?php   echo form_input(['name' => 'data[description]', 'placeholder' => 'Enter Description'], set_value('data[description]', '', FALSE), ['style' => 'width: 100%']); ?></td>
        </tr>
    </tbody>
    <thead>
		<tr>
			<td class="section green">Product</td>
			<td class="section green">Enabled</td>
			<td class="section green">Start Date</td>
			<td class="section green">Start Time</td>
			<td class="section green">End Date</td>
			<td class="section green">End Time</td>
            <td class="section green">Cost Per Use(£)</td>
			<td class="section green">Duration(days)</td>
			<td class="section green">Description</td>
		</tr>
	</thead>
	<tbody>
	<?php
    
		if (isset($package) && count($package) > 0 && isset($products)) {                      
			foreach ($products as $product => $description) {
                ?>
					<tr>
                        
                        <?php   echo form_input(['type' => 'hidden', 'name' => 'data[' . $product . '][player]'], $player['id']); ?>
                        <?php   echo form_input(['type' => 'hidden', 'name' => 'data[' . $product . '][product]'], $product); ?>
						<td  class="second green"><?php   echo html_escape($product); ?></td>
                        <td><?php   echo form_checkbox('data[' . $product . '][toggle]', 'TRUE', set_checkbox('data[' . $product . '][toggle]', 'TRUE', isset($package['items'][$product])), ["style" => "width: 15px;", "class" => "cb", "id" => "cb" . $product]); ?>
                            <?php	echo form_label('', "cb" . $product, ["class" => "cb-label"]); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][start_date]', 'autocomplete' => 'off',  'placeholder' => 'Enter Start Date'], set_value('data[' . $product . '][start_date]', '', FALSE), ['class' => 'dp', 'style' => 'width: 120px']); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][start_time]', 'autocomplete' => 'off',  'placeholder' => 'Enter Start Time'], set_value('data[' . $product . '][start_time]', '00:00', FALSE), ['class' => 'tp', 'style' => 'width: 60px']); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][end_date]', 'autocomplete' => 'off',  'placeholder' => 'Enter End Date'], set_value('data[' . $product . '][end_date]', '', FALSE), ['class' => 'dp', 'style' => 'width: 120px']); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][end_time]', 'autocomplete' => 'off',  'placeholder' => 'Enter End Time'], set_value('data[' . $product . '][end_time]', '23:59', FALSE), ['class' => 'tp', 'style' => 'width: 60px']); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][cc]', 'placeholder' => 'Enter Amount'], set_value('data[' . $product . '][cc]', '', FALSE), ['style' => 'width: 120px']); ?></td>
                        <td><?php   echo form_input(['name' => 'data[' . $product . '][cd]', 'placeholder' => 'Enter Duration'], set_value('data[' . $product . '][cd]', '', FALSE), ['style' => 'width: 120px']); ?></td>
                        <td><?php   echo html_escape($description); ?></td>
                    </tr>
				<?php
			}
		}
		else {
			?>
				<tr>
					<td colspan="9" style="padding: 10px; text-align: center">
						No products have been setup
					</td>
				</tr>
			<?php
		}
	?>
	</tbody>
	<thead>
        <tr class="first green">
			<td colspan="9">Form Submission</td>
		</tr>
    </thead>
    <tbody>
        <tr>
            <td class="second green">Add Product(s)</td>
            <td colspan="6"><?php echo form_submit('data[submit]', 'Add Product(s)'); ?></td>
        </tr>
    </tbody>
</table>
<?php	echo form_close(""); ?>


<h2>Subscriptions</h2>
<p>Use the form below to assign a new subscription to the player.</p>






<script>
	$(document).ready(function() {
		$('.tp').timepicker({ 'timeFormat': 'H:i' });
		$('.dp').datepicker();
	});
</script>