<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
	<div class="option">
		<a href="<?php echo base_url("licence/playerview/{$player}"); ?>"><img src="<?php echo base_url("images/licence/package.png"); ?>"></a><br />
		[ RETURN ]
	</div>
</div>

<img class="section_logo" src="<?php echo base_url("images/licence/package.png"); ?>">

<h1>Delete Licence</h1>
<p>Use this section to delete a Licence assigned to the Player</p>
<b>Deleted Licence cannot be restored!</b>
<hr style="clear:both; margin-top: 35px" />

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>

<h2>Delete Licence</h2>
<p>Use the below button to delete the licence</p>

<?php
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);
?>

<?php	echo form_open( $this->router->fetch_class() . "/deletelicence/{$player}/{$licence}"); ?>
<table class="info_table form" style="max-width: 420px;">
	<col />
	<thead>
        <tr class="first red">
			<td colspan="1">Are you sure you want to delete the licence?</td>
		</tr>
	</thead>
	<tbody>
        <tr class="second red">
            <td><?php	echo form_submit('data[submit]', 'DELETE'); ?></td>
        </tr>
	</tbody>
</table>
<?php	echo form_close(""); ?>