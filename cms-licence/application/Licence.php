<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Page Controller
 */
class Licence extends MY_Controller {


	/**
	 * Constructor
	 *
	 * @return void Controller Constructor
	 */
	function __construct() {

		// Run Parent Constructor
		parent::__construct();

		// Load Additional Functions
		$this->load->model('mod_permission');
        $this->load->model('mod_setup');
        $this->load->model('mod_licence');

	}


	/**
	 * Page Index
	 *
	 * @return void
	 */
	public function index() {

		// Require Authenticated Login
		$this->auth->require_login();

		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}

		// Variables
		$data = [];

		// Load Page Contents
		$page	=	$this->load->view("licence/home", $data, true);

		// Display Page Contents
		$this->display_portal("Licence", $page);

	}

	
	/**
	 * Display Products Page
	 *
	 * @return void
	 */
	public function products() {
	
		// Require Authenticated Login
		$this->auth->require_login();
	
		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}
	
		// Variables
		$data = [];

		// Load Libraries
		$this->load->model("mod_licence");
		$data['products'] = $this->mod_licence->get_products();
	
		// Load Page Contents
		$page	=	$this->load->view("licence/products/home", $data, true);
	
		// Display Page Contents
		$this->display_portal("Licensed Products",$page);
	
	}


	/**
	 * Display Packages Page
	 *
	 * @return void
	 */
	public function packages() {
	
		// Require Authenticated Login
		$this->auth->require_login();
	
		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}
	
		// Variables
		$data = [];

		// Load Libraries
		$this->load->model("mod_licence");
		$data['packages'] = $this->mod_licence->get_packages();
	
		// Load Page Contents
		$page	=	$this->load->view("licence/packages/home", $data, true);
	
		// Display Page Contents
		$this->display_portal("Licensed Packages",$page);
	
	}

	
	/**
	 * Creates a New Package
	 *
	 * @return void
	 */
	public function create() {
	
		// Require Authenticated Login
		$this->auth->require_login();
	
		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}

		// Load Libraries
		$this->load->model("mod_licence");
		
		// Load Additional Functions
		$this->load->helper('form');
		$this->load->library('form_validation');
	
		// Variables
		$data = [];

		// Populate Data
		$data['package'] = $this->mod_licence->package_model();
		$data['products'] = $this->mod_licence->get_products();

		// Do Submission
		if ( ! empty($_POST) ) {

			// Get Constraints
			$lengths = get_table_lengths('product');

			// Set Validation Rules
			$this->form_validation->set_rules('data[description]', 'Package Description', "trim|required|min_length[3]|max_length[35]");
			foreach ($data['products'] as $key => $value) {
				if ( ! empty($_POST['data'][$key]['toggle']) ) {
					$this->form_validation->set_rules('data[' . $key . '][cc]', "Credit Cost for {$key}", "trim|numeric|min_length[1]|max_length[7]");
					$this->form_validation->set_rules('data[' . $key . '][cd]', "Credit Duration for {$key}", "trim|integer|min_length[1]|max_length[7]");
				}
			}

			// Run Validation (Note changes for HMVC Validation)
			if ($this->form_validation->run($this) == FALSE) {

				// Load Error Window
				if (count($this->form_validation->error_array()) > 0) {
					$data['error']		=	$this->load->view("form/submission_error", [], TRUE);
				}

				// Load Message Window
				if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
					$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
				}

			}
			else {

				// Update Package
				$data['package']['description'] = $_POST['data']['description'];

				// Update Package
				$data['package']['items']	=	[];

				// Loop Items
				foreach ($_POST['data'] as $key => $value) {
					if ( is_array($value) && isset($data['products'][$key]) && isset($value['toggle']) && $value['toggle'] == "TRUE" ) {
						$data['package']['items'][$key] = $this->mod_licence->package_item_model($value);
					}
				}

				// Save Model
				$this->mod_licence->save_package( $data['package'] );

				// // Add Message
				$this->mod_global->system_log_entry("Created Licence Package {$data['package']['description']}");
				$this->session->set_flashdata('submission_message', 'This package has been successfully created');

				// Return Request
				header("Location: " . base_url($this->router->fetch_class() . '/packages'));
				exit();

			}

		}	
	
		// Load Page Contents
		$page	=	$this->load->view("licence/packages/create", $data, true);
	
		// Display Page Contents
		$this->display_portal("Package",$page);
	
	}


	/**
	 * Allow Control of Product
	 *
	 * @return void
	 */
	public function package() {

		// Require Authenticated Login
		$this->auth->require_login();

		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}

		// Create Array
		$data			=	[];
		
		// Load Libraries
		$this->load->model("mod_licence");

		// Get URI Data
		$package		=	$this->uri->rsegment('3');							// Passed Serial Number

		// Populate Data
		$data['package'] = $this->mod_licence->get_package($package);
		$data['products'] = $this->mod_licence->get_products();
		
		// Check if Package Exists
		if (!is_array($data['package'])) {

			// Error!
			$page	=	$this->load->view("errors/general_error", ["error" => "The specified package could not be loaded"], true);

		}
		else {

			// Load Additional Libraries
			$this->load->helper('form');
			$this->load->library('form_validation');

			// Load Message Window
			if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
				$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
			}

			// Do Submission
			if ( ! empty($_POST) ) {			

				// Get Constraints
				$lengths = get_table_lengths('product');

				// Set Validation Rules
				$this->form_validation->set_rules('data[description]', 'Package Description', "trim|required|min_length[3]|max_length[35]");
				foreach ($data['products'] as $key => $value) {
					if ( ! empty($_POST['data'][$key]['toggle']) ) {
						$this->form_validation->set_rules('data[' . $key . '][cc]', "Credit Cost for {$key}", "trim|numeric|min_length[1]|max_length[7]");
						$this->form_validation->set_rules('data[' . $key . '][cd]', "Credit Duration for {$key}", "trim|integer|min_length[1]|max_length[7]");
					}
				}

				// Run Validation (Note changes for HMVC Validation)
				if ($this->form_validation->run($this) == FALSE) {

					// Load Error Window
					if (count($this->form_validation->error_array()) > 0) {
						$data['error']		=	$this->load->view("form/submission_error", [], TRUE);
					}

					// Load Message Window
					if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
						$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
					}

				}
				else {

					// Update Package
					$data['package']['description'] = $_POST['data']['description'];

					// Update Package
					$data['package']['items']	=	[];

					// Loop Items
					foreach ($_POST['data'] as $key => $value) {
						if ( is_array($value) && isset($data['products'][$key]) && isset($value['toggle']) && $value['toggle'] == "TRUE" ) {
							$data['package']['items'][$key] = $this->mod_licence->package_item_model($value);
						}
					}

					// Save Model
					$this->mod_licence->save_package( $data['package'] );

					// // Add Message
					$this->mod_global->system_log_entry("Updated Licence Package {$package}");
					$this->session->set_flashdata('submission_message', 'This package has been successfully updated');

					// Return Request
					header("Location: " . base_url(uri_string()));
					exit();

				}

			}

			// Load Page Contents
			$page	=	$this->load->view("licence/packages/edit", $data, true);

		}

		// Display Page Contents
		$this->display_portal("Package", $page);

	}

	public function delete_package() {
	
		// Require Authenticated Login
		$this->auth->require_login();
	
		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}
	
		// Variables
		$data = [];

		
		// Load Libraries
		$this->load->model("mod_licence");

		// Get URI Data
		$package			=	$this->uri->rsegment('3');							// Passed Serial Number
		$data['package']	=	$this->mod_licence->get_package($package);

		
		// Load Additional Functions
		$this->load->helper('form');
		$this->load->library('form_validation');

		
		// Do Submission
		if ( ! empty($_POST) ) {

			if ( $this->mod_licence->delete_package($package) === false ) {
				
				$data['error']	=	"Error deleting selected package.";

			}
			else {		

				// Add Message
				$this->mod_global->system_log_entry("Deleted Licence Package {$package}");
				$this->session->set_flashdata('submission_message', 'Package has been deleted.');
	
				// Return Request
				header("Location: " . base_url("licence/packages/"));
				exit();
			}

		}
	
		// Load Page Contents
		$page	=	$this->load->view("licence/packages/delete", $data, true);
	
		// Display Page Contents
		$this->display_portal("Package",$page);
	}

	
	 /**
	  * Allows Assignment of Licenses to a Player
	  *
	  * @return void
	  */
	 public function playerview() {

		// Require Authenticated Login
		$this->auth->require_login();

		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}

		// Import Libraries and Functions
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('mod_player');

		// Variable Declarations
		$data				=	[];
		$data['id']			=	(int)$this->uri->rsegment('3');
		$data['player']		=	$this->mod_player->get_player($data['id']);

		// Populate Data
		$data['package']  	= 	$this->mod_licence->package_model();
		$data['products'] 	= 	$this->mod_licence->get_products();
		$data['licences'] 	= 	$this->mod_licence->get_licences($data['id']);

		// Check Player
		if ( empty($data['player']) || empty($data['player']['product']) || $data['player']['product'] <> "WLS" ) {

			// Error
			$page	=	$this->load->view("errors/general_error", ["error" => "The specified player does not handle licencing"], true);

		}
		else {

			// Do Submission
			if ( ! empty($_POST) ) {		

				// Do Package Submission
				if ( $_POST['data']['submit'] == "Add Package") {

					// Set Validation Rules
					
					$this->form_validation->set_rules('data[start_date]', "Start Date", "trim|required|min_length[10]|max_length[10]");
					$this->form_validation->set_rules('data[start_time]', "Start Time", "trim|required|min_length[5]|max_length[5]");
					$this->form_validation->set_rules('data[end_date]', "End Date", "trim|required|min_length[10]|max_length[10]");
					$this->form_validation->set_rules('data[end_time]', "End Time", "trim|required|min_length[5]|max_length[5]");
					$this->form_validation->set_rules('data[description]', "Description", "trim|max_length[65]");

					// Prepare Time
					$sDBTime = "";
					$eDBTime = "";
					if ( ! empty($_POST['data']['start_date']) && ! empty($_POST['data']['start_time']) && ! empty($_POST['data']['end_date']) && ! empty($_POST['data']['end_time']) ) {
		
						// Convert Start Time
						$sDDate = strtotime( str_replace("/", "-", $_POST['data']['start_date']) . " UTC" );
						$sTTime = strtotime("1970-01-01 {$_POST['data']['start_time']} UTC");
						$sDBTime = $sDDate + $sTTime;

						// Convert End Time
						$eDDate = strtotime( str_replace("/", "-", $_POST['data']['end_date']) . " UTC" );
						$eTTime = strtotime("1970-01-01 {$_POST['data']['end_time']} UTC");
						$eDBTime = $eDDate + $eTTime;

		
						// Errors
						if ( $sDDate === FALSE )		$errors[] = "Invalid Value entered for Date DD/MM/YYYY";
						if ( $sTTime === FALSE )		$errors[] = "Invalid Value entered for Time HH:MM";
						if ( $eDDate === FALSE )		$errors[] = "Invalid Value entered for Date DD/MM/YYYY";
						if ( $eTTime === FALSE )		$errors[] = "Invalid Value entered for Time HH:MM";
		
						// Check Time
						if ( $sDDate && $sTTime && $eDDate && $eTTime && $eDBTime && $sDBTime <= time() )
							$errors[] = "Entered Time and Date are in the Past";

					}				
					
					// Run Validation (Note changes for HMVC Validation)
					if ($this->form_validation->run($this) == FALSE) {
	
						// Load Error Window
						if (count($this->form_validation->error_array()) > 0) {
							$data['error']		=	$this->load->view("form/submission_error", [], TRUE);
						}
	
						// Load Message Window
						if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
							$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
						}
	
					}
					else {

						// Compare Start and End Times
						if ( $eDBTime >= $sDBTime ) {
							$_POST['data']['start']	=	$sDBTime;
							$_POST['data']['end']	=	$eDBTime;						
							
							// Save Model
							$this->mod_licence->assign_package( $_POST['data'] );
		
							// Add Message
							$this->mod_global->system_log_entry("Updated Player");
							$this->session->set_flashdata('submission_message', 'This player has been successfully updated');
		
							// Return Request
							header("Location: " . base_url(uri_string()));
							exit();

							
						} 
						else {			

							// Add Message
							$data['error']		=	"End Date cannot be earlier than the Start Date of the Licence. Please resubmit with correct details.";	

						}


					}

				}

				// Do Product Submission
				else {				
										
					// Get Constraints
					$lengths = get_table_lengths('product');
	
					// Set Validation Rules
					foreach ($data['products'] as $key => $value) {
						if ( ! empty($_POST['data'][$key]['toggle']) ) {
							$this->form_validation->set_rules('data[' . $key . '][start_date]', "Start Date for {$key}", "trim|required|min_length[10]|max_length[10]");
							$this->form_validation->set_rules('data[' . $key . '][start_time]', "Start Time for {$key}", "trim|required|min_length[5]|max_length[5]");
							$this->form_validation->set_rules('data[' . $key . '][end_date]', "End Date for {$key}", "trim|required|min_length[10]|max_length[10]");
							$this->form_validation->set_rules('data[' . $key . '][end_time]', "End Time for {$key}", "trim|required|min_length[5]|max_length[5]");
							$this->form_validation->set_rules('data[' . $key . '][cc]', "Credit Cost for {$key}", "trim|numeric|min_length[1]|max_length[7]");
							$this->form_validation->set_rules('data[' . $key . '][cd]', "Credit Duration for {$key}", "trim|integer|min_length[1]|max_length[7]");
	
							
							// Convert to MySQL Time
							$sDBTime = "";
							$eDBTime = "";
							if ( ! empty($_POST['data'][$key]['start_date']) && ! empty($_POST['data'][$key]['start_time']) && ! empty($_POST['data'][$key]['end_date']) && ! empty($_POST['data'][$key]['end_time']) ) {
				
								// Convert Start Time
								$sDDate = strtotime( str_replace("/", "-", $_POST['data'][$key]['start_date']) . " UTC" );
								$sTTime = strtotime("1970-01-01 {$_POST['data'][$key]['start_time']} UTC");
								$sDBTime = $sDDate + $sTTime;
	
								// Convert End Time
								$eDDate = strtotime( str_replace("/", "-", $_POST['data'][$key]['end_date']) . " UTC" );
								$eTTime = strtotime("1970-01-01 {$_POST['data'][$key]['end_time']} UTC");
								$eDBTime = $eDDate + $eTTime;
	
				
								// Errors
								if ( $sDDate === FALSE )		$errors[] = "Invalid Value entered for Date DD/MM/YYYY";
								if ( $sTTime === FALSE )		$errors[] = "Invalid Value entered for Time HH:MM";
								if ( $eDDate === FALSE )		$errors[] = "Invalid Value entered for Date DD/MM/YYYY";
								if ( $eTTime === FALSE )		$errors[] = "Invalid Value entered for Time HH:MM";
				
								// Check Time
								if ( $sDDate && $sTTime && $eDDate && $eTTime && $eDBTime && $sDBTime <= time() )
									$errors[] = "Entered Time and Date are in the Past";

						
								// Compare Start and End Times
								$flag	=	FALSE;

								if ( $eDBTime >= $sDBTime ) {
									$_POST['data'][$key]['start']	=	$sDBTime;
									$_POST['data'][$key]['end']		=	$eDBTime;
									$flag	=	TRUE;
								}
	
							}
	
						}
						
					}		
	
					// Run Validation (Note changes for HMVC Validation)
					if ($this->form_validation->run($this) == FALSE) {
	
						// Load Error Window
						if (count($this->form_validation->error_array()) > 0) {
							$data['error']		=	$this->load->view("form/submission_error", [], TRUE);
						}
	
						// Load Message Window
						if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
							$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
						}
	
					}
					else {
						debug_print($eDBTime);
						debug_print($sDBTime);
						
						// Compare Start and End Times
						if ( $flag	==	TRUE ) {
	
							// Update Package
							$data['package']['items']	=	[];
	
							// Loop Items
							debug_print($_POST['data']);
							
							foreach ($_POST['data'] as $key => $value) {
								if ( is_array($value) && isset($data['products'][$key]) && isset($value['toggle']) && $value['toggle'] == "TRUE" ) {
									$data['package']['items'][$key] = $this->mod_licence->player_product_model($value);									
		
									if ( $data['package']['items'][$key]['start'] == NULL && $data['package']['items'][$key]['end'] == NULL ) {
		
										$errors[] = "Start Time cannot be later than End Time";
		
										// Return Request
										header("Location: " . base_url(uri_string()));
										exit();
									}
		
								}
		
							}
							
							// Save Model
							$this->mod_licence->assign_products( $data['package']['items'], $_POST['data']['description'] );
							// Add Message
							$this->mod_global->system_log_entry("Updated Player");
							$this->session->set_flashdata('submission_message', 'This player has been successfully updated');
		
							// Return Request
							header("Location: " . base_url(uri_string()));
							exit();

						}
						else {
							// Add Message
							$data['error']		=	"End Date cannot be earlier than the Start Date of the Licence. Please resubmit with correct details.";		

						}
	
					}

				}
				
			}

			// Load Message Window
			if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
				$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
			}

			// Assign Licence View
			if ( (string)$this->uri->rsegment('4') == "assign") {

				$data['packages']	=	$this->mod_licence->get_packages();
				$data['products']	=	$this->mod_licence->get_products();

				$page	= $this->load->view("licence/player/assign", $data, true);

			}
			// View Licence View
			else {

				$page	= $this->load->view("licence/player/licences", $data, true);

			}

		}

		// Display Page Contents
		$this->display_portal("Player Licence", $page, ["css/jquery.timepicker.css"], ["js/jquery.timepicker.min.js"]);

	 }



	 
	 public function deletelicence() {
	 
		// Require Authenticated Login
		$this->auth->require_login();

		// Stop if user not Authorised
		if ( empty( $_SESSION['USER']['ADMIN']) ) {
			$this->_no_permission();
			return;
		}

		// Import Libraries and Functions
		$this->load->helper('form');
		$this->load->library('form_validation');

		// Variable Declarations
		$data				=	[];
		$data['player']		=	(int)$this->uri->rsegment('3');
		$data['licence']	=	(int)$this->uri->rsegment('4');

		// Do Submission
		if ( ! empty($_POST) ) {

			$this->mod_licence->delete_licence($data['licence']);						
			
			// Return Request
			header("Location: " . base_url() . "licence/playerview/{$data['player']}" );
			exit();

		}	

		 // Load Page Contents
		 $page	=	$this->load->view("licence/player/delete", $data, true);
	 
		 // Display Page Contents
		 $this->display_portal("Player Licence Deletion",$page);
	 
	 }
}

