

$(document).ready(function() {


  $("#menu-button").on( "click" , function() {
      $("#menu").slideToggle(0);
      $(this).toggleClass(".on");
      $('#container').toggleClass("container-blur")
      $('#mobile-navigator').toggleClass("container-blur")
      $('#cert-holder').css('display', 'none');
      $('#info-holder').css('display', 'none');
  });

  $("#cert").on( "click" , function() {
    $("#cert-holder").css('display', 'unset');
  });

  $("#cert-close").on( "click" , function() {
    $("#cert-holder").css('display', 'none');
  });

  $("#info").on( "click" , function() {
    $("#info-holder").css('display', 'unset');
  });

  $("#info-close").on( "click" , function() {
    $("#info-holder").css('display', 'none');
  });

  $("#gallery-tevent, #gallery-tevent-m").click(function() {
    $(" #pop-event ").addClass(" display ").addClass(" active ");
    $(" #pop-bigd ").removeClass("active");
    $(" #pop-media ").removeClass("active");
    $(" #pop-av ").removeClass("active");
    $(" #pop-command ").removeClass("active");
  })
  $(" #gallery-tbigd, #gallery-tbigd-m ").click(function() {
    $(" #pop-bigd ").addClass(" display ").addClass(" active ");
    $(" #pop-event ").removeClass("active");
    $(" #pop-media ").removeClass("active");
    $(" #pop-av ").removeClass("active");
    $(" #pop-command ").removeClass("active");
  })
  $(" #gallery-tmedia, #gallery-tmedia-m  ").click(function() {
    $(" #pop-media ").addClass(" display ").addClass(" active ");
    $(" #pop-bigd ").removeClass("active");
    $(" #pop-event ").removeClass("active");
    $(" #pop-av ").removeClass("active");
    $(" #pop-command ").removeClass("active");
  })
  $(" #gallery-tinstallation, #gallery-tinstallation-m  ").click(function() {
    $(" #pop-av").addClass(" display ").addClass(" active ");
    $(" #pop-bigd ").removeClass("active");
    $(" #pop-media ").removeClass("active");
    $(" #pop-event ").removeClass("active");
    $(" #pop-command ").removeClass("active");
  })
  $(" #gallery-tcommand, #gallery-tcommand-m  ").click(function() {
    $(" #pop-command ").addClass(" display ").addClass(" active ");
    $(" #pop-bigd ").removeClass("active");
    $(" #pop-media ").removeClass("active");
    $(" #pop-av ").removeClass("active");
    $(" #pop-event ").removeClass("active");
  })

  $(" .gallery-pop ").click(function(){
    $('.active').removeClass('active');
    $(this).addClass('active');  
  });

  $(" #close-event ").click(function() {
    $(" #pop-event ").removeClass(" display ");
  })
  $(" #close-bigd ").click(function() {
    $(" #pop-bigd ").removeClass(" display ");
  })
  $(" #close-media ").click(function() {
    $(" #pop-media ").removeClass(" display ");
  })
  $(" #close-av ").click(function() {
    $(" #pop-av ").removeClass(" display ");
  })
  $(" #close-command ").click(function() {
    $(" #pop-command ").removeClass(" display ");
  })

  $(" .gallery-text ").hover(function (){
    $(this).toggleClass('on');
    $(this).prev().toggleClass('on')
  });


}); 


    function nextTestimonial(){
      if          ($(" #testimonial-mobile-1 ").css("display") != "none") {
          $(" #testimonial-mobile-1 ").css("display", "none");
          $(" #testimonial-mobile-2 ").css("display", "block");
          $(" #testimonial-mobile-3 ").css("display", "none");
          $(" #testimonial-mobile-4 ").css("display", "none");
          $(" #testimonial-mobile-5 ").css("display", "none");
      } else if   ($(" #testimonial-mobile-2 ").css("display") != "none") {
          $(" #testimonial-mobile-1 ").css("display", "none");
          $(" #testimonial-mobile-2 ").css("display", "none");
          $(" #testimonial-mobile-3 ").css("display", "block");
          $(" #testimonial-mobile-4 ").css("display", "none");
          $(" #testimonial-mobile-5 ").css("display", "none");
      } else if   ($(" #testimonial-mobile-3 ").css("display") != "none") {
          $(" #testimonial-mobile-1 ").css("display", "none");
          $(" #testimonial-mobile-2 ").css("display", "none");
          $(" #testimonial-mobile-3 ").css("display", "none");
          $(" #testimonial-mobile-4 ").css("display", "block");
          $(" #testimonial-mobile-5 ").css("display", "none");
      } else if   ($(" #testimonial-mobile-4 ").css("display") != "none") {
          $(" #testimonial-mobile-1 ").css("display", "none");
          $(" #testimonial-mobile-2 ").css("display", "none");
          $(" #testimonial-mobile-3 ").css("display", "none");
          $(" #testimonial-mobile-4 ").css("display", "none");
          $(" #testimonial-mobile-5 ").css("display", "block");
      } else {
          $(" #testimonial-mobile-1 ").css("display", "block");
          $(" #testimonial-mobile-2 ").css("display", "none");
          $(" #testimonial-mobile-3 ").css("display", "none");
          $(" #testimonial-mobile-4 ").css("display", "none");
          $(" #testimonial-mobile-5 ").css("display", "none");
      }
  };




$(window).on( "load", function () {
  $(" #loader ").css('display', 'none');
  $(" #a1 ").addClass("a1");
  $(" #a2 ").addClass("a2");
  $(" #a3 ").addClass("a3");
  $(" #a4 ").addClass("a4");
  $(" #a5 ").addClass("a5");
  $(" #a6-willow ").addClass("a6");
  $(" #a6-etp ").addClass("a6");
  $(" #a7 ").addClass("a7");
  setTimeout(function() {
    $('#a7').css('display', 'none');
}, 2900);
} )

$( window ).on( "load", function() {
  if( $(' #logo ').css('display') === 'block'){
    $(" .pic-position:nth-of-type(2) ").addClass("gallery-one"); 
    $(" .pic-position:nth-of-type(3) ").addClass("gallery-two"); 
    $(" .pic-position:nth-of-type(4) ").addClass("gallery-three"); 
    $(" .pic-position:nth-of-type(5) ").addClass("gallery-four"); 
    $(" .pic-position:nth-of-type(6) ").addClass("gallery-five"); 
    $(" .gallery-text-holder ").css('display', 'initial');
    $(" #gallery ").addClass("gallery-load");
  } else {
    $(" .pic-position:nth-of-type(2) ").addClass("gallery-one-mobile"); 
    $(" .pic-position:nth-of-type(3) ").addClass("gallery-two-mobile"); 
    $(" .pic-position:nth-of-type(4) ").addClass("gallery-three-mobile"); 
    $(" .pic-position:nth-of-type(5) ").addClass("gallery-four-mobile"); 
    $(" .pic-position:nth-of-type(6) ").addClass("gallery-five-mobile"); 
    $(" .gallery-text-holder-mobile ").css('display', 'initial')
  }
})

window.addEventListener('resize', () => {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// ------------------- Open maps with relevant app crossplatform---------


function mapsSelector() {
  if 
    ((navigator.platform.indexOf("iPhone") != -1) || 
     (navigator.platform.indexOf("iPad") != -1) || 
     (navigator.platform.indexOf("iPod") != -1))
    window.open("maps://maps.google.com/maps?daddr=52.2195406,-0.8675442&amp;ll=");
else 
    window.open("https://www.google.com/maps/place/Willow+Communications+Limited/@52.2195406,-0.8675442,17z/data=!3m1!4b1!4m5!3m4!1s0x487708ce2f42d70f:0x2b073b0cfabaf405!8m2!3d52.2195373!4d-0.8653555");
}

// Smooth Scrolling //

function handle(delta) {
  var animationInterval = 20; //lower is faster
var scrollSpeed = 20; //lower is faster

  if (end == null) {
  end = $(window).scrollTop();
}
end -= 20 * delta;
goUp = delta > 0;

if (interval == null) {
  interval = setInterval(function () {
    var scrollTop = $(window).scrollTop();
    var step = Math.round((end - scrollTop) / scrollSpeed);
    if (scrollTop <= 0 || 
        scrollTop >= $(window).prop("scrollHeight") - $(window).height() ||
        goUp && step > -1 || 
        !goUp && step < 1 ) {
      clearInterval(interval);
      interval = null;
      end = null;
    }
    $(window).scrollTop(scrollTop + step );
  }, animationInterval);
}
}

function newTab(url) {
    window.open(
      url, "_blank");
}






function displayWindowSize(){
  var windowsize = $(window).innerWidth();

  if (windowsize <= 1365 && m == 0) {
      m = 1;
  } else if (windowsize >= 1365 && m == 1) {
      m = 0;
      console.log(windowsize);
      window.location = window.location;
      location.reload();
  }

  if (windowsize >= 1366 && d == 0) {
      d = 1;
  } else if (windowsize <= 1366 && d == 1) {
      d = 0;
      console.log(windowsize);
      window.location = window.location;
      location.reload();
  }
}

window.addEventListener("resize", displayWindowSize)


var scrollArea  = document.getElementById("container");
var up          = true;
var value       = 0;
var increment   = 150;
var ceiling     = $(document).height()-1000;
var bottom      = 0;

function scrollDown() {
  if (value < ceiling) {
    value += increment
  }
  skrollr.get().animateTo(value);

  if (value === 0) {
    $('#scroll-up').css('opacity', '0.3');
    $('#scroll-down').css('opacity', '0.6');
  } else if (value > ceiling || value == ceiling){
    $('#scroll-up').css('opacity', '0.6');
    $('#scroll-down').css('opacity', '0.3');
  } else {
    $('#scroll-up').css('opacity', '0.6');
    $('#scroll-down').css('opacity', '0.6');
  }

}

function scrollUp() {
  if (value > bottom) {
    value -= increment
  }
  skrollr.get().animateTo(value);

  switch(true){
    case (value <= bottom):
      $('#scroll-up').css('opacity', '0.3');
      $('#scroll-down').css('opacity', '0.6');
      break;
  
    case (value > ceiling || value == ceiling):
      $('#scroll-up').css('opacity', '0.6');
      $('#scroll-down').css('opacity', '0.3');
      break;
  
    default:
      $('#scroll-up').css('opacity', '0.6');
      $('#scroll-down').css('opacity', '0.6');
  }
}