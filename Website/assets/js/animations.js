// ---------------------- Rotor animation ----------------------

const circleType = new CircleType(
	document.getElementById("rotated")
).radius(80);

$(document).scroll(function() {
	var offset = $(document).scrollTop();
	offset = offset * 0.4;

	$(".circular-text").css({
		"-moz-transform": "rotate(" + offset + "deg)",
		"-webkit-transform": "rotate(" + offset + "deg)",
		"-o-transform": "rotate(" + offset + "deg)",
		"-ms-transform": "rotate(" + offset + "deg)",
		transform: "rotate(" + offset + "deg)"
	});
});	



var pins = new ScrollMagic.Controller({
	globalSceneOptions:{
		triggerHook: 'onLeave'
	}
})

new ScrollMagic.Scene({
	triggerElement: "#segment-1"
})
	.setPin("#segment-1", {pushFollowers:false})
	.duration("221%")
	.addTo(pins);

new ScrollMagic.Scene({
	triggerElement: "#segment-6"
})
	.setPin("#segment-6", {pushFollowers:false})
	.duration("100%")
	.addTo(pins);

/*
var wipeController = new ScrollMagic.Controller();

var wipeAnimation = new TimelineMax()
	.fromTo(" #segment-5 ", 1, {x: "100%"}, {x: "0%", ease: Linear.easeNone});



new ScrollMagic.Scene ({
		triggerElement: " #segment-4 ",
		triggerHook: "onLeave",
		duration: "100%"
	})
	.setPin(" #segment-4 ")
	.setTween(wipeAnimation)
	.addIndicators()
	.addTo(wipeController)

*/



// ------------------ Scroll Magic Controllers for different segments-----------


var thumbController = new ScrollMagic.Controller({
	globalSceneOptions: {
		duration: "200%",
		offset: "-400"
	}});

// ------------------------Segment 1--------------------------------

// ------------------------Segment 2--------------------------------
new ScrollMagic.Scene({triggerElement: "#segment-2"})
				.setClassToggle("#thumb1", "thumby1") // add class toggle
				.addTo(thumbController);
new ScrollMagic.Scene({triggerElement: "#segment-2"})
				.setClassToggle("#thumb2", "thumby2") // add class toggle
				.addTo(thumbController);
new ScrollMagic.Scene({triggerElement: "#segment-2"})
				.setClassToggle("#thumb3", "thumby3") // add class toggle
				.addTo(thumbController);
new ScrollMagic.Scene({triggerElement: "#segment-2"})
				.setClassToggle("#thumb4", "thumby4") // add class toggle
				.addTo(thumbController);
new ScrollMagic.Scene({triggerElement: "#segment-2"})
				.setClassToggle("#thumb5", "thumby5") // add class toggle
				.addTo(thumbController);
new ScrollMagic.Scene({triggerElement: "#segment-2"})
				.setClassToggle("#thumb6", "thumby6") // add class toggle
				.addTo(thumbController);

// ------------------------Segment 11--------------------------------
var controller = new ScrollMagic.Controller({
	globalSceneOptions: {
		duration: "145%",
		offset: "-170"
	}});


new ScrollMagic.Scene({triggerElement: "#trigger11"})
				.setClassToggle("#pop1", "pop1") // add class toggle
				/*.addIndicators({
					name: 'fade scene',
					colorTrigger: 'black',
					indent: 200,
					colorStart: '#75C695'
				})*/
				.addTo(controller);

new ScrollMagic.Scene({triggerElement: "#trigger11"})
				.setClassToggle("#segment11", "segment11") // add class toggle
				.addTo(controller);

// ------------------------Segment 12--------------------------------

new ScrollMagic.Scene({triggerElement: "#trigger11"})
				.setClassToggle("#pop2", "pop2") // add class toggle
				/*.addIndicators({
					name: 'fade scene',
					colorTrigger: 'black',
					indent: 200,
					colorStart: '#75C695' 
				})*/
				.addTo(controller);
new ScrollMagic.Scene({triggerElement: "#trigger12"})
				.setClassToggle("#segment12", "segment12") // add class toggle
				.addTo(controller);


