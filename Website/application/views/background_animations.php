<!-- Background depth effect section -->

<!-- Bottom Layer -->
<div    id="bottom-layer"   class="willowbg"
        data-0="transform:translateX(20%);" 
        data-13200="transform[cubic]:translateX(640%);"
        data-14700="transform:translateX(640%);" ></div>
<div    id="bottom-layer-mobile"        class="willowbg"></div>


<!-- Headers -->
<div    id="gallery-header"
        data-start="transform:translateY(0%)" 
        data-end="transform:translateY(-40%)"></div>

<div    id="bingo-header"
        data-start="transform:translateY(-20%)" 
        data-end="transform:translateY(0%)"></div>
<div    id="av-header"
        data-start="transform:translateY(50%)" 
        data-end="transform:translateY(-150%)"></div>
<div    id="installation-header"
        data-start="transform:translateY(50%)" 
        data-end="transform:translateY(-150%)"></div>


<!-- Headers mobile  -->

<div    id="gallery-header-mobile"
        data-start="transform:translateX(0%)" 
        data-end="transform:translateX(-40%)"></div>
<div    id="bingo-header-mobile"
        data-start="transform:translateX(-20%)" 
        data-end="transform:translateX(0%)"></div>
<div    id="av-header-mobile"
        data-start="transform:translateX(50%)" 
        data-end="transform:translateX(-150%)"></div>
<div    id="installation-header-mobile"
        data-start="transform:translateX(50%)" 
        data-end="transform:translateX(-100%)"></div>




</div><!-- Ending tag for main container -->

</div><!-- Ending tag for Skrollr wrapper -->