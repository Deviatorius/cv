<body ontouchstart="">

<div id="mobile-indicator"></div>
<div id="mobile-navigator">
    <div class="navigator" id="scroll-up" style="cursor: pointer;" onclick="scrollUp()"></div>
    <div class="navigator" id="scroll-down" style="cursor: pointer;" onclick="scrollDown()"></div>
</div>

<div id="warning">
        <div class="cover"><p>Please switch to fullscreen for Willow experience.</p></div>
</div>
<!-- WELCOME SCREEN -->
<div class="welcome-holder" id="a7">
    <div class="welcome mobile">Loading Touch Version</div>
    <div class="welcome desktop">Loading Desktop Version</div>
    <div class="welcome-img-default welcome-dot"></div>
    <div class="welcome-img-default welcome-hi"></div>
    <div class="welcome-img-default welcome-welcome"></div>
    <div class="welcome-img-default welcome-to"></div>
    <div class="welcome-img-default welcome-willow" id="a6-willow"></div>
    <div class="welcome-img-default welcome-etp" id="a6-etp"></div>
    <div class="loader" id="loader"></div>

    <div id="a1" class="welcome-animation">
        <div class="welcome-animation-hider"></div>
    </div>
    <div id="a2" class="welcome-animation">
        <div class="welcome-animation-hider"></div>
    </div>
    <div id="a3" class="welcome-animation">
        <div class="welcome-animation-hider"></div>
    </div>
    <div id="a4" class="welcome-animation">
        <div class="welcome-animation-hider"></div>
    </div>
    <div id="a5" class="welcome-animation">
        <div class="welcome-animation-hider"></div>
    </div>
</div>













<!-- MENU BUTTON-->
<div    id="menu-button">
    <button id="menu-button" class="menu" onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))" aria-label="Main Menu">
      <svg id="svg" viewBox="0 0 100 100">
        <path data-290="stroke: rgba(0, 0, 0,1);" data-550="stroke: rgba(178,125,193,1);" class="line line1" d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
        <path data-350="stroke: rgba(0, 0, 0,1);" data-550="stroke: rgba(178,125,193,1);" class="line line2" d="M 20,50 H 80" />
        <path data-510="stroke: rgba(0, 0, 0,1);" data-550="stroke: rgba(178,125,193,1);" class="line line3" d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
      </svg>
    </button>
</div>

<!-- MENU SLIDING ARROW WITH NOTICE -->
<div    id="menu-notice"
        data-start="left: 95%; opacity:0"
        data-550="left: -1%; opacity: 1"
        data-1800="opacity: 1;"
        data-1900="opacity: 0;"></div>

<!-- MENU CONTENT CONTAINER -->
<div id="menu">
    <div class="container">
        <div class="section">
            <div class="content-menu">
                <div class="icons">

                    <!-- EMAIL -->
                    <div class="icon icon-email con-tooltip top" onclick="location.href='mailto:hello@willowcommunications.com'" style="cursor: pointer;">
                        <div class="tooltip"><p>Email</p></div>
                    </div>
                    <p class="mobile">Email</p>

                    <!-- LOCATION -->
                    <div class="icon icon-location con-tooltip top" onclick="mapsSelector()" href = "https://www.google.com/maps/place/Willow+Communications+Limited/@52.2195406,-0.8675442,17z/data=!3m1!4b1!4m5!3m4!1s0x487708ce2f42d70f:0x2b073b0cfabaf405!8m2!3d52.2195373!4d-0.8653555"  target="blank" style="cursor: pointer;">
                        <div class="tooltip"><p>Location</p></div>
                    </div>
                    <p class="mobile">Location</p>
                </div>
                <div class="icons">

                    <!-- TELEPHONE -->
                    <div class="icon icon-telephone con-tooltip top" onclick="location.href='tel:+441604877001'" style="cursor: pointer;">
                        <div class="tooltip ">
                                <p>+441604 877001</p>
                        </div>
                    </div>
                    <p class="mobile">+441604 877001</p>
                    
                    <!-- CERTIFICATES -->
                    <div id="cert" class="icon icon-other con-tooltip top" target="blank" style="cursor: pointer;">
                        <div class="tooltip"><p>Accreditations</p></div>
                    </div>
                    <p class="mobile">Accreditations</p>
                </div>
                <div class="icons">

                    <!-- GAMBLING REGULATION -->
                    <div class="icon icon-regulation con-tooltip top" onclick="newTab('https://registers.gamblingcommission.gov.uk/53962');" target="blank" style="cursor: pointer;">
                        <div class="tooltip"><p>Gambling Commission</p></div>
                    </div>
                    <p class="mobile">Gambling</p>

                    <!-- INFO -->
                    <div id="info" class="icon icon-info con-tooltip top" target="blank" style="cursor: pointer;">
                        <div class="tooltip"><p>Information</p></div>
                    </div>
                    <p class="mobile">Information</p>
                </div>
            </div>
        </div>

        <!-- CERTIFICATE SUB-MENU -->
        <div class="submenu" id="cert-holder"><div class="file"></div><div class="title">Accreditations</div><div id="cert-close" class="close"></div>
            <div class="submenu-wrap">
                <div id="iso-9001"></div>
                <div id="iso-27001"></div>
            </div>
        </div>

        <!-- COMPANY INFO SUB-MENU -->
        <div class="submenu" id="info-holder"><div class="file"></div><div class="title">Information</div><div id="info-close" class="close"></div>
            <div class="submenu-wrap">
                Email: <br>hello@willowcommunications.com <br><br>
                Telephone: <br>+441604 877001<br><br>
                Registered Address: <br>Willow Communication Limited, Kilvey Road, Brackmills Industrial Estate, Northampton, NN4 7BQ, United Kingdom<br><br>
                Company Registration Number: <br>02445797<br><br>
                Gambling Commission Number: <br>53962<br><br>

            </div>
        </div>
    </div>
</div>

<!-- LOGO BAR -->
<div    id="logo" 
        data-start="transform:translateY(0%) rotate(-90deg); " 
        data-end="transform:translateY(6800%) rotate(-90deg);"></div>
<div    id="logo-mobile" 
        data-start="transform:translateX(0%);" 
        data-end="transform:translateX(-40%);"></div>





<!-- SKROLLR WRAPPER FOR MOBILE SUPPORT -->
<div    id="skrollr-body">

<!-- MAIN CONTENT CONTAINER -->
<div class="container container-deblur" id="container"
        data-0="transform:translateX(0%);" 
        data-13200="transform[cubic]:translateX(-600%);"
        data-14700="transform:translateX(-600%);">

    <!-- EVENT -->
    <section>
        <div class="content event">
            <div id="event-gif-wrap">
                <div id="event-gif" alt="Willow Communications Event Software for managing games, content, music, raffles and gambling accross any and all screens accross deployed estates."></div>
            </div>
            <p class="width600">Event is our all-in-one touchscreen entertainment and bingo platform.</p>
            <p class="width600">Event is designed for the holiday park, gaming, leisure and hospitality sectors.</p>
            <p class="width600">Event provides a suite of modular entertainment options.</p>
        </div>
    </section>

    <!-- GALLERY 1/2  -->
    <section>
                    <!-- MINI LOGO (SHAKING) -->
            <div    class="mini-logo"
                    data-0="top[WTF]:45%;"
                    data-3500="top:46%;"></div>
            <div    class="mini-logo"
                    data-0="right[WTF]:-145;"
                    data-3500="right:-155;"></div>

                    <!-- MINI LOGO (SHAKING) MOBILE-->
            <div    class="mini-logo-mobile"
                    data-0="top[WTF]:220vh;"
                    data-3500="top:220.5vh;"></div>
            <div    class="mini-logo-mobile"
                    data-0="right[WTF]:49.7%;"
                    data-3500="right:50.3%;"></div>

                    <!-- GALLERY CONTENT -->
            <div    id="gallery">
                    <div class="gallery-text-holder">
                        <div class="gallery-text" id="gallery-event" ></div>
                        <div class="gallery-text" id="gallery-tevent" style="cursor: pointer;"></div>
                        <div class="gallery-text" id="gallery-bigd"></div>
                        <div class="gallery-text" id="gallery-tbigd" style="cursor: pointer;"></div>
                        <div class="gallery-text" id="gallery-media"></div>
                        <div class="gallery-text" id="gallery-tmedia" style="cursor: pointer;"></div>
                        <div class="gallery-text" id="gallery-installation"></div>
                        <div class="gallery-text" id="gallery-tinstallation" style="cursor: pointer;"></div>
                        <div class="gallery-text" id="gallery-command"></div>
                        <div class="gallery-text" id="gallery-tcommand" style="cursor: pointer;"></div>
                    </div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
            </div>

                    <!-- GALLERY CONTENT MOBILE -->
            <div    id="gallery-mobile">
                    <div class="gallery-text-holder-mobile">
                        <div class="gallery-text" id="gallery-event"></div>
                        <div class="gallery-text" id="gallery-tevent"></div>
                        <div class="gallery-text" id="gallery-bigd"></div>
                        <div class="gallery-text" id="gallery-tbigd"></div>
                        <div class="gallery-text" id="gallery-media"></div>
                        <div class="gallery-text" id="gallery-tmedia"></div>
                        <div class="gallery-text" id="gallery-installation"></div>
                        <div class="gallery-text" id="gallery-tinstallation"></div>
                        <div class="gallery-text" id="gallery-command"></div>
                        <div class="gallery-text" id="gallery-tcommand"></div>
                    </div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
                    <div class="pic-position" ></div>
            </div>
            <div id="pop-event" class="gallery-pop"><div id="close-event" class="close"></div><div class="file"></div><div class="title">EVENT</div>
                <ul>
                    <li>Event packages four long established Willow products into one unified platform – Bingo, Digital Signage, Game Content and Media Deployment to 
                        provide a single all-in-one touchscreen entertainment and bingo platform.
                    <li>Event is designed for the holiday park, gaming, leisure and hospitality sectors.
                    <li>Event is fully Cloud based and fully modular facilitating a “pay for what you use” model. Event provides a suite of modular entertainment options: </li>
                        <ul>
                            <li>Bingo – Using our BIGD engine, Event brings the commercial bingo experience to your venue with both paper and print on demand games.
                            <li>Quiz – Quiz games are played on paper or “bring your own” device.
                            <li>Music – Chose from thousands of music tracks. Edit, share and schedule your own playlists
                            <li>Karaoke – Chose from thousands of Sunfly karaoke tracks.
                            <li>Music Videos – Chose from thousands of music videos.
                            <li>Skill Based Games – Magic Keys, The Vault, Pirates Plunder, Spin the Wheel, Lucky Draw are just some of the games available. 
                            <li>Digital Signage – Using our powerful Willow Media Cloud based Content Management System engine, schedule, and publish your own 
                                adverts and marketing messages from any Web enabled device. 
                            <li>Audio-Visual Control – Our Command application integrates within Event and provides cued and automated sound, lightning, video, and 
                                scene control. 
                        </ul>
                    <li>Some entertainment modules can be client branded/themed.
                    <li>Event has two unique content channels available on the full platform.
                    <li>Multiple, scalable hardware options are available as one size does not fit all.
                    <li>We can convert and use legacy equipment reducing capital outlay (minimum specifications apply).
                    <li>Event has scalable outputs and supports multiple resolutions.
                    <li>Event has fully licensed media content. 
                    <li>The Event broadcast feature allows clients to live stream content via our distribution engine.

                </ul>
            </div>
            <div id="pop-bigd" class="gallery-pop"><div id="close-bigd" class="close"></div><div class="file" style="filter: hue-rotate(350deg);"></div><div class="title">BIGD</div>
                <ul>
                    <li>BIGD is our Bingo Information Graphical Display platform. The application interprets the data received from legacy bingo equipment and displays 
                        it in a rich graphical format. 
                    <li>BIGD is an integrated, customisable zone-based Audio and Video jingle/music player.
                    <li>BIGD also functions as a Cloud based digital signage platform.
                    <li>Whether played locally or linked, our Bingo Game Manager brings the theatre back to Bingo by marrying the tradition of playing on paper with 
                        the creative possibilities of rich media games displayed on the latest screen and LED display technologies.
                    <li>Our Bingo Jackpot Manager enables an operator to combine various jackpot mechanics in order to play locally or linked in house or progressive 
                        jackpot games, including MCB.
                    <li>You can choose from two different BIGD platforms:- 
                    <ul>
                        <li>The standard BIGD platform can be added to any existing Bingo video distribution network. 
                        <li>The enhanced BIGD platform has been specifically designed to drive more complex games and also large format video walls and LED number boards.
                    </ul>
                </ul>
            </div>
            <div id="pop-media" class="gallery-pop"><div id="close-media" class="close"></div><div class="file" style="filter: hue-rotate(340deg);"></div><div class="title">WILLOW MEDIA</div>
                <ul>
                    <li>Our easy to use digital out of home Content Management System (CMS) allows our clients to manage their content, deployment and scheduling from 
                        any Web enabled device.
                    <li>We provide templates to integrate with third-party data, plugins and API’s.
                    <li>In gaming applications we provide smart templates and integration with third party technology to provide jackpot plugins and automated win 
                        celebrations.
                </ul>
            </div>
            <div id="pop-av" class="gallery-pop"><div id="close-av" class="close"></div><div class="file" style="filter: hue-rotate(330deg);"></div><div class="title">AUDIO-VISUAL</div>
                <ul>
                    <li>We design all our audio-visual solutions to integrate seamlessly, whether new installations or using legacy equipment.
                    <li>Our skilled audio-visual engineers are trained to minimise business disruption, often working out of hours or overnight to meet our clients’ 
                        requirements.
                    <li>As well as comprehensive on-site system training, we also provide seminar and webinar training for our client’s operators, hosts and engineering 
                        and project teams from our offices in Northampton.
                    <li>Currently servicing more than 1500 locations, we understand that downtime costs money and always react swiftly to service and maintenance 
                        interruptions.
                </ul>
            </div>
            <div id="pop-command" class="gallery-pop"><div id="close-command" class="close"></div><div class="file" style="filter: hue-rotate(320deg);"></div><div class="title">COMMAND</div>
                <ul>
                    <li>Command is our proprietary audio-visual and lighting control, and scheduling software platform.
                    <li>Command venue management software controls and schedules sound, lightning, video and media content. 
                    <li>For audio-visual systems managed by Command we can provide remote network diagnostics and programming changes.

                </ul>
            </div>
    </section>

    <!-- GALLERY 2/2 -->
    <section id="gallery-2">
    </section>
    <section id="pictures-mobile" >
    </section>

    <!-- BINGO -->
    <section>
        <div id="bingo">
            <div id="bingo-text">
            BIGD is our Bingo Information Graphical Display platform. The application interprets the data received from legacy bingo equipment and display it in a rich 
            graphical format. When used with a tablet or touchscreen BIGD adds user configurable skill games such as Open the Box and Wheel of Fortune. BIGD also functions 
            as a digital signage platform, audio and video jingle player and also as a zoned music player.
            </div>
            <div id="bingo-box">

                <!-- COLUMN  1 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  2 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-5"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  3 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-2"></div></div>
                </div>

                <!-- COLUMN  4 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-7"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  5 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-6"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  6 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-1"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  7 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  8 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-4"></div></div>
                </div>

                <!-- COLUMN  9 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-3"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  10 -->
                <div class="bingo-column"><p class="ticket-writing">BIGD</p></div>            
            </div>


            <!-- BINGO MOBILE -->

            <div class="bingo-box-mobile">

                <!-- TICKET 1 -->

                <!-- COLUMN  1 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  2 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-3"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  3 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-1"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  4 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-2"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  5 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-6"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  6 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-4"></div></div>
                </div>

                <!-- COLUMN  7 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  8 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-5"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  9 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-7"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  10 -->
                <div class="bingo-column"><p class="ticket-writing"></p></div>
            </div>
            <div class="bingo-box-mobile">

                <!-- Ticket 2 -->
                
                <!-- COLUMN  1 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-3"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  2 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-5"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  3 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  4 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-2"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  5 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-1"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  6 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-4"></div></div>
                </div>

                <!-- COLUMN  7 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-6"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  8 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  9 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-7"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  10 -->
                <div class="bingo-column"><p class="ticket-writing"></p></div>
            </div>
            <div class="bingo-box-mobile">

                <!-- Ticket 3 -->

                <!-- COLUMN  1 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-5"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  2 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-6"></div></div>
                </div>

                <!-- COLUMN  3 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-4"></div></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  4 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>
                
                <!-- COLUMN  5 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-2"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  6 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"><div class="stamp bingo-7"></div></div>
                </div>

                <!-- COLUMN  7 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-3"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  8 -->
                <div class="bingo-column">
                    <div class="bingo-number"><div class="stamp bingo-1"></div></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  9 -->
                <div class="bingo-column">
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                    <div class="bingo-number"></div>
                </div>

                <!-- COLUMN  10 -->
                <div class="bingo-column"><p class="ticket-writing"></p></div>
            </div>
        </div>
    </section>

    <!-- AV  -->
    <section>
        <div    class="content" id="av-content">
            <div    id="av-content-pictures"
                    data-5050="top: -30%;"
                    data-8500="top: -980%;">
            </div>
        </div>
        <div    class="content" id="av-content-mobile">
            <div id="av-content-pictures-mobile-wrap">
                <div    id="av-content-pictures-mobile"
                        data-1400="left: 1.35vw"
                        data-4700="left: -885vw;">
                </div>
            </div>
        </div>
    </section>

    <!-- INSTALLATION  -->
    <section>
    <div    class="content install" style="height: 980px;">

        <div class="install-holder">
            <div class="install-cover">
            </div>
        </div>
        <p style="margin-top: -235px;">We provide audio-visual system design and integration,</p>
        <p >backed by an in-house national installation and maintenance capability to the hospitality,</p>
        <p >leisure, holiday park and land-based gaming sectors.</p>

    </div>

    </section>

    <!-- CLIENTS 1/2 -->
    <section>
        <div class="content">
        </div>
    </section>

    <!-- CLIENTS 2/2 -->
    <section>
        <div class="content">

                <div id="clients-container">

                    <!-- DESKTOP -->
                    <div    id="clients-top"></div>
                        <div class="clients-centered"><div    id="clients-content"
                            data-10250="opacity: 0; height: 0vh;"
                            data-10700="opacity: 1; height: 55vh;">
                            <div class="clients-grid">
                                    <div id="clients-1" class="children">
                                    <div class="child aspers"></div>
                                        <div class="child carlton-bingo"></div>
                                        <div class="child grosvenor"></div>
                                        <div class="child marstons"></div>
                                        <div class="child parkdean-resorts"></div>
                                        <div class="child rileys"></div>
                                    </div>
                                <div id="clients-2" class="children">
                                    <div class="child away-resorts"></div>
                                    <div class="child club-2000"></div>
                                    <div class="child hardrock"></div>
                                    <div class="child mecca"></div>
                                    <div class="child park-holidays"></div>
                                    <div class="child shipley-estates"></div>
                                </div>
                                <div id="clients-3" class="children">
                                    <div class="child bjs-bingo"></div>
                                    <div class="child club-3000"></div>
                                    <div class="child hippodrome"></div>
                                    <div class="child merkur"></div>
                                    <div class="child pier-nine"></div>
                                    <div class="child shipleys"></div>
                                </div>
                                <div id="clients-4" class="children">
                                    <div class="child buzz-bingo"></div>
                                    <div class="child dr-martens"></div>
                                    <div class="child majestic-bingo"></div>
                                    <div class="child mr-mulligans"></div>
                                    <div class="child playland"></div>
                                    <div class="child victoria-gate"></div>
                                </div>
                            </div>
                        </div></div>
                    <div    id="clients-bot"
                            data-10200="margin-top: 0vh;"
                            data-10700="margin-top: 60vh;">
                    </div>

                    <!-- MOBILE -->
                    <div    id="clients-content-mobile"
                            data-500-center="margin-top: -30vh;"
                            data--500-center="margin-top: 30vh">
                            <div class="clients-grid">
                                    <div id="clients-1" class="children">
                                        <div class="child aspers"></div>
                                        <div class="child carlton-bingo"></div>
                                        <div class="child grosvenor"></div>
                                        <div class="child marstons"></div>
                                        <div class="child parkdean-resorts"></div>
                                        <div class="child rileys"></div>
                                    </div>
                                <div id="clients-2" class="children">
                                    <div class="child away-resorts"></div>
                                    <div class="child club-2000"></div>
                                    <div class="child hardrock"></div>
                                    <div class="child mecca"></div>
                                    <div class="child park-holidays"></div>
                                    <div class="child shipley-estates"></div>
                                </div>
                                <div id="clients-3" class="children">
                                    <div class="child bjs-bingo"></div>
                                    <div class="child club-3000"></div>
                                    <div class="child hippodrome"></div>
                                    <div class="child merkur"></div>
                                    <div class="child pier-nine"></div>
                                    <div class="child shipleys"></div>
                                </div>
                                <div id="clients-4" class="children">
                                    <div class="child buzz-bingo"></div>
                                    <div class="child dr-martens"></div>
                                    <div class="child majestic-bingo"></div>
                                    <div class="child mr-mulligans"></div>
                                    <div class="child playland"></div>
                                    <div class="child victoria-gate"></div>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
    </section>

    <!-- TESTIMONIALS -->
    <section    id="testimonials-desktop">
        <div    id="etp"
                data-13100="opacity: 0;"
                data-14100="opacity: 1;"
                ></div>
        <div    id="testimonials" 
                class="content" 
                data-10800="bottom: -1200px" 
                data-14700="bottom: 60px">
            <h2>Testimonials</h2>
            <div class="testimonial-grid">
                <table width="100%" >
                    <colgroup>
                        <col width="10%"><col width="10%">
                        <col width="10%"><col width="10%">
                        <col width="10%"><col width="10%">
                        <col width="10%"><col width="10%">
                        <col width="10%"><col width="10%">
                    </colgroup>
                    <tr>
                        <td colspan=3 class="adventure-leisure"></td>
                        <td colspan=7 style="padding-right: 40px;">  <q>Working with Willow is an absolute pleasure. Not only are they on the cutting edge of audio-visual technology they also have 
                                        a team of dedicated employees who provide a first class level of service. Working with Willow is a pleasure and I would highly 
                                        recommend to any leisure business looking improve their audio-visual offering. </q>
                    </tr>
                    <tr>
                        <td colspan=3></td>
                        <td colspan=7 style="padding-right: 40px;"> - <em> Andrew Scholey, Operations and Development Director</em> </td>
                    </tr>
                    <tr>
                        <td colspan=7 style="padding-left: 40px; text-align: right;">  <q>Willow have been the partner of choice for Marstons for all of our entertainment technology needs for the past sixteen years. 
                                        No project is too big or too small for them, and their innovative approach and attention to detail remains at a consistently 
                                        high standard, regardless of the size of the project. Whether it be the replacement of a single TV, or a complete redesign and 
                                        refurbishment of our audio and visual systems in our high profile bars or restaurants, we can always rely on the Willow team 
                                        to provide us with excellent ideas, advice and implementation throughout, whilst fully understanding and keeping the needs of 
                                        our business their main priority.</q></td>
                        <td colspan=3 class="marstons"></td>
                    </tr>
                    <tr>
                        <td colspan=7 style="padding-left: 40px;">- <em>Clare Chinn, Head of Estates Support</em></td>
                        <td colspan=3></td>
                    </tr>
                    <tr>
                        <td colspan=3 class="majestic-bingo"></td>
                        <td colspan=7 style="padding-right: 40px;">  <q>Willow are one of those rare companies where our passion for the sector is matched only by their own. Whether its providing 
                            our business with must have equipment and systems to deliver our core product to our customers, or working on some genuinely sector changing 
                            innovations, I cannot see a time where we are not working alongside Willow. </q></td>
                    </tr>
                    <tr>
                        <td colspan=3></td>
                        <td colspan=7 style="padding-right: 40px;">- <em>Mark Jepp, Managing Director</em></td>
                    </tr>
                    <tr>
                        <td colspan=7 style="padding-left: 40px;  text-align: right;">  <q>My relationship with Willow dates back to 2010 when I started developing the Hippodrome Casino. Over the years I have, and 
                                        continue to set, creative and technical challenges that they always deliver on. </q></td>
                        <td colspan=3 class="hippodrome"></td>
                    </tr>
                    <tr>
                        <td colspan=7 style="padding-left: 40px;">- <em> Simon Thomas, Chief Executive</em></td>
                        <td colspan=3></td>
                    </tr>
                    <tr>
                        <td colspan=3 class="park-holidays"></td>
                        <td colspan=7 style="padding-right: 40px;">  <q>It is a pleasure working with the team at Willow, who adapted their Event entertainment and bingo platform to suit the specific 
                                        requirements of Park Holidays UK. The transition from our previous system to their solution was seamless and effortless. Event 
                                        delivers an enhanced experience for our holiday guests and holiday homeowners alike, and a quick and easy user interface for our 
                                        Park Star Entertainers to navigate. The system does nothing but enhance all parts of entertainment within Park Holidays UK. I love 
                                        their enthusiasm and agile approach to problem solving and would recommend them to anyone looking to deliver a uniquely engaging 
                                        customer entertainment experience.</q></td>
                    </tr>
                    <tr>
                        <td colspan=3></td>
                        <td colspan=7 style="padding-right: 40px;">- <em>Jay Muzzell, Retail Support Manager</em></td>
                    </tr>

                </table>
            </div>
        </div>
    </section>

    <!-- TESTIMONIALS MOBILE -->
    <section>
        <div class="content">

            <div id="testimonial-mobile-1" class="testimonial-mobile-parent">
                <div class="client-logo-mobile park-holidays"></div><div class="break"><span class="br br-on"></span><span class="br"></span><span class="br"></span><span class="br"></span><span class="br"></span></div>
                <div class="testimonial-mobile">
                <q>It is a pleasure working with the team at Willow, who adapted their Event entertainment and bingo platform to suit the specific 
                    requirements of Park Holidays UK. The transition from our previous system to their solution was seamless and effortless. Event 
                    delivers an enhanced experience for our holiday guests and holiday homeowners alike, and a quick and easy user interface for our 
                    Park Star Entertainers to navigate. The system does nothing but enhance all parts of entertainment within Park Holidays UK. I love 
                    their enthusiasm and agile approach to problem solving and would recommend them to anyone looking to deliver a uniquely engaging 
                    customer entertainment experience.</q>
        
                    <br><br> - <em>Jay Muzzell, Retail Support Manager</em>
                </div>
            </div>

            <div id="testimonial-mobile-2" class="testimonial-mobile-parent">
                <div class="client-logo-mobile hippodrome"></div><div class="break"><span class="br"></span><span class="br br-on"></span><span class="br"></span><span class="br"></span><span class="br"></span></div>
                <div class="testimonial-mobile">
                    <q>My relationship with Willow dates back to 2010 when I started developing the Hippodrome Casino. Over the years I have, and 
                    continue to set, creative and technical challenges that they always deliver on. </q>
                    <br><br> - <em> Simon Thomas, Chief Executive</em></div>
            </div>

            <div id="testimonial-mobile-3" class="testimonial-mobile-parent">
                <div class="client-logo-mobile majestic-bingo"></div><div class="break"><span class="br"></span><span class="br"></span><span class="br br-on"></span><span class="br"></span><span class="br"></span></div>
                <div class="testimonial-mobile">
                    <q>Willow are one of those rare companies where our passion for the sector is matched only by their own. Whether its providing 
                    our business with must have equipment and systems to deliver our core product to our customers, or working on some genuinely sector changing 
                    innovations, I cannot see a time where we are not working alongside Willow. </q>
                    <br><br> - <em>Mark Jepp, Managing Director</em></div>
            </div>

            <div id="testimonial-mobile-4" class="testimonial-mobile-parent">
                <div class="client-logo-mobile adventure-leisure"></div><div class="break"><span class="br"></span><span class="br"></span><span class="br"></span><span class="br br-on"></span><span class="br"></span></div>
                <div class="testimonial-mobile">
                    <q>Working with Willow is an absolute pleasure. Not only are they on the cutting edge of BIGD technology they also have 
                    a team of dedicated employees who provide a first class level of service. Working with Willow is a pleasure and I would highly 
                    recommend to any leisure business looking improve their BIGD offering. </q>
                    <br><br> - <em> Andrew Scholey, Operations and Development Director</em></div>
            </div>

            <div id="testimonial-mobile-5" class="testimonial-mobile-parent">
                <div class="client-logo-mobile marstons"></div><div class="break"><span class="br"></span><span class="br"></span><span class="br"></span><span class="br"></span><span class="br br-on"></span></div>
                <div class="testimonial-mobile">
                    <q>Willow have been the partner of choice for Marstons for all of our entertainment technology needs for the past sixteen years. 
                    No project is too big or too small for them, and their innovative approach and attention to detail remains at a consistently 
                    high standard, regardless of the size of the project. Whether it be the replacement of a single TV, or a complete redesign and 
                    refurbishment of our audio and visual systems in our high profile bars or restaurants, we can always rely on the Willow team 
                    to provide us with excellent ideas, advice and implementation throughout, whilst fully understanding and keeping the needs of 
                    our business their main priority.</q>
                    <br><br>- <em>Clare Chinn, Head of Estates Support</em></div>
            </div>
            <button id="testimonial-button" onclick="nextTestimonial()">Next</button>
        </div>
    </section>












