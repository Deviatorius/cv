<html lang="en">
        <head>
                <title>Willow Communications - Entertainment Technology Partners  - Developers of interactive media and audio visual content</title>
                <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
                <meta name="description" content="Willow Communications Ltd develop and install audio visual systems, games, bingo systems and design media content primarily for the Leisure, Gaming and Corporate sectors">
                <link rel="icon" type="image/png" sizes="32x32" href="assets/images/icons/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="16x16" href="assets/images/icons/favicon-16x16.png">

                <!-- Brand Font -->
                <link   href="https://fonts.googleapis.com/css?family=Noto+Sans|Noto+Serif" rel="stylesheet">



                <!-- css -->
                <link   rel="stylesheet" type="text/css" href="assets/css/styles.css">

        </head>