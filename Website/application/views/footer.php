</body>
<footer>

<!-- Import Scripts -->
<script type="text/javascript" src="assets/js/jquery-3.6.0.min.js"></script>

<!--Skrollr src and init------------------------------->
<script type="text/javascript" src="assets/js/skrollr.js"></script>


<script type="text/javascript">
if($('#mobile-indicator').is(':visible')) {
        window.d = 0;
        window.m = 1;
        skrollr.init({
                forceHeight: false,
                edgeStrategy: 'set',
                easing: {
                        WTF: Math.random,
                        inverted: function(p) {
                                return 1-p;
                        }
                }
        
        });
        window.addEventListener('wheel', function(event) {
                if (event.deltaY < 0) {
                        console.log('scrolling up');
                        scrollUp();
                }
                else if (event.deltaY > 0) {
                        console.log('scrolling down');
                        scrollDown();
                }
        });
} else {
        window.d = 1;
        window.m = 0;
        skrollr.init({
                
                edgeStrategy: 'set',
                easing: {
                        WTF: Math.random,
                        inverted: function(p) {
                                return 1-p;
                        }
                }
        });
}
</script>


<!-- <script type="text/javascript">
        if (checkWheel() == false) {
                alert("desktop");
                skrollr.init({
                        edgeStrategy: 'set',
                        easing: {
                                WTF: Math.random,
                                inverted: function(p) {
                                        return 1-p;
                                }
                        }
                });
        } else {
                alert("mobile");
                document.getElementById('stylesheet-mobile').href='<?php echo base_url("/assets/css/styles-mobile.css"); ?>';
                $(" #stylesheet-mobile ").on("load", function(){
                        skrollr.init({
                                forceHeight: false,
                                edgeStrategy: 'set',
                                easing: {
                                        WTF: Math.random,
                                        inverted: function(p) {
                                                return 1-p;
                                        }
                                }
                                
                        });
                })
        }
        function checkTouch (){
                try{
                        document.createEvent("TouchEvent");
                        return true;
                } catch (e) {
                        return false;

                }
                
        }
        function checkWheel (){
                try{
                        document.createEvent("WheelEvent.deltaY");
                        alert("WheelEventPass");
                        return true;
                } catch (e) {
                        alert("WheelEventFail");
                        return false;

                }
                
        }
</script> -->
<script type="text/javascript" src="assets/js/main.js"></script>

</footer>
</html>

