<?php

class Landing_page extends MY_Controller {


    public function index() {


        $this->load->view("header");

        $this->load->view("landing_page");
        $this->load->view("background_animations");

        $this->load->view("footer");
 
    }
}