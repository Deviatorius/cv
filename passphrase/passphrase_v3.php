<?php
/**
 * Generates a Unique Word and Number Combination
 * From Date and Player ID and is Able to Restore
 * Player ID From the Unique Word.
 * Max Player ID 954143
 * 
 * Usage:
 * 
 * * Declaration
 * $variable = new passphrase;
 * 
 * * Generating Unique Word
 * $passphrase   = $variable->gen_passphrase($today, $player, $file);
 * 
 * * Restoring Player ID from Unique Word
 * $r_player     = $variable->restore_player($today, $passphrase, $file);
 * 
 * * Debugging
 * $variable->debug($passphrase, $r_player);
 */
class passphrase {

    /////////////////
    // Generation ///
    /////////////////

    // Text to File
    var $words = [];

    // Gen Passphrase
    var $int_today = "";
    var $int_player = "";
    var $seed = "";
    var $passphrase = "";

    // Gen Seed
    var $int_today_player = "";
    var $seed_delta = "";
    var $w_seed = [];
    var $n_seed = "";

    // Complexity
    var $c_w_seed = "";
    var $c_n_seed = "";
    var $c_w_first = "";
    var $c_w_second = "";
    var $c_w_third = "";
    var $c_n_first = "";
    var $c_n_second = "";
    var $c_n_third = "";
    var $jumble = "";
   
    /////////////////
    // Restoration //
    /////////////////

    var $first_segment = "";
    var $second_segment = "";
    var $r_w_zero = "";
    var $r_w_first = "";
    var $r_w_second = "";
    var $r_w_third = "";
    var $r_n_first = "";
    var $r_n_second = "";
    var $r_n_third = "";
    var $r_w_seed = "";
    var $r_n_seed = "";
    var $r_seed = [];
    var $r_player = "";
    var $r_seed_delta = "";
    
    /**
     * Outputs Class Debug Information
     * Variables and Logic
     */
    public function debug() {

        echo "Generated Passphrase =  {$this->passphrase}\r\n";
        echo "Given     Player ID  =  {$this->int_player}\r\n";
        echo "Restored  Player ID  =  {$this->r_player}\r\n";
        if ($this->int_player !== $this->r_player) {
            echo "!!ERROR RESTORING PLAYER ID!!\r\n";
        }
        else {
            echo "Player Restored Correctly\r\n";
        }

        
        echo "\r\n\r\n------START OF DEBUG ------";
        // Gen Passphrase
        echo "\r\n-------------------\r\n-------Input-------\r\n-------------------\r\n\r\n";
        echo "Today:   {$this->int_today} \r\n";
        echo "Player:  {$this->int_player} \r\n";

        // Gen Seed
        echo "\r\n-------------------\r\n--Seed Generation--\r\n-------------------\r\n\r\n";
        echo "int_today_player (Today + Player):              {$this->int_today_player}\r\n";
        echo "w_seed (int_today_player / dict_entry_count):   {$this->w_seed["seed"]}\r\n";
        echo "n_seed (int_today_player % dict_entry_count):   {$this->n_seed}\r\n";
        if (! empty($this->w_seed["overbuffer"])) {
            echo "Overbuffer: {$this->w_seed["overbuffer"]} (x900)\r\n";
            echo "Reference: {$this->w_seed["reference"]}\r\n";
        }
        else {
            echo "Overbuffer:                                     Not Set\r\n";
            echo "Reference:                                      Not Set\r\n";
        }

        // Complexity
        echo "\r\n-------------------\r\n----Complexity-----\r\n-------------------\r\n\r\n";
        echo "c_w_seed:     {$this->c_w_seed} (w_seed + 100)\r\n";
        echo "c_n_seed:     {$this->c_n_seed} (n_seed + 100)\r\n";
        echo "c_w_first:    {$this->c_w_first} ()\r\n";
        echo "c_w_second:   {$this->c_w_second} ()\r\n";
        echo "c_w_third:    {$this->c_w_third} ()\r\n";
        echo "c_n_first:    {$this->c_n_first} ()\r\n";
        echo "c_n_second:   {$this->c_n_second} ()\r\n";
        echo "c_n_third:    {$this->c_n_third} ()\r\n";
        echo "Jumble:       {$this->jumble} (Overbuffer n_first w_first ....)\r\n";

        // Restoration
        echo "\r\n-------------------\r\n----Restoration----\r\n-------------------\r\n\r\n";#
        if (strlen((string)$this->jumble) == 7 ) {
            echo "First Segment:   {$this->first_segment} -- "  . (($this->first_segment == substr($this->jumble, 0, 4))    ? "TRUE" : "FALSE") . "\r\n";
            echo "Second Segment:  {$this->second_segment} -- " . (($this->second_segment == substr($this->jumble, 4, 3))   ? "TRUE" : "FALSE") ."\r\n";
        }
        else {
            echo "First Segment:   {$this->first_segment} -- "  . (($this->first_segment == substr($this->jumble, 0, 3))    ? "TRUE" : "FALSE") . "\r\n";
            echo "Second Segment:  {$this->second_segment} -- " . (($this->second_segment == substr($this->jumble, 3, 3))   ? "TRUE" : "FALSE") ."\r\n";
        }
        echo "r_w_zero:        {$this->r_w_zero} \r\n";
        echo "r_w_first:       {$this->r_w_first} -- "      . (($this->r_w_first == $this->r_w_first)                   ? "TRUE" : "FALSE") . "\r\n";
        echo "r_w_second:      {$this->r_w_second} -- "     . (($this->r_w_second == $this->r_w_second)                 ? "TRUE" : "FALSE") . "\r\n";
        echo "r_w_third:       {$this->r_w_third} -- "      . (($this->r_w_third == $this->r_w_third)                   ? "TRUE" : "FALSE") . "\r\n";
        echo "r_n_first:       {$this->r_n_first} -- "      . (($this->r_n_first == $this->r_n_first)                   ? "TRUE" : "FALSE") . "\r\n";
        echo "r_n_second:      {$this->r_n_second} -- "     . (($this->r_n_second == $this->r_n_second)                 ? "TRUE" : "FALSE") . "\r\n";
        echo "r_n_third:       {$this->r_n_third} -- "      . (($this->r_n_third == $this->r_n_third)                   ? "TRUE" : "FALSE") . "\r\n";
        echo "r_w_seed:        {$this->r_w_seed} -- "       . (($this->r_w_seed == $this->r_w_seed)                     ? "TRUE" : "FALSE") . "\r\n";
        echo "r_n_seed:        {$this->r_n_seed} -- "       . (($this->r_n_seed == $this->r_n_seed)                     ? "TRUE" : "FALSE") . "\r\n";
        echo "r_seed_delta:    {$this->r_seed_delta} -- "   . (($this->r_seed_delta == $this->r_seed_delta)             ? "TRUE" : "FALSE") . "\r\n";

        // Restoration Summary
        

        echo "------END OF DEBUG------\r\n\r\n";
    }

    /**
     * Generates a Random Seeded Pass Phrase
     * 
     * @param string $today todays date as string
     * @param integer $player PC ID
     * @param string $file path to file
     */
    public function gen_passphrase($today, $player, $file): string {

        // Prepare Integers
        $int_today  = (int) strtotime($today, time()) / 3600;
        $int_player = (int) $player;

        // Load Words into Dictionary
        $dictionary   =  [];
        $dictionary   =  $this->txt_file_to_array($file);

        // Generate Seed
        $seed = $this->gen_seed($int_today, $int_player, $dictionary);

        $passphrase   = strtoupper($this->encode_seed($seed, $dictionary));

        // Add Vars for Debugging
        $this->int_today    = (! empty($int_today))     ? $int_today    : "Not Set";
        $this->int_player   = (! empty($int_player))    ? $int_player   : "Not Set";
        $this->seed         = (! empty($seed))          ? $seed         : "Not Set";
        $this->passphrase   = (! empty($passphrase))    ? $passphrase   : "Not Set";

        return $passphrase;
    }


    /**
     * Cretes an array from a text file
     * 
     * @param string $filepath path to file
     * @param int $n float of seed
     */
    private function txt_file_to_array($filepath): array {
        try {
            if ( ! empty( $this->words ) ) return $this->words;
        
        
            // Store the file
            $fp = @fopen($filepath, 'r'); 
    
            // Add Each Line to an Array
            if ($fp) {
                $i = 0;
                foreach (explode("\n", fread($fp, filesize($filepath))) as $word) {
                    $list[$i+100] = trim($word);
                    $i++;
                }
            }
    
            $this->words = (! empty($list)) ? $list : "Not Set";
            return $this->words;
        }
        catch (Exception $e) {
            $e->getMessage();
        }
    }


    /**
     * Generates a seed using date and player id
     * 
     * @param int $int_today todays date converted to integer using 
     * php date();
     * @param int $int_player player id
     * @param string $file File Location for Calculating Number of
     * Entries in Dictionary
     */
    private function gen_seed($int_today, $int_player, $file): int {
        try {
            // Number of Words in Database
            $dict_entry_count   =   count($file);

            // Seed Generated by Adding Day and Player ID
            $int_today_player   =   $int_today + $int_player;

            // Word Seed
            $w_seed             =   $this->wseed_overbuffer($int_today_player, $dict_entry_count); // 133

            // Number Seed
            $n_seed             =   $int_today_player % $dict_entry_count;                         // 505

            //Seed complexion for randomization
            $complexSeed        =   $this->introduce_complexity($w_seed, $n_seed);

            // Add Vars for Debugging
            $this->int_today_player  = (! empty($int_today_player)) ? $int_today_player : "Not Set";
            $this->w_seed            = (! empty($w_seed))           ? $w_seed : "Not Set";
            $this->n_seed            = (! empty($n_seed))           ? $n_seed : "Not Set";

            return $complexSeed;
        }
        catch (Exception $e) {
            $e->getMessage();
        }
    }


    /**
     * Adds Another Variable When $wSeed Suprasses the Maximum
     * Amount of Words in Dictionary
     * 
     * @param int $seed_delta 
     * @param int $dict_entry_count
     */
    private function wseed_overbuffer($int_today_player, $dict_entry_count):array  {
        try {
            if (! isset($int_today_player) || ! isset($dict_entry_count)) {
                return "Error - No Delta Seed or Dictionary Count received";
            }
            else {
                $w_seed = floor($int_today_player / $dict_entry_count);
                if ($w_seed >= $dict_entry_count) {
                    $overbuffer["seed"] = $w_seed - $dict_entry_count;
                    $overbuffer["overbuffer"] = floor($w_seed / $dict_entry_count);
                    $overbuffer["reference"] = $w_seed;
                }
                else {
                    $overbuffer["seed"] = $w_seed;
                }
            }
            
            return $overbuffer;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    /**
     * Complexifies the Word and Number Seeds Generated 
     * to Increase Randomness Factor of the Passphrase
     * 
     * @param array $c_w_seed Word Seed Array
     * @param int $c_n_Seed Number Seed
     */
    private function introduce_complexity($c_w_seed, $c_n_seed ):int {
        try {

            // Split Array into Variables
            foreach ($c_w_seed as $key => $value) {
                $$key = (int)$value;
                // $seed
                // $overbuffer
                // $reference
            }
            $c_w_seed = $seed;

            // Increase All Values to Have Minimum 3 Digits
            $c_w_seed      +=   100;
            $c_n_seed      +=   100;

            // Extract Each Digit from variables
            $c_w_first    =   substr($c_w_seed, 0, 1);
            $c_w_second   =   substr($c_w_seed, 1, 1);
            $c_w_third    =   substr($c_w_seed, 2, 1);
        
            $c_n_first    =   substr($c_n_seed, 0, 1);
            $c_n_second   =   substr($c_n_seed, 1, 1);
            $c_n_third    =   substr($c_n_seed, 2, 1);
        

            if (! isset($overbuffer)){
                // Prepare the Seed
                $jumble     =   $c_n_first . $c_w_first . $c_n_second . $c_w_second . $c_n_third . $c_w_third;
                //$jumble     =   $c_n_third . $c_w_first . $c_n_second . $c_w_third . $c_n_first . $c_w_second;

                // This one is quite random
                // $jumble     =   $c_n_first . $c_w_third . $c_n_second . $c_w_second . $c_n_third . $c_w_first;
                // $jumble     =   substr($jumble, 0, 3) . substr($jumble, 3, 3) + $c_n_second;

                
                // $jumble     =   $c_n_first . $c_w_third . $c_n_second . $c_w_second . $c_n_third . $c_w_first;
                // $jumble     =   (substr($jumble, 0, 5) + substr($jumble, 5, 1)) . substr($jumble, 5, 1);


            }
            else {
                if ($overbuffer > 0) { 
                    // If Overbuffer Exists Add It's Multiplier to the Start of the Seed And Then Prepare The Seed
                    $jumble     =   $overbuffer . $c_n_first . $c_w_first . $c_n_second . $c_w_second . $c_n_third . $c_w_third;
                }
                else {       
                    return "Overbuffer Expecting a Value Higher Than Zero";
                }
            }    

            // Add Vars for Debugging
            $this->c_w_seed     = (is_numeric($c_w_seed))      ? $c_w_seed     : "Not Set";
            $this->c_n_seed     = (is_numeric($c_n_seed))      ? $c_n_seed     : "Not Set";
            $this->c_w_first    = (is_numeric($c_w_first))     ? $c_w_first    : "Not Set";
            $this->c_w_second   = (is_numeric($c_w_second))    ? $c_w_second   : "Not Set";
            $this->c_w_third    = (is_numeric($c_w_third))     ? $c_w_third    : "Not Set";
            $this->c_n_first    = (is_numeric($c_n_first))     ? $c_n_first    : "Not Set";
            $this->c_n_second   = (is_numeric($c_n_second))    ? $c_n_second   : "Not Set";
            $this->c_n_third    = (is_numeric($c_n_third))     ? $c_n_third    : "Not Set";
            $this->jumble       = (is_numeric($jumble))        ? $jumble       : "Not Set";
    
            return $jumble;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    private function encode_seed($seed, $dictionary, $num = 1):string {
        try {
            $word   =   "";
            // Ability to generate multiple words at once
            for ($i=0; $i<$num; $i++) {
                if (! isset($seed)) {
                    return "Seed not received";
                }
                else {
                    // 
                    if (strlen((string)$seed) == 7) {
                        $first_segment    =   substr($seed, 0, 1);
                        $second_segment   =   substr($seed, 1, 3);
                        $third_segment    =   substr($seed, 4, 3);

                        $word            =   $first_segment;
                        $word           .=   $dictionary[$second_segment];
                        $word           .=   $third_segment;
                    }
                    elseif (strlen((string)$seed) == 6) {
                        $first_segment    =   substr($seed, 0, 3);
                        $second_segment   =   substr($seed, 3, 3);

                        $word           .=   $dictionary[$first_segment];
                        $word           .=   $second_segment;
                    }
                    else {
                        $word =  "Expecting seed length to be 6 or 7 characters long. Current length - " . strlen((string)$seed);
                    }
                    return $word;
                }
            }
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    /////////////////
    // Restoration //
    /////////////////


    /**
     * Restores Site ID from the Passphrase
     * 
     * @param int $today 
     * @param int $passphrase
     * @param int $file
     */
    
     public function restore_player($today, $passphrase, $file) {
        try {
            // Set Day
            $today          =    strtotime($today) / 3600;


            // Load Words into Dictionary
            $dictionary     =  [];
            $dictionary     =  $this->txt_file_to_array($file);

            // Restore Segment Seeds from Passphrase
            $r_Seed         =  [];
            $r_Seed         =  $this->restore_seed($passphrase, $dictionary);
            $r_s_word       =  $r_Seed[0];
            $r_s_number     =  $r_Seed[1];

            if (! empty($r_Seed[2])) {
                $r_s_word    += count($dictionary);
            }
            

            $r_w_seed          =  (count($dictionary) * $r_s_word);
            $r_seed_delta      =  $r_w_seed + $r_s_number;

            $r_seed_delta_length    =   strlen((string)$r_seed_delta);

            $first_segment = 1;
            if ($r_seed_delta_length < 6) {
                $second_segment = 6 - $r_seed_delta_length;
                switch ($second_segment) {
                    case 1: $zero = "0";
                    break;
                    case 2: $zero = "00";
                    break;
                    case 3: $zero = "000";
                    break;
                    case 4: $zero = "0000";
                    break;
                    case 5: $zero = "00000";
                    break;
                    default: $zero = "";
                }
                $r_delta      =  $first_segment;
                $r_delta     .=  $zero;
                $r_delta     .=  $r_seed_delta;
                $r_player     =  abs($today - $r_delta); 
            } 
            else {
                $r_int_today_player = substr($this->int_today, 0, 4) . $r_seed_delta;
                $r_player           =  abs($today - $r_seed_delta); 
            }


            // Add Vars for Debugging
            $this->r_seed_delta =   (! empty($r_seed_delta))    ? $r_seed_delta   : "Not Set";
            $this->r_player =       (! empty($r_player))        ? $r_player       : "Not Set";

            return $r_player;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }

    }
    /**
     * Restores Seed from Given Passphrase
     * 
     * @param string $passphrase 
     * @param array $dictionary
     */
    private function restore_seed($passphrase, $dictionary):array {
        try {
            if (! isset($passphrase) || ! isset($dictionary)) {
                $response = "Passphrase or Dictionary is not set. Cannot restore seed.";
            }
            else {

                // Split Passphrase Into Array
                preg_match_all('/([0-9]+|[a-zA-Z]+)/',$passphrase,$array);
                if (count($array[0]) == 2) {

                    // Extract Word from Passphrase
                    $word           =   mb_strtolower($array[0][0]);
                    // Extract Number from Passphrase
                    $second_segment  =   $array[0][1];

                    // Search Dictionary for the Key by Word
                    $first_segment   =   array_search($word, $dictionary, false);

                    // Assign Seed Keys
                    $r_w_zero         =   0;
                    $r_w_first        =   substr($first_segment, 1, 1);
                    $r_w_second       =   substr($second_segment, 0, 1);
                    $r_w_third        =   substr($second_segment, 2, 1);

                    $r_n_first        =   substr($first_segment, 0, 1);
                    $r_n_second       =   substr($first_segment, 2, 1);
                    $r_n_third        =   substr($second_segment, 1, 1);

                    // Restore Segments
                    $r_w_seed     =   $r_w_first . $r_w_second . $r_w_third;
                    $r_w_seed    -=   100;
                    $r_n_seed     =   $r_n_first . $r_n_second . $r_n_third;
                    $r_n_seed    -=   100;

                    // Prepare response
                    $r_seed[0]          =   $r_w_seed;
                    $r_seed[1]          =   $r_n_seed;

                }
                // 
                else {
                    // Extract Word from Passphrase
                    $word             =   mb_strtolower($array[0][1]);
                    // Extract Number from Passphrase
                    $second_segment   =   $array[0][2];

                    // Search Dictionary for the Key by Word
                    $first_segment    =   array_search($word, $dictionary, false);
                    $first_segment   +=   1000; // Needs Rethinking to Make More Flexible


                    // Assign Seed Keys
                    $r_w_zero         =   substr($first_segment, 0, 1);
                    $r_w_first        =   substr($first_segment, 2, 1);
                    $r_w_second       =   substr($second_segment, 0, 1);
                    $r_w_third        =   substr($second_segment, 2, 1);

                    $r_n_first        =   substr($first_segment, 1, 1);
                    $r_n_second       =   substr($first_segment, 3, 1);
                    $r_n_third        =   substr($second_segment, 1, 1);

                    // Restore Segments
                    $r_w_seed         =   $r_w_zero . $r_w_first . $r_w_second . $r_w_third;
                    $r_w_seed        -=   1100;
                    $r_n_seed         =   $r_n_first . $r_n_second . $r_n_third;
                    $r_n_seed        -=   100;

                    // Prepare response
                    $r_seed[0]        =   $r_w_seed;
                    $r_seed[1]        =   $r_n_seed;
                    $r_seed[2]        =   $r_w_zero;

                }
                
                // Add Vars for Debugging

                $this->first_segment    = (is_numeric($first_segment))  ? $first_segment    : "Not Set";
                $this->second_segment   = (is_numeric($second_segment)) ? $second_segment   : "Not Set";
                $this->r_w_zero         = (is_numeric($r_w_zero))       ? $r_w_zero         : "Not Set";
                $this->r_w_first        = (is_numeric($r_w_first))      ? $r_w_first        : "Not Set";
                $this->r_w_second       = (is_numeric($r_w_second))     ? $r_w_second       : "Not Set";
                $this->r_w_third        = (is_numeric($r_w_third))      ? $r_w_third        : "Not Set";
                $this->r_n_first        = (is_numeric($r_n_first))      ? $r_n_first        : "Not Set";
                $this->r_n_second       = (is_numeric($r_n_second))     ? $r_n_second       : "Not Set";
                $this->r_n_third        = (is_numeric($r_n_third))      ? $r_n_third        : "Not Set";
                $this->r_w_seed         = (is_numeric($r_w_seed))       ? $r_w_seed         : "Not Set";
                $this->r_n_seed         = (is_numeric($r_n_seed))       ? $r_n_seed         : "Not Set";
                $this->r_seed           = (is_numeric($r_seed))         ? $r_seed           : "Not Set";

                return $r_seed;
            }
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}


/////////////
// Testing //
/////////////

// Set Date
$today = date('d-m-Y', time());
//$today = "05-02-2022";

// Set PC ID
$player = 3; // My PC - "3", Meeting Room - "1740"

// Set Passphrase
$passphrase = "";

// Hold File Name and Path
$file = "list.txt";


$generator = new passphrase;

$passphrase   = $generator->gen_passphrase($today, $player, $file);
$r_player     = $generator->restore_player($today, $passphrase, $file);





$generator->debug($passphrase, $r_player);

//testplayer($generator, $today, $player, $passphrase, $file);






function testplayer($generator, $today, $player, $passphrase, $file) {
    for ($player=120000; $player<266000; $player++) {
        $passphrase = $generator->gen_passphrase($today, $player, $file);
    
        $r_player     = $generator->restore_player($today, $passphrase, $file);
    
        echo "{$passphrase}";
        if ($player == $r_player) {
            //echo "Successfully Restored {$player}.";
        }
        else {
            //echo "Error Restoring Player {$player} restored {$r_player}";
        }
    
        echo "\n\r";
    }
}

function testdate($generator, $today, $player, $passphrase, $file) {
    $day = substr($today, 2, 8);
    for ($i=1; $i<30; $i++) {
        $passphrase = $generator->gen_passphrase($i . $day, $player, $file);
    
        $r_player     = $generator->restore_player($i . $day, $passphrase, $file);
    
        echo "{$passphrase}";
        // if ($player == $r_player) {
        //     //echo "Successfully Restored {$player}.";
        // }
        // else {
        //     echo "Error Restoring Player {$player} restored {$r_player}";
        // }
    
        echo "\n\r";
    }
}


