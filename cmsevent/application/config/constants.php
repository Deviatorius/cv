<?php
defined('BASEPATH') OR exit('No direct script access allowed');
defined('FCPATH') OR define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);

/*
|--------------------------------------------------------------------------
| Inherit Site Settings
|--------------------------------------------------------------------------
|
| Inport Settings from Client Folder to allow Application to be Updated
*/

if ( file_exists(FCPATH . '\client\constants.php') ) {
    include_once(FCPATH . '\client\constants.php');
}


/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')             OR define('EXIT_SUCCESS', 0);                                                    // no errors
defined('EXIT_ERROR')               OR define('EXIT_ERROR', 1);                                                      // generic error
defined('EXIT_CONFIG')              OR define('EXIT_CONFIG', 3);                                                     // configuration error
defined('EXIT_UNKNOWN_FILE')        OR define('EXIT_UNKNOWN_FILE', 4);                                               // file not found
defined('EXIT_UNKNOWN_CLASS')       OR define('EXIT_UNKNOWN_CLASS', 5);                                              // unknown class
defined('EXIT_UNKNOWN_METHOD')      OR define('EXIT_UNKNOWN_METHOD', 6);                                             // unknown class member
defined('EXIT_USER_INPUT')          OR define('EXIT_USER_INPUT', 7);                                                 // invalid user input
defined('EXIT_DATABASE')            OR define('EXIT_DATABASE', 8);                                                   // database error
defined('EXIT__AUTO_MIN')           OR define('EXIT__AUTO_MIN', 9);                                                  // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')           OR define('EXIT__AUTO_MAX', 125);                                                // highest automatically-assigned error code

// Site Constants
defined('SITE_TITLE')        	    OR define('SITE_TITLE', 'Event Entertainment Portal'); 				            // Default Title
defined('SITE_TITLE_SPLITTER')	    OR define('SITE_TITLE_SPLITTER', ' &gt; '); 									// Default Splitter
defined('SITE_USES_GPDR')		    OR define('SITE_USES_GPDR', TRUE);	    		 								// Show GPDR Banner
defined('SITE_FORCES_SSL')		    OR define('SITE_FORCES_SSL', FALSE);                                            // Force HTTPs

// Defaults
defined('SESSION_NAME')		        OR define('SESSION_NAME', 'CMSEVENT');                                          // Session Name

// CMS Constants
defined('CMS_PATH_DISK')		    OR define('CMS_PATH_DISK', FCPATH . '\data\content'); 	            		    // Disk Path of Uploads
defined('CMS_PATH_TEMP')		    OR define('CMS_PATH_TEMP', sys_get_temp_dir() );                                // Disk Path of Temporary Uploads
defined('CMS_PATH_WEB')			    OR define('CMS_PATH_WEB', 'http://127.0.0.1/');      					   		// Root Web Path of CMS
defined('CMS_MAX_IMAGE_SIZE')	    OR define('CMS_MAX_IMAGE_SIZE', 52428800 ); 									// Max File Size
defined('CMS_MAX_VIDEO_SIZE')	    OR define('CMS_MAX_VIDEO_SIZE', 262144000); 									// Max File Size
defined('CMS_VIDEO_FORMATS')	    OR define('CMS_VIDEO_FORMATS', "WMV");  								        // Allowed Video Formats

// Internet Constants
defined('INTERNET_PROXY')		    OR define('INTERNET_PROXY', ''); 												// Internet Proxy
defined('INTERNET_CREDENTIALS')	    OR define('INTERNET_CREDENTIALS', ''); 											// Internet Credentials

// Mail Server Setup
defined('MAIL_FROM_NAME')		    OR define('MAIL_FROM_NAME', 'Event Entertainment Portal');                      // Default Mail Name
defined('MAIL_FROM_ADDRESS')	    OR define('MAIL_FROM_ADDRESS', 'hello@willowcommunications.com');               // Default Mail Address
defined('MAIL_SERVER')			    OR define('MAIL_SERVER', '');                                                   // Mail Server Address
defined('MAIL_USERNAME')		    OR define('MAIL_USERNAME', '');                                                 // Mail Server Username
defined('MAIL_PASSWORD')		    OR define('MAIL_PASSWORD', '');                                                 // Mail Server Password

// Mail Recievers
defined('MAIL_ADDRESS_STATUS')	    OR define('MAIL_ADDRESS_STATUS', 'it@willowsv.com');                            // Default Status Address

// Database Settings
defined('DATABASE_SERVER')          OR define('DATABASE_SERVER', '127.0.0.1');                                      // Default Database Hostname
defined('DATABASE_PORT')            OR define('DATABASE_PORT', '3306');                                             // Default Database Port
defined('DATABASE_USERNAME')        OR define('DATABASE_USERNAME', 'svmedia');                                      // Default Database Username
defined('DATABASE_PASSWORD')        OR define('DATABASE_PASSWORD', 'svmedia');                                      // Default Database Password
defined('DATABASE_DATABASE')        OR define('DATABASE_DATABASE', 'svmedia');                                      // Default Database Database

// Product Constants
defined('SITE_VERSION')			    OR define('SITE_VERSION', '1.0.3');                                             // Application Version
defined('SITE_TITLE')        	    OR define('SITE_TITLE', 'EVENT ENTERTAINMENT PORTAL');                          // Default Title
defined('SITE_TITLE_SPLITTER')	    OR define('SITE_TITLE_SPLITTER', ' &gt; ');                                     // Default Splitter

// Define Password Constants
defined('PASSWORD_ALGO')            OR define('PASSWORD_ALGO', PASSWORD_DEFAULT);                                   // Password Algo to Use
defined('PASSWORD_COST')            OR define('PASSWORD_COST', 12);                                                 // Password Cost
