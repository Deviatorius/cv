<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Mail Configuration
$config['useragent'] 	= 	"Event Interaction Portal";
$config['protocol'] 	= 	"smtp";
$config['smtp_host']	= 	MAIL_SERVER;
$config['smtp_user']	=	MAIL_USERNAME;
$config['smtp_pass']	=	MAIL_PASSWORD;
$config['smtp_port']	=	25;
$config['smtp_timeout']	=	15;
$config['crlf']			=	"\r\n";
$config['newline']		=	"\r\n";
$config['smtp_crypto']	=	NULL;
$config['mailtype']		=	"html";
$config['charset']		=	"utf-8";