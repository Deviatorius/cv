<?php
    /*
    | -------------------------------------------------------------------
    | Rendering Profiles
    | -------------------------------------------------------------------
    | This file contains an array of rendering profiles used
    | These can be overridden or complimented using the identical file
    | in the client folder.
    |
    | The Width and Height of the Video will be used to determine the 
    | Number of Pixels. The closest pixel count will be used to specify
    | Maximum Bit Rate as well as any additional proprties.
    */

    // Create Array
    $_RENDER    =   [   "SETTINGS"  => [
                            "THREADS"   =>  1
                        ],
                        "PROFILES"    => [

                        ]
                    ];


    // Default Profiles
    $_RENDER['PROFILES'][0] =   [
        "KEY"           =>  480000,
        "NAME"          =>  "800x600",
        "BITRATE"       =>  2097152,
        "DESCIPTION"    =>  "Low Resolution Profile Capped to 2MB"
    ];

    // 1024x768
    $_RENDER['PROFILES'][786432] =   [
        "KEY"           =>  786432,
        "NAME"          =>  "VGA",
        "BITRATE"       =>  4194304,
        "DESCIPTION"    =>  "VGA Profile Capped to 4MB"
    ];

    // 1280x720
    $_RENDER['PROFILES'][921600] =   [
        "KEY"           =>  921600,
        "NAME"          =>  "720P",
        "BITRATE"       =>  6291456,
        "DESCIPTION"    =>  "1080p Profile Capped to 6MB"
    ];

    // 1080p
    $_RENDER['PROFILES'][2073600] =   [
        "KEY"           =>  2073600,
        "NAME"          =>  "1080P",
        "BITRATE"       =>  10695475,
        "DESCIPTION"    =>  "1080p Profile Capped to 10.2MB"
    ];

    // 4K
    $_RENDER['PROFILES'][8294400] =   [
        "KEY"           =>  8294400,
        "NAME"          =>  "4K",
        "BITRATE"       =>  16986931,
        "DESCIPTION"    =>  "4k Profile Capped to 16.2MB"
    ];


    /*
    |--------------------------------------------------------------------------
    | Inherit Client Settings
    |--------------------------------------------------------------------------
    |
    | Import / Override Settings from Client Folder
    */

    if ( file_exists(FCPATH . '\client\rendering.php') ) {
        include_once(FCPATH . '\client\rendering.php');
    }

