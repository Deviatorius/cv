<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Advert Model
	 */
	class Mod_Music extends CI_Model {


		/**
		 * Returns Valid Server Modes
		 *
		 * @return void
		 */
		public function server_modes() {
			return [
				0	=>	"Master Mode",
				1	=>	"Slave Mode"
			];
		}


		/**
		 * Holds Cached Music Types
		 */
		public $music_types = NULL;



		/** ***************************************************************************************
		 * 	LEGACY PLAYLIST MODEL
		 *  ***************************************************************************************
		 */	

		/**
		 * Gets a Playlist Model
		 *
		 * @return array    The Playlist Model
		 */
		public function playlist_model() {

			// Build Model
			return  Array (
						"id"		        =>	NULL,
						"name" 		        => 	"",
						"playlist"			=>	TRUE,
                        "shuffled"	        =>	TRUE,
                        "plgroup"	        =>	NULL,
                        "order"		        =>  0,
                        "hidden"	        =>	FALSE,
                        "encrypted"	        =>	FALSE,
                        "disableadverts"    =>  FALSE,
                        "adverttracks"      =>  0,
						"adverttime"        =>  0,
						"level"				=>	NULL,
						"video"				=>	FALSE,
						"locations"	        =>	Array(),
						"brands"	        =>	Array(),
						"managers"	        =>	Array(),
						"edit"		        =>	FALSE,
						"notes"				=>	NULL
					);

		}



		/** ***************************************************************************************
		 * 	LEGACY PLAYLIST MODEL HELPERS
		 *  ***************************************************************************************
		 */


		/**
		 * Returns a Playlist Model based on a Database Result
		 *
		 * @param Array $db_list       A dynamic_list row result
		 * @param bool $get_managers  Get Manager Information
		 * @param bool $get_locations Get Location Information
		 *
		 * @return Array    A Populated Playlist
		 */
		public function build_playlist_model($db_list, $get_managers = FALSE, $get_locations = FALSE) {

			// Get Playlist Model
			$playlist = $this->playlist_model();

			// Populate Playlist
			if ( ! empty($db_list) && is_array($db_list) ) {

				// Can We Edit?
				$playlist['edit'] = FALSE;
				if ( ! empty($_SESSION['USER']) && ! empty($_SESSION['USER']['ADMIN']) )
					$playlist['edit'] = TRUE;

				// Populate Core Information
				if ( isset($db_list['id']) ) 		        $playlist['id'] = $db_list['id'];
                if ( isset($db_list['name']) ) 		        $playlist['name'] = $db_list['name'];
				if ( isset($db_list['playlist']) ) 	        $playlist['playlist'] = $db_list['playlist']==1 ? TRUE : FALSE;
				if ( isset($db_list['shuffled']) ) 	        $playlist['shuffled'] = $db_list['shuffled']==1 ? TRUE : FALSE;
                if ( isset($db_list['plgroup']) ) 	        $playlist['plgroup'] = $db_list['plgroup'];
                if ( isset($db_list['order']) )		        $playlist['order'] = $db_list['order'];
                if ( isset($db_list['hidden']) ) 	        $playlist['hidden'] = $db_list['hidden']==1 ? TRUE : FALSE;
                if ( isset($db_list['encrypted']) ) 	    $playlist['encrypted'] = $db_list['encrypted']==1 ? TRUE : FALSE;
				if ( isset($db_list['disableadverts']) ) 	$playlist['disableadverts'] = $db_list['disableadverts']==1 ? TRUE : FALSE;
				if ( isset($db_list['video']) ) 			$playlist['video'] = $db_list['video']==1 ? TRUE : FALSE;
                if ( isset($db_list['adverttracks']) )		$playlist['adverttracks'] = $db_list['adverttracks'];
				if ( isset($db_list['adverttime']) )		$playlist['adverttime'] = $db_list['adverttime'];
				if ( isset($db_list['notes']) )				$playlist['notes'] = $db_list['notes'];
				if ( isset($db_list['level']) )				$playlist['level'] = $db_list['level'];

				// Add Additional Information
				if ( isset($db_list['group_id']) )			$playlist['group_id'] = $db_list['group_id'];
				if ( isset($db_list['group_name']) )		$playlist['group_name'] = $db_list['group_name'];
				if ( isset($db_list['group_order']) )		$playlist['group_order'] = $db_list['group_order'];

				// Populate Managers
				if ( ! empty($db_list['id']) && $get_managers ) {

					// Load Users
					$pMans = $this->db->query(" SELECT	m.`username`, u.`name`
												FROM 	`music_list_manager` m, `user` u
												WHERE 	u.`username` = m.`username`
												AND 	m.`playlist` = ?
											 ",
												Array (
													$db_list['id']
												)
											)->result_array();

					// Add Managers
					foreach ($pMans as $key => $value) {
						$playlist['managers'][$value['username']] = $value['name'];
					}

					// Mark Admin
					if ( ! empty($_SESSION['USER']['USERNAME']) && ! empty($playlist['managers'][$_SESSION['USER']['USERNAME']]) )	$playlist['edit'] = TRUE;

				}

				// Populate Locations
				if ( ! empty($db_list['id']) && $get_locations ) {

					// Load Locations
					$pLocs = $this->db->query(" SELECT	p.`location`, s.`name`, s.`brand`, b.`description`
												FROM 	`music_list_location` p, `store` s
													LEFT JOIN `brand` b ON b.`id` = s.`brand`
												WHERE 	s.`id` = p.`location`
												AND 	p.`playlist` = ?
											 ",
												Array (
													$db_list['id']
												)
											)->result_array();

					// Add Locations
					foreach ($pLocs as $key => $value) {
						if ( empty($value['brand']) ) { $value['brand'] = 0; $value['description'] = "NO BRAND"; }
						$playlist['locations'][$value['location']] = $value['name'];
						$playlist['brands'][$value['brand']] = $value['description'];
					}

				}

			}

			// Return
			return $playlist;

		}


		/** ***************************************************************************************
		 * 	CACHE METHODS
		 *  ***************************************************************************************
		 */

		/**
		 * Cleans up the Temporary upload Location
		 *
		 * @return void
		 */
		public function clean_cache() {

			// Load Library
			$this->load->library("file");

			// Get Cache Path
			$cPath = sanatize_path(CMS_PATH_TEMP);

			// Build Results Array
			$result = $this->file->file_list($cPath);
			if (!$result)
				$result	= Array();

			// Delete Cache Files Older than 24h
			foreach ($result as $file) {
				if (is_file($cPath . $file)) {
					$age	=	@filemtime($cPath . $file);
					if ($age && (time() - $age) >= 86400) {
						@unlink($cPath . $file);
					}
				}
			}

		}


		/** ***************************************************************************************
		 * 	MISC FUNCTIONS
		 *  ***************************************************************************************
		 */

		/**
		 * Gets Music Players
		 *
		 * @param 	boolean 	$group	Group by Site
		 * @return 	array		A List of Players
		 */
		public function get_music_players( $group = true ) {

			// Hold Return
			$return = [];

			// Get Music Players
			$q = $this->db->query("	SELECT 			p.`id`, p.`store`, p.`serial`, p.`name`, p.`description`, s.`name` 'store_name' 
									FROM 			`player` p
										LEFT JOIN	`store` s ON s.`id` = p.`store`
									WHERE 			`product` = 'CMP'
									ORDER BY		p.`store`
								")->result_array();

			// Build
			foreach ( $q as $player ) {
				if ( $group ) {
					if ( empty($return[$player['store']]) )		$return[$player['store']] = [ 'name' => $player['store_name'], 'players' => [] ];
					$return[$player['store']]['players'][$player['id']] = $player;
				}
				else {
					$return[$player['id']] = $player;
				}	
			}

			// Return
			return $return;

		}



		/** ***************************************************************************************
		 * 	LEGACY PLAYLIST METHODS
		 *  ***************************************************************************************
		 */

		/**
		 * Gets All System Playlists
		 *
		 * @return array    An Array of Playlists
		 */
		public function get_all_playlists( $sort = 1 ) {

			// Build Sort
			switch ($sort) {
				case 2:
					$sort = "ORDER BY	`group_order`, `group_name`, `name`";
					break;
				default:
					$sort = "ORDER BY	`name`";
					break;
			}

			// Query Playlists
			$q = $this->db->query("		SELECT 		l.*, IFNULL(g.`id`, 0) 'group_id', IFNULL(g.`name`, 'No Group') 'group_name', IFNULL(g.`order`, -2000000001) 'group_order'
										FROM		`music_list` l
										LEFT JOIN	`music_list_group` g ON g.`id` = l.`plgroup`
										{$sort}
								  ")->result_array();

			// Return
			return $q;

		}
		 

		/**
		 * Gets a Playlist
		 *
		 * @param int $pid The Playlist ID
		 *
		 * @return Array    A Playlist Definition
		 */
		public function get_playlist( $pid ) {

			// Lookup Playlist
			$pl = $this->db->query(" SELECT * FROM `music_list` WHERE `id` = ? ", Array($pid))->row_array();

			// Return if not found
			if ( empty($pl) )	RETURN NULL;

			// Populate
			return $this->build_playlist_model( $pl, TRUE, TRUE );

		}


        /**
         * Check whether a playlist is unique
         *
         * @return bool
         */ 
        public function playlist_exists( $playlist ) {

            // Find Playlist
            if ( empty($playlist) )     return FALSE;

            // Lookup Playlist
            $q = $this->db->query(" SELECT `id` FROM `music_list` WHERE `name` = ? ", [$playlist])->result_array();

            // Return
            if ( empty($q) ) {
                return FALSE;
            }
            else {
                return @$q[0]['id'];
            }

        }


		/**
		 * Saves a Dynamic Playlist
		 *
		 * @param array $playlist The Playlist Structure
         * @param string $original The Original Playlist Name (if Updating)
		 *
		 * @return mixed Successful or Error
		 */
		public function save_playlist( $playlist, $original = NULL ) {

			// Check Playlist
			if ( empty($playlist) || ! is_array($playlist) )	return FALSE;

			// Set Defaults
			if ( empty($playlist['name']) )						$playlist['name'] = "Unnamed Playlist";
            if ( ! isset($playlist['shuffled']) )				$playlist['shuffled'] = TRUE;
            if ( empty($playlist['hidden']) )			        $playlist['hidden'] = 0;
			if ( empty($playlist['encrypted']) )			    $playlist['encrypted'] = 0;
			if ( empty($playlist['video']) )			    	$playlist['video'] = 0;
            if ( empty($playlist['disableadverts']) )			$playlist['disableadverts'] = 0;
			if ( empty($playlist['adverttracks']) )				$playlist['adverttracks'] = 0;
			if ( empty($playlist['adverttime']) )				$playlist['adverttime'] = 0;

			// Set Nullables
            if ( empty($playlist['plgroup']) || ! is_numeric($playlist['plgroup']) )		        $playlist['plgroup'] = NULL;
            if ( empty($playlist['adverttracks']) || ! is_numeric($playlist['adverttracks']) )		$playlist['adverttracks'] = NULL;	
			if ( empty($playlist['adverttime']) || ! is_numeric($playlist['adverttime']) )	        $playlist['adverttime'] = NULL;
			if ( empty($playlist['notes']) )	        											$playlist['notes'] = NULL;
			if ( empty($playlist['level']) )	        											$playlist['level'] = NULL;	
            
			// Start Transaction
			$this->db->trans_start();

			// Create Playlist
			if ( empty($playlist['id']) ) {

				// Create Playlist
				$this->db->query("	INSERT INTO `music_list`
										(`id`, `name`, `playlist`, `shuffled`, `plgroup`, `order`, `hidden`, `encrypted`, `disableadverts`, `adverttracks`, `adverttime`, `level`, `video`, `notes`)
									VALUES
										(NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
								",
									Array (
										$playlist['name'] , $playlist['playlist'], $playlist['shuffled'], $playlist['plgroup'], $playlist['order'], $playlist['hidden'], $playlist['encrypted'], $playlist['disableadverts'], $playlist['adverttracks'], $playlist['adverttime'], $playlist['level'], $playlist['video'], $playlist['notes']
									)
								);

				// Set ID
                $playlist['id'] = $this->db->insert_id();

                // Create Playlist
                if ( ! $this->create_disk_playlist( $playlist ) ) {
                    $this->db->trans_rollback();
                    return "Could not create the playlist file or folder";
                }
  
			}
			else {

                // Check Name
                if ( empty($original) ) {
                    $this->db->trans_rollback();
                    return "No Original name was specified for the update";
                }

				// Update Playlist
				$this->db->query("	UPDATE `music_list`
									SET	`name` 		        = 	?,
										`playlist`			= 	?,
										`shuffled` 	        = 	?,
										`plgroup` 	        = 	?,
										`order`		        = 	?,
										`hidden`	        = 	?,
										`encrypted`	        = 	?,
										`disableadverts`	= 	?,
										`adverttracks`	    = 	?,
                                        `adverttime`        = 	?,
										`level`				= 	?,
										`video`				=	?,
										`notes`				= 	?
									WHERE
										`id`		        = 	?
								",
									Array (
										$playlist['name'] , $playlist['playlist'] , $playlist['shuffled'], $playlist['plgroup'], $playlist['order'], $playlist['hidden'], $playlist['encrypted'], $playlist['disableadverts'], $playlist['adverttracks'], $playlist['adverttime'], $playlist['level'], $playlist['video'], $playlist['notes'], $playlist['id']
									)
                                );
                                
                // Update Disk Playlist
                if ( ! $this->update_disk_playlist( $playlist, $original ) ) {
                    $this->db->trans_rollback();
                    return "Could not update the playlist file or folder";
                }

			}

			// Add Locations
			$this->db->query(" DELETE FROM `music_list_location` WHERE `playlist` = ? ", Array($playlist['id']) );
			if ( ! empty($playlist['locations']) && is_array($playlist['locations']) ) {
				foreach ($playlist['locations'] as $key => $location) {
					$this->db->query("	INSERT INTO `music_list_location` (`playlist`, `location`) VALUES (?, ?)	", Array($playlist['id'], $key ));
				}
			}

			// Add Managers
			$this->db->query(" DELETE e FROM `music_list_manager` e LEFT JOIN `user` u ON u.`username` = e.`username` WHERE u.`disabled` = 0 AND e.`playlist` = ? ", Array($playlist['id']) );
			if ( ! empty($playlist['managers']) && is_array($playlist['managers']) ) {
				foreach ($playlist['managers'] as $key => $manager) {
					$this->db->query("	INSERT INTO `music_list_manager` (`playlist`, `username`) VALUES (?, ?)	", Array($playlist['id'], $key));
				}
			}

			// Commit
			$this->db->trans_complete();

			// Return
			return $playlist['id'];

		}


		/**
		 * Deletes a Playlist from the Database
		 *
		 * @param structure $playlist The Playlist Structure
		 *
		 * @return bool    Success
		 */
		public function delete_playlist( $playlist ) {

			// Check Params
            if ( ! is_array($playlist) ) 	                            return FALSE;
            if ( ! $playlist['id'] || ! is_numeric($playlist['id']) ) 	return FALSE;

            // Delete Playlist File
            $this->delete_disk_playlist( $playlist );

			// Delete Playlist
			$this->db->query(" DELETE FROM `music_list` WHERE `id` = ? ", Array($playlist['id']));
			return TRUE;

		}


		/** ***************************************************************************************
		 * 	PLAYLIST GROUP METHODS
		 *  ***************************************************************************************
		 */

		/**
		 * Gets a Playlist Group
		 *
		 * @param int $gid The Playlist ID
		 *
		 * @return Array    A Playlist Group
		 */
		public function get_playlist_group( $gid ) {

			// Lookup Playlist
			return $this->db->query(" SELECT * FROM `music_list_group` WHERE `id` = ? ", Array($gid))->row_array();

		}


		/**
		 * Gets All System Playlist Groups
		 *
		 * @return array    An Array of Groups
		 */
		public function get_all_playlist_groups() {

			// Query Playlists
			$q = $this->db->query("		SELECT 		*
										FROM		`music_list_group`
										ORDER BY	`name`
								  ")->result_array();

			// Return
			return $q;

		}


		/**
		 * Gets a List of Valid Playlist Groups
		 *
		 * @return Array    An Array of Groups
		 */
		public function get_playlist_groups() {

			// Create Array
			$return = [0 => "No Group"];

			// Grab Groups
			$gs = $this->get_all_playlist_groups();
			foreach ($gs as $value) {
				$return[$value['id']] = $value['name'];
			}

			// Return
			return $return;

		}


		/**
		 * Creates a Dynamic Playlist
		 *
		 * @param array $playlist The Playlist Structure
		 *
		 * @return int The Playlist ID
		 */
		public function create_playlist_group( $name, $order = 0 ) {

			// Check Playlist
			if ( empty($name) )	return FALSE;
			if ( empty($order) || ! is_numeric($order) ) 	$order = 0;

			// Create Playlist
			$this->db->query("	INSERT INTO `music_list_group`
									(`id`, `name`, `order`)
								VALUES
									(NULL, ?, ?)
							",
								Array (
									$name, $order
								)
							);

			// Grab ID
			$pid = $this->db->insert_id();

			// Return
			return $pid;

		}


		/**
		 * Updates a Playlist Group
		 *
		 * @param int $playlist The Playlist Structure
		 * @param array $playlist The Playlist Structure
		 *
		 * @return bool Successful
		 */
		public function update_playlist_group( $id, $name, $order = 0 ) {

			// Check Params
			if (!is_numeric($id)) 	return FALSE;
			if ( empty($name) )		return FALSE;
			if ( empty($order) || ! is_numeric($order) )	$order = 0;

			// Update Playlist
			$this->db->query("	UPDATE `music_list_group`
								SET	`name` 		= ?,
									`order`		= ?
								WHERE
									`id`		= ?
							",
								Array (
									$name , $order, $id
								)
							);


			// Return
			return TRUE;

		}


		/**
		 * Deletes a Playlist Group from the Database
		 *
		 * @param int $gid The Playlist Group
		 *
		 * @return bool    Success
		 */
		public function delete_playlist_group( $gid ) {

			// Check Params
			if ( ! is_numeric($gid) ) 	return FALSE;

			// Delete Playlist
			$this->db->query(" UPDATE `music_list` SET `plgroup` = NULL WHERE `plgroup` = ? ", Array($gid));
			$this->db->query(" DELETE FROM `music_list_group` WHERE `id` = ? ", Array($gid));
			return TRUE;

		}


        /**
         * Get Branch Playlists
         *
         * @param   int     $sid    The Location ID
         * @return  array           A List of Playlists
         */
        public function get_branch_playlists( $sid, $expanded = FALSE ) {

			// Check Params
			if ( ! is_numeric($sid) )   return [];

            // Get Database Playlists
            $q = $this->db->query("   	SELECT  	ml.*, l.*, IFNULL(g.`id`, 0) 'group_id', IFNULL(g.`name`, 'No Group') 'group_name', IFNULL(g.`order`, -2000000001) 'group_order'
                                        FROM    	`music_list_location` ml, `music_list` l
										LEFT JOIN	`music_list_group` g ON g.`id` = l.`plgroup`
                                        WHERE   	l.`id` = ml.`playlist`
                                        AND     	ml.`location` = ?
										ORDER BY	`group_order`, `group_name`, `name`
                                    ", 
                                        [$sid]
                                    )->result_array();

            // Return
            if ( ! $expanded ) {
                return $q;
            }
            else {

                // Build Array
                $return = [];
                foreach ( $q as $list ) {
                    $return[$list['id']]    =   $this->build_playlist_model( $list, TRUE, TRUE );
                }

                // Return
                return $return;

            }

        }


        /**
         * Get User Playlists
         *
         * @param   string     $user    The Username
         * @return  array               A List of Repositories
         */
        public function get_user_playlists( $user = "", $expanded = FALSE ) {

			// Default User
            if ($user == NULL && ! empty($_SESSION['USER']))	$user = $_SESSION['USER']['USERNAME'];
			
			// Get Playlists
			if ( $this->mod_user->is_admin($user) ) {

				// Get All Playlists
				$q = $this->db->query("		SELECT 		l.*, IFNULL(g.`id`, 0) 'group_id', IFNULL(g.`name`, 'No Group') 'group_name', IFNULL(g.`order`, -2000000001) 'group_order'
											FROM		`music_list` l
											LEFT JOIN	`music_list_group` g ON g.`id` = l.`plgroup`
											ORDER BY	`group_order`, `group_name`, `name`
									")->result_array();

			}
			else {

				// Get Managed Playlists
				$q = $this->db->query("		SELECT 		l.*, IFNULL(g.`id`, 0) 'group_id', IFNULL(g.`name`, 'No Group') 'group_name', IFNULL(g.`order`, -2000000001) 'group_order'
											FROM		`music_list_manager` m,
														`music_list` l
											LEFT JOIN	`music_list_group` g ON g.`id` = l.`plgroup`
											WHERE		l.`id` = m.`playlist`
											AND			m.`username` = ?
											ORDER BY	`group_order`, `group_name`, `name`
									",
										Array (
											$user
										)
									)->result_array();

			}

            // Return
            if ( ! $expanded ) {
                return $q;
            }
            else {

                // Build Array
                $return = [];
                foreach ( $q as $list ) {
                    $return[$list['id']]    =   $this->build_playlist_model( $list, TRUE, TRUE );
                }

                // Return
                return $return;

            }

        }


		/** ***************************************************************************************
		 * 	DISK FUNCTIONS
		 *  ***************************************************************************************
		 */

         /**
          * Sanatises a Playlist Name
          *
          * @param string $name The Proposed Name
          * @return string      The Sanatized Name
          */
         function sanatise_playlist_name( $name ) {

            // Sanatize Name
            return @mb_ereg_replace("([^\w\s\d\-_~,;&\[\]\(\)])", '', $name);

         }


         /**
          * Create a Music Playlist File
          *
          * @param string   $name   The Playlist to Create
          * @return bool            Successful
          */
        function create_disk_playlist( $playlist ) {

            // Sanatize Name
            $playlist['name'] = $this->sanatise_playlist_name( $playlist['name'] );
            if ( empty($playlist['name']) ) return FALSE;

            // Delete Previous Playlists
            @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] . ".xml" );
            @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] . ".xmle" );

            // Create Playlist Folder
            if ( ! is_dir( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR .  $playlist['name'] ) ) {
                if ( ! mkdir( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR .  $playlist['name'], 0777, TRUE ) )
                    return FALSE;
            }

            // Create Playlist File
            $fName = $playlist['name'] . (empty($playlist['encrypted']) ? ".xml" : ".xmle");
            if ( ! file_put_contents( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $fName, $this->get_playlist_file( $playlist ) ) ) {
                return FALSE;
            }

            // All Ok
            return TRUE;

        }


        /**
         * Deletes a Playlists Music Folder
         *
         * @param structure   $playlist     The Playlist Structure
         * @return bool                     Successful
        */
        function delete_disk_playlist( $playlist ) {

            // Sanatize Name
            $playlist['name'] = $this->sanatise_playlist_name( $playlist['name'] );
            if ( empty($playlist['name']) ) return FALSE;

            // Delete Previous Playlists
            @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] . ".xml" );
            @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] . ".xmle" );

            // Delete Playlist Files
            $dir = CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'];
            if ( is_dir($dir) ) {
                $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
                $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
                foreach($files as $file) {
                    if ($file->isDir()){
                        rmdir($file->getRealPath());
                    } else {
                        unlink($file->getRealPath());
                    }
                }
                rmdir($dir);
            }

            // Done
            return TRUE;

        }


        /**
         * Updates a Disk Playlist
         *
         * @param structure The Playlist Name
         * @param string The Original Playlist Name (If Renaming)
         * @return bool  Successful
         */
        function update_disk_playlist( $playlist, $original ) {
            
            // Sanatize Name
            $playlist['name'] = $this->sanatise_playlist_name( $playlist['name'] );
            if ( empty($playlist['name']) ) return FALSE;

            // Delete Old Playlists
            if ( ! empty($original) ) {

                // Delete Old Playlist Files
                @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $original . ".xml" );
                @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $original . ".xmle" );
                
                // Check Renames
                if ( mb_strtoupper($playlist['name']) <> mb_strtoupper($original) ) {
                    
                    // Attempt to Rename Existing Folder
                    if ( is_dir( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $original ) && ! is_dir( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] ) ) {
                        if ( ! rename( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $original , CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] ) )
                            return FALSE;
                    }

                    // Attempt to Create Playlist Folder
                    elseif ( ! is_dir( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR .  $playlist['name'] ) ) {
                        if ( ! mkdir( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR .  $playlist['name'], 0777, TRUE ) )
                            return FALSE;
                    }

                }               

            }

            // Delete the New Playlists (Might as well make sure these are empty too)
            @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] . ".xml" );
            @unlink( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $playlist['name'] . ".xmle" );     
                        
            // Create Playlist File
            $fName = $playlist['name'] . (empty($playlist['encrypted']) ? ".xml" : ".xmle");
            if ( ! file_put_contents( CMS_PATH_MUSIC . DIRECTORY_SEPARATOR . $fName, $this->get_playlist_file( $playlist ) ) ) {
                return FALSE;
            }

            // All Ok
            return TRUE;

        }


        /**
         * Gets a Playlist File for a Given Playlist
         *
         * @param structure A Playlist Structure
         * @return string   The File Data
         */
        function get_playlist_file( $playlist ) {

            // Get Library
            $this->load->library("xml");
            $this->load->library("locker");

			// Create Simple XML Document
			$xml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><PlayList />");

            // Add Parameters
            if ( ! empty($playlist['shuffled']) )           $this->xml->escaped_attribute($xml, "shuffled", "true");
            if ( ! empty($playlist['disableadverts']) )     $this->xml->escaped_attribute($xml, "disableadverts", "true");
            if ( ! empty($playlist['adverttracks']) )       $this->xml->escaped_attribute($xml, "adverttracks", $playlist['adverttracks']);
            if ( ! empty($playlist['adverttime']) )         $this->xml->escaped_attribute($xml, "adverttime", $playlist['adverttime']);

            // Create Node
            $bFolder = $xml->addChild("folder");
            $this->xml->escaped_attribute($bFolder, "path", 'CMS\\' . $playlist['name']);

            // Get Data
            $rData = $this->xml->output($xml, TRUE);

            // Return Data
            if ( ! empty($playlist['encrypted']) ) {
                return $this->locker->encrypt( $rData );
            }
            else {
                return $rData;
            }

        }



		/** ***************************************************************************************
		 * 	SLAVE FUNCTIONS
		 *  ***************************************************************************************
		 */

		 /**
		  * A Model that Defines a Slave
		  */
		public function slave_model( $existing = NULL, $import = NULL ) {

			// Create File
			if ( empty($existing) || ! is_array($existing) ) {
				$existing = [
					'id'				=>		NULL,
					'description'		=>		"",
					'password'			=>		"",
					'disabled'			=>		false,
					'types'				=>		[

					],
					'playlists'			=>		[

					]
				];
			}

			// Populate
			if ( ! empty($import) ) {
				if ( array_key_exists('id', $import) )				$existing['id']				=	empty($import['id'])			?	""		: $import['id'];
				if ( array_key_exists('description', $import) )		$existing['description']	=	empty($import['description'])	?	""		: $import['description'];
				if ( array_key_exists('password', $import) )		$existing['password']		=	empty($import['password'])		?	""		: $import['password'];
				if ( array_key_exists('disabled', $import) )		$existing['disabled']		=	empty($import['disabled'])		?	FALSE	: $import['disabled'] > 0;
			}

			// Return
			return $existing;

		}


		/**
		 * Gets Current Slaves
		 */
		public function get_slaves() {

			// Holds Return
			$return = [];

			// Grab Settings
			$q = $this->db->query(" SELECT * FROM `music_slave` ")->result_array();

			// Loop Settings
			foreach ( $q as $slave ) {
				$return[$slave['id']] 	= 	$this->slave_model($slave);
			}

			// Return
			return $return;

		}


		/**
		 * Gets a Slave from the Database
		 */
		public function get_slave( $id, $get_types, $get_lists ) {
			
			// Grab Slave
			$q = $this->db->query(" SELECT * FROM `music_slave` WHERE `id` = ? ", [$id])->result_array();
			if ( empty($q) )	return NULL;

			// Build Slave
			$m = $this->slave_model($q[0]);

			// Get Types
			if ( $get_types ) {
				$t = $this->db->query(" SELECT * FROM `music_type_dist` WHERE `slave` = ? ", [$id])->result_array();
				foreach ( $t as $type ) {
					$m['types'][$type['type']] = TRUE;
				}
			}

			// Get Types
			if ( $get_types ) {
				$t = $this->db->query(" SELECT * FROM `music_pl_dist` WHERE `slave` = ? ", [$id])->result_array();
				foreach ( $t as $type ) {
					$m['playlists'][$type['playlist']] = TRUE;
				}
			}

			// Return
			return $m;

		}


		/**
		 * Save or update a Slave
		 */
		public function save_slave( $slave ) {

			// Required
			if ( ! isset($slave['description']) 	|| empty($slave['description']) )		return false;
			if ( ! isset($slave['password']) 		|| empty($slave['password']) )			return false;
			
			// Booleans
			$slave['disabled'] 		= 	empty($slave['disabled']) ? 0 : 1;

			// Start Transaction
			$this->db->trans_start();

			// Create or Save
			if ( empty($slave['id']) ) {

				// Inject
				$this->db->query("	INSERT INTO `music_slave`
										( `description`, `password`, `disabled` )
									VALUES
										( ?, ?, ? ) 
								", 
								[
									$slave['description'],
									$slave['password'],
									$slave['disabled']
								]);

				// Return ID
				$slave['id'] = $this->db->insert_id();
				if ( empty($slave['id']) ) {
					$this->db->trans_rollback();
					return false;
				}	

			}
			else {

				// Update Slave
				$this->db->query(" 	UPDATE `music_slave`
									SET
										`description` 	= 	?,
										`password`		=	?,
										`disabled`		=	?
									WHERE
										`id`			=	?
								",
									[
										$slave['description'],
										$slave['password'],
										$slave['disabled'],
										$slave['id']
									]
								);

			}

			// Insert Types
			$this->db->query("	DELETE FROM `music_type_dist` WHERE `slave` = ? ", $slave['id'] );
			if ( ! empty($slave['types']) ) {
				foreach ( $slave['types'] as $k=> $v ) {
					$this->db->query("	INSERT INTO `music_type_dist` (`type`, `slave`, `player`) VALUES (?, ?, ?) ", [ $k, $slave['id'], NULL ] );
				}
			}

			// Insert Playlists
			$this->db->query("	DELETE FROM `music_pl_dist` WHERE `slave` = ? ", $slave['id'] );
			if ( ! empty($slave['playlists']) ) {
				foreach ( $slave['playlists'] as $k=> $v ) {
					$this->db->query("	INSERT INTO `music_pl_dist` (`playlist`, `slave`, `player`) VALUES (?, ?, ?) ", [ $k, $slave['id'], NULL ] );
				}
			}

			// Done
			$this->db->trans_complete();
			return $slave['id'];

		}


		/**
		 * Deletes a Slave
		 */
		public function delete_slave( $id ) {
			
			// Start Transaction
			$this->db->trans_start();
			
			// Delete
			$this->db->query("	DELETE FROM `music_type_dist` 	WHERE `slave`	= ? ", [ $id ]);
			$this->db->query("	DELETE FROM `music_pl_dist` 	WHERE `slave`	= ? ", [ $id ]);
			$this->db->query("	DELETE FROM `music_slave` 		WHERE `id`		= ? ", [ $id ]);

			// Return
			$this->db->trans_complete();
			return true;

		}


		/**
		 * Authenticates a Slave
		 */
		public function authenticate_slave( $id , $password ) {

			// Grab Slave
			$q = $this->db->query(" SELECT * FROM `music_slave` WHERE `id` = ? AND `password` = ? AND `disabled` = 0 ", [$id, $password])->result_array();
			if ( empty($q) )	return NULL;

			// Build Slave
			$m = $this->slave_model($q[0]);

			// Get Types
			$t = $this->db->query(" SELECT * FROM `music_type_dist` WHERE `slave` = ? ", [$id])->result_array();
			foreach ( $t as $type ) {
				$m['types'][$type['type']] = TRUE;
			}
			
			// Get Playlists
			$t = $this->db->query(" SELECT * FROM `music_pl_dist` WHERE `slave` = ? ", [$id])->result_array();
			foreach ( $t as $list ) {
				$m['playlists'][$list['playlist']] = TRUE;
			}

			// Return
			return $m;

		}


		/**
		 * Gets a Players Distribution
		 *
		 * @param [type] $id
		 * @return void
		 */
		public function authenticate_player( $id ) {

			// Holds Return
			$m = [
				'types'		=>	[],
				'playlists'	=>	[]
			];

			// Get Types
			$t = $this->db->query(" SELECT * FROM `music_type_dist` WHERE `player` = ? ", [$id])->result_array();
			foreach ( $t as $type ) {
				$m['types'][$type['type']] = TRUE;
			}
			
			// Get Playlists
			$t = $this->db->query(" SELECT * FROM `music_pl_dist` WHERE `player` = ? ", [$id])->result_array();
			foreach ( $t as $list ) {
				$m['playlists'][$list['playlist']] = TRUE;
			}

			// Return
			return $m;

		}



		/** ***************************************************************************************
		 * 	LIBRARY GROUP FUNCTIONS
		 *  ***************************************************************************************
		 */

		 /**
		  * Defines a Library Group
		  *
		  * @param array $existing	The Existing Model (Or Null for New)
		  * @param array $import	The Data to Import
		  * @return array			The Model
		  */
		 public function library_group_model( $existing = NULL, $import = NULL ) {

			// Create File
			if ( empty($existing) || ! is_array($existing) ) {
				$existing = [
					'id'				=>		NULL,
					'description'		=>		"",
					'access'			=>		[

					],
					'playlists'			=>		[

					]
				];
			}

			// Populate
			if ( ! empty($import) ) {
				if ( array_key_exists('id', $import) )				$existing['id']				=	empty($import['id'])			?	""		: $import['id'];
				if ( array_key_exists('description', $import) )		$existing['description']	=	empty($import['description'])	?	""		: $import['description'];
			}

			// Return
			return $existing;

		}


		/**
		 * Gets Library Groups
		 *
		 * @return void	Array of Models
		 */
		public function get_library_groups( $get_perms, $get_lists ) {

			// Holds Return
			$return = [];

			// Grab Settings
			$q = $this->db->query(" SELECT * FROM `music_pl_group` ORDER BY `description` ")->result_array();

			// Loop Settings
			foreach ( $q as $group ) {
				$return[$group['id']] 	= 	$this->library_group_model($group);
				if ( $get_perms )	$return[$group['id']]['access'] 	= $this->get_library_group_perms($group['id']);
				if ( $get_lists )	$return[$group['id']]['playlists'] 	= $this->get_library_group_lists($group['id']);
			}

			// Return
			return $return;

		}


		/**
		 * Gets a Library Group
		 *
		 * @param int $id	The ID to Get
		 * @param bool $get_perms	Populate Perms
		 * @return array	An Group Model
		 */
		public function get_library_group( $id, $get_perms, $get_lists ) {
			
			// Grab Slave
			$q = $this->db->query(" SELECT * FROM `music_pl_group` WHERE `id` = ? ", [$id])->result_array();
			if ( empty($q) )	return NULL;

			// Build Slave
			$m = $this->library_group_model(NULL, $q[0]);
			if ( $get_perms )	$m['access'] = $this->get_library_group_perms($id);
			if ( $get_lists )	$m['playlists'] = $this->get_library_group_lists($id);

			// Return
			return $m;

		}


		/**
		 * Gets Perms for a Library Group
		 *
		 * @param [type] $id
		 * @return void
		 */
		public function get_library_group_lists( $id ) {

			// Hold Return
			$return = [];

			// Grab Perms
			$t = $this->db->query("	SELECT p.*, g.`description` FROM `music_pl` p LEFT JOIN `music_pl_master_group` g ON g.`id` = p.`masterplgroup` WHERE p.`plgroup` = ? ", [$id])->result_array();
			foreach ( $t as $list ) {
				$return[$list['id']] = $this->library_playlist_model(NULL, $list);
			}

			// Return
			return $return;

		}


		/**
		 * Gets Lists for a Library Group
		 *
		 * @param [type] $id
		 * @return void
		 */
		public function get_library_group_perms( $id ) {

			// Hold Return
			$return = [];

			// Get Results
			$t = $this->db->query(" SELECT p.*, u.`name` FROM `music_pl_group_perm` p LEFT JOIN `user` u ON u.`username` = p.`username` WHERE `groupid` = ? ", [$id])->result_array();
			foreach ( $t as $type ) {
				$return[$type['username']] 				= 	[];
				$return[$type['username']]['name']		=	$type['name'];
				$return[$type['username']]['view']		=	($type['rights'] & 1) > 0;
				$return[$type['username']]['edit']		=	($type['rights'] & 2) > 0;
				$return[$type['username']]['create']	=	($type['rights'] & 4) > 0;
				$return[$type['username']]['delete']	=	($type['rights'] & 8) > 0;
			}

			// Return
			return $return;

		}


		/**
		 * Save of Update a Library Group
		 *
		 * @param [type] $model
		 * @return void
		 */
		public function save_library_group( $model ) {

			// Required
			if ( ! isset($model['description']) 	|| empty($model['description']) )		return false;
			
			// Start Transaction
			$this->db->trans_start();

			// Create or Save
			if ( empty($model['id']) ) {

				// Inject
				$this->db->query("	INSERT INTO `music_pl_group`
										( `id`, `description` )
									VALUES
										( NULL, ? ) 
								", 
								[
									$model['description'],
								]);

				// Return ID
				$model['id'] = $this->db->insert_id();
				if ( empty($model['id']) ) {
					$this->db->trans_rollback();
					return false;
				}	

			}
			else {

				// Update Slave
				$this->db->query(" 	UPDATE `music_pl_group`
									SET
										`description` 	= 	?
									WHERE
										`id`			=	?
								",
									[
										$model['description'],
										$model['id']
									]
								);

			}

			// Delete Current Types
			$this->db->query("	DELETE FROM `music_pl_group_perm` WHERE `groupid` = ? ", $model['id'] );

			// Insert Types
			if ( ! empty($model['access']) ) {
				foreach ( $model['access'] as $k=> $v ) {

					// Build Score
					$score = 0;
					if ( ! empty($v['view']) )		$score += 1;
					if ( ! empty($v['edit']) )		$score += 2;
					if ( ! empty($v['create']) )	$score += 4;
					if ( ! empty($v['delete']) )	$score += 8;

					// Add Perm
					$this->db->query("	INSERT INTO `music_pl_group_perm` (`groupid`, `username`, `rights`) VALUES (?, ?, ?) ", [ $model['id'], $k, $score ] );

				}
			}

			// Done
			$this->db->trans_complete();
			return $model['id'];

		}


		/**
		 * Deletes a Library Group
		 *
		 * @param [type] $id
		 * @return void
		 */
		public function delete_library_group( $id ) {
			
			// Start Transaction
			$this->db->trans_start();
			
			// Delete
			$this->db->query("	DELETE FROM `music_pl_group_perm` 	WHERE `groupid`	= ? ", [ $id ]);
			$this->db->query("	DELETE FROM `music_pl_group` 		WHERE `id`		= ? ", [ $id ]);

			// Return
			$this->db->trans_complete();
			return true;

		}



		/**
		 * Save of Update a Library Group
		 *
		 * @param array $groups
		 * @return void
		 */
		public function save_library_master_groups( $groups ) {

			// Start Transaction
			$this->db->trans_start();

			// Clear Groups
			$this->db->query(" DELETE FROM `music_pl_master_group` ");

			// Loop Groups
			foreach ( $groups as $k => $v ) {

				// Inject
				$this->db->query("	INSERT INTO `music_pl_master_group`
										( `id`, `description` )
									VALUES
										( ?, ? ) 
								", 
								[
									$k,
									$v
								]);

			}

			// Done
			$this->db->trans_complete();
			return true;

		}





		/** ***************************************************************************************
		 * 	LIBRARY FUNCTIONS
		 *  ***************************************************************************************
		 */


		/**
		 * Gets a File
		 */
		public function library_file_model( $existing, $import ) {
			
			// Create File
			if ( empty($existing) || ! is_array($existing) ) {
				$existing = [
					'gid'				=>		NULL,
					'filename'			=>		"",
					'artist'			=>		"",
					'title'				=>		"",
					'type'				=>		"",
					'year'				=>		"",
					'duration'			=>		"",
					'controversial'		=>		"",
					'categories'		=>		"",
					'disabled'			=>		false,
					'ldisabled'			=>		false,
					'added'				=>		null,
					'updated'			=>		null,
					'checksum'			=>		"",
					'size'				=>		0
				];
			}

			// Populate
			if ( ! empty($import) && is_array($import) ) {
				if ( array_key_exists('gid', $import) ) 				$existing['gid']			=	empty($import['gid'])			?	""		: $import['gid'];
				if ( array_key_exists('filename', $import) )			$existing['filename']		=	empty($import['filename'])		?	""		: $import['filename'];
				if ( array_key_exists('artist', $import) ) 				$existing['artist']			=	empty($import['artist'])		?	""		: $import['artist'];
				if ( array_key_exists('title', $import) ) 				$existing['title']			=	empty($import['title'])			?	""		: $import['title'];
				if ( array_key_exists('type', $import) ) 				$existing['type']			=	empty($import['type'])			?	NULL	: $import['type'];
				if ( array_key_exists('year', $import) ) 				$existing['year']			=	empty($import['year'])			?	NULL	: $import['year'];
				if ( array_key_exists('duration', $import) )			$existing['duration']		=	empty($import['duration'])		?	0		: $import['duration'];
				if ( array_key_exists('controversial', $import) )		$existing['controversial']	=	empty($import['controversial'])	?	FALSE	: $import['controversial'] > 0;
				if ( array_key_exists('categories', $import) )			$existing['categories']		=	empty($import['categories'])	?	""		: $import['categories'];
				if ( array_key_exists('disabled', $import) )			$existing['disabled']		=	empty($import['disabled'])		?	FALSE	: $import['disabled'] > 0;
				if ( array_key_exists('ldisabled', $import) )			$existing['ldisabled']		=	empty($import['ldisabled'])		?	FALSE	: $import['ldisabled'] > 0;
				if ( array_key_exists('added', $import) ) 				$existing['added']			=	empty($import['added'])			?	NULL	: @strtotime($import['added']);
				if ( array_key_exists('updated', $import) )				$existing['updated']		=	empty($import['updated'])		?	NULL	: @strtotime($import['updated']);
				if ( array_key_exists('checksum', $import) )			$existing['checksum']		=	empty($import['checksum'])		?	""		: $import['checksum'];
				if ( array_key_exists('size', $import) )				$existing['size']			=	empty($import['size'])			?	0		: @intval($import['size']);
			}
			elseif( ! empty($import) && is_object($import) ) {
				$existing['gid']			=	empty($import['gid'])			?	""		: (string)$import['gid'];
				$existing['filename']		=	empty($import['filename'])		?	""		: (string)$import['filename'];
				$existing['artist']			=	empty($import['artist'])		?	""		: (string)$import['artist'];
				$existing['title']			=	empty($import['title'])			?	""		: (string)$import['title'];
				$existing['type']			=	empty($import['type'])			?	NULL	: (string)$import['type'];
				$existing['year']			=	empty($import['year'])			?	NULL	: (string)$import['year'];
				$existing['duration']		=	empty($import['duration'])		?	0		: (string)$import['duration'];
				$existing['controversial']	=	empty($import['controversial'])	?	FALSE	: (string)$import['controversial'] > 0;
				$existing['categories']		=	empty($import['categories'])	?	""		: (string)$import['categories'];
				$existing['disabled']		=	empty($import['disabled'])		?	FALSE	: (string)$import['disabled'] > 0;
				$existing['ldisabled']		=	empty($import['ldisabled'])		?	FALSE	: (string)$import['ldisabled'] > 0;
				$existing['added']			=	empty($import['added'])			?	NULL	: @strtotime((string)$import['added']);
				$existing['updated']		=	empty($import['updated'])		?	NULL	: @strtotime((string)$import['updated']);
				$existing['checksum']		=	empty($import['checksum'])		?	""		: (string)$import['checksum'];
				$existing['size']			=	empty($import['size'])			?	0		: @intval((string)$import['size']);
			}

			// Return
			return $existing;

		}


		/**
		 * Updates the Library Version
		 */
		public function update_library_version( $date = NULL, $local = FALSE ) {
			if ( empty($date) ) {
				if ( $local )
					$this->mod_music->update_library_setting( 'LVERSION', gmdate("Y-m-d\TH:i:sO") );
				else {
					$this->mod_music->update_library_setting( 'VERSION', gmdate("Y-m-d\TH:i:sO") );
					$this->mod_music->update_library_setting( 'LVERSION', gmdate("Y-m-d\TH:i:sO") );
				}
				return true;
			}
			else {
				if ( $local )
					$this->mod_music->update_library_setting( 'LVERSION', $date );
				else {
					$this->mod_music->update_library_setting( 'VERSION', $date );
					$this->mod_music->update_library_setting( 'LVERSION', $date );
				}
				return true;
			}
		}


		/**
		 * Gets Valid Library Types
		 */
		public function get_library_types() {
			
			// Load Music Types
			if ( $this->music_types === null ) {

				// Define Array
				$this->music_types = [];

				// Load Types
				$q	=	$this->db->query("	SELECT * FROM `music_type` ORDER BY `id` ")->result_array();
				foreach ( $q as $ttype ) {
					$this->music_types[$ttype['id']] = $ttype['description'];
				}

			}

			// Return
			return $this->music_types;

		}


		/**
		 * Gets All Library Types
		 *
		 * @return void
		 */
		public function get_all_library_types() {

			// Do Types
			$t = [];

			// Load Types
			$q	=	$this->db->query("	SELECT * FROM `music_type` ORDER BY `id` ")->result_array();
			foreach ( $q as $ttype ) {
				$t[$ttype['id']] = $ttype['description'];
			}

			// Add Types
			if ( empty($t[1]) )		$t[1]	=	"Audio";
			if ( empty($t[2]) )		$t[2]	=	"Video";
			if ( empty($t[3]) )		$t[3]	=	"Karaoke";
			if ( empty($t[4]) )		$t[4]	=	"Game";
			if ( empty($t[5]) )		$t[5]	=	"Custom";

			// Return
			ksort($t);
			return $t;

		}




		/** ***************************************************************************************
		 * 	LIBRARY PLAYLIST FUNCTIONS
		 *  ***************************************************************************************
		 */


		/**
		 * Builds a Playlist Model from a Array or Object
		 *
		 * @param [type] $existing
		 * @param [type] $import
		 * @return void
		 */
		public function library_playlist_model( $existing, $import ) {
		
			// Create File
			if ( empty($existing) || ! is_array($existing) ) {
				$existing = [
					'id'				=>		NULL,
					'gid'				=>		NULL,
					'name'				=>		"",
					'shuffle'			=>		true,
					'plgroup'			=>		0,
					'plgroupdesc'		=>		'',
					'masterplgroup'		=>		[
						'id'				=>		NULL,
						'description'		=>		NULL
					],
					'selectable'		=>		true,
					'encrypted'			=>		false,
					'disableadverts'	=>		false,
					'adverttracks'		=>		false,
					'adverttime'		=>		false,
					'notes'				=>		"",
					'slaves'			=>		[

					],
					'players'			=>		[

					],
					'tracks'			=>		[

					]	
				];
			}

			// Populate
			if ( ! empty($import) && is_array($import) ) {
				if ( array_key_exists('id', $import) ) 					$existing['id']								=	empty($import['id'])				?	NULL		: $import['id'];
				if ( array_key_exists('gid', $import) ) 				$existing['gid']							=	empty($import['gid'])				?	NULL		: $import['gid'];
				if ( array_key_exists('name', $import) )				$existing['name']							=	empty($import['name'])				?	""			: $import['name'];
				if ( array_key_exists('shuffle', $import) )				$existing['shuffle']						=	empty($import['shuffle'])			?	FALSE		: $import['shuffle'] > 0;
				if ( array_key_exists('plgroup', $import) )				$existing['plgroup']						=	empty($import['plgroup'])			?	NULL		: $import['plgroup'];
				if ( array_key_exists('plgroupdesc', $import) )			$existing['plgroupdesc']					=	empty($import['plgroupdesc'])		?	NULL		: $import['plgroupdesc'];
				if ( array_key_exists('masterplgroup', $import) )		$existing['masterplgroup']['id']			=	empty($import['masterplgroup'])		?	NULL		: $import['masterplgroup'];
				if ( array_key_exists('description', $import) )			$existing['masterplgroup']['description']	=	empty($import['description'])		?	NULL		: $import['description'];
				if ( array_key_exists('encrypted', $import) )			$existing['encrypted']						=	empty($import['encrypted'])			?	FALSE		: $import['encrypted'] > 0;
				if ( array_key_exists('disableadverts', $import) )		$existing['disableadverts']					=	empty($import['disableadverts'])	?	FALSE		: $import['disableadverts'] > 0;
				if ( array_key_exists('selectable', $import) )			$existing['selectable']						=	empty($import['selectable'])		?	FALSE		: $import['selectable'] > 0;
				if ( array_key_exists('adverttracks', $import) )		$existing['adverttracks']					=	empty($import['adverttracks'])		?	NULL		: $import['adverttracks'];
				if ( array_key_exists('adverttime', $import) )			$existing['adverttime']						=	empty($import['adverttime'])		?	NULL		: $import['adverttime'];
				if ( array_key_exists('notes', $import) )				$existing['notes']							=	empty($import['notes'])				?	""			: $import['notes'];
			}
			elseif( ! empty($import) && is_object($import) ) {
				$existing['id']						=	empty($import['id'])				?	NULL	: 	(string)$import['id'];
				$existing['gid']					=	empty($import['gid'])				?	NULL	: 	(string)$import['gid'];
				$existing['masterplgroup']['id']	=	empty($import['gpg'])				?	NULL	: 	(string)$import['gpg'];
				$existing['name']					=	empty($import['name'])				?	""		: 	(string)$import['name'];
				$existing['shuffle']				=	empty($import['shuffle'])			?	FALSE	: 	(string)$import['shuffle'] > 0;
				$existing['plgroup']				=	empty($import['plgroup'])			?	NULL	: 	(string)$import['plgroup'];
				$existing['encrypted']				=	empty($import['encrypted'])			?	FALSE	: 	(string)$import['encrypted'] > 0;
				$existing['selectable']				=	empty($import['selectable'])		?	FALSE	: 	(string)$import['selectable'] > 0;
				$existing['disableadverts']			=	empty($import['disableadverts'])	?	FALSE	: 	(string)$import['disableadverts'] > 0;
				$existing['adverttracks']			=	empty($import['adverttracks'])		?	0		: 	(string)$import['adverttracks'] > 0;
				$existing['adverttime']				=	empty($import['adverttime'])		?	0		: 	(string)$import['adverttime'];
				$existing['notes']					=	empty($import['notes'])				?	""		: 	(string)$import['notes'];
			}

			// Return
			return $existing;

		}


		/**
		 * Gets a Library Playlist
		 *
		 * @param boolean $inc_lists
		 * @param boolean $inc_group
		 * @return void
		 */
		public function get_library_playlist( $id, $inc_list = true ) {

			// Grab Playlist
			if ( empty($id) )	return FALSE;
			$q = $this->db->query(" SELECT p.*, g.`description` FROM `music_pl` p LEFT JOIN `music_pl_master_group` g ON g.`id` = p.`masterplgroup` WHERE p.`id` = ? ", [$id])->result_array();
			if ( empty($q) )	return FALSE;

			// Build Model
			$tModel		= 	$this->library_playlist_model( NULL, $q[0] );

			// Do Distributions
			$t = $this->db->query(" SELECT * FROM `music_pl_dist` WHERE `playlist` = ? ", [ $id ])->result_array();
			foreach ( $t as $dist ) {
				if ( ! empty($dist['slave']) )		$tModel['slaves'][$dist['slave']] 		= TRUE;
				if ( ! empty($dist['player']) )		$tModel['players'][$dist['player']] 	= TRUE;
			}

			// Loop Tracks
			if ( $inc_list ) {

				// Do Playlist
				$contents	=	$this->db->query("	SELECT 		c.`item`, t.`gid`, t.`artist`, t.`title`, t.`type`, t.`year`, t.`controversial`, t.`duration`, t.`disabled`, t.`ldisabled`
													FROM		`music_pl_contents` c LEFT JOIN `music_track` t ON t.`gid` = c.`track`
													WHERE		c.`playlist` = ?
													ORDER BY	c.`item`
												",	[
														$id
													]
												)->result_array();


				// Get Tracks
				foreach ( $contents as $track ) {
					$tModel['tracks'][$track['item']]	=	$track;
				}

			}

			// Return
			return $tModel;

		}


		/**
		 * Gets the Contents for a Playlist
		 *
		 * @param [type] $id
		 * @return void
		 */
		public function get_library_playlist_data( $id ) {

			// Hold Return
			$return = [ 'data' => [], 'tracks' => [] ];

			// Grab Core Data
			$q = $this->db->query(" SELECT * FROM `music_pl` WHERE `id` = ?", [$id])->result_array();
			if ( empty($q) )	NULL;

			// Set Core
			$return['data'] = $q[0];

			// Do Playlist
			$contents	=	$this->db->query("	SELECT 			`item`, `track`
												FROM			`music_pl_contents`
												WHERE			`playlist` = ?
											",	[
													$id
												]
											)->result_array();

			// Get Tracks
			foreach ( $contents as $track ) {
				$return['tracks'][$track['item']]	=	$track['track'];
			}

			// Return
			return $return;

		}


		/**
		 * Deletes a Library Playlist
		 *
		 * @param [type] $id
		 * @return void
		 */
		public function delete_library_playlist( $id ) {

			// Perform Deletion
			$this->db->query("	DELETE FROM `music_pl_contents` WHERE `playlist` = ? ", [ $id ]);
			$this->db->query("	DELETE FROM `music_pl_dist` WHERE `playlist` = ? ", [ $id ]);
			$this->db->query("	DELETE FROM `music_pl` WHERE `id` = ? ", [ $id ]);
			return true;

		}


		/**
		 * Save or update a Playlist
		 */
		public function save_library_playlist( $model ) {

			// Required
			if ( ! isset($model['name']) 			|| empty($model['name']) )		return false;

			// Booleans
			$model['shuffle'] 			= 	empty($model['shuffle']) ? 0 : 1;
			$model['selectable'] 		= 	empty($model['selectable']) ? 0 : 1;
			$model['encrypted'] 		= 	empty($model['encrypted']) ? 0 : 1;
			$model['disableadverts']	= 	empty($model['disableadverts']) ? 0 : 1;

			// Nullables
			if ( empty($model['gid']) )				$model['gid'] = NULL;
			if ( empty($model['plgroup']) )			$model['plgroup'] = NULL;
			if ( empty($model['adverttracks']) )	$model['adverttracks'] = NULL;
			if ( empty($model['adverttime']) )		$model['adverttime'] = NULL;
			if ( empty($model['plgroup']) )			$model['plgroup'] = NULL;

			// Start Transaction
			$this->db->trans_start();

			// Create or Save
			if ( empty($model['id']) ) {

				// Inject
				$this->db->query("	INSERT INTO `music_pl`
										( `id`, `gid`, `name`, `shuffle`, `plgroup`, `masterplgroup`, `selectable`, `encrypted`, `disableadverts`, `adverttracks`, `adverttime`, `notes` )
									VALUES
										( NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) 
								", 
								[
									$model['gid'],
									$model['name'],
									$model['shuffle'],
									$model['plgroup'],
									$model['masterplgroup']['id'],
									$model['selectable'],
									$model['encrypted'],
									$model['disableadverts'],
									$model['adverttracks'],
									$model['adverttime'],
									$model['notes']
								]);

				// Return ID
				$model['id'] = $this->db->insert_id();
				if ( empty($model['id']) ) {
					$this->db->trans_rollback();
					return false;
				}	

			}
			else {

				// Update Slave
				$this->db->query(" 	UPDATE `music_pl`
									SET
										`name` 				= 	?,
										`gid` 				= 	?,
										`shuffle`			=	?,
										`plgroup`			=	?,
										`masterplgroup`		=	?,
										`selectable`		=	?,
										`encrypted`			=	?,
										`disableadverts`	=	?,
										`adverttracks`		=	?,
										`adverttime`		=	?,
										`notes`				=	?
									WHERE
										`id`				=	?
								",
									[
										$model['name'],
										$model['gid'],
										$model['shuffle'],
										$model['plgroup'],
										$model['masterplgroup']['id'],
										$model['selectable'],
										$model['encrypted'],
										$model['disableadverts'],
										$model['adverttracks'],
										$model['adverttime'],
										$model['notes'],
										$model['id']
									]
								);

			}

			// Delete Current Distribution
			 $this->db->query("	DELETE FROM `music_pl_dist` WHERE `playlist` = ? ", $model['id'] );
			
			// Insert Slaves
			if ( ! empty($model['slaves']) ) {
				foreach ( $model['slaves'] as $k=> $v ) {
					$this->db->query("	INSERT INTO `music_pl_dist` (`playlist`, `slave`, `player`) VALUES (?, ?, ?) ", [ $model['id'], $k, NULL ] );
				}
			}

			// Insert Players
			if ( ! empty($model['players']) ) {
				foreach ( $model['players'] as $k=> $v ) {
					$this->db->query("	INSERT INTO `music_pl_dist` (`playlist`, `slave`, `player`) VALUES (?, ?, ?) ", [ $model['id'], NULL, $k ] );
				}
			}

			// Done
			$this->db->trans_complete();
			return $model['id'];

		}


		/**
		 * Gets the Music Distribution for a Player
		 *
		 * @return void
		 */
		public function get_player_distribution( $id ) {

			// Holds Return
			$r = [
				'types'	=>	[],
				'lists'	=>	[]
			];

			// Get Playlists
			$q 	=	$this->db->query("	SELECT * FROM `music_pl_dist` WHERE `player` = ? ", [$id] )->result_array();
			foreach ( $q as $result ) {
				$r['lists'][$result['playlist']]	=	true;
			}

			// Get Types
			$q 	=	$this->db->query("	SELECT * FROM `music_type_dist` WHERE `player` = ? ", [$id] )->result_array();
			foreach ( $q as $result ) {
				$r['types'][$result['type']]	=	true;
			}

			// Return
			return $r;

		}


		/**
		 * Gets the Music Distribution for a Player
		 *
		 * @return void
		 */
		public function update_player_distribution( $id, $dist ) {

			// Start Transaction
			$this->db->trans_start();

			// Delete Current
			$this->db->query("	DELETE FROM `music_pl_dist` WHERE `player` = ? ", [$id]);
			$this->db->query("	DELETE FROM `music_type_dist` WHERE `player` = ? ", [$id]);

			// Inject Types
			foreach ($dist['types'] as $key => $value) {
				$this->db->query(" INSERT INTO `music_type_dist` (`type`, `slave`, `player`) VALUES (?, NULL, ?) ", [ $key, $id ]);
			}

			// Inject Playlists
			foreach ($dist['lists'] as $key => $value) {
				$this->db->query(" INSERT INTO `music_pl_dist` (`playlist`, `slave`, `player`) VALUES (?, NULL, ?) ", [ $key, $id ]);
			}

			// Done
			$this->db->trans_complete();
			return true;

		}


		/**
		 * Save or update a Tracklist
		 */
		public function save_library_playlist_tracklist( $model ) {

			// Start Transaction
			$this->db->trans_start();

			// Delete Track List
			$this->db->query("	DELETE FROM `music_pl_contents` WHERE `playlist` = ? ", [ $model['id'] ] );
			if ( ! empty($model['tracks']) ) {
				foreach ( $model['tracks'] as $k=> $v ) {
					$this->db->query("	INSERT INTO `music_pl_contents` (`playlist`, `item`, `track`) VALUES (?, ?, ?) ", [ $model['id'], $k, $v['gid'] ] );
				}
			}

			// Done
			$this->db->trans_complete();
			return true;

		}


		/**
		 * Gets Global Playlists
		 *
		 * @param boolean $get_dist
		 * @return void
		 */
		public function get_global_playlists( $get_dist = false ) {

			// Holds Return
			$return = [];

			// Grab Playlists
			$q = $this->db->query(" SELECT p.*, g.`description` FROM `music_pl` p LEFT JOIN `music_pl_master_group` g ON g.`id` = p.`masterplgroup` WHERE p.`gid` IS NOT NULL ORDER BY g.`description`, p.`name` ")->result_array();

			// Loop Lists
			foreach ( $q as $playlist ) {

				// Build Playlist
				$tPl = $this->library_playlist_model( NULL, $playlist );

				// Do Distributions
				$t = $this->db->query(" SELECT * FROM `music_pl_dist` WHERE `playlist` = ? ", [ $tPl['id'] ])->result_array();
				foreach ( $t as $dist ) {
					if ( ! empty($dist['slave']) )		$tPl['slaves'][$dist['slave']] 		= TRUE;
					if ( ! empty($dist['player']) )		$tPl['players'][$dist['player']] 	= TRUE;
				}

				// Add
				$return[$tPl['gid']]		=	$tPl;

			}

			// Return
			return $return;

		}


		/**
		 * Gets Local Playlists
		 *
		 * @param boolean $get_dist
		 * @return void
		 */
		public function get_local_playlists( $get_dist = false ) {
			
			// Holds Return
			$return = [];

			// Grab Playlists
			$q = $this->db->query(" SELECT p.*, g.`description` FROM `music_pl` p LEFT JOIN `music_pl_master_group` g ON g.`id` = p.`masterplgroup` WHERE p.`gid` IS NULL ")->result_array();

			// Loop Lists
			foreach ( $q as $playlist ) {

				// Build Playlist
				$tPl = $this->library_playlist_model( NULL, $playlist );

				// Do Distributions
				$t = $this->db->query(" SELECT * FROM `music_pl_dist` WHERE `playlist` = ? ", [ $tPl['id'] ])->result_array();
				foreach ( $t as $dist ) {
					if ( ! empty($dist['slave']) )		$tPl['slaves'][$dist['slave']] 		= TRUE;
					if ( ! empty($dist['player']) )		$tPl['players'][$dist['player']] 	= TRUE;
				}

				// Add
				$return[$tPl['id']]		=	$tPl;

			}

			// Return
			return $return;
		}



		/**
		 * Gets Library Playlists
		 *
		 * @param boolean $get_dist
		 * @return void
		 */
		public function get_library_playlists() {
			
			// Holds Return
			$return = [];

			// Grab Playlists
			$q = $this->db->query(" SELECT p.*, l.`description` 'plgroupdesc', g.`description` FROM `music_pl` p LEFT JOIN `music_pl_master_group` g ON g.`id` = p.`masterplgroup` LEFT JOIN `music_pl_group` l ON l.`id` = p.`plgroup` ")->result_array();

			// Loop Lists
			foreach ( $q as $playlist ) {

				// Build Playlist
				$tPl = $this->library_playlist_model( NULL, $playlist );

				// Add
				$return[$tPl['id']]		=	$tPl;

			}

			// Return
			return $return;
		}



		/** ***************************************************************************************
		 * 	TYPE FUNCTIONS
		 *  ***************************************************************************************
		 */


		/**
		 * Defines a Type Model	
		 *
		 * @param array $existing	The Existing Model
		 * @param array $import	The Data to Import
		 * @return array
		 */
		public function type_model( $existing = NULL, $import = NULL ) {

			// Create File
			if ( empty($existing) || ! is_array($existing) ) {
				$existing = [
					'id'				=>		NULL,
					'description'		=>		"",
					'slaves'			=>		[

					],
					'players'			=>		[

					]
				];
			}

			// Populate
			if ( ! empty($import) ) {
				if ( array_key_exists('id', $import) )				$existing['id']				=	empty($import['id'])			?	""		: $import['id'];
				if ( array_key_exists('description', $import) )		$existing['description']	=	empty($import['description'])	?	""		: $import['description'];
			}

			// Return
			return $existing;

		}


		/**
		 * Gets a Library Type
		 *
		 * @param int 		$id			The Library ID
		 * @param boolean 	$extended	Get Extended Data
		 * @return array
		 */
		public function get_library_type( $id, $extended = false ) {

			// Grab Type
			$q = $this->db->query("	SELECT * FROM `music_type` WHERE `id` = ?", [ $id ])->result_array();
			if ( empty($q) )	return [];

			// Return Type(s) There should be only one!
			foreach ( $q as $type ) {

				// Expand Model
				$type = $this->type_model( NULL, $type );

				// Populate
				if ( $extended ) {
					$r = $this->db->query(" SELECT * FROM `music_type_dist` WHERE `type` = ? ", $id )->result_array();
					foreach ( $r as $dist ) {
						if ( ! empty($dist['slave']) )		$type['slaves'][$dist['slave']] 	= TRUE;
						if ( ! empty($dist['player']) )		$type['players'][$dist['player']] 	= TRUE;
					}
				}

				// Return
				return $type;

			}

			// Nothing Returned
			return [];

		}


		/**
		 * Finds a Matching Library Type
		 */
		public function find_library_type( $input = NULL ) {
			
			// Load Music Types
			if ( $this->music_types === null )	$this->get_library_types();

			// Search
			if ( $input === NULL ) {
				return $this->music_types;
			}
			else {

				// Work Out
				if ( is_numeric($input) && isset($this->music_types[$input]) ) 	return $this->music_types[$input];
				if ( empty($input) )											return "Unknown";

				// Make Case Insensitive
				foreach ( $this->music_types as $k => $v ) {
					if ( mb_strtoupper($input) == mb_strtoupper($v) ) {
						return $k;
					}
				}
			
				// Not Found
				return 0;

			}

		}


		/**
		 * Creates a Library Type
		 */
		public function create_library_type( $model ) {
			
			// Create
			$this->db->trans_start();
			$this->db->query("	INSERT INTO `music_type` (`id`, `description`) VALUES ( NULL, ?) ", [ $model['description'] ]);
			$model['id'] = $this->db->insert_id();
			$this->update_library_type_dist( $model, FALSE );
			$this->db->trans_complete();
			return $model['id'];

		}


		/**
		 * Updates a Library Type
		 */
		public function update_library_type( $model ) {

			// Update
			$this->db->trans_start();
			$this->db->query("	UPDATE `music_type` SET `description` = ? WHERE `id` = ? ", [ $model['description'], $model['id'] ]);
			$this->update_library_type_dist( $model, FALSE );
			$this->db->trans_complete();
			return true;

		}


		/**
		 * Updates Distribution for a Type
		 *
		 * @param array $model	The Type Model
		 * @return bool
		 */
		public function update_library_type_dist( $model, $trans = true ) {

			// Start Transction
			if ( $trans ) $this->db->trans_start();
			
			// Delete Distributions
			$this->db->query(" DELETE FROM `music_type_dist` WHERE `type` = ? ", [ $model['id'] ]);

			// Create Slave Distributions
			foreach ( $model['slaves'] as $k => $v ) {
				$this->db->query(" INSERT INTO `music_type_dist` ( `type`, `slave`, `player` ) VALUES ( ?, ?, ? ) ", [ $model['id'], $k, NULL ]);
			}

			// Create Player Distributions
			foreach ( $model['players'] as $k => $v ) {
				$this->db->query(" INSERT INTO `music_type_dist` ( `type`, `slave`, `player` ) VALUES ( ?, ?, ? ) ", [ $model['id'], NULL, $k ]);
			}

			// Commit and Return
			if ( $trans ) $this->db->trans_complete();
			return true;

		}


		/**
		 * Deletes a Library Type
		 */
		public function delete_library_type( $id ) {
			$this->db->query("	DELETE FROM `music_type_dist` WHERE `type` = ? ", [ $id ]);
			$this->db->query("	DELETE FROM `music_type` WHERE `id` = ? ", [ $id ]);
			return true;
		}


        /**
         * Gets Library Categories
         *
         * @return array   Categories
         */
		public function get_library_categories( $sort = true, $type = 1 ) {

			// Holds Return
			$return = [];

			// Get Categories
			$q = $this->db->query("	SELECT * FROM `music_category` ORDER BY `description` ")->result_array();

			// Sort by Version
			if ( $sort ) {
				usort($q, function($a, $b) {
					return version_compare($a['id'], $b['id']);
				});
			}

			// Add to Nested Array
			foreach ( $q as $result ) {

				// Grab ID
				$id 	= 	$result['id'];
				$main 	= 	floor($id);

				// Build Array
				switch ( $type ) {
					case 4:

						// Add Main
						if ( empty($return[$main]) )	$return[$main]	=	[ 'description' => $result['description'], 'children' => [] ];

						// Are we a Main Category?
						if ( empty($return[$main]) )	$return[$main]	=	[ 'description' => $result['description'], 'children' => [] ];
						if ( $main == $id ) {
							$return[$main]['description']	=	$result['description'];
						}
						else {
							$return[$main]['children'][$result['id']]	=	$result['description'];
						}

						// Done
						break;

					case 3:
					case 2:

						// Include Subcategory
						if ( $id == $main )
							$return[$result['id']] = $result['description'];
						else
							$return[$result['id']] = ( $type==3 ? "&#160;&#160;&#160;&#160;&#160;&#160;&#160;" : "") . ( empty($return[$main]) ? "N/A - " : $return[$main] ) . " > " . $result['description'];
						
						// Done
						break;

					default:

						// Default Type (Simple Array)
						$return[$id] 	= 	$result['description'];
						
						// Done
						break;
				}



			}

			// Return
			return $return;

		}


		/**
		 * A Search Model
		 */
		public function search_library( $params ) {

			// Ensure Params
			if ( empty($params['page']) || intval($params['page'])==0 )				$params['page'] = 1;		// Ensure Page is Integer and 1
			if ( empty($params['per_page']) || intval($params['per_page'])==0 )		$params['per_page'] = 10;	// Ensure Page is Integer and 1

			// Limit Results
			if ( $params['per_page'] <= 1 )		$params['per_page'] = 1;
			if ( $params['per_page'] >= 1000 )	$params['per_page'] = 1000;

			// Holds Return
			$return = [
				'count'		=>	0,
				'per_page'	=>	$params['per_page'],
				'page'		=>	0,
				'pages'		=>	0,
				'returned'	=>	0,
				'results'	=>	[

				]
			];

			// Build Start and Stop
			$params['start'] 	= ( $params['page'] - 1 ) * $params['per_page'];
			$params['end']		=	$params['start'] + $params['per_page'];

			// Enforce Compliance
			if ( empty($params['params']) )		$params['params'] = "";

			// Build Query
			$sql	=	" SELECT * FROM `music_track` WHERE 1 ";

			// Add Params
			if ( ! empty($params['params']) ) {
				$sql	.=	"AND (`artist` LIKE '%" . $this->db->escape_str($params['params']) . "%' OR `title` LIKE '%" . $this->db->escape_str($params['params']) . "%') ";
			}

			// Add Type
			if ( ! empty($params['type']) ) {
				$sql	.=	"AND `type` = " . $this->db->escape_str($params['type']) . " ";
			}
			
			// Do Year
			$params['from']		=	empty($params['from']) 	? 	0 	: intval($params['from']);
			$params['to']		=	empty($params['to']) 	? 	0 	: intval($params['to']);
			if ( $params['from'] > 0) {
				$sql	.=	"AND `year` >= " . $this->db->escape_str($params['from']) . " ";
			}
			if ( $params['to'] > 0 ) {
				$sql	.=	"AND `year` <= " . $this->db->escape_str($params['to']) . " ";
			}

			// Do Category
			if ( ! empty($params['category']) && is_numeric($params['category']) ) {
				$mc = floor($params['category']) == $params['category'];
				if ( ! $mc ) {
					$sql	.=	"AND find_in_set( " . $this->db->escape_str($params['category']) . " , `categories` ) > 0 ";
				}
				else {
					$sql	.=	"AND CONCAT(',', `categories`) like '%," . $this->db->escape_str($params['category']) . ".%' ";
				}
			}

			// Add Controversal
			if ( ! empty($params['controversial']) ) {
				$sql	.=	"AND `controversial` = " . ($params['controversial'] == "YES" ? 1 : 0) . " ";
			}

			// Add Disabled
			if ( ! empty($params['disabled']) ) {
				$sql	.=	"AND ( `disabled` = " . ($params['disabled'] == "YES" ? 1 : 0) . " OR `ldisabled` = " . ($params['disabled'] == "YES" ? 1 : 0) . " ) ";
			}

			// Do Ordering
			switch ( $params['order'] ) {
				case "ARTIST":
					$sql .= "ORDER BY `artist` " . (empty($params['direction']) ? " ASC " : " DESC ");
					break;
				case "TITLE":
					$sql .= "ORDER BY `title` " . (empty($params['direction']) ? " ASC " : " DESC ");
					break;
				case "TYPE":
					$sql .= "ORDER BY `type` " . (empty($params['direction']) ? " ASC " : " DESC ");
					break;
				case "YEAR":
					$sql .= "ORDER BY `year` " . (empty($params['direction']) ? " ASC " : " DESC ");
					break;
				case "DURATION":
					$sql .= "ORDER BY `duration` " . (empty($params['direction']) ? " ASC " : " DESC ");
					break;
			}

			// Perform Search
			$q					=	$this->db->query($sql);
			$return['count']	=	$q->num_rows();
			$return['page']		=	$params['page'];
			$return['pages']	=	$return['count'] > 0 ? ceil($return['count'] / $params['per_page']) : 0;
			
			// Get 50 Rows
			if ( $return['count'] >= 1 && $params['start'] <= $return['count']) {
				for ( $i = $params['start']; $i < $params['end']; $i++ ) {
					$tRow = $q->row_array($i);											// Get Row i
					if ( isset($return['results'][$tRow['gid']]) )		break;			// Break if No More Rows
					$return['results'][$tRow['gid']]				=	$tRow;			// Add to Array
				}
			}

			// Do Returned
			$return['returned']	=	count($return['results']);
			
			// Return
			return $return;

		}


        /**
         * Gets Library Tracks
         *
         * @return array   Categories
         */
		public function get_library_tracks_by_category( $category , $maincat = false ) {

			// Holds Return
			$return = [];

			// Get Tracks
			if ( ! $maincat ) {

				// Get Tracks that match a Sub Category
				$q = $this->db->query("	SELECT * FROM `music_track` WHERE find_in_set( ?, `categories` ) > 0 ", [ $category ])->result_array();

			}
			else {

				// Get Tracks of a Main Category
				$q = $this->db->query("	SELECT * FROM `music_track` WHERE CONCAT(',', `categories`) like ? ORDER BY `artist` ", [ '%,' . $category . '.%' ])->result_array();

			}
			
			// Add to Array
			foreach ( $q as $result ) {
				$return[$result['gid']] = $result;
			}

			// Return
			return $return;

		}


		/**
		 * Gets the Library Settings
		 *
		 * @return void
		 */
		public function get_library_settings() {

			// Holds Return
			$return = [];

			// Grab Settings
			$q = $this->db->query(" SELECT * FROM `music_settings` ")->result_array();

			// Loop Settings
			foreach ( $q as $setting ) {
				$return[$setting['key']] 	= 	$setting['value'];
			}

			// Return
			return $return;

		}


		/**
		 * Is the server a Master
		 *
		 * @return boolean
		 */
		public function is_library_master( $settings = null ) {
			if ( $settings == null ) $settings = $this->get_library_settings();
			return empty($settings['MODE']);
		}


		/**
		 * Gets the Library Count
		 *
		 * @return void
		 */
		public function get_library_count() {

			// Grab Settings
			$q = $this->db->query(" SELECT count(*) 'count' FROM `music_track` ")->result_array();
			return empty($q[0]['count']) ? 0 : $q[0]['count']; 

		}


		/**
		 * Gets the Library Stats
		 *
		 * @return void
		 */
		public function get_library_stats() {

			// Grab Settings
			$q = $this->db->query("		SELECT 	tr.`type`, ty.`description`, count(*) 'count' 
										FROM 	`music_track` tr
											LEFT JOIN `music_type` ty ON ty.`id` = tr.`type` 
										GROUP BY tr.`type` 
									")->result_array();

			// Return
			return $q;

		}


		/**
		 * Gets Library Data
		 *
		 * @return void
		 */
		public function get_library_data( $key = "id" ) {

			// Grab Settings
			$return = [];

			// Add Track Data
			foreach ( $this->db->query(" SELECT *, DATE_FORMAT(`added`,'%Y-%m-%dT%TZ') 'added', DATE_FORMAT(`updated`,'%Y-%m-%dT%TZ') 'updated' FROM `music_track` ORDER BY `gid` ")->result_array() as $track) {
				if ( $key == "file" ) {
					$return[mb_strtoupper($track['filename'])] = $this->library_file_model(NULL, $track);
				}
				else {
					$return[$track['gid']] = $this->library_file_model(NULL, $track);
				}
				
			}

			// Return
			return $return;
	
		}



		/**
		 * Gets the Library Files
		 *
		 * @return void
		 */
		public function get_library_files( $start = 0, $limit = 0 ) {

			// Grab Settings
			if ( $limit > 0 )
				return $this->db->query(" SELECT `gid`, `filename`, `artist`, `title`, `checksum`, `size` FROM `music_track` WHERE `gid` >= ? ORDER BY `gid` LIMIT ? ", [ $start, $limit ])->result_array();
			else
				return $this->db->query(" SELECT `gid`, `filename`, `artist`, `title`, `checksum`, `size` FROM `music_track` WHERE `gid` >= ? ORDER BY `gid` ", [ $start ])->result_array();
	
		}



		/**
		 * Updates a Library Setting
		 *
		 * @param string $key		The Key to Update
		 * @param string $value		The Value to Set
		 * @return void
		 */
		public function update_library_setting( $key, $value ) {
			$this->db->query(" UPDATE `music_settings` SET `value` = ? WHERE `key` = ? ", [ $value, $key ]);
			return true;
		}


		/**
		 * Checks the Library for a Existing File
		 *
		 * @return void
		 */
		public function library_entries_from_file( $file ) {

			// Return
			$q = $this->db->query(" SELECT * FROM `music_track` WHERE `filename` = ? ", [$file])->result_array();
			if ( ! empty($q) )	
				return $this->library_file_model(null, $q[0]);
			else
				return null;

		}


		/**
		 * Checks the Library for a Existing File
		 *
		 * @return void
		 */
		public function get_library_track( $id ) {

			// Return
			$q = $this->db->query(" SELECT *, DATE_FORMAT(`added`,'%Y-%m-%dT%TZ') 'added', DATE_FORMAT(`updated`,'%Y-%m-%dT%TZ') 'updated' FROM `music_track` WHERE `gid` = ? ", [$id])->result_array();
			if ( ! empty($q) )	
				return $this->library_file_model(null, $q[0]);
			else
				return null;

		}


		/**
		 * Adds a Track to the Library
		 */
		public function create_library_entry( $data ) {

			if ( ! isset($data['filename']) 	|| empty($data['filename']) )		return false;
			if ( ! isset($data['artist']) 		|| empty($data['artist']) )			return false;
			if ( ! isset($data['title']) 		|| empty($data['title']) )			return false;
			
			// Nullables
			if ( ! isset($data['type']) 		|| empty($data['type']) )			$data['type'] = NULL;
			if ( ! isset($data['year']) 		|| empty($data['year']) )			$data['year'] = NULL;
			if ( ! isset($data['duration']) 	|| empty($data['duration']) )		$data['duration'] = NULL;
			if ( ! isset($data['checksum']) 	|| empty($data['checksum']) )		$data['checksum'] = NULL;
			if ( ! isset($data['size']) 		|| empty($data['size']) )			$data['size'] = 0;
			if ( ! isset($data['categories']) 	|| empty($data['categories']) )		$data['categories'] = "";

			// Booleans
			$data['controversial'] 	= empty($data['controversial']) ? 0 : 1;
			$data['disabled'] 		= empty($data['disabled']) ? 0 : 1;
			$data['ldisabled'] 		= empty($data['ldisabled']) ? 0 : 1;

			// Dates
			$data['gid']			= empty($data['gid'])		? NULL	: $data['gid'];
			$data['added'] 			= empty($data['added']) 	? NULL 	: gmdate("Y-m-d H:i:s", $data['added']);
			$data['updated'] 		= empty($data['updated']) 	? NULL 	: gmdate("Y-m-d H:i:s", $data['updated']);

			// Inject
			$this->db->query("	INSERT INTO `music_track`
									( `gid`, `filename`, `artist`, `title`, `type`, `year`, `duration`, `controversial`, `categories`, `added`, `updated`, `checksum`, `disabled`, `ldisabled`, `size` )
								VALUES
									( IFNULL( ? , NULL), ?, ?, ?, ?, ?, ?, ?, ?, IFNULL( ? , CURRENT_TIMESTAMP), IFNULL( ? , CURRENT_TIMESTAMP), ?, ?, ?, ? ) 
							", 
							[
								$data['gid'],
								$data['filename'],
								$data['artist'],
								$data['title'],
								$data['type'],
								$data['year'],
								$data['duration'],
								$data['controversial'],
								$data['categories'],
								$data['added'],
								$data['updated'],
								$data['checksum'],
								$data['disabled'],
								$data['ldisabled'],
								$data['size']
							]);

			// Return ID
			return $this->db->insert_id();

		}



		/**
		 * Adds a Track to the Library
		 */
		public function update_library_entry( $data, $update_date = true ) {

			if ( ! isset($data['gid']) 			|| empty($data['gid']) )			return false;
			if ( ! isset($data['filename']) 	|| empty($data['filename']) )		return false;
			if ( ! isset($data['artist']) 		|| empty($data['artist']) )			return false;
			if ( ! isset($data['title']) 		|| empty($data['title']) )			return false;
			
			// Nullables
			if ( ! isset($data['type']) 		|| empty($data['type']) )			$data['type'] = NULL;
			if ( ! isset($data['year']) 		|| empty($data['year']) )			$data['year'] = NULL;
			if ( ! isset($data['duration']) 	|| empty($data['duration']) )		$data['duration'] = NULL;
			if ( ! isset($data['checksum']) 	|| empty($data['checksum']) )		$data['checksum'] = NULL;
			if ( ! isset($data['size']) 		|| empty($data['size']) )			$data['size'] = 0;
			if ( ! isset($data['categories']) 	|| empty($data['categories']) )		$data['categories'] = "";

			// Booleans
			$data['controversial'] 	= empty($data['controversial']) ? 0 : 1;
			$data['disabled'] 		= empty($data['disabled']) ? 0 : 1;
			$data['ldisabled'] 		= empty($data['ldisabled']) ? 0 : 1;

			// Dates
			$data['added'] 			= empty($data['added']) 	? NULL 	: gmdate("Y-m-d H:i:s", $data['added']) . " UTC";
			$data['updated'] 		= empty($data['updated']) 	? NULL 	: gmdate("Y-m-d H:i:s", $data['updated']) . " UTC";

			// Inject
			$this->db->query("	UPDATE `music_track`
								SET
									`filename` 		=  	?,
									`artist`		= 	?,
									`title`			= 	?,
									`type`			= 	?,
									`year`			=	?,
									`duration`		=	?,
									`controversial`	= 	?,
									`categories`	= 	?,
									`disabled`		=	?,
									`ldisabled`		=	?,
									`added`			=	IFNULL( ? , CURRENT_TIMESTAMP),									
									`updated`		=	IFNULL( ? , CURRENT_TIMESTAMP),
									`checksum`		= 	?,
									`size`			=	?
								WHERE
									`gid`			=	?
							", 
							[
								$data['filename'],
								$data['artist'],
								$data['title'],
								$data['type'],
								$data['year'],
								$data['duration'],
								$data['controversial'],
								$data['categories'],
								$data['disabled'],
								$data['ldisabled'],
								empty($data['added']) 						? 	NULL	: $data['added'],
								empty($data['updated']) || $update_date 	? 	NULL 	: $data['updated'],
								$data['checksum'],
								$data['size'],
								$data['gid']
							]);

			// Return ID
			return $data['gid'];

		}


		/**
		 * Update Library Locally Updated
		 */
		public function update_library_enabled( $track, $state ) {

			// Inject
			$this->db->query("	UPDATE `music_track`
								SET
									`ldisabled`		=	?									
								WHERE
									`gid`			=	?
							", 
							[
								empty($state) ? 0 : 1,
								$track
							]);

			// Return ID
			return true;

		}


		/**
		 * Syncs this Server with a Master
		 */
		public function sync() {

			// Holds Return
			$return = [
				'result'	=>	false,
				'error'		=>	"",
				'version'	=>	"",
				'types'		=>	[
					'added'		=>	[],
					'removed'	=>	[],
					'updated'	=>	[]		
				],
				'cats'		=>	[
					'added'		=>	[],
					'removed'	=>	[],
					'updated'	=>	[]		
				],
				'tracks'	=>	[
					'added'		=>	[],
					'removed'	=>	[],
					'updated'	=>	[]					
				],
				'playlists'	=>	[
					'added'		=>	[],
					'removed'	=>	[],
					'updated'	=>	[]					
				],
				'delete'	=>	[

				]
			];

			// Pull Settings
			$settings	=	$this->get_library_settings();

			// Pre Fire Tests
			if ( empty($settings['MODE']) ) {

				// We are not a Slave Server
				$return['error']	=	"This Server is a Master Server so it can't be Synced";
				return $return;

			}
			elseif ( empty($settings['SERVER']) ) {

				// We don't have a Master Server
				$return['error']	=	"There is no Master Server defined to sync with";
				return $return;

			}
			else {

				// Grab URL
				$lUrl 		= 	$this->build_master_url( 'version' , $settings );
				$pUrl 		= 	$this->build_master_url( 'pull' , $settings );
				$data 		= 	$this->poll_master($lUrl);
				//$mVersion	=	"";

				// Did we get Data?
				if ( empty($data['result']) ) {
					$return['error']	=	$data['error'];				// Return Error
				}
				else {

					// Cast Result to Array
					$xml   	= @simplexml_load_string($data['data'], 'SimpleXMLElement', LIBXML_NOCDATA);
					if ( ! $xml )
						$return['error']	=	"Invalid Response from the Master Server";
					elseif ( ! empty($xml['error']) )
						$return['error']	=	(string)$xml['error'];
					elseif ( empty($xml['version']) )
						$return['error']	=	"The Master Server returned no Library Version";
					else {

						// Grab Version
						$mVersion			=	(string)$xml['version'];
						//$mVersion = "";

						// Return True (If Version Matches there's nothing to do!)
						if ( $settings['VERSION'] == $mVersion ) {
							$return['result'] 	= 	true;
							$return['version']	=	$mVersion;
							$this->update_library_setting( 'SYNCED', gmdate("Y-m-d\TH:i:sO") );
							return $return;
						}

						// Pull Library
						$data 		= 	$this->poll_master($pUrl);
						if ( empty($data['result']) ) {
							$return['error']	=	$data['error'];
						}
						else {

							// Cast Result to Array
							$xml   	= @simplexml_load_string($data['data'], 'SimpleXMLElement', LIBXML_NOCDATA);
							if ( ! $xml )
								$return['error']	=	"Invalid Response from the Master Server";
							elseif ( ! empty($xml['error']) )
								$return['error']	=	(string)$xml['error'];
							elseif ( empty($xml['version']) )
								$return['error']	=	"The Master Server returned no Library Version";
							else {

								// Grab Version
								$mVersion			=	(string)$xml['version'];
								//$mVersion = "";

								// Return True (If Version Matches there's nothing to do!)
								if ( $settings['VERSION'] == $mVersion ) {
									$return['result'] 	= 	true;
									$return['version']	=	$mVersion;
									$this->update_library_setting( 'SYNCED', gmdate("Y-m-d\TH:i:sO") );
									return $return;
								}

	
								/**
								 * START DATABASE TRANSACTION
								 */

								$this->db->trans_start();


								/**
								 * SYNCRONISE TYPES
								 */

								// Get Current Types
								$database_types 		=	$this->mod_music->get_library_types();
								$library_types			=	[];
								
								// Read Library Types
								foreach ( $xml->types->type as $type ) {
									if ( empty($type['id']) ) 	continue;
									$library_types[(string)$type['id']] = (string)$type['description'];
								}

								// Remove Deleted Types
								foreach ( $database_types as $tk => $tv ) {
									if ( ! isset($library_types[$tk]) ) {
										$this->db->query(" DELETE FROM `music_type` WHERE `id` = ?", [$tk]);
										$return['types']['removed'][] = "Removed Type {$tk} - {$tv}";
									}
								}

								// Update Types
								foreach ( $library_types as $nk => $nv ) {
									if ( ! isset($database_types[$nk]) ) {
										$this->db->query("	INSERT INTO `music_type` (`id`, `description`) VALUES ( ?, ?) ", [ $nk, $nv ]);
										$return['types']['added'][] = "Added Type {$nk} - {$nv}";
									}
									elseif ( $database_types[$nk] <> $library_types[$nk] ) {
										$this->db->query("	UPDATE `music_type` SET `description` = ? WHERE `id` = ? ", [ $nv, $nk ]);
										$return['types']['updated'][] = "Updated Type {$nk} - from {$database_types[$nk]} to {$nv}";
									}
								}


								/**
								 * SYNCRONISE CATEGORIES
								 */

								$database_categories	=	$this->mod_music->get_library_categories();
								$library_categories		=	[];

								// Read Library Categories
								foreach ( $xml->categories->category as $cat ) {
									if ( empty($cat['id']) ) 	continue;
									$library_categories[(string)$cat['id']] = (string)$cat['description'];
								}

								// Remove Deleted Categories
								foreach ( $database_categories as $tk => $tv ) {
									if ( ! isset($library_categories[$tk]) ) {
										$this->db->query(" DELETE FROM `music_category` WHERE `id` = ?", [$tk]);
										$return['cats']['removed'][] = "Removed Category {$tk} - {$tv}";
									}
								}

								// Update Categories
								foreach ( $library_categories as $nk => $nv ) {
									if ( ! isset($database_categories[$nk]) ) {
										$this->db->query("	INSERT INTO `music_category` (`id`, `description`) VALUES ( ?, ?) ", [ $nk, $nv ]);
										$return['cats']['added'][] = "Added Category {$nk} - {$nv}";
									}
									elseif ( $database_categories[$nk] <> $library_categories[$nk] ) {
										$this->db->query("	UPDATE `music_category` SET `description` = ? WHERE `id` = ? ", [ $nv, $nk ]);
										$return['cats']['updated'][] = "Updated Category {$nk} - from {$database_categories[$nk]} to {$nv}";
									}
								}


								/**
								 * SYNCRONISE TRACKS
								 */

								$database_tracks		=	$this->mod_music->get_library_data();
								$library_tracks			=	[];

								// Process Library Tracks
								foreach ( $xml->tracks->track as $track ) {
									if ( empty($track['gid']) ) 	continue;
									$library_tracks[(string)$track['gid']] = $this->library_file_model( NULL, $track );
								}
								
								// Remove Deleted Tracks
								foreach ( $database_tracks as $tk => $tv ) {
									if ( ! isset($library_tracks[$tk]) ) {
										$this->db->query(" DELETE FROM `music_track` WHERE `gid` = ?", [$tk]);
										$return['tracks']['removed'][] 	= 	"Removed Track {$tk} - {$tv['title']} by {$tv['artist']}";
										$return['delete'][] 			= 	$tv['filename'];
									}
								}

								// Update Tracks
								foreach ( $library_tracks as $nk => $nv ) {
									if ( ! isset($database_tracks[$nk]) ) {
										$this->create_library_entry($nv);
										$return['tracks']['added'][] = "Added Track {$nk} - {$nv['title']} by {$nv['artist']}";
									}
									else {

										// Has Track Changed?
										$changes	=	$this->find_changes( $database_tracks[$nk], $library_tracks[$nk] );
										if ( ! empty($changes['filechanged']) ) {
											$return['delete'][] = $database_tracks[$nk]['filename'];
										}
										if ( ! empty($changes['changes']) ) {

											// Maintain Local Changes
											$nv['ldisabled']	=	$database_tracks[$nk]['ldisabled'];

											// Update Track
											$this->update_library_entry($nv, false);
											$return['tracks']['updated'][] = "Updated Track {$nk} - " . implode("\n" , $changes['changes']);
											
										}

									}

								}

								// Get System Playlists
								$shared_playlists 		=	$this->get_global_playlists();
								$active_lists			=	[];
								$active_groups 			=	[];

								// Add Master Groups
								foreach ( $xml->groups->group as $group ) {
									$active_groups[(string)$group['id']]	=	(string)$group['desc'];
								}

								// Save Groups
								$this->save_library_master_groups($active_groups);

								// Load Shared Playlists
								foreach ( $xml->playlists->list as $playlist ) {

									// Grab GID
									$gid 	=	(string)$playlist['gid'];

									// Check
									if ( ! empty($gid) ) {

										// Action
										if ( ! isset($shared_playlists[$gid]) ) {

											// This is a New Playlist
											$pl = $this->library_playlist_model(null, $playlist);

											// Create Playlist
											$pl['id']		= 	$this->save_library_playlist( $pl );
											
											// Do List
											$count = 1;
											foreach ( explode(",", (string)$playlist['contents']) as $track ) {
												if ( isset($database_tracks[$track]) ) {
													$pl['tracks'][$count] = [ 'gid' => $track ];
													$count++;
												}
											}

											// Save Playlist
											$this->save_library_playlist_tracklist($pl);
											$return['playlists']['added'][] 	= 	"Added Playlist {$pl['id']} - {$pl['name']}";
											$active_lists[$pl['gid']] = TRUE;

										}
										else {

											// Update List
											$pl = $shared_playlists[$gid];

											// Update Playlist
											$pl 						=	$shared_playlists[$gid];
											$pl['name']					=	(string)$playlist['name'];
											$pl['shuffle']				=	(string)$playlist['shuffle'] > 0;
											$pl['plgroup']				=	(string)$playlist['plgroup'];
											$pl['masterplgroup']['id']	=	(string)$playlist['gpg'];
											$pl['encrypted']			=	(string)$playlist['encrypted'] > 0;
											$pl['disableadverts']		=	(string)$playlist['disableadverts'] > 0;
											$pl['adverttracks']			=	(string)$playlist['adverttracks'] > 0;
											$pl['adverttime']			=	(string)$playlist['adverttime'];
											$pl['notes']				=	(string)$playlist['notes'];

											// Update Playlist
											$this->save_library_playlist( $pl );

											// Do List
											$count = 1;
											foreach ( explode(",", (string)$playlist['contents']) as $track ) {
												if ( isset($database_tracks[$track]) ) {
													$pl['tracks'][$count] = [ 'gid' => $track ];
													$count++;
												}
											}

											// Save Playlist
											$this->save_library_playlist_tracklist($pl);
											$return['playlists']['updated'][] 	= 	"Updated Playlist {$pl['id']} - {$pl['name']}";
											$active_lists[$pl['gid']] = TRUE;

										}

									}
								}


								// Delete Removed Playlists
								foreach ( $shared_playlists as $gid => $list ) {
									if ( ! isset($active_lists[$gid]) ) {
										$return['playlists']['removed'][] 	= 	"Removed Playlist {$list['id']} - {$list['name']}";
										$this->delete_library_playlist($list['id']);
									}
								}

								// Update Version
								$this->update_library_version( $mVersion, FALSE );
								$this->update_library_setting( 'SYNCED', gmdate("Y-m-d\TH:i:sO") );
								$return['version']	=	$mVersion;

								// Mark Successful
								$return['result'] = true;

								// Commit
								$this->db->trans_complete();

								// Output to Log
								@file_put_contents( LIBSYNC_LOG, $this->sync_text_report($return, false) . "\n", FILE_APPEND | LOCK_EX);

								// Delete Invalid Tracks
								if ( ! empty($return['delete']) ) {
									$cPath 		= 	sanatize_path(CMS_PATH_MUSIC_LIB);
									foreach ( $return['delete'] as $track ) {
										if ( file_exists($cPath . $track) && is_file($cPath . $track) ) {
											unlink( $cPath . $track );
										}
									}
								}

							}

						}

					}

				}

			}

			// Return
			return $return;

		}



		/**
		 * Finds Changes between two Files
		 */
		public function find_changes( $database, $library ) {

			// Holds Changes
			$difference = [
				'filechanged'	=>	false,
				'changes'		=>	[

				]
			];

			// Has Checksum Changed?
			if ( $database['checksum'] <> $library['checksum'] ) {
				$difference['changes'][] 	= 	"Checksum changed to {$library['checksum']}";
				$difference['filechanged']	=	true;
			}			

			// Has File Changed?
			if ( $database['filename'] <> $library['filename'] ) {
				$difference['changes'][] 	= 	"Filename changed from {$database['filename']} to {$library['filename']}";
				$difference['filechanged']	=	true;
			}	

			// Find Changes		
			if ( $database['artist'] <> $library['artist'] )				$difference['changes'][] = "Artist changed from {$database['artist']} to {$library['artist']}";
			if ( $database['title'] <> $library['title'] )					$difference['changes'][] = "Title changed from {$database['title']} to {$library['title']}";
			if ( $database['type'] <> $library['type'] )					$difference['changes'][] = "Type changed from {$database['type']} to {$library['type']}";
			if ( $database['year'] <> $library['year'] )					$difference['changes'][] = "Year changed from {$database['year']} to {$library['year']}";
			if ( $database['duration'] <> $library['duration'] )			$difference['changes'][] = "Duration changed from {$database['duration']} to {$library['duration']}";
			if ( $database['controversial'] <> $library['controversial'] )	$difference['changes'][] = "Controversial changed from {$database['controversial']} to {$library['controversial']}";
			if ( $database['categories'] <> $library['categories'] )		$difference['changes'][] = "Categories changed from {$database['categories']} to {$library['categories']}";
			if ( $database['disabled'] <> $library['disabled'] )			$difference['changes'][] = "Disabled changed from {$database['disabled']} to {$library['disabled']}";
			if ( $database['added'] <> $library['added'] )					$difference['changes'][] = "Added changed from " . gmdate("d/m/Y H:i:s e", $database['added']) . " to ". gmdate("d/m/Y H:i:s e", $library['added']);
			if ( $database['updated'] <> $library['updated'] )				$difference['changes'][] = "Updated changed from " . gmdate("d/m/Y H:i:s e", $database['updated']) . " to ". gmdate("d/m/Y H:i:s e", $library['updated']);
			if ( $database['size'] <> $library['size'] )					$difference['changes'][] = "Size changed from {$database['size']} to {$library['size']}";

			// Return
			return $difference;

		}


		/**
		 * Gets a Report as Text
		 */
		public function sync_text_report( $data, $html = true ) {

			// Header
			$o 	=	"<b>Sync Result - " . gmdate("d/m/Y H:i:s e") . "</b>\n\n";

			// Check Result
			if ( empty($data['result']) ) {

				// Error Occured
				$o 	.=	"Error " . ( empty($data['error']) ? "Unknown Error" : $data['error'] );

			}
			else {

				// Add Types
				$o		.=	"<b>Library Types</b>\n";
				if ( empty($data['types']['added']) )
					$o .= 	"No New Types Were Added\n";
				else
					$o .= 	implode("\n", $data['types']['added']) . "\n";
				if ( empty($data['types']['updated']) )
					$o .= 	"No Types Were Updated\n";
				else
					$o .= 	implode("\n", $data['types']['updated']) . "\n";
				if ( empty($data['types']['removed']) )
					$o .= 	"No Types Were Removed\n";
				else
					$o .= 	implode("\n", $data['types']['removed']) . "\n";

				// Add Categories
				$o		.=	"\n<b>Library Categories</b>\n";
				if ( empty($data['cats']['added']) )
					$o .= 	"No New Categories Were Added\n";
				else
					$o .= 	implode("\n", $data['cats']['added']) . "\n";
				if ( empty($data['cats']['updated']) )
					$o .= 	"No Categories Were Updated\n";
				else
					$o .= 	implode("\n", $data['cats']['updated']) . "\n";
				if ( empty($data['cats']['removed']) )
					$o .= 	"No Categories Were Removed\n";
				else
					$o .= 	implode("\n", $data['cats']['removed']) . "\n";

				// Add Tracks
				$o		.=	"\n<b>Library Tracks</b>\n";
				if ( empty($data['tracks']['added']) )
					$o .= 	"No New Tracks Were Added\n";
				else
					$o .= 	implode("\n", $data['tracks']['added']) . "\n";
				if ( empty($data['tracks']['updated']) )
					$o .= 	"No Tracks Were Updated\n";
				else
					$o .= 	implode("\n\n", $data['tracks']['updated']) . "\n";
				if ( empty($data['tracks']['removed']) )
					$o .= 	"No Tracks Were Removed\n";
				else
					$o .= 	implode("\n", $data['tracks']['removed']) . "\n";

				// Add Playlists
				$o		.=	"\n<b>Playlists</b>\n";
				if ( empty($data['playlists']['added']) )
					$o .= 	"No Playlists Were Added\n";
				else
					$o .= 	implode("\n", $data['playlists']['added']) . "\n";
				if ( empty($data['playlists']['updated']) )
					$o .= 	"No Playlists Were Updated\n";
				else
					$o .= 	implode("\n\n", $data['playlists']['updated']) . "\n";
				if ( empty($data['playlists']['removed']) )
					$o .= 	"No Playlists Were Removed\n";
				else
					$o .= 	implode("\n", $data['playlists']['removed']) . "\n";

				// Add Deletions
				$o		.=	"\n<b>File Deletions</b>\n";
				if ( empty($data['delete']) )
					$o .= 	"No Invalid Files Were Deleted\n";
				else
					$o .= 	implode("\n", $data['delete']) . "\n";

				// Add library Version
				$o .=	"\n<b>Version</b>\n";
				$o .=	"Library is now version " . $data['version'];

			}

			// Remove HTML
			if ( ! $html )
				return str_replace("</b>", "", str_replace("<b>", "", $o));
			else
				return $o;

		}



		/**
		 * Builds a Master URL for Library Sync
		 */
		public function build_master_url( $function , $settings ) {

			// Parameters
			$id			=	empty($settings['ID']) 			? 	"" 	: $settings['ID'];
			$password	=	empty($settings['PASSWORD']) 	? 	"" 	: $settings['PASSWORD'];

			// Build the URL
			$master		=	rtrim($settings['SERVER'], "/");
			$master		.=	"/poll/library/" . $function . "?type=slave";

			// Add Parameters
			if ( ! empty($id) )			$master .=	"&token=" . urlencode($id);
			if ( ! empty($password) )	$master .=	"&password=" . urlencode($password);

			// Return
			return $master;

		}


		/**
		 * Pulls Data from a Master Server
		 */
		public function poll_master( $url ) {
			
			// Holds Return
			$return = [
				'result'	=>	false,
				'error'		=>	"",
				'data'		=>	"",
				'response'	=>	null
			];
			
			try {

				/* Use cURL to pull the library */
				$ch = curl_init();

				// Set Curl Options
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
				curl_setopt($ch, CURLOPT_TIMEOUT, 20);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
				curl_setopt($ch, CURLOPT_USERAGENT, "SVMedia CMS");
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_ENCODING , "gzip");

				// Do Proxy
				if ( INTERNET_PROXY <> "" ) {
					curl_setopt($ch, CURLOPT_PROXY, INTERNET_PROXY);																// Set Proxy Server						
					if ( $this->credentials <> "" )	curl_setopt($ch, CURLOPT_PROXYUSERPWD, INTERNET_CREDENTIALS);					// Do Auth
				}

				// Grab Data
				$return['data'] 	= 	curl_exec($ch);
				$return['response']	=	curl_getinfo($ch, CURLINFO_HTTP_CODE);
				$return['result']	=	! empty($return['data']);

				// Set Error
				if ( empty($return['result']) && empty($return['response']) ) {
					$return['error']	=	"No response was returned from the server";
				}

				// Return
				return $return;

			}
			catch (Exception $e) {

				// Set Error
				$return['result']	=	false;
				$return['error']	=	$e->getMessage();

			}

			// Return
			return $return;

		}




    }