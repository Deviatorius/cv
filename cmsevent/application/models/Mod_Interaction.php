<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * User Model
	 */
	class Mod_Interaction extends CI_Model {


        /**
         * Creates an Interaction Based on an Asset
         *
         * @param int $file
         * @param string $message
         * @param int $player
         * @return bool
         */
        public function create_asset_interaction( $file, $message, $player ) {

            // Check Data
            if ( empty($file) || empty($player))     return false;

			// Start Transaction
			$this->db->trans_start();

            // Add Entry
            $this->db->query("  INSERT INTO `interaction`
                                    (`id`, `type`, `player`, `message`, `additional1`, `additional2`)
                                VALUES
                                    (NULL, 2, ?, ?, ?, ?) 
                            ",
                                Array (
                                    $player,
                                    $message,
                                    $file,
                                    ""
                                )
                            );



			// Commit
			$this->db->trans_complete();

            // Return
            return true;

        }


        /**
         * Creates an Interaction Based on a Message
         *
         * @param string $message
         * @param int $player
         * @return void
         */
        public function create_shoutout_interaction($message, $player) {

            // Check Data
            if ( empty($message) || empty($player))     return false;

 			// Start Transaction
			$this->db->trans_start();

            // Add Entry
            $this->db->query("  INSERT INTO `interaction`
                                    (`id`, `type`, `player`, `message`)
                                VALUES
                                    (NULL, 1, ?, ?) 
                            ",
                                Array (
                                    $player,
                                    $message
                                )
                            );

            // Commit
            $this->db->trans_complete();

            // Return
            return true;
        }

        /**
         * Create Either Song or Karaoke Interaction.
         *
         * @param int $player 
         * @param string $filename
         * @param int $gid
         * @param string $singer (optional) - For Karaoke Request
         * @return void
         */
        public function create_song_interaction($player, $filename, $gid, $type, $singer = NULL) {

            // Check Data
            if ( empty($filename) || empty($gid) || empty($player) || empty($type) )     return false;
            
            switch ($type) {

                // Music
                case '1':
                    // Start Transaction
                    $this->db->trans_start();
    
                    // Add Entry
                    $this->db->query("  INSERT INTO `interaction`
                                            (`id`, `type`, `player`, `additional1`, `additional2`)
                                        VALUES
                                            (NULL, 3, ?, ?, ?) 
                                    ",
                                        Array (
                                            $player,
                                            $filename,
                                            $gid
                                        )
                                    );
        
                    // Commit
                    $this->db->trans_complete();
        
                    // Return
                    return true;
                    break;

                // Music Video
                case '2':
                    // Start Transaction
                    $this->db->trans_start();

                    // Add Entry
                    $this->db->query("  INSERT INTO `interaction`
                                            (`id`, `type`, `player`, `additional1`, `additional2`)
                                        VALUES
                                            (NULL, 3, ?, ?, ?) 
                                    ",
                                        Array (
                                            $player,
                                            $filename,
                                            $gid
                                        )
                                    );
        
                    // Commit
                    $this->db->trans_complete();
        
                    // Return
                    return true;
                    break;

                // Karaoke
                case '3':

                    // Start Transaction
                    $this->db->trans_start();

                    // Add Entry
                    $this->db->query("  INSERT INTO `interaction`
                                            (`id`, `type`, `player`, `message`, `additional1`, `additional2`)
                                        VALUES
                                            (NULL, 4, ?, ?, ?, ?) 
                                    ",
                                        Array (
                                            $player,
                                            $singer,
                                            $filename,
                                            $gid
                                        )
                                    );
        
                    // Commit
                    $this->db->trans_complete();
        
                    // Return
                    return true;
                    break;
                
                // Unknown Type
                default:
                    return false;
                    break;
            }

        }

        /**
         * Undocumented function
         *
         * @param int $player
         * @param int $asset
         * @return string
         */
        public function get_interaction( $player, $asset ) {
        
            // Basic Checks
            if ( empty($player) || empty($asset) ) {
                return false;
            }

            // Query
            $q  =   $this->db->query("  SELECT * FROM `interaction` WHERE `id` = ? AND `player` = ?", Array ( $asset, $player ) )->result_array();
            if ( ! empty( $q ) ) return $q[0];
            return $q;
            
        }
    }
?>