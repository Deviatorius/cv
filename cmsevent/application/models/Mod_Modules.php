<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Global Model
	 */
	class Mod_Modules extends CI_Model {
        

        /**
         * Gets a Module Path
         *
         * @param string $pModule
         * @return void
         */
        public function module_path( $pModule ) {

            // Convert
            $pModule    =   mb_strtoupper( $pModule );

            // Load Paths
            if ( is_array($this->config->item('modules_locations')) ) {
                foreach ( $this->config->item('modules_locations') as $path => $relative ) {
                    foreach (scandir($path) as $module) {

						// Set Module Path
                        $module_path = str_replace('\\', '/', $path . rtrim($module, '/') . '/');
                        $module_ref  = mb_strtoupper($module);

						// Is Module Valid?
						if ( is_dir($module_path) && $module !== "." && $module !== ".." && $module_ref == $pModule) {

                            // Return
                            return $module_path;

                        }

                    }
                }
            }
            
        }


    
        /**
         * Gets All Modules
         *
         * @return void
         */
        public function get_modules() {

            // Create Array
            $modules = [];

            // Load Paths
            if ( is_array($this->config->item('modules_locations')) ) {
                foreach ( $this->config->item('modules_locations') as $path => $relative ) {
                    foreach (scandir($path) as $module) {

						// Set Module Path
                        $module_path = str_replace('\\', '/', $path . rtrim($module, '/') . '/');
                        $module_ref  = mb_strtoupper($module);

						// Is Module Valid?
						if (is_dir($module_path) && $module !== "." && $module !== "..") {

							// Load Module Settings INI
							$settings_path	=	$module_path . "settings.ini";
							if ( file_exists($settings_path) && is_file($settings_path) ) {

                                // Read Settings
                                $iSettings  =   @parse_ini_file( $settings_path, TRUE);
                                if ( ! empty($iSettings) ) {

                                    // Create Module
                                    $modules[$module] = Array(
                                        'name'		=>	$module,
                                        'path'		=>	$module_path,
                                        'url'       =>  base_url("client/{$module}/"),
                                        'key'       =>  ! empty($dbk[$module_ref]) ? $dbk[$module_ref] : NULL,
                                        'settings'	=>	[
                                            'navigation'    => [
                                                'link'      =>  "",
                                                'class'     =>  "",
                                                'text'      =>  ""
                                            ]
                                        ],
                                    );

                                    // Update Module URL Location (Incase Not using Default of Client)
                                    $rPath = str_replace( "\\", "/", FCPATH);
                                    if ( substr( $module_path, 0, mb_strlen($rPath) ) == $rPath ) {
                                        $modules[$module]['url']    =   base_url( substr( $module_path, mb_strlen($rPath) ) );
                                    }

                                    // Load Module Settings
                                    $modules[$module]['settings']   =   import_override_settings( $modules[$module]['settings'], $iSettings, true );

                                }
                            }
                        }
                    }
                }
            }

            // Return Array
            return $modules;

        }
        

    }

?>