<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * User Model
	 */
	class Mod_Auth extends CI_Model {
		
		// Text to File
		var $words = [];

		// Restoration
		var $first_segment = "";
		var $second_segment = "";
		var $r_w_zero = "";
		var $r_w_first = "";
		var $r_w_second = "";
		var $r_w_third = "";
		var $r_n_first = "";
		var $r_n_second = "";
		var $r_n_third = "";
		var $r_w_seed = "";
		var $r_n_seed = "";
		var $r_seed = [];
		var $r_player = "";
		var $r_seed_delta = "";

		/**
		 * Cretes an array from a text file
		 * 
		 * @param string $filepath path to file
		 * @param int $n float of seed
		 */
		private function txt_file_to_array($filepath): array {
			try {
				if ( ! empty( $this->words ) ) return $this->words;
			
				// Store the file
				$fp = @fopen($filepath, 'r'); 
		
				// Add Each Line to an Array
				if ($fp) {
					$i = 0;
					foreach (explode("\n", fread($fp, filesize($filepath))) as $word) {
						$list[$i+100] = trim($word);
						$i++;
					}
				}
		
				$this->words = (! empty($list)) ? $list : "Not Set";
				return $this->words;
			}
			catch (Exception $e) {
				$e->getMessage();
			}
		}

		
		/**
		 * Gets a Site Using a Room Code
		 *
		 * @param string $username the user record to retrieve
		 *
		 * @return Array of the Users Record or NULL if invalid
		 */
		public function get_site( $roomcode ) {

			// Return Code
			$pId = $this->restore_player($roomcode);

			// Get Registered Player
			$q = $this->db->query(	"	SELECT 	p.*, p.`description` 'player_description', s.`name` 'site', s.`brand`, b.`description` 'brand_name', b.`identifier`
										FROM	`player` p,
												`store` s
													LEFT JOIN `brand` b ON b.`id` = s.`brand`
										WHERE	s.`id` 		= p.`store`
										AND		p.`id` 		= ?
										AND		p.`product` = ?
									",
										Array (
											$pId,
											'ESA'
										)
									)->row_array();
									
			// Do we have a player?
			if ( empty($q) )	return NULL;

			// Return First Row
			return $q;

		}




		/**
		 * Restores Site ID from the Passphrase
		 * 
		 * @param int $today 
		 * @param int $passphrase
		 * @param int $file
		 */
		
		public function restore_player($passphrase) {
			try {
				// Set Day
				$today          =    strtotime(date('d-m-Y', time())) / 3600;

				// Set Word Library
				$file = "list.txt";

				// Load Words into Dictionary
				$dictionary     =  [];
				$dictionary     =  $this->txt_file_to_array($file);

				// Restore Segment Seeds from Passphrase
				$r_Seed         =  [];
				$r_Seed         =  $this->restore_seed($passphrase, $dictionary);
				if (empty($r_Seed)) return -1;

				$r_s_word       =  $r_Seed[0];
				$r_s_number     =  $r_Seed[1];

				if (! empty($r_Seed[2])) {
					$r_s_word    += count($dictionary);
				}
				

				$r_w_seed          =  (count($dictionary) * $r_s_word);
				$r_seed_delta      =  $r_w_seed + $r_s_number;

				$r_seed_delta_length    =   strlen((string)$r_seed_delta);

				$first_segment = 1;
				if ($r_seed_delta_length < 6) {
					$second_segment = 6 - $r_seed_delta_length;
					switch ($second_segment) {
						case 1: $zero = "0";
						break;
						case 2: $zero = "00";
						break;
						case 3: $zero = "000";
						break;
						case 4: $zero = "0000";
						break;
						case 5: $zero = "00000";
						break;
						default: $zero = "";
					}
					$r_delta      =  $first_segment;
					$r_delta     .=  $zero;
					$r_delta     .=  $r_seed_delta;
					$r_player     =  abs($today - $r_delta); 
				} 
				else {
					$r_int_today_player = substr($today, 0, 4) . $r_seed_delta;
					$r_player           =  abs($today - $r_seed_delta); 
				}


				// Add Vars for Debugging
				$this->r_seed_delta =   (! empty($r_seed_delta))    ? $r_seed_delta   : "Not Set";
				$this->r_player =       (! empty($r_player))        ? $r_player       : "Not Set";

				return $r_player;
			}
			catch (Exception $e) {
				echo $e->getMessage();
			}

		}
		/**
		 * Restores Seed from Given Passphrase
		 * 
		 * @param string $passphrase 
		 * @param array $dictionary
		 */
		private function restore_seed($passphrase, $dictionary):array {
			try {
				if (! isset($passphrase) || ! isset($dictionary)) {
					$response = "Passphrase or Dictionary is not set. Cannot restore seed.";
				}
				else {

					// Split Passphrase Into Array
					preg_match_all('/([0-9]+|[a-zA-Z]+)/', $passphrase, $array);

					// Check Passphrase Validity
					if (count($array[0]) < 2 || count($array[0]) > 3) {
						return [];
					}
					
					if (count($array[0]) == 2) {

						// Extract Word from Passphrase
						$word             =   mb_strtolower($array[0][0]);

						// Extract Number from Passphrase
						$second_segment   =   str_pad(ltrim($array[0][1], "0"), 3, "0", STR_PAD_LEFT);

						// Search Dictionary for the Key by Word
						$first_segment    =   array_search($word, $dictionary, false);

						// Assign Seed Keys
						$r_w_zero         =   0;
						$r_w_first        =   substr($first_segment, 1, 1);
						$r_w_second       =   substr($second_segment, 0, 1);
						$r_w_third        =   substr($second_segment, 2, 1);

						$r_n_first        =   substr($first_segment, 0, 1);
						$r_n_second       =   substr($first_segment, 2, 1);
						$r_n_third        =   substr($second_segment, 1, 1);

						// Restore Segments
						$r_w_seed         =   $r_w_first . $r_w_second . $r_w_third;
						$r_w_seed        -=   100;
						$r_n_seed         =   $r_n_first . $r_n_second . $r_n_third;
						$r_n_seed        -=   100;

						// Prepare response
						$r_seed[0]        =   $r_w_seed;
						$r_seed[1]        =   $r_n_seed;

					}
					// 
					else {
						// Extract Word from Passphrase
						$word             =   mb_strtolower($array[0][1]);

						// Extract Number from Passphrase
						$second_segment   =   str_pad(ltrim($array[0][2], "0"), 3, "0", STR_PAD_LEFT);

						// Search Dictionary for the Key by Word
						$first_segment    =   array_search($word, $dictionary, false);
						$first_segment   +=   1000; // Needs Rethinking to Make More Flexible


						// Assign Seed Keys
						$r_w_zero         =   substr($first_segment, 0, 1);
						$r_w_first        =   substr($first_segment, 2, 1);
						$r_w_second       =   substr($second_segment, 0, 1);
						$r_w_third        =   substr($second_segment, 2, 1);

						$r_n_first        =   substr($first_segment, 1, 1);
						$r_n_second       =   substr($first_segment, 3, 1);
						$r_n_third        =   substr($second_segment, 1, 1);

						// Restore Segments
						$r_w_seed         =   $r_w_zero . $r_w_first . $r_w_second . $r_w_third;
						$r_w_seed        -=   1100;
						$r_n_seed         =   $r_n_first . $r_n_second . $r_n_third;
						$r_n_seed        -=   100;

						// Prepare response
						$r_seed[0]        =   $r_w_seed;
						$r_seed[1]        =   $r_n_seed;
						$r_seed[2]        =   $r_w_zero;

					}
					
					// Add Vars for Debugging

					$this->first_segment    = (is_numeric($first_segment))  ? $first_segment    : "Not Set";
					$this->second_segment   = (is_numeric($second_segment)) ? $second_segment   : "Not Set";
					$this->r_w_zero         = (is_numeric($r_w_zero))       ? $r_w_zero         : "Not Set";
					$this->r_w_first        = (is_numeric($r_w_first))      ? $r_w_first        : "Not Set";
					$this->r_w_second       = (is_numeric($r_w_second))     ? $r_w_second       : "Not Set";
					$this->r_w_third        = (is_numeric($r_w_third))      ? $r_w_third        : "Not Set";
					$this->r_n_first        = (is_numeric($r_n_first))      ? $r_n_first        : "Not Set";
					$this->r_n_second       = (is_numeric($r_n_second))     ? $r_n_second       : "Not Set";
					$this->r_n_third        = (is_numeric($r_n_third))      ? $r_n_third        : "Not Set";
					$this->r_w_seed         = (is_numeric($r_w_seed))       ? $r_w_seed         : "Not Set";
					$this->r_n_seed         = (is_numeric($r_n_seed))       ? $r_n_seed         : "Not Set";
					$this->r_seed           = (is_numeric($r_seed))         ? $r_seed           : "Not Set";

					return $r_seed;
				}
			}
			catch (Exception $e) {
				echo $e->getMessage();
			}
		}
	}
?>