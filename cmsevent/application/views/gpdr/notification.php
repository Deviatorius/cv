<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="gpdr">
    <style>
        #gpdr * {
            -moz-box-sizing: 		border-box; 
            -webkit-box-sizing: 	border-box; 
            box-sizing: 			border-box; 
        }
        #gpdr {
            position: fixed;
            top: 0px;
            left: 0px;
            right: 0px;
            background : #000000;
            height: 144px;
            padding: 10px;
            z-index: 89489489;
        }
        #gpdr .gpdr-icon {
            height: 65%;
            float: left;
            margin-right: 10px;
            margin-top: 16px;
        }
        #gpdr .gpdr-header {
            color: #ff8000;
            margin-bottom: 5px;
            font-size: 3.2em;
        }
        #gpdr .gpdr-body {
            font-size: 2.1em;
            color: white;
        }
        #gpdr .gpdr-link {
            margin-top: 5px;
            font-size:1.2em;
        }
        #gpdr a {
            color: white;
        }
        #gpdr .gpdr-accept {
            float:right;
            margin-top: 10px;
            width: 165px;
            height: 65px;
            font-size: 2em;
        }
    </style>

    <img class="gpdr-icon" src="<?php echo base_url("images/cookie.png") ?>">
    <button class="gpdr-accept">Accept</button>
    <div style="margin-left: 120px;">
        <div class="gpdr-header">Cookies &amp; Privacy Policy</div>
        <div class="gpdr-body">This website uses cookies to ensure a secure experience and identify you as a valid user</div>
        <div class="gpdr-link"><a href="<?php echo base_url("home/privacy") ?>">More Information</a></div>
    </div>
    <script>
        $("#gpdr").ready(function() {
            $(".gpdr-accept").button();
            $(".gpdr-accept").on("click", function() {
                $.ajax({url: "<?php echo base_url("home/accept") ?>", 
                    success: function(result){
                        $("#gpdr").remove();
                    }
                });
            })
        });
    </script>
    
</div>