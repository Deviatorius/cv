<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="policy">

    <h1>Cookie Policy</h1>

    <h3 style="margin-top: 30px">What are Cookies?</h3>
    <p>A Cookie is a small file which is downloaded to your device while browsing a website. These hold a modest amount of information specific to you that can be accesses either by your browser or the server.<br />
    This allows our server to identify you and tailor the site to the data you have been authorised to view. This file is neither a virus nor spyware.</p>
    <p>Cookies have specific purposes depending on their intention. An Authentication Cookie for example will store a unique identification to allow the server to identify you once you have logged in</p>

    <h3 style="margin-top: 30px">What are they used for?</h3>
    <p>Cookies allow servers to identify you between pages and visits, an example of this is an Authentication Cookie. When you sign in the server will automatically install a cookie assigning you an ID that is unique to your specific login. <br />
    As you browse different pages this identifciation is sent back to the server so you don't need to keep re-entering your login details everytime you change page.
    </p>

    <h3 style="margin-top: 30px">What's in a Cookie?</h3>
    <p>A cookie is essentially a table of data containing keys and values. An example would be Firstname, John, Lastname, Smith<br />
    Each page you visit on the site will send this information back to the server to allow the server to identify the visitor
    </p>

    <h3 style="margin-top: 30px">Types of Cookie?</h3>
    <p>There are three main types of cookies:</p>

    <ul>
        <li><b>Session or Authentication</b> - These are used by sites to keep you logged in and track information relevant to your session (such as items added to a shopping basket).</li>
        <li><b>Perminant Cookies</b> - These remain on your system after you have closed your browser. An example would be a "Remeber Me" feature that alows you to be automatically logged in.</li>
        <li><b>Third Party Cookies</b> - These are installed by third parties with the aim of collecting information to carry out research into behaviour, demographics, or for Analytics and Marketing.</li>
    </ul>

    <p>
        Some Cookies are essential and without them the site simply can't function.
        Sites now based in the EU or targetting users in the EU are now required by law to disclose what cookies are in use and get consent for their use of cookies as well as offering an Opt-Out for non-essential cookies.
    </p>

    <h3 style="margin-top: 30px">What cookies do we use?</h3>
    <p>
        We only use Authentication Cookies for this service. These meet the requirements for Essential Cookies and <b>can't</b> be opted out of.<br />
    </p>
    <p>
        These cookies are session only and an information contained in them will be destoyed when you close your browser.<br />
        By logging into this service you consent to the use of all essential cookies
    </p>




    <h1 style="margin-top: 50px">Privacy Policy</h1>
    <h3 style="margin-top: 30px">Data Protection Policy</h3>

    <p>
        Willow Communications Ltd (Willow) needs to keep certain information on its customers, employees, suppliers and third parties 
        to carry out the services it provides, to meet its objectives and to comply with legal obligations.
    </p>

    <p>
        Willow is committed to ensuring any personal data will be dealt with in line with the Data Protection
    Act 2018 and the General Data Protection Regulation (GDPR) 2018.<br />
    To comply with the law, personal information will be collected and used fairly, stored safely and not disclosed to any other person unlawfully.  
    </p>

    <p>
        The aim of this policy is to ensure that everyone handling personal data is fully aware of their obligations and acts in accordance with the Data Protection Act 2018 and the General Data Protection Regulation (GDPR) requirements.<br />
        Before personal information is collected, Willow will consider whether it is necessary.<br />
        Willow will inform people whose information is gathered about the need for its retention, what it will be used for and who will hold the information.<br />
        Personal sensitive information will not be used apart from the exact purpose for which permission was given.
    </p>

    <h3>GDPR Principles</h3>

    <p>In line with the GDPR 2018 principles, Willow will ensure that personal data is:</p>
    <ul>
        <li>collected, stored and processed in a fair, transparent and lawful manner</li>
        <li>collected, stored and processed for a relevant and necessary purpose</li>
        <li>stored in an accurate, up to date and secure manner</li>
        <li>stored for no longer than is necessary</li>
    </ul>

    <p>The definition of ‘processing’ is obtaining, using, holding, amending, disclosing, destroying and deleting personal data. This includes some paper based personal data as well as that kept on computer.</p>



    <h3>Individual’s Rights</h3>
    <p>The individual has a variety of rights about the way their data is processed. These are as follows:</p>

    <ul>
        <li>Where use of personal data requires consent, that consent may be withdrawn at any time</li>
        <li>Where legitimate interest is relied upon to process data, the individual may ask for processing to stop at any time</li>
        <li>The individual may request a copy of the data held on them</li>
        <li>The individual may change or stop the way in which they are communicated with or in which their data is processed and if it is not required for the purpose for which the individual provided it, then it will be stopped</li>
        <li>If the individual is not satisfied with the manner in which their data is processed then they can complain to the Office of the Information Commissioner</li>
    </ul>

    <h3>Processing Personal Data</h3>

    <p>There are six lawful bases for processing (that includes collecting) personal data set out in Article 6 of the GDPR.  At least one must apply whenever personal data is processed.  They are:</p>

    <ul>
        <li>a) <b>consent:</b>  the individual has given clear consent for the organisation to process their personal data for a specific purpose</li>
        <li>b) <b>contract:</b>  the processing is necessary for fulfilling a contract with an individual, or because specific steps need to be taken before a contract is entered into</li>
        <li>c) <b>legal obligation:</b>  the processing is necessary in order to comply with the law (not including contractual obligations)</li>
        <li>d) <b>vital interests:</b>  the processing is necessary to protect someone’s life</li>
        <li>e) <b>public task:</b>  the processing is necessary for performance of a task in the public interest or for official functions, and the task or function has a clear basis in law </li>
        <li>f) <b>legitimate interest:</b>  the processing is necessary for the legitimate interests of the organisation or the legitimate interests of a third party unless there is a good reason to protect the individual’s personal data which overrides those legitimate interests.</li>
    </ul>


    <h3 style="margin-top: 30px">What data is being collected?</h3>
    <p>To provide the service we may process the following information</p>

    <ul>
        <li>Names</li>
        <li>Phone Numbers (where relevant)</li>
        <li>E-Mail Addresses</li>
        <li>Digital Media including (but not limited to) Photos, Video and other types of data uploaded to the server by End Users</li>
        <li>IP Addresses and Activity (For Auditing and Security)</li>
    </ul>

    <p>Personal information is kept in the following formats:</p>

    <ul>
        <li>Electronic</li>
        <li>Paper</li>
    </ul>

    <p>People within Willow who will process personal information are: </p>
    <ul>
        <li>Employees</li>
    </ul>


    <h3 style="margin-top: 30px">Responsibilities</h3>
    <p>The needs we have for processing personal data are as stated in this policy.  Overall responsibility for personal data rests with the Board of Directors.  </p>
    <p>The Board of Directors is responsible for:</p>

    <ul>
        <li>understanding and communicating  obligations under the GDPR</li>
        <li>identifying potential problem areas or risks</li>
        <li>producing clear and effective procedures</li>
    </ul>

    <p>All Directors, employees, customers, suppliers and relevant third parties who process personal information on behalf of Willow must ensure they not only understand but also act in line with this policy and the data protection principles.</p>
    <p>Breach of this policy will result in disciplinary proceedings or other appropriate action.</p>

    <h3 style="margin-top: 30px">Policy implementation</h3>

    <p>To meet our responsibilities Directors, employees, customers, suppliers and relevant third parties will:</p>

    <ul>
        <li>Ensure any personal data is collected in a fair and lawful way</li>
        <li>Explain why it is needed at the start</li>
        <li>Ensure that only the minimum amount of information needed is collected and used</li>
        <li>Ensure the information used is up to date and accurate</li>
        <li>Review the length of time information is held</li>
        <li>Ensure it is kept safely</li>
        <li>Ensure the rights people have in relation to their personal data can be exercised </li>
    </ul>

    <p>We will ensure that:</p>

    <ul>
        <li>Everyone managing and handling personal information is trained to do so</li>
        <li>Anyone wanting to make enquiries about handling personal information, whether an employee, customer, supplier or relevant third party, knows what to do </li>
        <li>Any disclosure of personal data will be in line with our procedures.</li>
    </ul>

    <p>Queries about handling personal information will be dealt with as promptly as possible.</p>

    <h3 style="margin-top: 30px">How will the information be used?</h3>
    <p>The information will be used to enable users to create marketing and other digital services as well as providing a personalised login for user(s)</p>
    <p>E-Mail Addresses will be used to send notifications from the service or to send status information and planned upgrades</p>

    <h3 style="margin-top: 30px">Gathering and checking</h3>

    <p>Before personal information is collected, Willow will consider whether it is necessary.<br />
    Willow will inform people whose information is gathered about the need for its retention, what it will be used for and who will hold the information.<br />
    Personal sensitive information will not be used apart from the exact purpose for which permission was given.</p>


    <h3 style="margin-top: 30px">How long will the data be stored for?</h3>
    <p>Willow will ensure that information is kept according to the legal retention period guidelines.</p>
    <p>If a user decides they no longer require use of the service or their account or service is terminated we will delete any data we hold on them for this service</p>
    <p>Data uploaded to this service can be deleted at anytime by the uploaded by logging into their account</p>


    <h3 style="margin-top: 30px">Data Security</h3>
    <p>The Company will take steps to ensure that personal data is kept secure at all times against unauthorised or unlawful loss or disclosure. The following measures will be taken:</p> 
    <ul>
        <li>password protection for information kept electronically</li>
        <li>paper records kept in locked drawers/cabinets </li>
    </ul>

    <h3 style="margin-top: 30px">Procedure in case of a breach</h3>
    <p>When a breach of data protection occurs, consideration will be given to reviewing practices. In addition Willow will consider whether the breach should be reported to the Information Commissioner and/or to any affected parties.</p>



    <h3 style="margin-top: 30px">Subject access rights</h3>

    <p>Anyone whose personal information Willow processes has the right to know:</p>

    <ul>
        <li>What information we hold and process about them</li>
        <li>How to gain access to this information</li>
        <li>How to keep it up to date</li>
        <li>What we are doing to comply with the Act.</li>
    </ul>

    <p>They also have the right to prevent processing of their personal data in some circumstances and the right to correct, rectify, block or erase information regarded as wrong.<br/>
    Individuals have a right under the Act to access certain personal data being kept about them on computer and certain files.  Any person wishing to exercise this right should apply in writing to the Managing Director.<br/>
    The individual’s right to access their information shall not adversely affect the rights and freedoms of others and they will not be able to access the personal data of third parties without the explicit consent of that third party.<br/>
    We may also require proof of identity before access is granted. <br/>
    Queries about handling personal information will be dealt with as quickly as possible but we will work towards providing it within the 40 days required by the Act from receiving the written request. </p>

    <p style="margin-top: 100px;">
        &nbsp;
    </p>

</div>
