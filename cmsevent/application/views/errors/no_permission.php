<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<style>
	.error_container {
		width: 500px;
		height: 300px;
		border: 2px dashed #f5dc08;
		text-align: center;
		background: #f8fbe0 url(<?php echo base_url('images/warning-15.png'); ?>);
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center center;
		position: relative;
		padding: 10px;
		margin:0 auto;
		margin-top: 50px;
	}
	.error_body {
		margin-top: 40px;
		font-size: 1.50em;
	}
	.error_footer {
		position: absolute;
		bottom: 0px;
		text-align: center;
		width: 100%;
	}
</style>

<div class="error_container">
	<h2 class="error_title">Access Denied</h2>
	<p class="error_body">
		<?php
			if (isset($error))
				echo 	$error;
			else
				echo 	html_escape("You don't have permission to access the requested resource");
		 ?>		
	</p>
	<p class="error_footer">Please contact support for further information</p>
</div>