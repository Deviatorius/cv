<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<style>
	.error_container {
		width: 600px;
		height: 300px;
		text-align: center;
		background: url(<?php echo base_url('images/warning-15.png'); ?>);
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center center;
		position: absolute;
		padding: 10px;
		margin: auto;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		color: white;
	}
	.error_body {
		margin-top: 40px;
		font-size: 1.50em;
		color: white;
	}
	.error_footer {
		position: absolute;
		bottom: 0px;
		text-align: center;
		width: 100%;
		color: white;
	}
</style>

<div class="error_container">
	<h2 class="error_title">An Error Occured</h2>
	<p class="error_body">
		<?php
			if (isset($error))
				echo 	$error;
			else
				echo 	validation_errors();
		 ?>		
	</p>
	<p class="error_footer">Please contact support for further information</p>
</div>