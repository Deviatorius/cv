<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<img class="section_logo" src="<?php echo base_url("images/info.png"); ?>">
<h1>Welcome to the Event Entertainment Portal</h1>
<p>Please select one of the options below or open and close the main menu by clicking the top bar.
</p>
<hr style="clear:both; margin-top: 35px" />

<?php

    // Messages
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);

 
?>

<div id="main">

    <div id="menu">
        <div id="menu-content" class="">

            <!-- Main Menu -->
            <div id="shoutout" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Shoutout</div>
            <div id="upload" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Share Picture</div>
            <div id="request" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Song Request</div>
            <div id="karaoke" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Karaoke Request</div>
            <div id="musicvideo" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Music Videos</div>

            <!-- Shoutout Menu -->
            <div id="shoutout-text" class="display-none shoutout-menu multi-bright" ><?php echo form_textarea(['id' => 'shoutout-message', 'name' => 'message', 'data' => '3', 'type' => 'textarea', 'placeholder' => 'Message', 'maxlength' => '500' ]); ?></div>
            <div id="shoutout-submit" onclick="AjaxSubmitHandler(this)"  class="cursor display-none shoutout-menu multi-bright" menu="shoutout-menu"><?php echo form_submit('data[submit]', 'Save'); ?><div class="menu-arrow multi-bright"></div><div class="text">Submit</div></div>


            <!-- Upload Menu -->
            <div id="upload-select" class="cursor display-none upload-menu multi-bright" onclick="input_click(this);"> <div class="menu-arrow multi-bright"></div>Select Image</div>
            
            <div id="upload-selected" class="display-none upload-menu-confirm multi-bright"  onclick="fullscreen(this);"  style="height: 500px;"> <img id="imagePreview" /><img id="backPreview" /></div>
            <div id="upload-text" class="display-none upload-menu-confirm multi-bright" ><?php echo form_input(['id' => 'caption', 'name' => 'caption', 'data' => '3', 'placeholder' => 'Caption']); ?></div>
            <div id="upload-submit" onclick="AjaxSubmitHandler(this)"  class="cursor display-none upload-menu-confirm multi-bright" menu="upload-menu-confirm">
                <!-- Button -->
                <?php echo form_submit('data[submit]', 'Save'); ?><div class="menu-arrow multi-bright"></div><div class="text">Submit</div>
                <!-- Progress Bar -->
                <div id="progressbar" style="display: none;"><div id="progresslabel">Please Wait</div></div>
            </div>
            
            <!-- Song Request Menu -->
            <div id="request-title" class="display-none request-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Music Search</div>
            <div id="request-search" class="display-none request-menu multi-bright" ><?php echo form_textarea(['id' => 'music-search', 'data' => '4', 'name' => 'Title', 'placeholder' => 'Title or Artist', 'style' => 'height: 100%; text-align: center;'], ); ?></div>
            <div id="request-submit" onclick="AjaxSubmitHandler(this)" class="cursor display-none request-menu multi-bright" menu="request-menu"><div class="menu-arrow multi-bright"></div>Search</div>

            <!-- Karaoke Request Menu -->
            <div id="karaoke-title" class="display-none karaoke-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Karaoke Search</div>
            <div id="karaoke-search" class="display-none karaoke-menu multi-bright" ><?php echo form_textarea(['id' => 'karaoke-search', 'data' => '4', 'name' => 'Title', 'placeholder' => 'Title or Artist', 'style' => 'height: 100%; text-align: center;'], ); ?></div>
            <div id="karaoke-submit" onclick="AjaxSubmitHandler(this)" class="cursor display-none karaoke-menu multi-bright" menu="karaoke-menu"><div class="menu-arrow multi-bright"></div>Search</div>

            <!-- Music Video Request Menu -->
            <div id="musicvideo-title" class="display-none musicvideo-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Music Video Search</div>
            <div id="musicvideo-search" class="display-none musicvideo-menu multi-bright" ><?php echo form_textarea(['id' => 'musicvideo-search', 'data' => '4', 'name' => 'Title', 'placeholder' => 'Title or Artist', 'style' => 'height: 100%; text-align: center;'], ); ?></div>
            <div id="musicvideo-submit" onclick="AjaxSubmitHandler(this)" class="cursor display-none musicvideo-menu multi-bright" menu="musicvideo-menu"><div class="menu-arrow multi-bright"></div>Search</div>

            <!-- QR Menu -->
            <!-- <div id="qr-notice" class="display-none qr-menu multi-bright text" style="-webkit-box-pack: center; display: block;">QR Code</div>
            <div id="qr-show" class="display-none qr-menu" style="height: 560px;"></div> -->

            <!-- Dialog -->
            <div id="dialog-title" class="display-none dialog-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Dialog</div>
            <div id="dialog-content" class="display-none dialog-menu multi-bright" style="-webkit-box-pack: center !important; display: block; text-align: center; height: 200px !important; padding: 2px;"></div>

            <!-- Search Results -->
            <div id="result-list" class="display-none result-menu multi-bright" style="height: 500px; display: block; overflow-y: scroll; font-size: 3.5em;"></div>
            <div id="result-singer" class="display-none result-menu multi-bright" style="font-size: 3.5em;"><?php echo form_input(['id' => 'karaoke-singer', 'name' => 'caption', 'placeholder' => 'Singer Name']); ?></div>
            
            <div id="result-list-holder" style="display: none;" class="display-none result-menu multi-bright">
                <input style="display: none;" data="1" value=""></input>
                <input style="display: none;" data="2" value=""></input>
                <input style="display: none;" data="3" value=""></input>
                <input style="display: none;" data="4" value=""></input>
            </div>
            <div id="result-submit" onclick="AjaxSubmitHandler(this)" menu="result-menu" class="cursor display-none result-menu multi-bright"><div class="menu-arrow multi-bright"></div>Request</div>

        </div>
        <div id="menu-button">
            <div id="back" class="cursor multi-bright" valign="center">Back</div>
        </div>
    </div>
</div>


<!------ START of Hidden forms -->

<!-- Image Upload -->
<?php echo form_open_multipart('home/upload', ['id' => 'upload-submit-form', 'style' => 'display: none;', 'get' => 'home/upload']); ?>
<?php echo form_upload("UPLOAD", "", ['id' => 'upload-select-input', 'accept' => 'image/*']) ?>
<?php echo form_input("CAPTION", "", ['id' => 'caption-form', 'data' => '3']); ?>
<?php echo form_close(""); ?>

<!-- Shoutout -->
<?php echo form_open('home/shoutout', ['id' => 'shoutout-submit-form', 'style' => 'display: none;', 'get' => 'home/shoutout']); ?>
<?php echo form_textarea("MESSAGE", "", ['id' => 'shoutout-textarea', 'data' => '3']); ?>
<?php echo form_close(""); ?>

<!-- Track Search -->
<?php echo form_open( "home/search/1", ['id' => 'request-submit-form', 'style' => 'display: none;', 'get' => 'home/search/1'] ); ?>
<?php echo form_input(['name' => 'data[params]', 'data' => '4'], ['class' => 'form_input']); ?> 
<?php echo form_input(['name' => 'data[type]', 'data' => '5'], ['class' => 'form_input']); ?> 
<?php echo form_close(""); ?>

<!-- Karaoke Search -->
<?php echo form_open( "home/search/3", ['id' => 'karaoke-submit-form', 'style' => 'display: none;', 'get' => 'home/search/3'] ); ?>
<?php echo form_input(['name' => 'data[params]', 'data' => '4'], ['class' => 'form_input']); ?> 
<?php echo form_input(['name' => 'data[type]', 'data' => '5'], ['class' => 'form_input']); ?> 
<?php echo form_close(""); ?>

<!-- Music Video Search -->
<?php echo form_open( "home/search/2", ['id' => 'musicvideo-submit-form', 'style' => 'display: none;', 'get' => 'home/search/2'] ); ?>
<?php echo form_input(['name' => 'data[params]', 'data' => '4'], ['class' => 'form_input']); ?> 
<?php echo form_input(['name' => 'data[type]', 'data' => '5'], ['class' => 'form_input']); ?> 
<?php echo form_close(""); ?>

<!-- Track Request -->
<?php echo form_open('home/request', ['id' => 'result-submit-form', 'style' => 'display: none;', 'get' => 'home/request']); ?>
<?php echo form_input("TRACK", "", ['data' => '1']); ?>
<?php echo form_input("GID", "", ['data' => '2']); ?>
<?php echo form_input("SINGER", "", ['data' => '3']); ?>
<?php echo form_input("TYPE", "", ['data' => '4']); ?>
<?php echo form_close(""); ?>

<!------ END of Hidden Forms -->


<!-- DO QR CODE -->
<?php 
	if ( ! empty($_SESSION['USER']['ROOMCODE']) ) {
		?>
			<script>
				$(document).ready(function() {
					$('#qr-show').qrcode({width: 530,height: 530,text: "<?php echo html_escape($_SERVER['HTTP_REFERER'] . "login/" . $_SESSION['USER']['ROOMCODE']) ?>"});
				});
			</script>
		<?php
	}
?>


<script>

    // Variables
    var SubmissionStatus = false;
    var ActiveMenu = ".main-menu";
    var fs = Boolean;
    var inputFile = document.getElementById("upload-select-input");

    // Initialize Progress Bar
    $("#progressbar").progressbar({
        value: false
    });
    $(" .ui-progressbar-value ").css({ 'background': 'url(images/white-40x100.png) #ffffff repeat-x 50% 50%;' });


    // Ready
    $(document).ready(function() { 

        // Back
        $("#back").click(function() {
            back();
        });

        // Menu Select
        $(".main-menu").click(function() {
            target = this.id;
            unload_menu('.main-menu');
            setTimeout(function(){
                load_menu("." + target + "-menu");
                setTimeout(function(){
                    $("#no-back").css("display", "none");
                }, 150);
            }, 160)
        });

        // Handles result selection
        $(document).on("click", " #result-list .result ", function(){
            $(" #result-list .result ").removeClass("selected");
            var data1 = $(" #result-list-holder [data='1']");
            var data2 = $(" #result-list-holder [data='2']");
            var data4 = $(" #result-list-holder [data='4']");

            $(data1).attr("value", "");
            $(data2).attr("value", "");
            $(data4).attr("value", "");

            $(data1).attr("value", $(this).attr("filename") );
            $(data2).attr("value", $(this).attr("gid") );
            $(data4).attr("value", $(this).attr("type") );

            $(this).addClass("selected");
        });

        // Display Picture Before Upload
        inputFile.onchange = evt => {
            const [file] = inputFile.files
            
            if (file) {
                $("#upload-submit").css("opacity", "1");
                $("#upload-selected").css("opacity", "1");
                imagePreview.src = URL.createObjectURL(file)
                backPreview.src = URL.createObjectURL(file)

                unload_menu('.upload-menu');
                setTimeout(function(){
                    load_menu(".upload-menu-confirm");
                    setTimeout(function(){
                        $("#no-back").css("display", "none");
                    }, 150);
                }, 160)
            }
        }

        // Pass Karaoke Singer Data
        $(' #karaoke-singer ').on("change", function(){
            $(' #result-list-holder [data="3"] ').attr("value", $(this).val());
        });

    });


    /**
     * Returns To Main Menu
     */
    function back(){
        if (ActiveMenu == ".main-menu") {
            $(" #back ").addClass("shake");
            setTimeout(function(){
                $(" #back ").removeClass("shake");
            }, 250);
        }
        else {  
            unload_menu(ActiveMenu);
            setTimeout(function(){
                load_menu(".main-menu");
            }, 130)
            $("#no-back").css("display", "block");
            document.getElementById('upload-select-input').value = "";
            if (ActiveMenu == ".upload-menu"){
                $("#upload-submit").css("opacity", "0");
                $("#upload-selected").css("opacity", "0");

            }
        }
    }


    /**
     * Removes Previous Menu From View
     * @param {*} target Shared Class for example ".example-menu"
     */
    function unload_menu(target){
        console.log("Unload Menu - " + target);

        $(target).addClass("unload");

        setTimeout(function(){
            $(target).addClass("display-none");
            $(target).removeClass("unload");
            $(target).removeClass("display");
        }, 150)
    }


    /**
     * Pulls Targeted Menu Into View
     * @param {*} target Shared Class for example ".example-menu"
     */
    function load_menu(target){
        console.log("Load Menu - " + target);
        ActiveMenu = target;

        $(target).removeClass("display-none");
        $(target).addClass("display");
        $(target).addClass("load");

        setTimeout(function(){
            $(target).removeClass("load");
        }, 150)
    }


    /**
     * Input Click
     * Item ID + "-input"
     * id="item-input" for click
     */
    function input_click(item) {
        item = item.id;
        document.getElementById(item + "-input").click();
    }


    /**
     * Toggles Selected Item to FullScreen
     */
    function fullscreen(item) {
        if ( fs == true ) {
            $(item).removeClass("fullscreen");
            fs = false;
        } else {
            $(item).addClass("fullscreen");
            fs = true;
        }
    }


    /**
     * Form Submit Functions
     */
    function AjaxSubmitHandler( item ) {

        if ( SubmissionStatus === true ) return;
        SubmissionStatus = true;

        // Color Submission button
        $(item).css("background", "#00f1558c");

        // Disable Back Button
        $(" #back ").fadeOut(150);

        // Set Objects
        menu = $(item).attr("menu");
        item = item.id;
        form = $("#" + item + "-form");
        action = $(form).attr("action");

        // Make Upload Progress Bar
        if ( item ==  "upload-submit" ) {

            $(" #upload-submit .menu-arrow ").css("display", "none");
            $(" #upload-submit .text").html("");
            $(" #upload-submit #progressbar ").css("display", "unset");
        }

        // Music Request Function
        if ( item == "request-submit" || item == "musicvideo-submit") {
            $(" #result-singer ").hide();
        }

        // Karaoke Request Function
        if ( item == "karaoke-submit" ) {
            $(" #result-singer ").show();
        }        

        // Build Data Array
        var data = [
            undefined,
            $("." + menu + " [data='1']").val(),
            $("." + menu + " [data='2']").val(),
            $("." + menu + " [data='3']").val(),
            $("." + menu + " [data='4']").val(),
            $("." + menu + " [data='5']").val()
        ];

        // Transfer Data to Form
        for (let i = 0; i < data.length; i++) {
            ( data[i] === undefined ) ? "" : transferData(data[i], i, item);
        }
        
        // Grab Form Data
        var formData = new FormData( document.getElementById( item + "-form" ) );

        // Set AJAX Parameters
        var params = {
            type: "POST",
            url: "<?php echo base_url(); ?>" + $(form).attr("get"),
            data: formData,
            cache: false,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function (msg) {
                // Log Message
                console.log(msg);

                setTimeout(function(){

                    // Function Returned False
                    if ( msg.status == false ) {
                        // Set Messages
                        $(" #dialog-title ").html("Please try again:").css("background", "#e00e0e61");
                        $(" #dialog-content ").html(msg.message);

                        // Restore Submission Button Color
                        $("#" + item).css("background", "#00000061");

                        // Unload Menu
                        ( menu == "upload-menu" ) ? unload_menu("." + menu + "-confirm") : unload_menu("." + menu);
                        
                        // Load Message Menu
                        setTimeout(function(){
                            load_menu(".dialog-menu");
                        }, 160)

                        // Return
                        setTimeout(function(){         
                            SubmissionStatus = false;
                            $(" #back ").fadeIn(150);
                            unload_menu(".dialog-menu")
                            $(" #dialog-title ").css("background", "#00000061");
                            setTimeout(function(){
                                load_menu("." + menu);
                            }, 160)
                        }, 3000);
                    }

                    // Function Returned True
                    else {

                        // Set Messages
                        $(" #dialog-title ").html("Success!").css("background", "#00f1558c");
                        $(" #dialog-content ").html(msg.message);
                        $(" #request-message ").val("");

                        // Restore Submission Button Color
                        $("#" + item).css("background", "#00000061");

                        // Unload Menu
                        ( menu == "upload-menu" ) ? unload_menu("." + menu + "-confirm") : unload_menu("." + menu);

                        // Restore Submission Button Color
                        $(" #upload-text input ").val("");                        

                        // Display Retrieved Data
                        if ( menu == "request-menu" || menu == "karaoke-menu" || menu == "musicvideo-menu") {

                            // Clear Previous Search Results
                            $(" #result-list ").empty();

                            // Add Results to Field
                            for (const gid in msg) {
                                if (Object.hasOwnProperty.call(msg, gid)) {
                                    const element = msg[gid];
                                    $(" #result-list ").append("<div class='result' gid='" + element.gid + "' type='" + element.type + "' filename='" + element.filename + "'>" + element.title + " - <span>" + element.artist + "</span></div>");
                                    
                                }
                            }

                            // Load Message Menu
                            setTimeout(function(){
                                $(" #back ").fadeIn(150);
                                load_menu(".result-menu");   
                                SubmissionStatus = false;
                            }, 160)

                        // Post Data
                        } else {

                            // Load Message Menu
                            setTimeout(function(){
                                load_menu(".dialog-menu");
                            }, 160)

                            // Return
                            setTimeout(function(){         
                                SubmissionStatus = false;
                                $(" #back ").fadeIn(150);
                                unload_menu(".dialog-menu")
                                $(" #dialog-title ").css("background", "#00000061");
                                document.getElementById('upload-select-input').value = "";
                                $("#upload-submit").css("opacity", "0");
                                $("#upload-selected").css("opacity", "0");

                                setTimeout(function(){
                                    $(" #result-list .selected").removeClass("selected");

                                    if ( menu == "upload-menu-confirm" ) {
                                        load_menu(".main-menu");
                                    } else {
                                        load_menu("." + menu);

                                    }
                                }, 160)
                            }, 3000);
                        }
                    }

                    // For File Upload Function
                    if ( item ==  "upload-submit" ) {

                        // Reset File Upload
                        $(" #upload-submit .menu-arrow ").css("display", "unset");
                        $(" #upload-submit .text").html("Submit");
                        $(" #upload-submit #progressbar ").css("display", "none");
                    }
                }, 1200);

            },
            error: function (msg) {
                // Log Message
                console.log(msg);   

                // Reset locks
                $(" #back ").fadeIn(150);
                SubmissionStatus = false;



            }
        }

        // Set Options AJAX parameters
        if ( menu == "upload-menu-confirm" ) {
            params.xhr = function() {
                var xhrobj = $.ajaxSettings.xhr();
                if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        console.log("Uploaded " + total + " - " + percent + "%" );
                        showProgress(event, position, total, percent);
                    }, false);
                }
                return xhrobj;
            }
        }

        // Start AJAX
        $.ajax(params);
        
        $(" #result-submit-form > input").attr("value", "");
        $(" #result-list-holder > input ").attr("value", "");

    }


    /**
     * Transfers Data From Menu Fields to Hidden Forms.
     */
    function transferData(data, i, form) {
        switch (i) {
            // Additional 1
            case 1:
                $("#" + form + "-form [data='1']").attr("value", data);
                console.log ("Form Data Transfer " + i + ": Value (" + data + ") - Transfered (" + $("#" + form + "-form [data='1']").attr("value", data) + ")" );
                break;
            // Additional 2
            case 2:
                $("#" + form + "-form [data='2']").attr("value", data);
                console.log ("Form Data Transfer " + i + ": Value (" + data + ") - Transfered (" + $("#" + form + "-form [data='2']").attr("value", data) + ")" );
                break;
            // Message
            case 3:
                // Handle Input and Textbox
                if ( form == "shoutout-submit" ) {
                    $("#" + form + "-form [data='3']").html(data);
                    console.log ("Form Data Transfer " + i + ": Value (" + data + ") - Transfered (" + $("#" + form + "-form [data='3']").html(data) + ")" );
                }
                else {
                    $("#" + form + "-form [data='3']").attr("value", data);
                    console.log ("Form Data Transfer " + i + ": Value (" + data + ") - Transfered (" + $("#" + form + "-form [data='3']").attr("value", data) + ")" );
                }
                break;
            // Search
            case 4:
                $("#" + form + "-form [data='4']").attr("value", data);
                console.log ("Form Data Transfer " + i + ": Value (" + data + ") - Transfered (" + $("#" + form + "-form [data='4']").attr("value", data) + ")" );
                break;
            // Type
            case 5:
                $("#" + form + "-form [data='5']").attr("value", data);
                console.log ("Form Data Transfer " + i + ": Value (" + data + ") - Transfered (" + $("#" + form + "-form [data='5']").attr("value", data) + ")" );
                break;
            default: 
                break;
        }
    }


    /**
    * Updates Progress Bar
    * 
    * @return Void
    */
    function showProgress(ev, position, total, percentComplete) {
        $('#upload-status-message').text("Uploading Asset - " + bytesToSize(position) + " of " + bytesToSize(total));
        $("#progressbar").progressbar("value", percentComplete);
        $("#progresslabel").text(percentComplete + "%");
    }

</script>