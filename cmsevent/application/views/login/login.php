<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?php echo SITE_TITLE ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=800, initial-scale=0.50, user-scalable=no">
		<meta name="description" content="<?php echo SITE_TITLE ?>">
		<meta name="author" content="Robin Hickmott">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url("images/apple/apple-touch-icon-57x57.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url("images/apple/apple-touch-icon-114x114.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url("images/apple/apple-touch-icon-72x72.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url("images/apple/apple-touch-icon-144x144.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo base_url("images/apple/apple-touch-icon-60x60.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo base_url("images/apple/apple-touch-icon-120x120.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo base_url("images/apple/apple-touch-icon-76x76.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo base_url("images/apple/apple-touch-icon-152x152.png"); ?>" />
		<link rel="shortcut icon" href="<?php echo base_url("favicon.ico"); ?>" />
		<link href="<?php echo base_url("css/login.css"); ?>" rel="stylesheet">
		<link href="<?php echo base_url("css/jquery-ui.min.css"); ?>" rel="stylesheet" />
		
		<!-- Import Scripts -->
		<script type="text/javascript" src="<?php echo base_url("js/jquery-3.6.0.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("js/jquery-ui.min.js"); ?>"></script>

  	</head>
  	<body>
		<div class="body">

			<div class="bg"></div>
			<div class="bg2"></div>
			<div id="logo"></div>
			<div class="login">
				<form action="<?php echo $request; ?>" method="post">
					<input type="text" placeholder="ACCESS CODE" name="X_LOGIN_REQUEST[ROOMCODE]" autocomplete="off"><br>
					<input type="submit" value="Go!">
				</form>
			</div>



			<div class="grad"></div>
			<div class="header">
				<div class="error">
					<?php
						switch($flags) {
							case "failed":
								echo('The Entered Access Code was not Valid');	break;
							case "security":
								echo('Your session has expired');				break;
							default:
								echo('&nbsp;');									break;
						}
					?>
				</div>
			</div>
			<br>

		</div>
	</body>

	<?php
		if ( SITE_USES_GPDR && empty($_SESSION['GPDR']) ) {
			echo $this->load->view("gpdr/notification");
		}
	?>

</html>