<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<?php   
        
    // Ensure Set
    if ( empty($title) )        $title      = "Document";
    if ( empty($content) )      $content    = "No Content was Supplied";

    // Output Header
    echo $this->load->view("panel/header", ["title" => $title]);

?>

<body>
    <div id="header">
        <div class="header-top">
            <div class="header-left"><img class="logo" src="<?php echo base_url("images/svmedia.png"); ?>"></div>
            <div class="header-right">

            </div>				
        </div>
        <div class="header-navigation">
            <a href="<?php echo base_url("home") ?>"><div class="nav-option">Home</div></a>
        </div>        
    </div>

    <div id="content">
        <?php echo($content); ?>
    </div>
</body>

<?php

    // Output Footer
    echo $this->load->view("panel/footer", []);

?>

<?php
    if ( SITE_USES_GPDR && empty($_SESSION['GPDR']) ) {
        echo $this->load->view("gpdr/notification");
    }
?>