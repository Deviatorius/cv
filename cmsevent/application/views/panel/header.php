<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?php echo($title); ?></title>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=800, initial-scale=0.50, user-scalable=no">
		<meta name="description" content="Willow Client Portal">
		<meta name="author" content="Robin Hickmott">
		<link rel="manifest" href="manifest.json">
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo base_url("images/apple/apple-touch-icon-57x57.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url("images/apple/apple-touch-icon-114x114.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url("images/apple/apple-touch-icon-72x72.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url("images/apple/apple-touch-icon-144x144.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo base_url("images/apple/apple-touch-icon-60x60.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo base_url("images/apple/apple-touch-icon-120x120.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo base_url("images/apple/apple-touch-icon-76x76.png"); ?>" />
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo base_url("images/apple/apple-touch-icon-152x152.png"); ?>" />
		<link rel="shortcut icon" href="<?php echo base_url("favicon.ico"); ?>" />
		
		<!-- Import Style Sheets -->
		<link href="<?php echo base_url("css/jquery-ui.min.css"); ?>" rel="stylesheet" />
		<!-- <link href="<?php // echo base_url("css/jquery-ui.theme.min.css"); ?>" rel="stylesheet" /> -->
		<link href="<?php echo base_url("css/main.css?version=" . SITE_VERSION); ?>" rel="stylesheet" />
		<?php
			if (isset($sheets) && is_array($sheets)) {
				foreach ($sheets as $sheet) {
					echo('<link href="' . $sheet . ( strstr($sheet, "?") === FALSE ? "?" : "&" ) . "version=" . SITE_VERSION . "\" rel=\"stylesheet\">\n");
				}
			}
		?>

		<!-- Import Scripts -->
		<script type="text/javascript" src="<?php echo base_url("js/jquery-3.6.0.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("js/jquery-ui.min.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("js/qrcode.js"); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("home/js?version=" . SITE_VERSION); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("js/system.js?version=" . SITE_VERSION); ?>"></script>
		<?php
			if (isset($scripts) && is_array($scripts)) {
				foreach ($scripts as $script) {
					echo('<script type="text/javascript" src="' . $script . ( strstr($script, "?") === FALSE ? "?" : "&" ) . "version=" . SITE_VERSION . "\"></script>\n");
				}
			}
		?>
  </head>
	