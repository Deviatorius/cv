<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<div id="footer">
	
	<span class="footer_left">&copy; 2022 Willow Communications Ltd</span>
	<span class="footer_right"><?php if ( SITE_USES_GPDR ) echo '<a href="' . base_url("home/privacy") . '">Privacy &amp; Cookie Policy</a>'; ?> &nbsp&nbsp Version <?php echo(SITE_VERSION) ?></span>
</div>