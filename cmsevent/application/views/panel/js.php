<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Core Javascript File for Passing Codeignitor Parameters into the Global Space
	 */

?>

/**
 * @var String The Base URL of the Control Panel
 */
var ci_base_url = '<?php echo base_url(); ?>';


/**
 * Returns the CodeIgnitor Base URL
 *
 * @return string    The Path
 */
function base_url(address) {
	if ( typeof address !== "string" )	address = "";
	return "<?php echo base_url(); ?>" + address;
}
