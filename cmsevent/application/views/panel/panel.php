<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
	<?php echo($header); ?>
	<body>
		<div id="header">
			<div class="header-top">
				<div class="header-right">
					<div class="name"><?php echo html_escape($_SESSION['USER']['SITE']) ?></div>
					<div class="code">Room Code <?php echo html_escape($_SESSION['USER']['ROOMCODE']) ?></div>
				</div>				
				<div id="arrows-down">
					<div></div><div></div>
				</div>
				<div id="arrows-up">
					<div></div><div></div>
				</div>
				
				<div id="close" style="display: none;"></div>
				<div id="open" style="display: unset;"></div>
			</div>
			<div class="header-navigation">
				<div class="bg"></div>
				<div class="bg2"></div>
				<div id="logo"></div>
				<div class="menu">
					<?php
						if (isset($navigation) && is_array($navigation)) {
							foreach ($navigation as $nav) {
								echo("<a href=\"" . html_escape($nav['link']) . "\"><div class=\"nav-option {$nav['class']}\">" . html_escape($nav['text']) . "</div></a>");
							}
						}
					?>
				</div>
			</div>
		</div>
		<div id="content">
			<?php
				if ( ! empty($critical_error) ) {
					echo '<div class="form_error" style="margin: 20px 10px 40px 10px; padding: 15px; text-align: center; border: 1px black solid">';
					echo '<b>' . html_escape($critical_error) . '</b>';
					echo "</div>";
				}
			?>
			<?php echo($content); ?>
		</div>
		<?php echo($footer); ?>
	</body>
	<?php
		if ( SITE_USES_GPDR && empty($_SESSION['GPDR']) ) {
			echo $this->load->view("gpdr/notification");
		}
	?>
</html>


<script>

	$(document).on("click", "#open", function(){
		$(" #header ").removeClass("retract");
		$(" #header ").addClass("expand");
		$(" #open ").css("display", "none");
		$(" #close ").css("display", "unset");
		$(" #arrows-down ").css("display", "none");
		$(" #arrows-up ").css("display", "unset");
	});

	$(document).on("click", "#close", function(){
		$(" #header ").removeClass("expand");
		$(" #header ").addClass("retract");
		$(" #open ").css("display", "unset");
		$(" #close ").css("display", "none");
		$(" #arrows-down ").css("display", "unset");
		$(" #arrows-up ").css("display", "none");
	});



</script>