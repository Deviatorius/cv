<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<img class="section_logo" src="<?php echo base_url("images/info.png"); ?>">
<h1>Play games wether you're young or old, there's fun for everyone.</h1>
<p>Please select one of the options below or open and close the main menu by clicking the top bar.
</p>
<hr style="clear:both; margin-top: 35px" />

<?php

    // Messages
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);

 
?>

<div id="main">

    <div id="menu">
        <div id="menu-content" class="">

            <!-- Main Menu -->
            <div id="kids" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>For Kids</div>
            <div id="lotto" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Lottery</div>
            <div id="cnc" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Command & Conquer</div>
            <div id="event" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Event Games</div>
            <div id="sports" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Sports</div>

            <!-- Kids Menu -->
            <div id="kids-title" class="display-none kids-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Title</div>
            <div id="kids-text" class="display-none kids-menu multi-bright"></div>
            <div id="kids-button" class="cursor display-none kids-menu multi-bright"><div class="menu-arrow multi-bright"></div>Button</div>

            <!-- Lotto Menu -->
            <div id="lotto-title" class="display-none lotto-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Title</div>
            <div id="lotto-text" class="display-none lotto-menu multi-bright"></div>
            <div id="lotto-button" class="cursor display-none lotto-menu multi-bright"><div class="menu-arrow multi-bright"></div>Button</div>

            <!-- CNC Menu -->
            <div id="cnc-title" class="display-none cnc-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Title</div>
            <div id="cnc-text" class="display-none cnc-menu multi-bright"></div>
            <div id="cnc-button" class="cursor display-none cnc-menu multi-bright"><div class="menu-arrow multi-bright"></div>Button</div>

            <!-- Event Menu -->
            <div id="event-title" class="display-none event-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Title</div>
            <div id="event-text" class="display-none event-menu multi-bright"></div>
            <div id="event-button" class="cursor display-none event-menu multi-bright"><div class="menu-arrow multi-bright"></div>Button</div>

            <!-- Sports Menu -->
            <div id="sports-title" class="display-none sports-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Title</div>
            <div id="sports-text" class="display-none sports-menu multi-bright"></div>
            <div id="sports-button" class="cursor display-none sports-menu multi-bright"><div class="menu-arrow multi-bright"></div>Button</div>

            <!-- Dialog -->
            <div id="dialog-title" class="display-none dialog-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Dialog</div>
            <div id="dialog-content" class="display-none dialog-menu multi-bright" style="-webkit-box-pack: center !important; display: block; text-align: center; height: 200px !important; padding: 2px;"></div>


        </div>
        <div id="menu-button">
            <div id="back" class="cursor multi-bright" valign="center">Back</div>
        </div>
    </div>
</div>





<!-- Hidden forms, use if need to implement them inside the menu and transfer inputs from menu to hidden forms upon submit -->
<?php //echo form_open_multipart('home/index', ['id' => 'upload-form', 'style' => 'display: none;']); ?>
<?php //echo form_upload("UPLOAD", "", ['id' => 'upload-select-input', 'accept' => 'image/*']) ?>
<?php //echo form_input("CAPTION", "", ['id' => 'caption-form']); ?>
<?php //echo form_close(""); ?>

<?php //echo form_open('home/index', ['id' => 'shoutout-form', 'style' => 'display: none;']); ?>
<?php //echo form_textarea("MESSAGE", "", ['id' => 'shoutout-textarea']); ?>
<?php //echo form_close(""); ?>


<script>

    $(document).ready(function() { 
 
    });




    /**
     * MENU SCRIPTS
     */

    var ActiveMenu = ".main-menu";

    // On Load
    $(document).ready(function(){

        // Back
        $("#back").click(function() {
            back();
        });


        // Menu Select
        $(".main-menu").click(function() {
            target = this.id;
            unload_menu('.main-menu');
            setTimeout(function(){
                load_menu("." + target + "-menu");
                setTimeout(function(){
                    $("#no-back").css("display", "none");
                }, 150);
            }, 160)
        });
    });


    /**
     * Returns To Main Menu
     */
    function back(){
        if (ActiveMenu == ".main-menu") {
            $(" #back ").addClass("shake");
            setTimeout(function(){
                $(" #back ").removeClass("shake");
            }, 250);
        }
        else {  
            unload_menu(ActiveMenu);
            setTimeout(function(){
                load_menu(".main-menu");
            }, 130)
            $("#no-back").css("display", "block");
            document.getElementById('upload-select-input').value = "";
            if (ActiveMenu == ".upload-menu"){
                $("#upload-submit").css("opacity", "0");
                $("#upload-selected").css("opacity", "0");

            }
        }
    }


    /**
     * Removes Previous Menu From View
     * @param {*} target Shared Class for example ".example-menu"
     */
    function unload_menu(target){
        console.log("Unload Menu - " + target);

        $(target).addClass("unload");

        setTimeout(function(){
            $(target).addClass("display-none");
            $(target).removeClass("unload");
            $(target).removeClass("display");
        }, 150)
    }


    /**
     * Pulls Targeted Menu Into View
     * @param {*} target Shared Class for example ".example-menu"
     */
    function load_menu(target){
        console.log("Load Menu - " + target);
        ActiveMenu = target;

        $(target).removeClass("display-none");
        $(target).addClass("display");
        $(target).addClass("load");

        setTimeout(function(){
            $(target).removeClass("load");
        }, 150)
    }


    /**
     * Input Click
     * Item ID + "-input"
     * id="item-input" for click
     */
    function input_click(item) {
        item = item.id;
        document.getElementById(item + "-input").click();
    }

    var fs = Boolean;

    function fullscreen(item) {
        if ( fs == true ) {
            $(item).removeClass("fullscreen");
            fs = false;
        } else {
            $(item).addClass("fullscreen");
            fs = true;
        }
    }

</script>