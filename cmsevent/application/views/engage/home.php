<style>
    #more-show {
        background-image: url("images/grosvenor/qr-code.png") !important;
        background-size: contain !important;
        background-position: center !important;
        background-color: white !important;
        background-repeat: no-repeat !important;
    }
    #today-text {
        display: flex;
        align-items: center;
        align-content: center;
        flex-wrap: wrap;
        justify-content: center;
        overflow-y: scroll;
    }
    #today-text > div{
        width: 100%;
        text-align: center;
        height: 85px;
    }
    #today-text > div:nth-child(2n) {
	    background:#0a223c; 
    }
</style>


<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<img class="section_logo" src="<?php echo base_url("images/info.png"); ?>">
<h1>Play games wether you're young or old, there's fun for everyone.</h1>
<p>Please select one of the options below or open and close the main menu by clicking the top bar.</p>
<hr style="clear:both; margin-top: 35px" />

<?php

    // Messages
	if (isset($message)) 	echo($message);
	if (isset($error))		echo($error);

 
?>

<div id="main">

    <div id="menu">
        <div id="menu-content" class="">

            <!-- Main Menu -->
            <div id="tournament" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Tournament</div>
            <div id="pricing" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Pricing</div>
            <div id="today" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Today</div>
            <div id="mmenu" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>Menu</div>
            <div id="more" class="cursor main-menu multi-bright"><div class="menu-arrow multi-bright"></div>More</div>

            <!-- Tournament Menu -->
            <div id="tournament-title" class="display-none tournament-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Tournament</div>
            <div id="tournament-video" class="display-none tournament-menu multi-bright" onclick="fullscreen(this);"  style="height: 500px; height: 500px; align-content: center; justify-content: center; align-items: center;">
                <video autoplay muted loop>
                    <source src="<?php echo base_url("images/grosvenor/grosvenor.mp4")?>" >
                </video>
            </div>

            <!-- Pricing Menu -->
            <div id="pricing-title" class="display-none pricing-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Costs</div>
            <div id="pricing-text" class="display-none pricing-menu multi-bright"></div>
            <div id="pricing-button" class="cursor display-none pricing-menu multi-bright" onclick="window.location='https://www.grosvenorcasinos.com/live-casino#login'"><div class="menu-arrow multi-bright"></div>Online Tickets</div>

            <!-- Today Menu -->
            <div id="today-title" class="display-none today-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Today:</div>
            <div id="today-text" class="display-none today-menu multi-bright">
                <div>Live Casino</div>
                <div>Inferno Jackpots</div>
                <div>Black Jack</div>
                <div>Poker Tournament</div>
                <div>Speed Roulette</div>
            </div>

            <!-- Menu Menu -->
            <div id="mmenu-title" class="display-none mmenu-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Food Menu</div>
            <div id="mmenu-text" class="display-none mmenu-menu multi-bright"></div>
            <div id="mmenu-button" class="cursor display-none mmenu-menu multi-bright"><div class="menu-arrow multi-bright"></div>Order</div>

            <!-- More Menu -->
            <div id="more-notice" class="display-none more-menu multi-bright text" style="-webkit-box-pack: center; display: block;">QR Code</div>
            <div id="more-show" class="display-none more-menu" style="height: 560px;">
                <!-- <image src="images/grosvenor/qr-code.png"></image>     -->
            </div>

            <!-- Dialog -->
            <div id="dialog-title" class="display-none dialog-menu multi-bright text" style="-webkit-box-pack: center; display: block;">Dialog</div>
            <div id="dialog-content" class="display-none dialog-menu multi-bright" style="-webkit-box-pack: center !important; display: block; text-align: center; height: 200px !important; padding: 2px;"></div>


        </div>
        <div id="menu-button">
            <div id="back" class="cursor multi-bright" valign="center">Back</div>
        </div>
    </div>
</div>





<!-- Hidden forms, use if need to implement them inside the menu and transfer inputs from menu to hidden forms upon submit -->
<?php //echo form_open_multipart('home/index', ['id' => 'upload-form', 'style' => 'display: none;']); ?>
<?php //echo form_upload("UPLOAD", "", ['id' => 'upload-select-input', 'accept' => 'image/*']) ?>
<?php //echo form_input("CAPTION", "", ['id' => 'caption-form']); ?>
<?php //echo form_close(""); ?>

<?php //echo form_open('home/index', ['id' => 'shoutout-form', 'style' => 'display: none;']); ?>
<?php //echo form_textarea("MESSAGE", "", ['id' => 'shoutout-textarea']); ?>
<?php //echo form_close(""); ?>


<script>

    $(document).ready(function() { 
 
    });




    /**
     * MENU SCRIPTS
     */

    var ActiveMenu = ".main-menu";

    // On Load
    $(document).ready(function(){

        // Back
        $("#back").click(function() {
            back();
        });


        // Menu Select
        $(".main-menu").click(function() {
            target = this.id;
            unload_menu('.main-menu');
            setTimeout(function(){
                load_menu("." + target + "-menu");
                setTimeout(function(){
                    $("#no-back").css("display", "none");
                }, 150);
            }, 160)
        });
    });


    /**
     * Returns To Main Menu
     */
    function back(){
        if (ActiveMenu == ".main-menu") {
            $(" #back ").addClass("shake");
            setTimeout(function(){
                $(" #back ").removeClass("shake");
            }, 250);
        }
        else {  
            unload_menu(ActiveMenu);
            setTimeout(function(){
                load_menu(".main-menu");
            }, 130)
            $("#no-back").css("display", "block");
            document.getElementById('upload-select-input').value = "";
            if (ActiveMenu == ".upload-menu"){
                $("#upload-submit").css("opacity", "0");
                $("#upload-selected").css("opacity", "0");

            }
        }
    }


    /**
     * Removes Previous Menu From View
     * @param {*} target Shared Class for example ".example-menu"
     */
    function unload_menu(target){
        console.log("Unload Menu - " + target);

        $(target).addClass("unload");

        setTimeout(function(){
            $(target).addClass("display-none");
            $(target).removeClass("unload");
            $(target).removeClass("display");
        }, 150)
    }


    /**
     * Pulls Targeted Menu Into View
     * @param {*} target Shared Class for example ".example-menu"
     */
    function load_menu(target){
        console.log("Load Menu - " + target);
        ActiveMenu = target;

        $(target).removeClass("display-none");
        $(target).addClass("display");
        $(target).addClass("load");

        setTimeout(function(){
            $(target).removeClass("load");
        }, 150)
    }


    /**
     * Input Click
     * Item ID + "-input"
     * id="item-input" for click
     */
    function input_click(item) {
        item = item.id;
        document.getElementById(item + "-input").click();
    }

    var fs = Boolean;

    function fullscreen(item) {
        if ( fs == true ) {
            $(item).removeClass("fullscreen");
            fs = false;
        } else {
            $(item).addClass("fullscreen");
            fs = true;
        }
    }

</script>