<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<table class="info_table" style="width: 100%">
	<col style="width: 80px" />
	<col />
	<thead>
		<tr class="first red">
			<td colspan="2"><?php echo empty($header) ? "Notices" :  html_escape($header) ?></td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td valign="top"><img style="width: 64px" src="<?php echo base_url("images/help.png"); ?>"></td>
			<td valign="top" style="padding: 15px; text-align: left">
				<?php
					if (isset($notices)) {
						if (is_array($notices)) {
							echo '<div class="message_body">';
							foreach ($notices as $err) {
								echo $err . "<br />";
							}
							echo '</div>';
							
						}
						else {
							echo 	'<div class="message_body">'. $notices . '</div>';
						}
					}
					else {
						echo 	'<div class="message_body">Nothing to Display</div>';
					}
				?>
			</td>
		</tr>
	</tbody>
</table>