<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<div class="form_message form_error">
	<img class="message_icon" src="<?php echo base_url("images/error.png"); ?>">
	<div class="message_panel">
		<div class="message_header"><?php echo empty($header) ? "The following errors were detected in your submission" :  html_escape($header) ?></div>
		<?php
			if (isset($error)) {
				if (is_array($error)) {
					echo '<div class="message_body">';
					foreach ($error as $err) {
						echo $err . "<br />";
					}
					echo '</div>';
					
				}
				else {
					echo 	'<div class="message_body">'. $error . '</div>';
				}
			}
			else {
				if ( ! @empty($this->form_validation->error_array()) ) {
					foreach ($this->form_validation->error_array() as $err) {
						echo $err . "<br />";
					}
				}
				else {
					echo 	'<div class="message_body">Nothing to Display</div>';
				}
			}
		?>
	</div>
</div>