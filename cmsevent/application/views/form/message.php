<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>
<div class="form_message form_info">
	<img class="message_icon" src="<?php echo base_url("images/info.png"); ?>">
	<div class="message_panel">
		<div class="message_header"><?php echo empty($header) ? "Message" :  html_escape($header) ?></div>
		<?php
			if (isset($message)) {
				if (is_array($message)) {
					echo '<div class="message_body">';
					foreach ($message as $err) {
						echo $err . "<br />";
					}
					echo '</div>';
					
				}
				else {
					echo 	'<div class="message_body">'. $message . '</div>';
				}
			}
			else {
				echo 	'<div class="message_body">Nothing to Display</div>';
			}
		?>
	</div>
</div>