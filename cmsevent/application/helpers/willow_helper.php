<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	/**
	 * Prints Debug Messages to the Output when not in Production Mode
	 * 
	 * @param Object $input The Object to Print
	 * 
	 * @return None
	 */
	if ( ! function_exists('debug_print') ) {
		function debug_print($input = "") {
			if (ENVIRONMENT !== 'production') {
				echo("<pre class=\"debug\"><b>Debug Data is show below</b>\n");
				print_r($input);
				echo("</pre>");
			}
		}
	}



	/**
	 * Prints Debug Messages to the Error Log
	 * 
	 * @param Object $input The Object to Print
	 * 
	 * @return None
	 */
	if ( ! function_exists('debug_log') ) {
		function debug_log($input = "") {
			trigger_error(print_r($input, true), E_USER_NOTICE);
		}
	}


	/**
	 * Quick Helper Function to Module URL
	 */
	if ( ! function_exists('module_url') ) {
		function module_url($path = "") {
			$ci =& get_instance();
			return base_url("client/" . $ci->uri->segment(1) . "/" . $path);
		}
	}

	
	/**
	 * Gets Field Contraints for Tables
	 * 
	 * @param string $table The Table to Get contraints from
	 * 
	 * @return Array    A list of Fields and Assoiated Sizes
	 */	
	if ( ! function_exists('get_table_lengths') ) {
		function get_table_lengths($table) {
			
			// Return Array
			$return = [];
			
			// Get Code Ignitor Reference
			$CI =& get_instance();
			
			// Lookup Lengths
			$field_data = $CI->db->query("	SELECT	COLUMN_NAME, CHARACTER_MAXIMUM_LENGTH
											FROM 	information_schema.columns
											WHERE 	table_schema = DATABASE()
											AND		table_name = ?
											AND		CHARACTER_MAXIMUM_LENGTH > 0
											",
												Array(
													$table
												)
											)->result_array();
			
			// Loop Fields
			foreach ($field_data as $field) {
				$return[$field['COLUMN_NAME']] = $field['CHARACTER_MAXIMUM_LENGTH'];
			}
			
			// Return
			return $return;
			
		}
	}


	/**
	 * Imports Settings from One Array to Another
	 *
	 * @param [type] $source
	 * @param [type] $destin
	 * @return void
	 */
	if ( ! function_exists('import_override_settings ') ) {
		function import_override_settings( &$array1, &$array2, $ignoreblank = false  ) {
			
			// Holds Array
			$merged = $array1;

			// Merge Values
			foreach ( $array2 as $key => &$value ) {
				if ( is_array( $value ) && isset( $merged [$key] ) && is_array( $merged [$key] ) ) {
					$merged [$key] = import_override_settings ( $merged [$key], $value, $ignoreblank );
				}
				else {
					if ( $value === "" && $ignoreblank ) {
						// Ignore 
					}
					else {
						$merged [$key] = $value;
					}	
				}
			}
		  
			// Return
			return $merged;

		}
	}


	/**
	 * Byte Convert
	 * 
	 * @param int $bytes The Size
	 * 
	 * @return String    A 2dp Respresentation of the File Size
	 */
	if ( ! function_exists('byte_convert') ) {
		function byte_convert($bytes) {
			
			// Get SI Definitions
			$symbol = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	
			// Calculate Nearest Unit
			$exp = 0;
			$converted_value = 0;
			if( $bytes > 0 ) {
			  $exp = floor( log($bytes)/log(1024) );
			  $converted_value = ( $bytes/pow(1024,floor($exp)) );
			}
	
			// Return Nearest Unit to 2DP
			return sprintf( '%.2f ' . $symbol[$exp], $converted_value );
		
		}
	}
	
	
	/**
	 * Sanatises a Path
	 * 
	 * @param string 	$path 	The Path
	 * @param bool		$dir 	Incoming is Directory
	 * 
	 * @return sting    The Sanatizes Path
	 */	
	if ( ! function_exists('sanatize_path') ) {
		function sanatize_path($path, $dir = TRUE) {
			
			// Check
			if ( empty($path) || ! is_string($path) )	return $path;
			
			// Sanatize
			$path	=	str_replace('\\',"/", $path);		// Swap Slashes
			$path 	= 	preg_replace('#/+#','/',$path); 	// Remove Double Slashes
			
			// Add Trailing Slash
			if ($dir && substr($path, -1, 1) != '/') {
				$path .= '/';								
			}
			
			// Return
			return $path;
			
		}
	}
	
	
	/**
	 * Gets Image Exif Data
	 * 
	 * @param string $filename The Filename
	 * 
	 * @return mixed Data or False
	 */	
	if ( ! function_exists( 'exif_imagetype' ) ) {
	    function exif_imagetype ( $filename ) {
	        if ( ( list($width, $height, $type, $attr) = getimagesize( $filename ) ) !== false ) {
	            return $type;
	        }
			return false;
	    }
	}

	
	/**
	 * Gets Settings for a Variable
	 *
	 * @param string $settings The Settings for a Field
	 *
	 * @return array    An Array of Parameters
	 */
	if ( ! function_exists( 'get_variable_settings' ) ) {
		function get_variable_settings($settings) {

			// Return Array
			$r	=	Array();

			// Explode Settings
			$v	=	str_getcsv($settings, ",");

			// Iterate Settings
			foreach ($v as $c => $setting) {

				// Get Parts
				$p = explode("=", $setting, 2);

				// Check
				switch (count($p)) {
					case 2:
						$p[0] 		= 	mb_strtolower($p[0]);
						$r[$p[0]]	=	$p[1];
						break;
					case 1:

						if ( count($v) == 1)
							$r['value']	=	$p[0];
						else
							$r[$c] = $p[0];
						break;

				}

			}

			// Return
			return $r;

		}
	}


	/**
	 * Correct the variables stored in array.
	 * @param    integer    $mask Integer of the bit
	 * @return    array
	 */
	if ( ! function_exists( 'bit_mask' ) ) {
		function bit_mask($mask = 0) {
			if(!is_numeric($mask)) {
				return array();
			}
			$return = array();
			while ($mask > 0) {
				for($i = 0, $n = 0; $i <= $mask; $i = 1 * pow(2, $n), $n++) {
					$end = $i;
				}
				$return[] = $end;
				$mask = $mask - $end;
			}
			sort($return);
			return $return;
		}
	}



	/**
	 * Write an ini configuration file
	 * 
	 * @param string $file
	 * @param array  $array
	 * @return bool
	 */	
	if ( ! function_exists('write_ini_file') ) {
		function write_ini_file($file, $array = []) {
			// check first argument is string
			if (!is_string($file)) {
				throw new \InvalidArgumentException('Function argument 1 must be a string.');
			}
	
			// check second argument is array
			if (!is_array($array)) {
				throw new \InvalidArgumentException('Function argument 2 must be an array.');
			}
	
			// process array
			$data = array();
			foreach ($array as $key => $val) {
				if (is_array($val)) {
					$data[] = "[$key]";
					foreach ($val as $skey => $sval) {
						if (is_array($sval)) {
							foreach ($sval as $_skey => $_sval) {
								if (is_numeric($_skey)) {
									$data[] = $skey.'[]='.(is_numeric($_sval) ? $_sval : (ctype_upper($_sval) ? $_sval : '"'.$_sval.'"'));
								} else {
									$data[] = $skey.'['.$_skey.']='.(is_numeric($_sval) ? $_sval : (ctype_upper($_sval) ? $_sval : '"'.$_sval.'"'));
								}
							}
						} else {
							$data[] = $skey.'='.(is_numeric($sval) ? $sval : (ctype_upper($sval) ? $sval : '"'.$sval.'"'));
						}
					}
				} else {
					$data[] = $key.'='.(is_numeric($val) ? $val : (ctype_upper($val) ? $val : '"'.$val.'"'));
				}
				// empty line
				$data[] = null;
			}
	
			// open file pointer, init flock options
			$fp = fopen($file, 'w');
			$retries = 0;
			$max_retries = 100;
	
			if (!$fp) {
				return false;
			}
	
			// loop until get lock, or reach max retries
			do {
				if ($retries > 0) {
					usleep(rand(1, 5000));
				}
				$retries += 1;
			} while (!flock($fp, LOCK_EX) && $retries <= $max_retries);
	
			// couldn't get the lock
			if ($retries == $max_retries) {
				return false;
			}
	
			// got lock, write data
			fwrite($fp, "\xEF\xBB\xBF" . implode(PHP_EOL, $data) . PHP_EOL);
	
			// release lock
			flock($fp, LOCK_UN);
			fclose($fp);
	
			return true;
		}
	}


	/**
	 * Adds Support for is_countable for PHP Versions < 7.3
	 */
	if ( ! function_exists('is_countable') ) {
		function is_countable($var) {
			return (is_array($var) || $var instanceof Countable);
		}
	}


	/**
	 * Checks if an XML has a Valid Name
	 *
	 * @param [type] $s
	 * @return void
	 */
	if ( ! function_exists('is_valid_xml_name') ) {
		function is_valid_xml_name($name) {
			try {
				new DOMElement("$name");
				return TRUE;
			} catch (DOMException $e) {
				return FALSE;
			}
		}
	}


	/**
	 * Attempts to work out if the connection is secure
	 *
	 * @return boolean
	 */
	if ( ! function_exists('is_secure') ) {
		function is_secure( $ignorelocal = FALSE ) {

			// Bypass Local Addresses if Flag Set
			if ( $ignorelocal && ! filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE ) ) {
				return true;
			}

			// Return
			return ( ! empty($_SERVER['HTTPS']) && mb_strtoupper($_SERVER['HTTPS']) !== 'OFF' );

		}
	}

	
?>