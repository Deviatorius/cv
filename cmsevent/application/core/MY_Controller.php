<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Site Controller Prototype defining Global Methods
 */
class MY_Controller extends MX_Controller {

	/**
	 * Holds any CSS Scripts used by this Module
	 */
	var $css		=	[];


	/**
	 * Holds any JS Scripts Used by this Module
	 */
	var $js 		=	[];


	/**
	 * Holds Client Settings
	 */
	var $settings	=	[];


	/**
	 * constructor
	 *
	 * @return None
	 */
	public function __construct() {

		// Construct Controller
		parent::__construct();

		// Load Prerequisites
		$this->load->library('auth');

		// Read Client Files
		if ( file_exists(FCPATH . "client\settings.ini") ) {

			// Parse Client Settings
			$iSettings  =   @parse_ini_file( FCPATH . "client\settings.ini", TRUE);
			if ( empty($iSettings) )    $iSettings = [];

			// Import Settings
			$this->settings = import_override_settings( $this->settings, $iSettings );

		}

		// Read Module Settings
		$mName = $this->router->fetch_module();
		if ( ! empty($mName) ) {

			// Does the Module have Additional Settings?
			if ( file_exists(FCPATH . "client\\{$mName}\\settings.ini") ) {

				// Parse Module Settings
				$mSettings  =   @parse_ini_file(FCPATH . "client\\{$mName}\\settings.ini", TRUE );
				if ( empty($mSettings) )    $mSettings = [];

				// Import Settings
				$this->settings = import_override_settings( $this->settings, $mSettings );

			}

		}

		// Enforce SSL
		if ( empty($_SERVER['HTTPS']) && ! empty(SITE_FORCES_SSL) ) {
			$sUrl = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			header("Location: {$sUrl}");
			exit();
		}

	}


	/**
	 * Provides a Logout Function for All Controllers
	 *
	 * @return None    Description
	 */
	public function logout() {

		// Get Current Flag
		$gpdr_accepted = ! empty($_SESSION['GPDR']);

		// Destroy Session
		$this->session->sess_destroy();

		// Start New Session
		session_start();

		// Set GPDR Flag
		if ( $gpdr_accepted )	$_SESSION['GPDR'] = "accepted";

		// Redirect
		header("Location: " . base_url());
		exit();

	}


	/**
	 * Summary
	 *
	 * @param string $params     	Params Passed into the System
	 * @param string $output     	The Content to Display
	 * @param array $style_sheets 	An Array of Style Sheets
	 * @param array $scripts      	An Array of Scripts to Import
	 * @param string $main_title   	Main title Override
	 *
	 * @return none
	 */
	protected function display_portal( $params = [], $output = "", $style_sheets = [], $scripts = [], $main_title = NULL ) {

		// Variables
		$subtitle 	=	"";
		$data		=	[];

		// Look for Parameters Passed as an Array
		if ( is_array($params) ) {

			// Read Array Parameters
			if ( ! empty($params['output']) )		$output 		= 	$params['output'];
			if ( ! empty($params['sheets']) )		$style_sheets 	= 	$params['style_sheets'];
			if ( ! empty($params['scripts']) )		$scripts 		= 	$params['scripts'];
			if ( ! empty($params['title']) )		$main_title		=	$params['title'];
			if ( ! empty($params['subtitle']) )		$subtitle		=	$params['subtitle'];

		}
		else {

			// Assume Legacy Method Use
			$subtitle = $params;

		}

		// Check Params
		if ($main_title===NULL || !is_string($main_title))	$main_title = SITE_TITLE;
		if ($output==NULL || ! is_string($output))			$output = "";

		// Add Subsection
		if ($subtitle !== NULL && is_string($subtitle)) {
			$main_title===""	?	$main_title = $subtitle	:	$main_title .= SITE_TITLE_SPLITTER . $subtitle;
		}

		// Get Current Module
		$controller = strtolower($this->router->fetch_module());

		// If Not in a Module use the Class
		if ($controller=="") {
			$controller = strtolower($this->router->fetch_class());
		}

		// Make Sure Sheets and Scripts are an Array
		if ( ! is_array($this->css) )		$this->css 		= 	Array($this->css);
		if ( ! is_array($this->js) )		$this->js 		= 	Array($this->js);

		// Add Legacy Options
		if ( ! is_array($style_sheets) )	$style_sheets 	=	Array($style_sheets);
		if ( ! is_array($scripts) )			$scripts 		=	Array($scripts);

		// Legacy Options Assume that Header Script performs Base URL
		foreach ( $style_sheets as &$sheet ) {
			if ( ! empty($sheet) ) 			$sheet = base_url($sheet);
		}
		foreach ( $scripts as &$script ) {
			if ( ! empty($script) ) 		$script = base_url($script);
		}

		// Merge Arrays
		if ( ! empty($style_sheets) )		$this->css		=	array_merge( $this->css, $style_sheets );
		if ( ! empty($scripts) )			$this->js		=	array_merge( $this->js, $scripts );

		// Grab Module Data
		$modules = $this->mod_modules->get_modules();

		// Add Params
		$data['navigation']	=	Array();
		$data['title']		=	$main_title;
		$data['sheets']		=	$this->css;
		$data['scripts']	=	$this->js;
		$data['content']	=	$output;
		$data['header']		=	$this->load->view("panel/header", $data, true);
		$data['footer']		=	$this->load->view("panel/footer", [], true);

		// Add Standard Links
		$data['navigation']['home']				=	Array( "link" => base_url("home"), "class" => "", "text" => "Interact");
		$data['navigation']['play']				=	Array( "link" => base_url("play"), "class" => "", "text" => "Play");
		$data['navigation']['experience']		=	Array( "link" => base_url("experience"), "class" => "", "text" => "Experience");
		$data['navigation']['engage']		=	Array( "link" => base_url("engage"), "class" => "", "text" => "Engage");

		// Add Module Links
		if (is_array($modules)) {
			foreach ($modules as $module) {
				if ( isset($module['settings']['navigation']) && is_array($module['settings']['navigation']) ) {

					// Sanatize Name
					$module['name'] = strtolower($module['name']);

					// Build Permission ID
					$module['perm'] = @$module['settings']['navigation']['permission'];
					if ( ! empty($module['settings']['navigation']['usekey']) )
						$module['perm'] += @$module['key'];

					// Output Link
					if ( ! isset($module['settings']['navigation']['permission']) || ( isset($module['settings']['navigation']['permission']) && ! empty( $_SESSION['USER']['PERMISSIONS'][$module['perm']]) ) ) {
						$data['navigation'][$module['name']] = Array( "link" => @base_url($module['settings']['navigation']['link']), "class" => @$module['settings']['navigation']['class'], "text" => @$module['settings']['navigation']['text']);
					}

				}
			}
		}

		// Add Logout Option
		$data['navigation']['logout']	=	Array( "link" => base_url("{$controller}/logout"), "class" => "", "text" => "Logout");

		// Create Controller Mappings
		$link_map	=	[	"templates" 	=> "adverts"	];
		$link_map	=	[	"versions" 		=> "setup"		];

		// Style Navigation
		if ( isset($data['navigation'][$controller]) ) {

			// Controller has a Direct Link
			$data['navigation'][$controller]['class'] .= " selected";

		}
		elseif ( ! empty($link_map[$controller]) && isset($data['navigation'][$link_map[$controller]]) )  {

			// Controller has a Mapped Link
			$data['navigation'][$link_map[$controller]]['class'] .= " selected";

		}

		// Load Header View
		$this->load->view("panel/panel", $data);

	}


	/**
	 * Displays a No Permission screen
	 *
	 * @return Type    Description
	 */
	public function _no_permission($message = NULL) {

		// Set Message
		if ( !is_string($message) )	$message = "You do not have permission to access this function";

		// Show Error
		$page = $this->load->view("errors/general_error", ["error" => $message], true);

		// Display Page Contents
		$this->display_portal("No Permission", $page);

		// Done
		return TRUE;

	}


}