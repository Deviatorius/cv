<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image {

	/**
	 * Image Resize Function
	 * 
	 * @param unknown $str          The Image as a String
	 * @param unknown $width        The Required Image Width
	 * @param unknown $height       The Required Image Height
	 * @param unknown $proportional Keep Proportional
	 * @param unknown $output       Output Type (File/Rescource)
	 * @param unknown $quality      The JPG Quality
	 * 
	 * @return Type    boolean or resource
	 */
	function smart_resize_string( $str, $width = 0, $height = 0, $proportional = false, $output = 'file', $quality = 100 ) {
		
	    # Setting defaults and meta
	    $info                         	= 	getimagesizefromstring($str);
	    $image                        	= 	null;
	    $final_width                  	= 	0;
	    $final_height                 	= 	0;
	    $cropHeight 					= 	0;
		$cropWidth 						= 	0;

	    # Get Width and Height
	    list($width_old, $height_old) 	= 	$info;

		# Do Full Size
		if ($width == 0)	$width = $width_old;
		if ($height == 0)	$height = $height_old;

		# Calculating Dimensions
		if ($proportional) {
		
			if      ($width  == 0)  $factor = $height / $height_old;
			elseif  ($height == 0)  $factor = $width / $width_old;
			else                    $factor = min( $width / $width_old, $height / $height_old );
		
			$final_width  = round( $width_old * $factor );
			$final_height = round( $height_old * $factor );
			
		}
		else {
		
			$final_width = ( $width <= 0 ) ? $width_old : $width;
			$final_height = ( $height <= 0 ) ? $height_old : $height;
			$widthX = $width_old / $width;
			$heightX = $height_old / $height;
		
			$x = min($widthX, $heightX);
			$cropWidth = ($width_old - $width * $x) / 2;
			$cropHeight = ($height_old - $height * $x) / 2;					
		
		}

	    # Create New Holder
	    $image			=	imagecreatefromstring ($str);
	    $image_resized 	= 	imagecreatetruecolor( $final_width, $final_height );
	    
	   	# Resize Image
	    if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {

			$transparency 	= 	imagecolortransparent($image);
			$palletsize 	= 	imagecolorstotal($image);

			if ($transparency >= 0 && $transparency < $palletsize) {
				$transparent_color  = imagecolorsforindex($image, $transparency);
				$transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			}
			elseif ($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			}
		}
		
		# Copy Image
		imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);

		# Preparing a method of providing result
		switch ( strtolower($output) ) {
			case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
				break;
			case 'file':
				$output = $file;
				break;
			case 'return':
				return $image_resized;
				break;
			default:
				break;
		}
	    
		# Writing image according to type to the output destination and image quality
		switch ( $info[2] ) {
			case IMAGETYPE_GIF:   
				imagegif($image_resized, $output);    
				break;
			case IMAGETYPE_JPEG:  
				imagejpeg($image_resized, $output, $quality);   
				break;
			case IMAGETYPE_PNG:
				$quality = 9 - (int)((0.9*$quality)/10.0);
				imagepng($image_resized, $output, $quality);
				break;
			default: 
				return false;
		}
	
		# Default
	    return true;

	}

	
	/**
	 * Resize an Image
	 * 
	 * @param unknown $file            The Image File
	 * @param unknown $width           The Requested Width
	 * @param unknown $height          The Requested Height
	 * @param unknown $proportional    Keep the Image Proportional
	 * @param unknown $output          Output Type (File/Rescource)
	 * @param unknown $delete_original Delete Original File
	 * @param unknown $quality         The Quality to Use
	 * 
	 * @return Type    Description
	 */
	function smart_resize_image( $file, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = false, $quality = 100 ) {
		
	    # Setting defaults and meta
	    $info                         	= 	getimagesize($file);
	    $image                        	= 	null;
	    $final_width                  	= 	0;
	    $final_height                 	= 	0;
	    $cropHeight 					= 	0;
		$cropWidth 						= 	0;
	    
	    # Get Width and Height
	    list($width_old, $height_old) 	= 	$info;
	
		# Do Full Size
		if ($width == 0)	$width = $width_old;
		if ($height == 0)	$height = $height_old;

		# Calculating Dimensions
		if ($proportional) {
		
			if      ($width  == 0)  $factor = $height / $height_old;
			elseif  ($height == 0)  $factor = $width / $width_old;
			else                    $factor = min( $width / $width_old, $height / $height_old );
		
			$final_width  = round( $width_old * $factor );
			$final_height = round( $height_old * $factor );
			
		}
		else {
		
			$final_width = ( $width <= 0 ) ? $width_old : $width;
			$final_height = ( $height <= 0 ) ? $height_old : $height;
			$widthX = $width_old / $width;
			$heightX = $height_old / $height;
		
			$x = min($widthX, $heightX);
			$cropWidth = ($width_old - $width * $x) / 2;
			$cropHeight = ($height_old - $height * $x) / 2;					
		
		}
	
		# Loading image to memory according to type
	    switch ( $info[2] ) {
	      case IMAGETYPE_JPEG:  $image = imagecreatefromjpeg($file);  break;
	      case IMAGETYPE_GIF:   $image = imagecreatefromgif($file);   break;
	      case IMAGETYPE_PNG:   $image = imagecreatefrompng($file);   break;
	      default: return false;
	    }
	    
	    
	    # Create New Holder
	    $image_resized = imagecreatetruecolor( $final_width, $final_height );
	    
	    # Resize Image
	    if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {

			$transparency 	= 	imagecolortransparent($image);
			$palletsize 	= 	imagecolorstotal($image);

			if ($transparency >= 0 && $transparency < $palletsize) {
				$transparent_color  = imagecolorsforindex($image, $transparency);
				$transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			}
			elseif ($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			}
		}
		
		# Copy Image
		imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);
		
	    # Taking care of original, if needed
	    if ( $delete_original ) {
	      @unlink($file);
	    }
	
		# Preparing a method of providing result
		switch ( strtolower($output) ) {
			case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
				break;
			case 'file':
				$output = $file;
				break;
			case 'return':
				return $image_resized;
				break;
			default:
				break;
		}
	    
		# Writing image according to type to the output destination and image quality
		switch ( $info[2] ) {
			case IMAGETYPE_GIF:   
				imagegif($image_resized, $output);    
				break;
			case IMAGETYPE_JPEG:  
				imagejpeg($image_resized, $output, $quality);   
				break;
			case IMAGETYPE_PNG:
				$quality = 9 - (int)((0.9*$quality)/10.0);
				imagepng($image_resized, $output, $quality);
				break;
			default: 
				return false;
		}
	
		# Default
	    return true;

	}

	
	
	/**
	 * Crop an Image
	 * 
	 * @param unknown $file            The File to Crop
	 * @param unknown $width           The Requested Width
	 * @param unknown $height          The Requested Height
	 * @param unknown $left            Left Position
	 * @param unknown $top             Top Position
	 * @param unknown $output          Output Type (File/Rescource)
	 * @param unknown $delete_original Delete Source File
	 * @param unknown $quality         JPG Quality
	 * 
	 * @return mixed    Result
	 */
	function smart_crop_image( $file, $width = 0, $height = 0, $left = 0, $top = 0, $output = 'file', $delete_original = false, $quality = 100 ) {
	    return smart_crop_stretch_image($file, $width, $height, $width, $height, $left, $top, $output, $delete_original, $quality);
	}


	/**
	 * Summary
	 * 
	 * @param unknown $file            The File to Crop
	 * @param unknown $width           The Requested Width
	 * @param unknown $height          The Requested Height
	 * @param unknown $cropWidth       Crop Width
	 * @param unknown $cropHeight      Crop Height
	 * @param unknown $left            Left Start
	 * @param unknown $top             Top Start
	 * @param unknown $output          Output Type
	 * @param unknown $delete_original Delete Source File
	 * @param unknown $quality         JPG Quality
	 * 
	 * @return Type    Description
	 */
	function smart_crop_stretch_image( $file, $width = 0, $height = 0, $cropWidth=0, $cropHeight=0, $left = 0, $top = 0, $output = 'file', $delete_original = false, $quality = 100 ) {
	  
		# Setting defaults and meta
		$info                         	= 	getimagesize($file);
		$image                        	= 	null;
	    
	    # Get Width and Height
	    list($width_old, $height_old) 	= 	$info;

		# Loading image to memory according to type
	    switch ( $info[2] ) {
	      case IMAGETYPE_JPEG:  $image = imagecreatefromjpeg($file);  break;
	      case IMAGETYPE_GIF:   $image = imagecreatefromgif($file);   break;
	      case IMAGETYPE_PNG:   $image = imagecreatefrompng($file);   break;
	      default: return false;
	    }

	    # Create New Holder
	    $image_resized = imagecreatetruecolor( $width, $height );
	    
	    # Set Bounds
	    if ($cropWidth==0)	$cropWidth = $width_old;
	    if ($cropHeight==0)	$cropHeight = $height_old;
	    
	    # Resize Image
	    if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {

			$transparency 	= 	imagecolortransparent($image);
			$palletsize 	= 	imagecolorstotal($image);

			if ($transparency >= 0 && $transparency < $palletsize) {
				$transparent_color  = imagecolorsforindex($image, $transparency);
				$transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			}
			elseif ($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			}
		}
		
		# Copy Image
		imagecopyresampled($image_resized, $image, 0, 0, $left, $top, $width, $height, $cropWidth, $cropHeight);

	    # Taking care of original, if needed
	    if ( $delete_original ) {
	      @unlink($file);
	    }
	
		# Preparing a method of providing result
		switch ( strtolower($output) ) {
			case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
				break;
			case 'file':
				$output = $file;
				break;
			case 'return':
				return $image_resized;
				break;
			default:
				break;
		}
	    
		# Writing image according to type to the output destination and image quality
		switch ( $info[2] ) {
			case IMAGETYPE_GIF:   
				imagegif($image_resized, $output);    
				break;
			case IMAGETYPE_JPEG:  
				imagejpeg($image_resized, $output, $quality);   
				break;
			case IMAGETYPE_PNG:
				$quality = 9 - (int)((0.9*$quality)/10.0);
				imagepng($image_resized, $output, $quality);
				break;
			default: 
				return false;
		}
	
		# Default
	    return true;

	  }

	

	  /**
	   * Returns Dimensions of an Image
	   *
	   * @param string $path	The Path of the Image
	   * @return array			The Image Dimensions
	   */
	  function get_image_dimensions( $path ) {

		// Build Return
		$return = [ "width" => 0, "height" => 0 ];
		try {
			
			// Grab Dims
			$rDims = getimagesize( $path );
			if ( empty($rDims) ) {
				return FALSE;
			}
			else {
				$return['width'] 	= $rDims[0];
				$return['height'] 	= $rDims[1];
			}

		} 
		catch (Exception $e) {
			return FALSE;
		}

		// Return
		return $return;

	  }


		/**
		 * Checks an Image Upload
		 *
		 * @param string $path
		 * @return boolean or error
		 */
		public function check_extension( &$upload, $fInfo ) {

			// Check Type
			switch ( exif_imagetype($upload['tmp_name']) ) {
				case "1":	// GIF

					// Check File is a GIF
					if ( strtolower($fInfo['extension']) <> "gif" ) {
						return "The file uploaded did not match the extension";
					}

					// Done
					$upload['extension'] = "gif";
					return TRUE;
					break;

				case "2":	// JPG

					// Check File is a Jpg
					if ( strtolower($fInfo['extension']) <> "jpg" && strtolower($fInfo['extension']) <> "jpeg" ) {
						return "The file uploaded did not match the extension";
					}

					// Done
					$upload['extension'] = "jpg";
					return TRUE;
					break;

				case "3":	// PNG

					// Check File is a PNG
					if ( strtolower($fInfo['extension']) <> "png" ) {
						return "The file uploaded did not match the extension";
					}

					// Done
					$upload['extension'] = "png";
					return TRUE;
					break;

				default:
					
					// Invalid File Type
					$upload['extension'] = "";
					return "Uploaded file was not an accepted format";
					break;

			}

		}

}