<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Define Missing Constants
defined('PASSWORD_ALGO')        OR define('PASSWORD_ALGO', PASSWORD_DEFAULT); 	// Password Algo to Use
defined('PASSWORD_COST')        OR define('PASSWORD_COST', 12); 				// Password Cost

class Auth {

	// Get Code Ignitor Reference
	protected $CI;

	/**
	 * Contructor the the Auth Library
	 *
	 * @return None
	 */
	public function __construct() {

		// Assign the CodeIgniter super-object
		$this->CI =& get_instance();

		// Load Prerequisites
		$this->CI->load->helper('url');
		$this->CI->load->library('session');
		$this->CI->load->model('mod_auth');

		// Serialise the Session
		$this->check_for_session_hijack();

		// Process Login Requests
		$this->process_login_request();

	}

	

	/**
	 * Checks the Session for a IP / Agent Combination and Destroys it if it's incorrect
	 *
	 * @return Type    Description
	 */
	public function check_for_session_hijack() {

		// Serial Parts
		$sp1	=	empty($_SERVER['REMOTE_ADDR']) 				? 	"" :	$_SERVER['REMOTE_ADDR'];
		$sp2	=	empty($_SERVER['HTTP_X_FORWARDED_FOR'])		? 	"" :	$_SERVER['HTTP_X_FORWARDED_FOR'];
		$sp3	=	empty($_SERVER['HTTP_USER_AGENT']) 			? 	"" :	$_SERVER['HTTP_USER_AGENT'];

		// Build Serial
		$serial	=	md5("SERIAL" . $sp1 . $sp2 . $sp3);

		// Assign Serial
		if (!isset($_SESSION['SERIAL'])) {
			$_SESSION['SERIAL'] = $serial;
		}
		else {
			if ($_SESSION['SERIAL'] != $serial) {

				// Destroy
				session_regenerate_id();						// GENERATE NEW SESSION ID
				session_unset();								// DUMP SESSION

				// Set Reason Flag
				$this->CI->session->set_flashdata('LOGIN', 'security');

			}
		}

	}



	/**
	 * Processes a Login Request
	 *
	 * @return None    Processes a Login Request
	 */
	public function process_login_request() {

		// Does the Post Contain a Login Request?
		if (isset($_POST['X_LOGIN_REQUEST'])) {

			// Store Login Request
			$_SESSION['X_LOGIN_REQUEST'] = $_POST['X_LOGIN_REQUEST'];

			// Return Request
			header("Location: " . base_url(uri_string()));
			exit();

		}
		elseif (isset($_SESSION['X_LOGIN_REQUEST'])) {

			// Check Login Request
			if (is_array($_SESSION['X_LOGIN_REQUEST']) && isset($_SESSION['X_LOGIN_REQUEST']['ROOMCODE']) && is_string($_SESSION['X_LOGIN_REQUEST']['ROOMCODE'])) {

				// Convert to Uppercase
				$_SESSION['X_LOGIN_REQUEST']['ROOMCODE']	=	mb_strtoupper($_SESSION['X_LOGIN_REQUEST']['ROOMCODE']);

				// Get Requested User
				$site_data	=	$this->CI->mod_auth->get_site($_SESSION['X_LOGIN_REQUEST']['ROOMCODE']);

				// Check we have a Record
				if ( empty($site_data) || empty($site_data['id']) || empty($site_data['store']) ) {

					// Set Login Flag
					$this->CI->session->set_flashdata('LOGIN', 'failed');

				}
				else {

					// Save Details
					$_SESSION['USER']['ROOMCODE'] 	=	empty($_SESSION['X_LOGIN_REQUEST']['ROOMCODE']) 	?	NULL			:		$_SESSION['X_LOGIN_REQUEST']['ROOMCODE'];
					$_SESSION['USER']['SITE'] 		=	empty($site_data['site']) 							?	"Unnamed Site"	:		$site_data['site'];
					$_SESSION['USER']['PLAYER']		=	empty($site_data['id']) 							?	NULL			:		$site_data['id'];
					$_SESSION['USER']['STARTED']	=	time();

				}

			}

			// Unset Login Request
			unset($_SESSION['X_LOGIN_REQUEST']);

		}

	}



	/**
	 * Does the Session Contain a User Entity
	 *
	 * @return bool    Whether a User it Logged into the current session
	 */
	public function session_has_valid_user() {

		// Check Session
		if ( ! isset($_SESSION['USER']) || ! is_array($_SESSION['USER']) )								return false;		// No User in Session
		if ( ! isset($_SESSION['USER']['ROOMCODE']) || ! is_string($_SESSION['USER']['ROOMCODE']) )		return false;		// No Room Code Stored
		if ( empty($_SESSION['USER']['STARTED']) || ! is_integer($_SESSION['USER']['STARTED']) )		return false;		// No Session Start
		if ( empty($_SESSION['USER']['PLAYER']) )														return false;		// No Player Stored
		
		// Check Start
		if ( time() - $_SESSION['USER']['STARTED'] >= 3600 ) {

			// Set Reason Flag
			session_destroy();
			$this->CI->session->set_flashdata('LOGIN', 'security');
			return false;

		}

		// Allowed
		return true;

	}


	/**
	 * Marks Login required and Terminates to a Login Screen if the User is not Valid
	 *
	 * @return None
	 */
	public function require_login() {

		// Check for Login
		if (!$this->session_has_valid_user()) {

			// Grab Flag
			$flag = "";

			// Assign and Remove Login Messages
			if (isset($_SESSION['LOGIN'])) {
				$flag = $_SESSION['LOGIN'];
				unset($_SESSION['LOGIN']);
			}

			// Display Login
			$this->display_login($flag);

		}

	}


	/**
	 * Displays a Login Screen and Terminates Further Execution
	 *
	 * @param string $flags any flags associated with this error request
	 *
	 * @return void
	 */
	public function display_login($flags="") {

		// Build Data Array
		$data	=	Array("flags" => $flags, "request" => base_url(uri_string()));

		// Display Login Screen
		echo($this->CI->load->view('login/login', $data, TRUE));
		exit();

	}


}