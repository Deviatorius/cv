<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Class for Dealing with XML
	 */
	class Xml {

		/** ADD ATTRIBUTE DOESEN'T REQUIRE SANITISATION **/

		/**
		 * Cleans a String for XML Output
		 *
		 * @param string $in The String to Sanatise
		 *
		 * @return String    The Sanatised String
		 */
		public function clean( $in ) {

			// Remove Invalid UTF-8 Data
			$in		=	iconv("UTF-8","UTF-8//IGNORE",$in);

			// Strip Invalid XML Characters
			$in		=	$this->remove_utf8_control_chars($in);

			// Return Escaped String
			return htmlspecialchars($in, ENT_COMPAT, 'UTF-8');

		}

		/**
		 * Removes Invalid and Non Printable XML Characters from a String
		 *
		 * @param String $string The String to Sanatise
		 *
		 * @return String    The String minus any invalid characters
		 */
		public function remove_utf8_control_chars($string) {
			return preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', $string);
		}


		/**
		 * Adds an Attribute with Escaping
		 *
		 * @param xml $parent The SimpleXML Item
		 * @param name $name   The Attribute Name
		 * @param string $value  The Attribute Value
		 *
		 * @return Xml    The Simple XML Element
		 */
		public function escaped_attribute( $parent, $name, $value ) {
			return $parent->addAttribute($name, $value);
		}


		/**
		 * Adds a XML Child with Escaping
		 *
		 * @param xml $parent The SimpleXML Item
		 * @param name $name   The Attribute Name
		 * @param string $value  The Attribute Value
		 *
		 * @return Xml    The Simple XML Element
		 */
		public function escaped_child( $parent, $name, $value ) {
			$item =  $parent->addChild($name);
			$item->value = $value;
			return $item;
		}


		/**
		 * Outputs XML to the Browser
		 *
		 * @return void
		 */
		public function output( $xml, $return = FALSE ) {

			// Format XML Output
			$doc = new DOMDocument('1.0','UTF-8');
			$doc->preserveWhiteSpace = false;
			$doc->loadXML($this->remove_utf8_control_chars($xml->asXML()));
			$doc->formatOutput = true;

			// Output
			if ( ! $return ) {
				header('Content-type: text/xml; charset=UTF-8');
				echo $doc->saveXML();
			}
			else {
				return $doc->saveXML();
			}

		}


	}

?>