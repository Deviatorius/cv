<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSV {


    /**
     * Corrects the Character Encoding
     *
     * @return void
     */
    public function check_encoding( $sData ) {

        // Detect encoding
        switch (true) {
            case (substr($sData,0,3) == "\xef\xbb\xbf") :
                $sData = substr($sData, 3);
                break;
            case (substr($sData,0,2) == "\xfe\xff") :
                $sData = mb_convert_encoding(substr($sData, 2), "UTF-8", "UTF-16BE");
                break;
            case (substr($sData,0,2) == "\xff\xfe") :
                $sData = mb_convert_encoding(substr($sData, 2), "UTF-8", "UTF-16LE");
                break;
            case (substr($sData,0,4) == "\x00\x00\xfe\xff") :
                $sData = mb_convert_encoding(substr($sData, 4), "UTF-8", "UTF-32BE");
                break;
            case (substr($sData,0,4) == "\xff\xfe\x00\x00") :
                $sData = mb_convert_encoding(substr($sData, 4), "UTF-8", "UTF-32LE");
                break;
            default:
                $sData = iconv(mb_detect_encoding($sData, mb_detect_order(), true), "UTF-8", $sData);
        };

        // Strip Invalid Characters
        return $this->strip_invalid_utf8($sData);

    }


	/**
	 * Removes Invalid and Non Printable XML Characters from a String
	 *
	 * @param String $string The String to Sanatise
	 *
	 * @return String    The String minus any invalid characters
	 */
	public function strip_invalid_utf8($string) {
        return preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x7F]/', '', iconv("UTF-8","UTF-8//IGNORE",$string));
	}


    /**
     * Get CSV Headers
     */
    function read_headers( $file ) {
        try {

            // Open and Read 4KB From the file
            $f = fopen($file, 'r');
            $d = $this->check_encoding(fread($f, 4096));

            // Explode into Lines
            $s = explode("\n", $d);

            // Get Headers
            $line = str_getcsv($s[0]);

            // Check
            if ( empty($line) ) {
                return [];
            }
            else {
                return $line;
            }

        }
        catch (Exception $e) {
            return [];
        }

    }


    /**
     * Get CSV Headers
     */
    function read_headers_from_string( $s, $check_encoding = false ) {
        try {

            // Explode to Lines and Get First
            if ( $check_encoding ) $s = $this->check_encoding($s);
            $s = explode("\n", $s);
            $line = str_getcsv($s[0]);

            // Check
            if ( empty($line) ) {
                return [];
            }
            else {
                return $line;
            }

        }
        catch (Exception $e) {
            return [];
        }
    }


    /**
     * Merge from File
     */
    function merge_from_file( $file , $order ) {
        try {

            // Open and Read 4KB From the file
            $d = $this->check_encoding(file_get_contents($file));

            // Return
            return $this->merge_from_string($d,$order);

        }
        catch (Exception $e) {
            return [];
        }
    }


    /**
     * Merge from File
     */
    function merge_from_string( $s , $order, $check_encoding = false ) {
        try {

            // Holds Return
            $return     =   [];
            $hparsed    =   false;

            // Open File
            if ( $check_encoding ) $s = $this->check_encoding($s);
            $s = explode("\n", $s);
            foreach ( $s as $entry ) {

                // Skip Empty
                if ( empty($entry) )    continue;

                // Skip Headers
                if ( ! $hparsed ) {
                    $hparsed = true;
                    continue;
                }

                // Parse String
                $line = str_getcsv($entry);

                // Build Copy
                $tNew = $order;

                // Populate
                foreach ( $tNew as $key => &$value ) {
                    $value = isset($line[$value]) ? $line[$value] : null;
                }

                // Add
                $return[] = $tNew;

            }

            // Return
            return $return;

        }
        catch (Exception $e) {
            return [];
        }
    }



}