<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File {

	/**
	 * Returns Directory Structure as Array
	 * 
	 * @param string $path    The Path
	 * @param string $exclude Files to Exclude
	 * 
	 * @return Array    An Array of Files
	 */
	public function file_array( $path , $exclude = ".|..|Thumbs.db") {
		
		// Format Inputs
		$path = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;				// Trim Slashes
	    $exclude_array = explode("|", $exclude);		// Build Excludes

	    // Open Folder
	    if (!file_exists($path))
	    	return null;
	    if (!$folder_handle = opendir($path))
	    	return null;

	    // Build Results Array
	    $result = [	"type"	=>	"dir",	"path"	=>	$path,	"files" => []	];

	    // Populate Array
	    while(false !== ($filename = readdir($folder_handle))) {
	        if(!in_array(strtolower($filename), $exclude_array)) {
	            if(is_dir($path . $filename . DIRECTORY_SEPARATOR)) {

	                // Need to include full "path" or it's an infinite loop
	            	$result['files'][] = $this->file_array($path . $filename . DIRECTORY_SEPARATOR, $exclude);

	            } else {
	                $result['files'][] = array_merge(["type" => "file"], pathinfo($filename));
	            }
	        }
	    }
		
		// Return Results
	    return $result;

	}

	
	
	/**
	 * Returns a Simple File List
	 * 
	 * @param string $path       The Path
	 * @param string $exclude    Files to Exclude
	 * @param string $start_path The Start Path
	 * 
	 * @return Array    An Array of Files
	 */
	function file_list( $path, $exclude = ".|..|Thumbs.db", $start_path="" ) {

		// Build Missing Start Path
		if ($start_path=="")	$start_path	=	$path;

		// Trim Paths
		$path 			= 	rtrim($path, "/");
		$start_path		=	rtrim($start_path,"/");

		// Trim Slashes
	    $exclude_array = explode("|", $exclude);		// Build Excludes

		// Open Folder
	    if (!file_exists($path))
	    	return null;
	    if (!$dh = opendir($path))
	    	return null;

		// Do Listing
		$files = Array();
		$inner_files = Array();

		while($file = readdir($dh)) {
			if(!in_array(strtolower($file), $exclude_array)) {
				if(is_dir($path . "/" . $file)) {
		            $inner_files = $this->file_list($path . "/" . $file,$exclude,$start_path);
		            if(is_array($inner_files)) $files = array_merge($files, $inner_files);
		        } else {
		        	array_push($files, str_replace($start_path . "/","",$path . "/" . $file));
		        }
		    }
		}

		closedir($dh);
		return $files;

	}
	
	
	/**
	 * Fix Path Issues
	 * 
	 * @param string $p The Path
	 * 
	 * @return string    The Fixed Path
	 */
	function fixpath($p) {
	    $p=str_replace('\\','/',trim($p));
	    return (substr($p,-1)!='/') ? $p.='/' : $p;
	}

	
}