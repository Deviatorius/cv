
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Page Controller
 */
class Engage extends MY_Controller {

	
	/**
	 * Page Index
	 *
	 * @return void
	 */
	public function index() {

		// Require Authenticated Login
		$this->auth->require_login();

		// Load Additional Functions
		$this->load->helper('form');
		$this->load->library('form_validation');

		// Variables
		$data 	= [];

		// Load Message Window
		if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
			$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
		}

		// Load Page Contents
		$page	=	$this->load->view("engage/home", $data, true);

		// Display Page Contents
		$this->display_portal("Welcome", $page);

	}

}