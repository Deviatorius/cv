<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Page Controller
 */
class Home extends MY_Controller {

	
	/**
	 * Page Index
	 *
	 * @return void
	 */
	public function index() {

		// Require Authenticated Login
		$this->auth->require_login();

		// Load Additional Functions
		$this->load->helper('form');
		$this->load->library('form_validation');

		// Variables
		$data 	= [];

		// Load Message Window
		if (isset($_SESSION['submission_message']) && $_SESSION['submission_message'] !== "") {
			$data['message']	=	$this->load->view("form/message", ['message' => $_SESSION['submission_message']], TRUE);
		}

		// Load Page Contents
		$page	=	$this->load->view("home/home", $data, true);

		// Display Page Contents
		$this->display_portal("Welcome", $page);
	}


	public function search() {
		
		// Holds Current Error Level
		$current_error_level		=	error_reporting();
		error_reporting(1);

		// Check Logged In
		if ( empty($_SESSION['USER']) ) {
			die( json_encode(['status' => false, 'message' => 'You are not signed in']) );
		}
		elseif ( empty($_SESSION['USER']['PLAYER']) ) {
			die( json_encode(['status' => false, 'message' => 'Could not identify your site']) );
		}
		else {

			// Require Authenticated Login
			$this->auth->require_login();
	
			// Import Libraries
			$this->load->library('pagination');
			$this->load->helper('form');
			$this->load->library('form_validation');
	
			// Load Model
			$this->load->model('mod_music');
	
			// Variables
			$data					=	[];
			$data['type']			=	$this->uri->rsegment('3');
			$data['types']			=	$this->mod_music->get_library_types();
			// $data['categories']		=	$this->mod_music->get_library_categories( TRUE, 3 );
			// $data['controversial']	=	[ '' => "N/A", 'YES' => 'Yes', 'NO' => 'No' ];
			// $data['disabled']		=	[ '' => "N/A", 'YES' => 'Yes', 'NO' => 'No' ];
			$data['results']		=	[];
	
			// Do Nulls
			$data['types']			=	empty($data['types']) 		? [ '' => "N/A"] : ['' => "N/A"] + $data['types'];
			// $data['categories']		=	empty($data['categories']) 	? [ '' => "N/A"] : ['' => "N/A"] + $data['categories'];
	
			// Read Search Parameters
			$data['search']		=	[
				'search'		=>		"1",
				'params'		=>		empty($_POST['data']['params']) 			? "" : $_POST['data']['params'],
				'type'			=>		empty($data['type']) 						? 0  : $data['type'],
				'from'			=>		empty($_GET['from']) 						? "" : urldecode($_GET['from']),
				'to'			=>		empty($_GET['to']) 							? "" : urldecode($_GET['to']),
				'category'		=>		empty($_GET['category']) 					? "" : urldecode($_GET['category']),
				'controversial'	=>		empty($_GET['controversial'])				? "" : urldecode($_GET['controversial']),
				'disabled'		=>		empty($_GET['disabled']) 					? "" : urldecode($_GET['disabled']),
				'page'			=>		empty($data['page']) 						? 0  : intval(urldecode($data['page'])),
				'per_page'		=>		250,
				'order'			=>		empty($_GET['order']) 						? "ARTIST" 	: urldecode($_GET['order']),
				'direction'		=>		empty($_GET['direction'])					? 0 		: intval(urldecode($_GET['direction']))
			];
	
			// Perform Search
			if ( $data['search']['search'] ) {
				
				// Perform Search
				$data['results']					=	$this->mod_music->search_library( $data['search'] );
				$data['results']['results']			=	array_values($data['results']['results']);
		
				//Highlight Terms
				foreach ( $data['results']['results'] as &$track ) {
	
					// Escape
					$track['artist']	=	html_escape($track['artist']);
					$track['title']		=	html_escape($track['title']);
	
					// Handle Nulls
					if ( empty($track['type']) )	$track['type'] = 0;		// Sending Null returns an Array of Types
	
					// Highlight
					if ( ! empty($data['search']['params']) ) {
	
						// Replace
						$terms				=	html_escape($data['search']['params']);
						$track['artist'] 	= 	preg_replace("/(" . preg_quote($terms, "/") . ")/i", "<span style='font-weight: bold'>$1</span>", $track['artist']);
						$track['title'] 	= 	preg_replace("/(" . preg_quote($terms, "/") . ")/i", "<span style='font-weight: bold'>$1</span>", $track['title']);
	
					}
				}
	
				$resultArray = [];
	
				foreach ($data['results']['results'] as $gid => $array) {
					$resultArray[$gid]	=	array(
						'gid'			=>	$array['gid'],
						'filename'		=>	$array['filename'],
						'artist'		=>	$array['artist'],
						'title'			=>	$array['title'],
						'type'			=>	$data['search']['type'],
						'controversial'	=>	$array['controversial']
					);
				};

				if ( empty($resultArray) ) {
					die( json_encode(['status' => false, 'message' => '0 results were found.']) );
				}

				
				echo json_encode($resultArray);
			
			}

		}
		
		// Return Data
		error_reporting($current_error_level);

	}

	
	/**
	 * Handles QR Login
	 *
	 * @return void
	 */
	public function login() {

		// Declare Variables
		$code	=	$this->uri->rsegment('3');

		// Handle
		if ( ! empty($code) ) {
			$_SESSION['X_LOGIN_REQUEST']['ROOMCODE']	=	mb_strtoupper($code);
		}

		// Redirect
		header("Location: " . base_url() );
		return;

	}


	/**
	 * Allows Asset Uploads
	 *
	 * @return void
	 */
	public function upload() {
		
		// Holds Current Error Level
		$current_error_level		=	error_reporting();
		error_reporting(1);

		// Check Logged In
		if ( empty($_SESSION['USER']) ) {
			die( json_encode(['status' => false, 'message' => 'You are not signed in']) );
		}
		elseif ( empty($_SESSION['USER']['PLAYER']) ) {
			die( json_encode(['status' => false, 'message' => 'Could not identify your site']) );
		}
		elseif ( count($_FILES)==0 || ! isset($_FILES['UPLOAD']) ) {
			die( json_encode(['status' => false, 'message' => 'No files were uploaded']) );
		}
		elseif ( ! isset($_FILES['UPLOAD']) && $_FILES['UPLOAD']['error'] != UPLOAD_ERR_OK ) {
			die( json_encode(['status' => false, 'message' => 'An Error occured in the upload process']) );
		}
		elseif ( $_FILES['UPLOAD']['size'] > CMS_MAX_IMAGE_SIZE ) {
			die( json_encode(['status' => false, 'message' => 'File Size was bigger than the allowed maximum upload size of ' . @byte_convert(CMS_MAX_IMAGE_SIZE) ]) );
		}
		else {

			// Import Libraries
			$this->load->library("image");
			$this->load->model("mod_interaction");

			// Pointer
			$tFile 	 = 	$_FILES['UPLOAD'];
			$fInfo 	 = 	pathinfo($_FILES['UPLOAD']['name']);
			$errors	 =	[];
			$message =	( ! empty($_POST['CAPTION']) ) ? $_POST['CAPTION'] : "";
			
			// Check Image Extension Matches Uploaded Image
			$cResult = $this->image->check_extension($tFile, $fInfo);
			if ( $cResult !== TRUE )	{	
				$errors[] = $cResult;
			}
			else {

				// Uploaded File was OK
				switch ( mb_strtolower($tFile['extension']) ) {
					case "jpeg":
					case "jpg":
					case "png":
					case "gif":
					
						// Build Location Path
						$cPath = sanatize_path(CMS_PATH_DISK);

						// Build Asset Name
						$fName = uniqid("file_" . $_SESSION['USER']['PLAYER'] . "_", false) . "." . $tFile['extension'];

						// Grab Dimensions
						$iDims = $this->image->get_image_dimensions( $_FILES['UPLOAD']['tmp_name'] );
						
						// Resize Image if One Dimension is Larger than the target size
						if ( ! empty($iDims) && ( $iDims['width'] > 1920 || $iDims['height'] > 1080 ) ) {
							$this->image->smart_resize_image( $_FILES['UPLOAD']['tmp_name'], 1920, 1080, TRUE, 'FILE', FALSE, 90 );
						}

						// Move Asset
						$result = @rename( $_FILES['UPLOAD']['tmp_name'], $cPath . $fName);
						if ( $result ) {
							
							// Create Entru
							$interaction_id = $this->mod_interaction->create_asset_interaction( $fName, $message, $_SESSION['USER']['PLAYER'] );
														
							if ( empty($interaction_id) ) {
								die( json_encode(['status' => false, 'message' => 'An Error occured on the server while processing your upload']) );
							}

						}
						else {

							// Didn't Move File
							die( json_encode(['status' => false, 'message' => 'An Error occured on the server while processing your upload']) );

						}

						// Done
						break;

					default:

						$errors[] = "The Uploaded File was not a valid image!";
						break;

				}

			}

			// Did We Upload?
			if ( ! empty($errors) ) {

				// No
				die( json_encode(['status' => false, 'message' => 'File Size was bigger than the allowed maximum upload size of ' . @json_encode("\n", $errors) ]) );

			}
			else {

				// Upload Was Successful
				echo( json_encode(['status' => true, 'message' => 'Upload was Successful']) );

			}

		}

		// Return Data
		error_reporting($current_error_level);

	}


	/**
	 * Allows Asset Uploads
	 *
	 * @return void
	 */
	public function shoutout() {
		
		// Holds Current Error Level
		$current_error_level		=	error_reporting();
		error_reporting(1);

		// Check Logged In
		if ( empty($_SESSION['USER']) ) {
			die( json_encode(['status' => false, 'message' => 'You are not signed in.']) );
		}
		elseif ( empty($_SESSION['USER']['PLAYER']) ) {
			die( json_encode(['status' => false, 'message' => 'Could not identify your site.']) );
		}
		elseif ( empty($_POST['MESSAGE']) ) {
			die( json_encode(['status' => false, 'message' => 'Message not received.']) );
		}
		else {

			// Variables
			$errors	 =	[];
			$message =	$_POST['MESSAGE'];

			// Import Libraries
			$this->load->model("mod_interaction");
			$this->load->helper('form');
			$this->load->library('form_validation');

			// Set Validation Rules
			$this->form_validation->set_rules('MESSAGE', 'message', "trim|required|min_length[10]|max_length[500]");
			
			// Run Validation (Note changes for HMVC Validation)
			if ($this->form_validation->run($this) == FALSE) {
				if (count($this->form_validation->error_array()) > 0) {
					$errors = $this->form_validation->error_array();
				}
			}

			if ( ! empty($errors) ) {
				die( json_encode(['status' => false, 'message' => 'The message field must be at least 10 characters in length.']) );
			}
			
			// Create Entru
			$interaction_id = $this->mod_interaction->create_shoutout_interaction( $message, $_SESSION['USER']['PLAYER'] );
										
			if ( empty($interaction_id) ) {
				die( json_encode(['status' => false, 'message' => 'An Error occured on the server while processing your message.']) );
			}


			// Upload Was Successful
			echo( json_encode(['status' => true, 'message' => 'Message Sent Successfully']) );


		}

		// Return Data
		error_reporting($current_error_level);

	}


		/**
	 * Allows Asset Uploads
	 *
	 * @return void
	 */
	public function request() {
		
		// Holds Current Error Level
		$current_error_level		=	error_reporting();
		error_reporting(1);

		// Check Logged In
		if ( empty($_SESSION['USER']) ) {
			die( json_encode(['status' => false, 'message' => 'You are not signed in.']) );
		}
		elseif ( empty($_SESSION['USER']['PLAYER']) ) {
			die( json_encode(['status' => false, 'message' => 'Could not identify your site.']) );
		}
		elseif ( empty($_POST['TRACK']) || empty($_POST['GID']) || empty($_POST['TYPE']) ) {
			die( json_encode(['status' => false, 'message' => 'Request not received.']) );
		}
		else {

			// Variables
			$errors		=	[];
			$singer		=	( empty($_POST['SINGER']) ) ? NULL : $_POST['SINGER'];

			// Import Libraries
			$this->load->model("mod_interaction");
			$this->load->helper('form');
			$this->load->library('form_validation');
			
			// Create Entru
			$interaction_id = $this->mod_interaction->create_song_interaction( $_SESSION['USER']['PLAYER'], $_POST['TRACK'], $_POST['GID'], $_POST['TYPE'], $singer);
										
			if ( empty($interaction_id) ) {
				die( json_encode(['status' => false, 'message' => 'An Error occured on the server while processing your request.']) );
			}

			// Upload Was Successful
			echo( json_encode(['status' => true, 'message' => 'Track requested.']) );


		}

		// Return Data
		error_reporting($current_error_level);

	}


	/**
	 * Allows Asset Retrieval
	 *
	 * @return void
	 */
	public function asset() {

		// Declare Variables
		$player	=	(int)$this->uri->rsegment('3');
		$asset	=	(int)$this->uri->rsegment('4');

		// Load Model
		$this->load->model("mod_interaction");

		// Obtain Asset File Name
		$file	=	$this->mod_interaction->get_interaction( $player, $asset );

		// Sanatize Path
		$sPath 	= 	sanatize_path(CMS_PATH_DISK, FALSE);

		// Remove Attempts to traverse up
		$sPath 	= 	str_replace ( "../" , "", $sPath );
		$sPath 	= 	str_replace ( "./" , "" , $sPath );
		$sPath 	.= 	"/";

		// Checks
		if ( empty($file) || empty($file['additional1']) ) {

			// File not found
			http_response_code(404);
			echo("404 - The requested file was not found");
			die();

		}
		elseif ( file_exists($sPath . $file['additional1']) ) {

			// Pass to Apache
			header("Content-type: application/octet-stream");
			header('Content-Disposition: attachment; filename="' . str_replace('"', '\"', basename($file['additional1'])) . '"');
			header("Content-Transfer-Encoding: binary");
			header("X-Sendfile: " . $sPath . $file['additional1']);
			exit();

		}
		else {

			// File not found
			http_response_code(404);
			echo("404 - The requested file was not found");
			die();

		}
		

	}


	/**
	 * Page Index
	 * 
	 * @return void
	 */
	public function js() {
		header('Content-type: text/javascript');
		$this->load->view("panel/js", []);
	}


	/**
	 * Privacy Policy
	 *
	 * @return void
	 */
	public function privacy() {

		// Display Privacy Policy
		if ( $this->auth->session_has_valid_user() ) {
			
			// Output using Portal
			$this->display_portal("Privacy Policy", $this->load->view("gpdr/privacy", [], true));

		}
		else {

			// Output as Document
			echo $this->load->view("panel/document", [ "title" => "Privacy and Cookie Policy", "content" => $this->load->view("gpdr/privacy", [], true) ]);

		}

	}


	/**
	 * Accepts the GPDR Notice
	 *
	 * @return void
	 */
	public function accept() {
		$_SESSION['GPDR'] = "ACCEPT";
	}


}