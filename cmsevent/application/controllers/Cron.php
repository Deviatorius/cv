<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Page Controller
 */
class Cron extends MX_Controller {

	/**
	 * Page Index
	 *
	 * @return void
	 */
	public function index() {

		// No Script Timeout
		set_time_limit(0);
	
		// Load Models
		$this->load->model("mod_player");
		$this->load->model("mod_render");
		$this->load->model("mod_bingo");
		$this->load->model("mod_music");
	
		/**
		 * ########################################################################
		 * ##							CLEANUP LOGS
		 * ########################################################################						
		 */

		// Get 3 Months Ago
		$hData = strtotime("-6 MONTHS");
		
		// Delete Historical Data
		echo("DELETING HISTORICAL DATA\n\n");
		$this->db->query("	DELETE FROM `player_log` WHERE `timestamp` <= ? ", [$hData]);
		$this->db->query("	DELETE FROM `system_log` WHERE `date` <= ? ", [ date("Y-m-d H:i:s", $hData) ]);
	

		/**
		 * ########################################################################
		 * ##						UPDATE PLAYER STATUS
		 * ########################################################################						
		 */

		// Get All Players
		echo("CHECKING CURRENT STATUS\n\n");
		foreach ($this->db->query(" SELECT p.`id`, p.`serial`, p.`lastupdate`, p.`name`, p.`lastreportip`, s.`name` 'store_name' FROM `player` p LEFT JOIN `store` s ON s.`id` = p.`store` ")->result_array() as $player) {
			
			// Variable
			$history = NULL;

			// Grab Last Status from History
			$lS = $this->db->query(" SELECT `timestamp`, `status` FROM `player_log` WHERE `player` = ? ORDER BY `player_log`.`timestamp` DESC", [$player['id']])->row_array();

			// Set Last Status
			if ( ! empty($lS['status']) ) {
				$history = $lS['status'];
			}

			// Get Last Status
			$player['status'] = $this->mod_player->get_player_status($player);
			
			// Build Unknown
			if ( ! is_array($player['status']) || empty($player['status']['result']) ) {
				$player['status'] = ["status" => 3, "result" => "UNKNOWN"];
			}
			
			// Has Status Changed
			if ( empty($history) || $history <> $player['status']['result'] ) {
				
				// Log New Status
				echo("{$player['name']} AT {$player['store_name']} STATUS NOW {$player['status']['result']}\n");
				$this->db->query(" INSERT INTO `player_log` VALUES ( NULL, ?, ?, ?) ", [ $player['id'], time(), $player['status']['result'] ]);
				
			}
			
		}
	

		/**
		 * ########################################################################
		 * ##						BINGO REPORTING
		 * ########################################################################						
		 */

		// Grab Completed Games
		echo("SENDING BINGO REPORTS\n\n");
		$this->mod_bingo->send_reports();
		echo("APPLY AUTOMATIC POLICIES\n\n");
		$this->mod_bingo->apply_automatic_policies();



		/**
		 * ########################################################################
		 * ##						LIBRARY SYNC
		 * ########################################################################						
		 */

		// Grab Music Settings
		echo("CHECKING MEDIA LIBRARY\n\n");
		$settings	=	$this->mod_music->get_library_settings();
		$mVersion 	= 	empty($settings['SYNCED'])		?	0	: 	@strtotime($settings['SYNCED']);
		$mMode		=	empty($settings['MODE'])		?	0	:	$settings['MODE'];

		// Sync Library
		if ( $mMode && time() - $mVersion >= 1800 ) {
			
			// Need to Sync
			echo("SYNCING MEDIA LIBRARY\n\n");
			$this->mod_music->sync();

		}


		/**
		 * ########################################################################
		 * ##						CASH CONTROL
		 * ########################################################################						
		 */


		// Grab Client Function
		if ( file_exists(FCPATH . '\client\cc.php') ) {
			
			// Include
			include_once(FCPATH . '\client\cc.php');

			// Do Function
			if ( class_exists('Client_CC') ) {
				
				// Create Instance
				$tCc = new Client_CC();
				if ( method_exists ( $tCc , 'cron' ) ) {

					echo("RUNNING CLIENT CASH CONTROL CRON TASKS\n\n");
					$tCc->cron( $this );

				}		

			}

		}

	}



	/**
	 * ########################################################################
	 * ##				SEPERATE TASK TO ALLOW RENDERING TASKS
	 * ########################################################################						
	 */
	public function render() {

		// No Script Timeout
		set_time_limit(0);
	
		// Load Models
		$this->load->model("mod_player");
		$this->load->model("mod_render");
		$this->load->model("mod_bingo");

		// Grab Next Queue Item
		if ( FFMPEG_ENABLED ) {

			// Run Queue
			echo("CHECKING RENDER QUEUE\n\n");

			// Build Paths
			$rPath = sanatize_path(FFMPEG_TEMP);
			$cPath = sanatize_path(CMS_PATH_DISK);
			
			// Grab Queue Item
			$qI = $this->mod_render->get_next_queue_entry();

			// Reset Failed Adverts
			$this->mod_render->try_again();
			
			// Is Item
			if ( ! empty($qI) ) {

				// Status
				echo("PROCESSING RENDER QUEUE ITEM\n");

				// Is there an issue?
				if ( empty($qI['source']) || ! file_exists( $rPath . $qI['source'] ) ) {
					
					// File Not Found
					$this->mod_render->update_queue( $qI['id'], 3 );

				}
				else {

					// Mark Processing
					$this->mod_render->update_queue( $qI['id'], 1 );
					
					// Variables
					$sPath = $rPath . $qI['source'];
					$dPath = $rPath . str_replace(".dat", ".mp4", $qI['source']);

					// Render Video
					$this->mod_render->render_video( $sPath , $dPath );

					// Update Status
					$qI = $this->mod_render->get_queue_item( $qI['id'] );

					// Still Applicable?
					if ( empty($qI) || $qI['status'] == 2 ) {

						// Status
						$this->mod_global->system_log_entry("Asset {$qI['source']} Render Request Cancelled");
						echo("REQUEST WAS CANCELLED\n");

						// Render Request was Cancelled
						@unlink($sPath);
						@unlink($dPath);

						// Mark Expired
						$this->mod_render->update_queue( $qI['id'], 1000 );

					}
					else {

						// Check
						if ( file_exists($dPath) ) {
						
							// Status
							echo("RENDERING COMPLETE\n");
							$this->mod_global->system_log_entry("Asset {$qI['source']} Render Complete to {$qI['destination']}");

							// Move Asset
							if ( $qI['type'] == 2 )
								@rename( $dPath, $qI['destination']);
							else
								@rename( $dPath, $cPath . $qI['destination']);
							
							// Cleanup
							$this->mod_render->complete_queue( $qI['id'] );
							@unlink($sPath);
	
						}
						else {
	
							// Status
							echo("RENDERING FAILED\n");
							$this->mod_global->system_log_entry("Asset {$qI['source']} Rendering Failed");

							// Error
							$this->mod_render->update_queue( $qI['id'], 3 );
	
						}

					}

				}

			}

			// Status
			echo("CLEANING RENDER QUEUE\n\n");

			// Get Cancelled Requests
			$qE = $this->mod_render->get_cancelled_items();
			if ( ! empty($qE) ) {
				foreach ($qE as $cancelled) {

					// Calculate Time
					$cancelled['lastaction'] = time() - strtotime($cancelled['lastaction']);

					// Expire Requests
					if ( $cancelled['lastaction'] >= 3600 ) {

						// Delete Files
						if ( ! empty($cancelled['source']) || file_exists($rPath . $cancelled['source']) ) {
							@unlink($rPath . $cancelled['source']);
						}

						// Update Status
						$this->mod_render->update_queue( $cancelled['id'], 1000 );

					}
				}
			}

			// Delete Expired
			$this->mod_render->clear_expired();

		}

		// Require Authenticated Login
		echo("CRON RUN COMPLETED");

	}



}