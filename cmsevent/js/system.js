/**
 * @Application Flags
 */

var dialogShown = false;				// Flag we have an active modal dialog


// Set Date Picker Defaults
$.datepicker.setDefaults({
	dateFormat: "dd/mm/yy",
	constrainInput: false,
	firstDay: 1
});


/**
* isNumeric
*
* @version		1.00
*
* @license		None
* @author		Robin Hickmott
* @copyright	Willow Communications Ltd
* @purpose		Checks if an Number is Set
*/

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


/**
 * isSet
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright		Willow Communications Ltd
 * @purpose		Checks if an Element is SET (XML)
 */

 function isSet(elementName) {
 	try {
 		if (elementName !== undefined && elementName !== "") {
	 		return true;
 		}
 	}
 	catch (err) {
 		return false;
 	}

 	// Default
 	return false;
 }
 
 
 /**
 * showDialog
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright		Willow Communications Ltd
 * @purpose		Shows a Dialogue Box to the User
 */

function showDialog(windowText, windowTitle, fClose) {
	if (dialogShown===false) {

		// Do Dialog
		var dialog = $('<div></div>')
		.html('<div>' + windowText + '</div>')
		.dialog({
			autoOpen: false,
			title: windowTitle,
			resizable: false,
			modal: true,
			width:'400px',
			buttons: {
				'Ok': function() {
					$(this).dialog('close');
				}
			},
			close: function() { $(this).html(''); dialogShown = false; if ($.isFunction(fClose)) fClose(); }
		});

		dialogShown = true;
		$(dialog).dialog('open');

	}

	// prevent the default action, e.g., following a link
	return false;
}


 /**
 * loadIframeDialog
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright	Willow Communications Ltd
 * @purpose		Loads a URL Into an iFrame Dialogue with it's own DOM
 */

function loadIframeDialog(pUrl, pTitle, fClose, dOpts) {

	// Variables
	var tWidth = 'auto';				// Default Width
	var tHeight = 'auto';				// Default Height

	// Process Options
	if (typeof dOpts == 'object') {
		if (dOpts['width'] !== undefined)		tWidth = dOpts['width'];
		if (dOpts['height'] !== undefined)		tHeight = dOpts['height'];
    }
	
	// Create IFrame
	var iframe = $('<iframe frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>').attr({
		width: 	tWidth,
		height: tHeight,
		src: pUrl		 
	});		
	
	// Show Dialog
	var dialog = $("<div id='iframe_dialog'></div>").append(iframe).appendTo("body").dialog({
		autoOpen: true,
		modal: true,
		resizable: true,
		width: 'auto',
		height: 'auto',
		title: pTitle,
		close: function () {
			
			// Clear iFrame
			iframe.attr("src", "");
			
			// Call Closing Function
			if ($.isFunction(fClose)) fClose();
			
			// Remove Dialogue
			$("#iframe_dialog").remove();
			
		}
	});
			
}


/**
 * showConfirm
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright	Willow Communications Ltd
 * @purpose		Shows a Confirmation Box to the User
 */

function showConfirm(windowText, windowTitle, fYes, fNo) {

	// Do Dialog
	var dialog = $('<div />')
	.html('<div>' + windowText + '<br />Do you wish to continue?</div>')
	.dialog({
		autoOpen: true,
		title: windowTitle,
		resizable: false,
		modal: true,
		width:'400px',
		buttons: {
			'Yes': function() {
				if ($.isFunction(fYes)) fYes();
				$(this).dialog('close');
			},
			'No': function() {
				if ($.isFunction(fNo)) fNo();
				$(this).dialog('close');
			}
		},
		close: function() { $(this).dialog('destroy').remove(); }
	});

}


/**
 * ajaxData
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright	Willow Communications Ltd
 * @purpose		Core Ajax Wrapper
 */

function ajaxData( url, fSuccess, fFail, dOpts) {

	// Variables
	var tPostType = "GET";				// Default Submit Type
	var tData = null;					// Params to Pass to Server

	// Process Options
	if (typeof dOpts == 'object') {

		// Override Submit Type
		if (isSet(dOpts['type'])) {
			switch(dOpts['type'].toString().toUpperCase()) {
				case "POST":
					tPostType = "POST";
					break;
			}
		}

		// Pass Data
		if (dOpts['data'] !== undefined && typeof dOpts['data'] === 'string') {
			tData = dOpts['data'];
		}

    }

	// Check Input
	if (!isSet(url)) {
		if ($.isFunction(fFail)) fFail("No Url Specified for Dialog");
		return false;
	}

	// Do Ajax
	$.ajax({
	  url: url,
	  type: tPostType,
	  cache: false,
	  data: tData,
	  dataType: "text",
	  success: function(data) {
			// Return Data
			if ($.isFunction(fSuccess))	fSuccess(data);
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
	  		// Comms Error
	  		if ($.isFunction(fFail)) fFail("No valid reply returned from Web Server for the requested address");
	  }
	});

}


 /**
 * loadDialog
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright	Willow Communications Ltd
 * @purpose		Loads Content from a URL into a Dialog
 */

function loadDialog(pUrl, pTitle, fSuccess, fFail, fClose, dOpts) {

	// Variables
	var tWidth = 'auto';				// Default Width
	var tHeight = 'auto';				// Default Height
	var tText = 'Loading Please Wait'	// Default Text

	// Process Options
	if (typeof dOpts == 'object') {
		if (dOpts['width'] !== undefined)		tWidth = dOpts['width'];
		if (dOpts['height'] !== undefined)		tHeight = dOpts['height'];
		if (dOpts['text'] !== undefined)		tText = dOpts['text'];
    }

	// Do Dialog
	var dialog = $('<div class=\"dialog-load\" style=\"text-align:center\" />')
	.html('<br /><img src=\"' + base_url("images/ajax-loader.gif") + '" class=\"inline\"> <span>' + tText + '</span>')
	.dialog({
		title: pTitle,
		resizable: true,
		modal: true,
		width: '200px',
		height: 'auto',
		autoOpen : true,
		close: function() { $(this).dialog('destroy').remove(); }
	});

	// Load Content
	ajaxData(pUrl,
		function(data) {

			// Close Loader
			$(dialog).dialog('close');

			// Show New Dialog
			var d = $("<div class=\"dialog-box\" />");
			$(d).html(data);
			$(d).dialog({
				title: pTitle,
				resizable: true,
				modal: true,
				width: tWidth,
				height: tHeight,
				autoOpen : true,
				close: function() { if ($.isFunction(fClose)) fClose(); $(this).dialog('destroy').remove();  }
			});

			// Done
			if ($.isFunction(fSuccess)) fSuccess(data);

		},
		function (e) {
			$(dialog).html("<div align=\"center\"><img class=\"inline\" src=\"" + base_url("images/cross.png") + "\" style=\"width: 64px; height: 64px\"><span>" + e + "</span></div>");
			if ($.isFunction(fFail)) fFail(e);
		}
	);

}



 
 /**
 * loadContent
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright	Willow Communications Ltd
 * @purpose		Loads Content into a DIV or Displays an Error
 */

 function loadContent(pText, pDiv, pUrl, fSuccess, fFail) {
 
 	// Check URL
	if (!isSet(pDiv)) {
		if ($.isFunction(fFail)) fFail("Container Not Specified");
		return false;
	}

	// Check URL
	if (!isSet(pUrl)) {
		if ($.isFunction(fFail)) fFail("URL Not Specified");
		return false;
	}

	// Show Loading Animation
	$(pDiv).empty();
	$(pDiv).append("<img src=\"" + base_url("images/ajax-loader.gif") + "\" /> <b>" + pText + "</b>");

	// Do Request
	ajaxData(pUrl,
		function (data) {
			$(pDiv).empty();
			$(pDiv).append(data);
			if ($.isFunction(fSuccess)) fSuccess(data);
		},
		function (e) {
			$(pDiv).empty();
			$(pDiv).append("<img src=\"" + base_url("images/cross.png") + "\" class=\"inline\" /><span>Error Loading Data</span>");
			if ($.isFunction(fFail)) fFail(e);
		},
		{"xml":false}
	);
	
 }


/**
 * previewOutput
 *
 * @version		1.00
 *
 * @license		None
 * @author		Robin Hickmott
 * @copyright	Willow Communications Ltd
 */

function previewOutput(player,output) {

	// Do Defaults
	if (player===undefined)	player = "";
	if (output===undefined) output = "";

	// Do Popup
	window.open(base_url('status/outputviewer/') + player + "/" + output, '_blank' , 'width=600,height=600,toolbar=1,menubar=0,location=1,status=0,scrollbars=1,resizable=1');

}



/**
 * Converts Bytes to Something Sensible
 * 
 * @param int bytes 	The Number of Bytes
 * @param int decimals 	The Number of Decimal Places
 */
function bytesToSize(bytes,decimals) {
	if(bytes == 0) return '0 Bytes';
	var k = 1000,
		dm = decimals || 2,
		sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
		i = Math.floor(Math.log(bytes) / Math.log(k));
	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};



/**
 * Copies an Element to the Clipboard
 * 
 * @param object element 	The Element to Copy
 */
function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
	$temp.remove();
	alert("Copied to Clipboard");
}



/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();


// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};


// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};


// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};


/**
 * Triggers a Full Screen Frame
 */
function full_screen_frame( pUrl, pTitle ) {

	// Launch Full Screen Frame
	$("<div id='full-screen-frame'>")
		.append("<div id='full-screen-frame-notification'></div><img class='link close-full-screen-frame' src='" + base_url('images/cross.png') + "'>")
		.append("<h2>" + pTitle + "</h2>")
		.append("<iframe frameBorder='0' id='full-screen-frame-content' src='" + pUrl + "'></iframe>")
		.appendTo("body");

	// Catch Close
	$(document).on("click", ".close-full-screen-frame", function(event ) {
		$("#full-screen-frame").remove();
		$( this ).unbind( event );
	});

}