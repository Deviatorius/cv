<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<style>
	.unread {
		font-weight: bold;
	}
	.unread td {
		background-color: #05a6d138 !important;
	}
</style>


<div class="full-screen-frame-inner-content">

	<div class="menu">
		<div class="menu-item link" title="Back to Menu">
			<a href="<?php echo base_url("home/mobile"); ?>">
				<img class="icon" src="<?php echo base_url("images/back-icon.png"); ?>" /><br />
				BACK        
			</a>
		</div>
	</div>

	<?php 
		if( ! empty($errors) ) {
			$this->load->view("form/submission_error", [ 'header' => 'Error Communicating with Event', 'error' => $errors ]);
		}
	?>

	<h3>Current Shoutouts</h3>
    <table class="data_table" style="clear: both">
		<col style="width: 160px" />
        <col />
        <col style="width: 80px" />
		<col style="width: 80px" />
        <thead>
		<tr class="first purple">
                <td colspan="4">Customer Shoutouts</td>
            </tr>
            <tr class="second purple">
                <td>Type</td>
                <td>Message</td>
                <td align="center">Preview</td>
				<td align="center">Delete</td>
            </tr>						
        </thead>
        <tbody id="results">
            <tr>
				<td colspan="4" style="padding: 20px; text-align: center"><img src="<?php echo base_url("images/ajax-loader.gif") ?>"> Getting Shoutouts</td>
			</tr>
        </tbody>
    </table>

</div>

<script>
	
	// Poll
	function poll() {
		$.ajax({
			url: base_url('home/mobile/shoutouts/poll'),
			type: "json",
			cache: false,
			dataType: "json",
			success: function(data) {
				try {
					$("#results").empty();
					if ( data == "" || Object.keys(data).length <= 0 ) throw 'No Result';
					jQuery.each(data, function(index, itemData) {
						var item = $('<tr />').attr('interaction', index).attr("class", itemData.flags & 2 ? "read" : "unread");
						switch ( itemData.type ) {
							case '2':
								$('<td />').html("Image").appendTo(item);
								break;
							default:
								$('<td />').html("Shoutout").appendTo(item);
								break;
						}
						$('<td />').html(itemData.message).appendTo(item);
						$('<td align="center" />').html('<img class="link preview" src="' + base_url("images/zoom_icon.png") + '">').appendTo(item);
						$('<td align="center" />').html('<img class="link delete" src="' + base_url("images/delete_icon.png") + '">').appendTo(item);
						$("#results").append(item);
					});	
				} 
				catch (e) {
					var item = $('<tr />');
					$('<td colspan="4" style="text-align: center">').text('No Current Shoutouts').appendTo(item);
					$("#results").append(item);
				}
			}
		});
	}


	/**
	 * Events
	 */
	$(document).ready(function()  {

		// Poll
		poll();

		// Capture Event Messages
		window.parent.$(window.parent.document).on("eventMessage", function(evt, data) {
			poll();
		});

		$(document).on("click", ".preview", function() {
			var interaction = $(this).closest("tr").attr("interaction");
			window.parent.fullScreenInnerDialog( "<?php echo base_url("home/interaction/"); ?>" + interaction + "?view=preview", "Preview Shoutout" );
		});

		$(document).on("click", ".delete", function(){
			var interaction = $(this).closest("tr").attr("interaction");                   
			showConfirm("This will delete the interaction<br />Do you wish to continue?", "Delete Interaction", function() {
				ajaxData("<?php echo base_url("home/mobile/delete/") ?>" + interaction, function(data) {
					if ( data == "OK" ) {
						poll();
					}
					else {
						showDialog("Error Deleting Interaction - " + data, "Can't delete interaction.");
					}
				});
			});
		});

	});

	
</script>