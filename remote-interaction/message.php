<!-- QR Code Generator -->
<script type="text/javascript" src="<?php echo base_url('js/qrcode.js') ?>"></script>

<?php 

    // Load Core
    require_once(FCPATH . 'client\\event\\shoutouts\\main-css.php');

    // Load Theme
    if ( ! empty($theme) ) {
        require_once(FCPATH . $theme['path']);
    } 
    
    // Keys
    $next = $id;           // Next Item in the Array
    $prev = $id;           // Prev Item in the Array

    // Do we have Interactions
    if ( ! empty($interactions) && ! empty($id) && count($interactions) >= 2 ) {

        // Het Numeric List
        $keys   =   array_keys($interactions);
        $start  =   0;
        $curr   =   0;
        $end    =   array_key_last($keys);

        // Find Current
        foreach ( $keys as $k => $v ) {
            if ( $v == $id ) {
                $curr = $k;
                break;
            }
        }

        // Do Next
        $prev = $curr == $start ?   $keys[$end]   : $keys[$curr - 1];
        $next = $curr == $end   ?   $keys[$start]     : $keys[$curr + 1];

    }

    // Prepare message
    if ( isset($interaction['message']) ) {
        $message    =   explode(" ", $interaction['message'] );        
    }

?>

<div id="main">
    <div id="qr">
        <div id="qr-background">
            <video autoplay muted loop id="qr-background-video">
                <source src="<?php echo base_url("client/event/shoutouts/videos/{$theme['name']}.mp4")?>" >
            </video>
        </div>
        <div id="qr-overlay">
            <div id="qr-qr"></div>
            <div id="qr-text">
                <div>Share your moments with us by scanning the QR code or by going to the link below: <br> <div><?php echo $result['address']; ?></div></div>
                <div>Site code of the day is: <div><?php echo $result['passphrase']; ?></div></div>
            </div>
        </div>
    </div>
    <div id="wrapper">
        <div id="borders">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>

        <div id="background">
            <video autoplay muted loop id="background-video">
                <source src="<?php echo base_url("client/event/shoutouts/videos/{$theme['name']}.mp4")?>" >
            </video>
        </div>

        <!-- Picture holders -->
        <div id="photo" style="display: none;">
            <div></div>
            <div></div>
        </div>

        <div id="message">
        </div>

        <div id="shoutout">
            <div>
                <?php 
                    if ( ! empty($message) && is_array($message) ) {
                        foreach ($message as $key => $word) {
                            ?> <span style="--i:<?php echo $key; ?>"><?php echo $word; ?></span><?php
                        }                       
                    }
                ?>
            </div>
        </div>

        <div id="logo">
        </div>

        <div id="decor"></div>
    </div>

    <div id="controls">
        <div id="buttons">
            <div class="link prev" title="Previous"><a href="<?php echo base_url("home/interaction/{$prev}?view=preview&theme=") . ( ! empty($theme)  ? $theme['filename'] : "Basic"); ?>"><img src="<?php echo base_url("/images/back-icon.png"); ?>" /><br> PREVIOUS </a></div>
            <div class="link next" title="Next"><a href="<?php echo base_url("home/interaction/{$next}?view=preview&theme=") . ( ! empty($theme)  ? $theme['filename'] : "Basic"); ?>"><img src="<?php echo base_url("/images/next-icon.png"); ?>" /><br> NEXT </a></div>
            <div class="link delete" title="Delete" interaction="<?php echo $id; ?>"><a ><img src="<?php echo base_url("/images/trash-icon.png"); ?>" /><br> DELETE </a></div>
            <div class="link play" title="Play" interaction="<?php echo $id; ?>" theme="<?php echo $theme['filename']?>" dur="40" qr="60"><a ><img src="<?php echo base_url("/images/play-icon.png"); ?>" /><br> PLAY </a></div> 
        </div>
        <div>        
            <div id="options">
            <div class="header">Options</div>
            <div class="option">Duration (seconds)<input class="duration osk"></input></div>
            <div class="option">QR Display (seconds)&nbsp<input class="qr osk"></input></div>
            </div>
            <div id="themes">
                <div class="header">Themes</div>
                <?php 
                    foreach ( $themes as $sTheme ) {
                        if ($sTheme['filename'] == $theme['filename']) {
                            ?>
                                <a href="<?php echo base_url("home/interaction/{$id}?view=preview&theme={$sTheme['filename']}"); ?>"><div name="<?php echo $sTheme['name'] ?>" class="link theme selected" interaction="<?php echo $id ?>"><?php echo $sTheme['name'] ?></div></a>
                            <?php
                        } 
                        else {
                            ?>
                                <a href="<?php echo base_url("home/interaction/{$id}?view=preview&theme={$sTheme['filename']}"); ?>"><div name="<?php echo $sTheme['name'] ?>" class="link theme" interaction="<?php echo $id ?>"><?php echo $sTheme['name'] ?></div></a>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>
    </div>
    <div id="loading" style="display: none;"></div>

</div>


<?php 


    // VIEW 
    if ( empty($view) ) {
        ?>
            <script>

                // Hide Controls
                $("#controls").hide();

                /**
                * Everything Has Loaded
                */
                $(window).on('load', function(){ 
                    setTimeout(function(){
                        $(" #borders ").hide();
                        $(" #wrapper ").fadeOut();
                        $(" #qr ").fadeIn();
                        // Do Close
                        setTimeout(function() {
                            try {
                                window.chrome.webview.postMessage("CLOSE");    
                            } catch (error) {
                                // Do Nothing (Not Supported)
                            }
                        }, <?php echo $qr_show; ?>000);

                    }, <?php echo $border_speed * 2; ?>000);
                });
                
                // Generate QR Code
                $('#qr-qr').qrcode({text: "<?php echo $result['qrcode']; ?>"});
            </script>
        <?php
    }

    // PREVIEW
    if ( ! empty($view) ){ ?>
        <script>
            // Preview Styling
            $(" #wrapper ").css("height", "100%").css("width", "80%");

            // Content and loader
            $(" #wrapper-preload ").hide();
            $(" #wrapper ").show();

            // Everything Has Loaded
            $(window).on('load', function(){ 

                try {
                    // Mark as Read
                    window.parent.e_EventSocket.send("control&action=readinteraction&id=" + encodeURIComponent("<?php echo html_escape($id) ?>"));
                } 
                catch (error) {
                    console.log(error);
                }

                // Add Number Picker UI
		        dpUI.numberPicker(".duration", {min:5, max: 7200, start: 40, afterChange:function(){   $(" .play ").attr("dur", $(this).val())   }});
		        dpUI.numberPicker(".qr", {min:0, max: 7200, start: 60, afterChange:function(){   $(" .play ").attr("qr", $(this).val())   }});

            });

            // Do Controls
            $(" .delete ").on("click", function(){
			    var interaction = $(this).closest("div").attr("interaction");    
                showConfirm("This will delete the interaction<br />Do you wish to continue?", "Delete Interaction", function() {
                    ajaxData("<?php echo base_url("home/mobile/delete/") ?>" + interaction, function(data) {
                        if ( data == "OK" ) {
                            $(".next a")[0].click();
                            lockControls();
                        }
                        else {
                            showDialog("Error Deleting Interaction - " + data, "Can't delete interaction.");
                        }
                    });
			    });
            });

            $(" .play ").on("click", function(){
			    var interaction = $(this).closest("div").attr("interaction");    
			    var theme = $(this).closest("div").attr("theme"); 
                var duration = $(this).closest("div").attr("dur"); 
                var qr = $(this).closest("div").attr("qr"); 
                if ( window.parent.e_EventSocket === undefined || ! window.parent.e_EventConnected  ) {
                    showDialog("Were not currently connected to Event!", "Error Triggering Message")
                }
                else {
                    window.parent.e_EventSocket.send("control&action=webmessage&url=" + encodeURIComponent("<?php echo external_url(); ?>home/interaction/" + interaction + "?theme=" + theme + "&dur=" + duration + "&qr=" + qr) );
                    $(".next a")[0].click();
                    lockControls();
                }
            });

            $(" #controls .link ").on("click", function(){
                lockControls();
            });

            $(" .theme ").on("click", function(){
                $(this).closest("a").click();
            });


            // Time in MS Until Automatic Unlock
            var minimumTime = 5000;

            // Holds Lock Timer
            var lockTimer = null;

            //Lock Touchscreen Controls
            function lockControls() {
                clearTimeout(lockTimer);
                $(" #loading ").show();
                lockTimer     =   setInterval(unlockControls, minimumTime);
            }

            // Unlock Controls
            function unlockControls() {
                clearTimeout(lockTimer);
                $(" #loading ").hide();
            }


        </script>
    <?php }

?>