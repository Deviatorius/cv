<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * Advert Model
	 */
	class Mod_OTB extends CI_Model {


        /**
         * The Database this Module is Using
         */
        var $path = NULL;


        /**
         * Shuffle Speed
         */
        var $shuffleSpeed = [
            "0.5"      =>  "Slow (0.5sec)" ,
            "0.3"      =>  "Average (0.3sec)",
            "0.1"      =>  "Fast (0.1sec)"
        ];


        /**
         * Returns Game Model
         */
        public function otb_model( $data = null ) {
            
            // Create Model
            $r = [  
                'game'          =>  [
                    "id"                    =>      "",
                    "name"                  =>      "Unnamed Game",
                    "shuffleSpeed"          =>      0,
                    'prize1'                =>      "",
                    'prize2'                =>      "",
                    'prize3'                =>      "",
                    'prize4'                =>      "",
                    'prize5'                =>      "",
                    'prize6'                =>      "",
                    'prize7'                =>      "",
                    'prize8'                =>      "",
                    'prize9'                =>      "",
                    'prize10'               =>      "",
                ],
                'pd'            => [
                    'flag'                  =>       0,
                    'box1open'              =>       0,
                    'box2open'              =>       0,
                    'box3open'              =>       0,
                    'box4open'              =>       0,
                    'box5open'              =>       0,
                    'box6open'              =>       0,
                    'box7open'              =>       0,
                    'box8open'              =>       0,
                    'box9open'              =>       0,
                    'box10open'             =>       0,
                    'prize1claimed'         =>       0,
                    'prize2claimed'         =>       0,
                    'prize3claimed'         =>       0,
                    'prize4claimed'         =>       0,
                    'prize5claimed'         =>       0,
                    'prize6claimed'         =>       0,
                    'prize7claimed'         =>       0,
                    'prize8claimed'         =>       0,
                    'prize9claimed'         =>       0,
                    'prize10claimed'        =>       0
                ]
                ];

            // Populate Model
            if ( ! empty($data) && is_array($data) ) { 
                $this->walk( $r, $data );
            }

            // Return
            return $r;

        }


        /**
         * Walk the Tree
         *
         * @param [type] $d - Model to Populate
         * @param [type] $e - Array to Pupolate the Model With
         * @return void
         */
        public function walk( &$d, &$e ) {
            foreach ( $e as $k => $value ) {
                if ( is_array($value) && isset($d[$k]) && is_array($d[$k]) ) {
                    $this->walk( $d[$k], $value );
                }
                else {
                    $d[$k] = $value;
                }
            }
        }


        /**
         * Gets All Game Sessions
         *
         * @return void
         */
        public function get_games() {

            // Holds Return Data
            $rData = [];

            // Scan Directories
            if ( @is_dir($this->path) && @file_exists($this->path) ) {
                foreach (scandir($this->path) as $file) {
                    if ( ! is_dir($file) && $file !== "." && $file !== ".." ) {

                        // Grab File Data
                        $ext = pathinfo($file);

                        // Grab Game
                        if ( mb_strtolower($ext['extension']) == "ini" ) {

                            // Read File
                            $fContents  =   @parse_ini_file( $this->path . $file, true );
                            $cMode      =   $this->otb_model($fContents);

                            // Assign Data
                            $rData[]    =   $cMode;

                        }

                    }
                }
            }

            // Return
            return $rData;

        }


        /**
         * Gets Game Session by ID
         */
        public function get_game( $id ) {

            // Does File Exist
            if ( @is_dir($this->path) && @file_exists( $this->path . $id . ".ini" ) ) {

                // Read File
                $fContents =    @parse_ini_file( $this->path . $id . ".ini", true );
                $cMode      =   $this->otb_model($fContents);

                return $cMode;

            }
            
            // Not Found
            return false;

        }


        /**
         * Saves the Model
         */
        public function save( $model ) {

            // Save Model
            if ( empty($model) )    return FALSE;

            // Create Model
            if ( empty($model['game']['id']) ) {

                // Generate ID
                for ( $i=1; $i <= 50; $i++ ) {

                    // Generate ID
                    $model['game']['id']    =   uniqid("");

                    // Find File
                    if ( ! file_exists( $this->path . $model['game']['id'] . ".ini" ) ) {

                        // Write Settings
                        if ( write_ini_file( $this->path . $model['game']['id'] . ".ini" , $model ) ) {
                            return TRUE;
                        }

                    }

                }

            }
            else {

                // Update an Existing Model
                if ( file_exists( $this->path . $model['game']['id'] . ".ini" ) ) {

                    // Write Settings
                    if ( write_ini_file( $this->path . $model['game']['id'] . ".ini" , $model ) ) {
                        return TRUE;
                    }
                
                }

            }

            // Not Saved
            return FALSE;

        }


        /**
         * Deletes a Open the Box
         */
        public function delete( $id ) {

            // Does File Exist
            if ( @is_dir($this->path) && @file_exists( $this->path . $id . ".ini" ) ) {

                // Read File
                @unlink($this->path . $id . ".ini");
                return true;
                
            }
            else {
                
                // Not Found
                return false;

            }

        }


	}

?>