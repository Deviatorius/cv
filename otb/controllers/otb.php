<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Page Controller
 */
class OTB extends MY_Controller {


	/**
	 * Holds Module Settings
	 */
	var $module = [];

	/**
	 * Constructor
	 *
	 * @return void Controller Constructor
	 */
	function __construct() {

		// Run Parent Constructor
		parent::__construct();

		// Add Module Scripts & Stylesheets
		$this->module_version 	= 	"OTB2.1";
		$this->css[]			=	module_url("css/otb.css?module={$this->module_version}");
		$this->js[]				=	module_url("js/otb.js?module={$this->module_version}");

		// Load Module Settings
		$this->module		=	$this->mod_modules->get_module("otb");

		// Import Module Settings
		if ( ! empty( $this->module['settings']['additional'] ) && is_array($this->module['settings']['additional']) ) {
			$this->settings = import_override_settings( $this->settings, $this->module['settings']['additional'], true );
		}

		// Check Required Settings
		if ( empty($this->settings['hosts']['event_host']) )	die("Cannot Use Module - No Event Hostname Set");
		if ( empty($this->settings['hosts']['event_port']) )	die("Cannot Use Module - No Event Port Set");

		// Sanatize Event Path
		if ( empty($this->settings['hosts']['event_server']) ) {
			$this->settings['hosts']['event_server'] = "ws://" . $this->settings['hosts']['event_host'] . "/ws?port=" . $this->settings['hosts']['event_port'];
			$this->settings['hosts']['event_server'] = str_replace("ws://127.0.0.1", "ws://{$_SERVER['HTTP_HOST']}", $this->settings['hosts']['event_server']);
		}

		// Sanatize Settings
		if ( empty($this->settings['otb']['data'])  ) $this->settings['otb']['data'] = $this->module['path'] . "data/";
		if ( empty($this->settings['otb']['mode'])  ) $this->settings['otb']['mode'] = "GAMES";

		// Load Model
		$this->load->model('mod_otb');
		$this->mod_otb->path = $this->settings['otb']['data'];
		$this->mod_otb->pdPath = $this->settings['otb']['pdData'];

	}



	/**
	 * Page Index
	 *
	 * @return void
	 */
	public function index() {

		// Variables
		$data 				= 	[];									// Holds Data passed to Views
		$data['module']		= 	$this->module;						// Pass Module Settings to the View
		$data['games']		=	$this->mod_otb->get_games();			// Grab Current OTB games

		// Load Page Contents
		$page	=	$this->load->view("home", $data, TRUE);

		// Display Page Contents
		$this->display_portal("Open the Box", $page );

	}



	/**
	 * Create a New Open the Box
	 *
	 * @return void
	 */
	public function create() {

		// Load Additional Functions
		$this->load->helper('form');
		$this->load->library('form_validation');

		// Variables
		$error 					=	[];												// Holds Submissions Errors
		$data 					= 	[];												// Holds Data passed to Views
		$data['error']			=	null;											// No Submission Errors
		$data['module']			= 	$this->module;									// Pass Module Settings to the View

		// Handle Submissions
		if ( ! empty($_POST) ) {

			// Validation Rules
			$this->form_validation->set_rules('data[name]', 			'Game Name', 			"trim|required|min_length[1]|max_length[50]");
			$this->form_validation->set_rules('data[shuffleSpeed]', 	'Shuffle Speed', 		"trim|required|numeric|min_length[1]|max_length[4]");
			$this->form_validation->set_rules('data[prize1]',		 	'Box One Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize2]', 			'Box Two Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize3]', 			'Box Three Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize4]', 			'Box Four Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize5]', 			'Box Five Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize6]', 			'Box Six Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize7]', 			'Box Seven Prize',		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize8]', 			'Box Eight Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize9]', 			'Box Nine Prize', 		"trim|required|min_length[1]|max_length[16]");
			$this->form_validation->set_rules('data[prize10]', 			'Box Ten Prize', 		"trim|required|min_length[1]|max_length[16]");

			// Run CI Validation (Note changes for HMVC Validation)
			if ($this->form_validation->run($this) == FALSE) {
				foreach ($this->form_validation->error_array() as $cierror) {
					$error[] = $cierror;
				}
			}

			// Process Errors
			if ( count($error) <= 0 ) {

				// No Errors
				$eModel = $this->mod_otb->otb_model();

				// Populate Model
				$eModel['game']['name']						=	$_POST['data']['name'];
				$eModel['game']['shuffleSpeed']				=	$_POST['data']['shuffleSpeed'];
				$eModel['game']['prize1']					=	$_POST['data']['prize1'];
				$eModel['game']['prize2']					=	$_POST['data']['prize2'];
				$eModel['game']['prize3']					=	$_POST['data']['prize3'];
				$eModel['game']['prize4']					=	$_POST['data']['prize4'];
				$eModel['game']['prize5']					=	$_POST['data']['prize5'];
				$eModel['game']['prize6']					=	$_POST['data']['prize6'];
				$eModel['game']['prize7']					=	$_POST['data']['prize7'];
				$eModel['game']['prize8']					=	$_POST['data']['prize8'];
				$eModel['game']['prize9']					=	$_POST['data']['prize9'];
				$eModel['game']['prize10']					=	$_POST['data']['prize10'];

				// Save Model
				if ( $this->mod_otb->save($eModel) ) {

					// Redirect
					$this->session->set_flashdata('submission_message', "Open the Box {$_POST['data']['name']} created");
					session_write_close();
					header("Location: " . base_url("otb"));
					exit();

				}
				else {

					// Error Saving
					$error[]			=	"An Error Occured creating the Open the Box";
					$data['error']		=	$this->load->view("form/submission_error", ["error" => $error], TRUE);

				}

			}
			else {

				// Display Errors
				$data['error']		=	$this->load->view("form/submission_error", ["error" => $error], TRUE);

			}

		}

		// Load Page Contents
		$page	=	$this->load->view("create", $data, TRUE);

		// Display Page Contents
		$this->display_portal("Open the Box", $page );

	}


	/**
	 * Edit an Existing Open the Box
	 *
	 * @return void
	 */
	public function edit() {

		// Load Additional Functions
		$this->load->helper('form');
		$this->load->library('form_validation');

		// Variables
		$error 					=	[];														// Holds Submissions Errors
		$data 					= 	[];														// Holds Data passed to Views
		$data['error']			=	null;													// No Submission Errors
		$data['module']			= 	$this->module;											// Pass Module Settings to the View
		$data['id']				=	$this->uri->rsegment('3');								// Grab Open the Box ID
		$data['otb']		=	$this->mod_otb->get_game( $data['id'] );						// Get Open the Box

		// Check
		if ( ! is_array($data['otb']) ) {

			// No Such Open the Box
			$page	=	$this->load->view("errors/general_error", ["error" => "The specified open the box could not be loaded or has already been deleted"], TRUE);

		}
		else {

			// Handle Submissions
			if ( ! empty($_POST) ) {

				// Validation Rules
				$this->form_validation->set_rules('data[name]', 			'Game Name', 			"trim|required|min_length[2]|max_length[50]");
				$this->form_validation->set_rules('data[shuffleSpeed]', 	'Shuffle Speed', 		"trim|required|numeric|min_length[1]|max_length[4]");
				$this->form_validation->set_rules('data[prize1]',			'Box One Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize2]',			'Box Two Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize3]', 			'Box Three Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize4]', 			'Box Four Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize5]', 			'Box Five Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize6]', 			'Box Six Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize7]', 			'Box Seven Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize8]', 			'Box Eight Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize9]', 			'Box Nine Prize', 		"trim|required|min_length[1]|max_length[16]");
				$this->form_validation->set_rules('data[prize10]', 			'Box Ten Prize', 		"trim|required|min_length[1]|max_length[16]");

				// Run CI Validation (Note changes for HMVC Validation)
				if ($this->form_validation->run($this) == FALSE) {
					foreach ($this->form_validation->error_array() as $cierror) {
						$error[] = $cierror;
					}
				}

				// Process Errors
				if ( count($error) <= 0 ) {

					// Update Model
					$data['otb']['game']['name']					=	$_POST['data']['name'];
					$data['otb']['game']['shuffleSpeed']			=	$_POST['data']['shuffleSpeed'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize1'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize2'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize3'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize4'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize5'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize6'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize7'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize8'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize9'];
					$data['otb']['game']['prize']					=	$_POST['data']['prize10'];

					// Save Model
					if ( $this->mod_otb->save($data['otb']) ) {

						// Redirect
						$this->session->set_flashdata('submission_message', "Open the Box {$_POST['data']['name']} updated");
						session_write_close();
						header("Location: " . base_url("otb"));
						exit();

					}
					else {

						// Error Saving
						$error[]			=	"An Error Occured updating the Open the Box";
						$data['error']		=	$this->load->view("form/submission_error", ["error" => $error], TRUE);

					}

				}
				else {

					// Display Errors
					$data['error']		=	$this->load->view("form/submission_error", ["error" => $error], TRUE);

				}

			}

			// Load Page Contents
			$page	=	$this->load->view("edit", $data, TRUE);

		}

		// Display Page Contents
		$this->display_portal("Open the Box", $page );

	}


	/**
	 * Edit an Existing Open the Box
	 *
	 * @return void
	 */
	public function play() {

		// Variables
		$error 					=	[];														// Holds Submissions Errors
		$data 					= 	[];														// Holds Data passed to Views
		$data['module']			= 	$this->module;											// Pass Module Settings to the View
		$data['id']				=	$this->uri->rsegment('3');								// Grab Open the Box ID
		$data['otb']			=	$this->mod_otb->get_game( $data['id'] );					// Get Open the Box

		// Check
		if ( ! is_array($data['otb']) ) {

			// No Such Open the Box
			$page	=	$this->load->view("errors/general_error", ["error" => "The specified open the box could not be loaded or has been deleted"], TRUE);

		}
		else {

			// Load Page Contents
			$page	=	$this->load->view("play", $data, TRUE);

		}

		// Display Page Contents
		$this->display_portal("Open the Box", $page );

	}


	/**
	 * Deletes a Open the Box
	 */
	public function delete() {

		// Variable Declarations
		$data				=	[];
		$data['id']			=	$this->uri->rsegment('3');
		$data['confirm']	=	$this->uri->rsegment('4');
		$data['otb']	=	$this->mod_otb->get_game( $data['id'] );

		// Check
		if ( ! is_array($data['otb']) ) {

			// No Such Template
			$page	=	$this->load->view("errors/general_error", ["error" => "The specified open the box could not be loaded or has already been deleted"], TRUE);

		}
		else {

			// Action Deletion
			if ( $data['confirm'] == "go" ) {
				$this->mod_otb->delete($data['id']);
				$this->session->set_flashdata('submission_message', "Open the Box {$data['countdown']['name']} deleted");
				session_write_close();
				header("Location: " . base_url("otb"));
				exit();
			}

			// Load Page Contents
			$page	=	$this->load->view("delete", $data, TRUE);

		}

		// Display Page Contents
		$this->display_portal("Open the Box", $page);

	}


	/**
	 * Do Help Page
	 *
	 * @return void
	 */
	public function help() {

		// Display Portal
		echo $this->load->view("panel/header_dialog", [ "title" => "Help" ], true);
		echo $this->load->view("help/home", [], TRUE);

	}
	
}