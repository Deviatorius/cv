<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<div class="menu">
    <div class="menu-item link event-help" title="Help Me">
        <img class="icon" src="<?php echo base_url("images/help-icon.png"); ?>" /><br />
        HELP
    </div>
    <div class="menu-item link" title="Back to Menu">
        <a href="<?php echo base_url("home/menu/" . $module['settings']['path']); ?>">
            <img class="icon" src="<?php echo base_url("images/back-icon.png"); ?>" /><br />
            BACK        
        </a>
    </div>
    <div class="menu-item link" title="Create Game">
        <a href="<?php echo base_url("otb/create"); ?>">
            <img class="icon" src="<?php echo base_url("images/add-icon.png"); ?>" /><br />
            ADD
        </a>
    </div>
</div>

<h2>Current Open the Box Games</h2>
<p>View and Manage Current Open the Box Games</p>

<table class="data_table">
    <col />
    <col style="width: 75px" />
    <col style="width: 75px" />
    <col style="width: 75px" />
    <thead>
        <tr class="first purple">
            <td colspan="4">Current Games</td>
        </tr>
        <tr class="second purple">
            <td>Open the Box Name</td>
            <td align="center">Play</td>
            <td align="center">Edit</td>
            <td align="center">Delete</td>						
        </tr>						
    </thead>
    <tbody>
        <?php
            if ( empty($games) ) {
                ?>
                    <tr>
                        <td colspan="4" style="text-align: center; padding: 10px;">No Open the Box Games Setup</td>
                    </tr>  
                <?php
            }
            else {
                foreach ( $games as $game ) {
                    ?>
                        <tr class="game-instance" game="<?php echo html_escape($game['game']['id']); ?>" >
                            <td><?php echo html_escape($game['game']['name']) ?></td>
                            <td align="center"><img title="Play Game" class="game-link play link" src="<?php echo base_url("images/zoom_icon.png"); ?>"></td>
                            <td align="center"><img title="Edit Game" class="game-link edit link" src="<?php echo base_url("images/edit_icon.png"); ?>"></td>
                            <td align="center"><img title="Delete Game" class="game-link delete link" src="<?php echo base_url("images/delete_icon.png"); ?>"></td>
                        </tr> 
                    <?php
                }
            }
        ?>  
    </tbody>
</table>

<script>

    /**
     * Handle Icon Clicks
     */
    $(".play").click(function()   {     location.href = '<?php echo base_url("otb/play/") ?>' + $(this).parents("tr").attr("game");  });
    $(".edit").click(function()   {     if ( $(this).css("opacity") == 1 )     location.href = '<?php echo base_url("otb/edit/") ?>' + $(this).parents("tr").attr("game");  });
    $(".delete").click(function() {     if ( $(this).css("opacity") == 1 )     location.href = '<?php echo base_url("otb/delete/") ?>' + $(this).parents("tr").attr("game");  });


    /**
     * Reset Table
     *
     * @return void
     */
    function reset() {
        $(".play").attr("src", "<?php echo base_url("images/zoom_icon.png"); ?>");
        $(".edit").attr("src", "<?php echo base_url("images/edit_icon.png"); ?>");
        $(".delete").attr("src", "<?php echo base_url("images/delete_icon.png"); ?>");
        $(".game-link").addClass("link");
        $(".game-link").css("opacity", 1);
    }

    /**
     * Handle Event Traffic
     */
    $(document).on("eventMessage", function(evt, data) {
		try {

            // Check for Data and Display Comms Error
            console.log(data);
            if ( data == "" ) 	return;

            // Parse Data
            var xml = $.parseXML(data);

            // Find the SVMedia XML implementation
            $(xml).find('Event').each( function() {

                // Reset Table
                reset();

                // Nothing to do here if were not running a game
                if ( $(this).attr('GameName') !== undefined && $(this).attr('GameName').toUpperCase() != "OTB" ) {
                    return;
                }   

                // Loop Instances
                var iId = $(this).attr('GameInstance');
                $(".game-instance").each(function() {
                    if ( $(this).attr("game") == iId ) {
                        $(this).find(".edit").css("opacity", 0.2);
                        $(this).find(".edit").removeClass("link");
                        $(this).find(".delete").css("opacity", 0.2);
                        $(this).find(".delete").removeClass("link");
                        $(this).find(".play").attr("src", "<?php echo base_url("images/music/mini-current.png"); ?>")
                    }
                });

            });

        }
        catch (err) {

            var vDebug = ""; 
            for (var prop in err) {           vDebug += "property: "+ prop+ " value: ["+ err[prop]+ "]\n"; } 
            vDebug += "toString(): " + " value: [" + err.toString() + "]"; 
            console.log(vDebug);
            showDialog( err, "Processing Game Message");
            return false;

        }
    });

</script>