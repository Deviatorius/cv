<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>


<div class="menu">
    <div class="menu-item link event-help" title="Help Me">
        <img class="icon" src="<?php echo base_url("images/help-icon.png"); ?>" /><br />
        HELP
    </div>
    <div class="menu-item link" title="Back to Menu">
        <a href="<?php echo base_url("otb"); ?>">
            <img class="icon" src="<?php echo base_url("images/back-icon.png"); ?>" /><br />
            BACK        
        </a>
    </div>
</div>

<h2>Create Open the Box</h2>
<p>Create a New Open the Box Game</p>

<?php
    // Output Errors
    if ( ! empty($error) ) echo $error;
?>

<?php	echo form_open("otb/create"); ?>
<table class="info_table" style="width: 100%;">
    <col style="width: 250px" />
    <col />
    <thead>
        <tr class="first purple">
            <td colspan="2">Open the Box Settings</td>
        </tr>					
    </thead>
    <tbody>
        <tr>
            <td class="second purple">Open the Box Name</td>
            <td><?php	echo form_input(['name' => 'data[name]', 'placeholder' => 'Enter Open the Box Name'], set_value('data[name]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
        <tr>
            <td class="second purple">Shuffle Speed</td>
            <td><?php echo form_dropdown(['name' => 'data[shuffleSpeed]'], $this->mod_otb->shuffleSpeed, set_value('data[shuffleSpeed]', 0.3, FALSE), ['style' => 'width: 300px', 'class' => 'form_dropdown']); ?></td>
        </tr>
    <tbody>
	<thead>
		<tr class="first purple">
			<td colspan="2">Open the Box Details</td>
		</tr>
	</thead>
    <tbody>
        <tr>
            <td class="second purple">Prize 1</td>
            <td><?php	echo form_input(['name' => 'data[prize1]', 'placeholder' => 'Enter Prize 1'], set_value('data[prize1]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 2</td>
            <td><?php	echo form_input(['name' => 'data[prize2]', 'placeholder' => 'Enter Prize 2'], set_value('data[prize2]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 3</td>
            <td><?php	echo form_input(['name' => 'data[prize3]', 'placeholder' => 'Enter Prize 3'], set_value('data[prize3]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 4</td>
            <td><?php	echo form_input(['name' => 'data[prize4]', 'placeholder' => 'Enter Prize 4'], set_value('data[prize4]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 5</td>
            <td><?php	echo form_input(['name' => 'data[prize5]', 'placeholder' => 'Enter Prize 5'], set_value('data[prize5]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 6</td>
            <td><?php	echo form_input(['name' => 'data[prize6]', 'placeholder' => 'Enter Prize 6'], set_value('data[prize6]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 7</td>
            <td><?php	echo form_input(['name' => 'data[prize7]', 'placeholder' => 'Enter Prize 7'], set_value('data[prize7]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 8</td>
            <td><?php	echo form_input(['name' => 'data[prize8]', 'placeholder' => 'Enter Prize 8'], set_value('data[prize8]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 9</td>
            <td><?php	echo form_input(['name' => 'data[prize9]', 'placeholder' => 'Enter Prize 9'], set_value('data[prize9]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td class="second purple">Prize 10</td>
            <td><?php	echo form_input(['name' => 'data[prize10]', 'placeholder' => 'Enter Prize 10'], set_value('data[prize10]', "", FALSE), ['style' => 'width: 300px', 'class' => 'osk']); ?></td>
        </tr>
    </tbody>
    <thead>
        <tr class="first purple">
            <td colspan="2">Create Game</td>
        </tr>					
    </thead>
    <tbody>
        <tr>
            <td class="second purple">Save Game</td>
            <td><?php echo form_submit('data[submit]', 'Save', ['style' => 'width: 80px' ]); ?></td>
        </tr>
    </tbody>
</table>
<?php	echo form_close(""); ?>
