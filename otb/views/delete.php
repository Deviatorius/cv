<?php	defined('BASEPATH') OR exit('No direct script access allowed');	?>

<style>
    .button {
        background-color: #c278d7;
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
    }
</style>
<div class="menu">
    <div class="menu-item link event-help" title="Help Me">
        <img class="icon" src="<?php echo base_url("images/help-icon.png"); ?>" /><br />
        HELP
    </div>
    <div class="menu-item link" title="Back to Menu">
        <a href="<?php echo base_url("otb"); ?>">
            <img class="icon" src="<?php echo base_url("images/back-icon.png"); ?>" /><br />
            BACK        
        </a>
    </div>
</div>

<h2>Delete Open the Box Game</h2>
<p>Deletes an Active Open the Box Game</p>

<?php
    // Output Errors
    if ( ! empty($error) ) echo $error;
?>

<table class="info_table" style="width: 800px;">
    <col style="width: 250px" />
    <col />
    <thead>
        <tr class="first purple">
            <td colspan="2">Open the Box Details</td>
        </tr>					
    </thead>
    <tbody>
        <tr>
            <td class="second purple">Open the Box Name</td>
            <td><?php	echo html_escape($otb['game']['name']) ?></td>
        </tr>
        <tr>
            <td class="second purple">Confirm Deletion</td>
            <td><button class="link button" onclick="DeleteConfirmation()">Delete Game Instance</button></a></td>
        </tr>
    <tbody>
</table>
<script>
    function DeleteConfirmation() {
        showConfirm("Are you sure you want to permanently delete this game instance?", "Delete Game", function() {
            ajaxData("<?php echo base_url("otb/delete/{$otb['game']['id']}/go") ?>",
			function (data) {
				showDialog("Successfully deleted this game instance!", "Successful Deletion", function(){
                    window.history.back();
                });
			},
			function (e) {
				showDialog("Unable to delete game instance! Please try again.", "Error");
			},
			{"xml":false}
		);
        });
    }
</script>