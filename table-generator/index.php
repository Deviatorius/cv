<?php

namespace Learning\OOP\Experiment;


class Table {

    private $columnNames;
    private $columnWidth;
    private $rowNames;
    private $tableRows;
    private $style;
    private $bodies = 0;

    function __construct($platform = null, $style = null ) {

        // Table Row Object
        $this->tableRows = new TableRows;

        // Table Styling
        if ( $platform !== null && $style !== null ) {
            $this->style = $this->table_style_set($platform, $style);
        }

    }

    /**
     * Adds another header and body to the table
     *
     * @return void
     */
    public function extend() {
        $this->bodies++;
    }

    /**
     * Adds Column Names
     *
     * @param mixed ...$columnNamesIn
     * @return void
     */
    public function new_column(...$columnNamesIn) {
        $this->columnNames[$this->bodies] = $columnNamesIn;
    }

    /**
     * Adds Column Width
     *
     * @param mixed ...$width
     * @return void
     */
    public function col_width(...$width) {
        $this->columnWidth[$this->bodies] = $width;
    }

    /**
     * Adds New Row To Body
     *
     * @param mixed ...$data
     * @return void
     */
    public function new_row(...$data) {
        if ( ! empty($this->columnNames[$this->bodies]) && count($this->columnNames[$this->bodies]) == count($data) ) {
            $this->tableRows->add($data, $this->bodies);
        }
        else {
            return debug($this->columnNames[$this->bodies], $data);
        }
    }

    /**
     * Renders HTML Table With Settings Passed Prior to 
     * Calling the Function
     *
     * @return void
     */
    public function deploy() {

        if ( isset( $this->style ) ) {
            $this->deploy_with_style( $this->style );
            return;
        }
        
        // Gather Data
        $rowCount   =   $this->tableRows->get_rows();
        $rowData    =   $this->tableRows->get_data();

        // Start Table
        echo "<table>";

        for ($bodyNumber=0; $bodyNumber <= $this->bodies; $bodyNumber++) { 

            // Add Column Style
            if ( ! empty($this->columnWidth[$bodyNumber]) ) {
                foreach ($this->columnWidth[$bodyNumber] as $key => $value) {
                    if ( $value == "" ) {
                        echo "<col />";
                    }
                    else {
                        echo "<col style='width: {$value}px;' />";
                    }
                }
            }

            // Start Head
            echo "<thead>";
            echo "<tr>";

                foreach ($this->columnNames[$bodyNumber] as $columnNumber => $columnName) {
                    echo "<td>{$columnName}</td>";
                }

            echo "</tr>";
            echo "</thead>";

            // Start Body
            echo "<tbody>";
    
                // Loop Through the Amount of Rows We Have
                for ($i=1; $i < $rowCount[$bodyNumber] + 1  ; $i++) { 
                    echo "<tr>";
    
                        // Add Data to Cells
                        foreach ($rowData[$bodyNumber][$i] as $key => $value) {
                            echo "<td matrix='{$bodyNumber}-{$i}-{$key}'>{$value}</td>";
                        }
    
                    echo "</tr>";
                }
    
            echo "</tbody>";
            
        }
        echo "</table>";




    }

    /**
     * Overrides Deploy if Styling Specified
     *
     * @param obj $style
     * @return void
     */
    private function deploy_with_style($style) {
       
        // Gather Data
        $rowCount   =   $this->tableRows->get_rows();
        $rowData    =   $this->tableRows->get_data();

        // Start Table
        echo "<table class=\"{$style->table()}\">";

        for ($bodyNumber=0; $bodyNumber <= $this->bodies; $bodyNumber++) { 

            // Add Column Style
            if ( ! empty($this->columnWidth[$bodyNumber]) ) {
                foreach ($this->columnWidth[$bodyNumber] as $key => $value) {
                    if ( $value == "" ) {
                        echo "<col />";
                    }
                    else {
                        echo "<col style='width: {$value}px;' />";
                    }
                }
            }

            // Start Head
            echo "<thead class=\"{$style->get_head()}\">";
            echo "<tr  class=\"{$style->head_row()}\">";

                foreach ($this->columnNames[$bodyNumber] as $columnNumber => $columnName) {
                    echo "<td class=\"{$style->head_cell()}\">{$columnName}</td>";
                }

            echo "</tr>";
            echo "</thead>";
            
            // Start Body
            echo "<tbody class=\"{$style->body()}\">";
    
                // Loop Through the Amount of Rows We Have
                for ($i=1; $i < $rowCount[$bodyNumber] + 1  ; $i++) { 
                    echo "<tr class=\"{$style->body_row()}\">";
    
                        // Add Data to Cells
                        foreach ($rowData[$bodyNumber][$i] as $key => $value) {
                            echo "<td  class=\"{$style->body_cell()}\" matrix='{$bodyNumber}-{$i}-{$key}'>{$value}</td>";
                        }
    
                    echo "</tr>";
                }
    
            echo "</tbody>";
            
        }
        echo "</table>";

    }  
    
    /**
     * Used for Setting Target Platform and additional classes
     *
     * @param str $platform
     * @param str $style
     * @return object
     */
    private function table_style_set($platform, $style){
        switch ( strtoupper($platform) ) {
            case 'EVENT':
                return new TableStyleEvent($style);
                break;
            case 'CMS':
                return new TableStyleCMS($style);
                break;
            default:
                return null;
                break;
        }
    }

}

/**
 * Controller for Table Rows
 */
class TableRows {

    private $tableRows = [];
    private $dataArray = [];

    /** Add New Row to Body Index */
    public function add($data, $bodies) {

        if ( array_key_exists($bodies, $this->tableRows) ) {
            $this->tableRows[$bodies]++;
        }
        else {
            $this->tableRows[$bodies] = 1;
        }
        $this->dataArray[$bodies][$this->tableRows[$bodies]] = $data;
    }

    /**
     * Returns Row Count for Each Body
     *
     * @return array
     */
    public function get_rows():array {
        return $this->tableRows;
    }

    /**
     * Returns All Cell Data
     *
     * @return array
     */
    public function get_data():array {
        return $this->dataArray;
    }

    function __destruct() {
        //debug($this->dataArray);
    }
}

interface TableStyle {
    public function table():string;
    public function get_head():string;
    public function head_row():string;
    public function head_cell():string;
    public function body():string;
    public function body_row():string;
    public function body_cell():string;
}

/**
 * Returns Event System Styling Class Names
 */
class TableStyleEvent implements TableStyle {
    
    private $Table;
    private $Head;
    private $HeadRow;
    private $HeadCell;
    private $Body;
    private $BodyRow;
    private $BodyCell;

    function __construct($style) {
        $this->Table        = "class='table-{$style}' ";
        $this->Head         = "class='head-{$style}' ";
        $this->HeadRow      = "class='headrow-{$style}' ";
        $this->HeadCell     = "class='headcell-{$style}' ";
        $this->Body         = "class='body-{$style}' ";
        $this->BodyRow      = "class='bodyrow-{$style}' ";
        $this->BodyCell     = "class='bodycell-{$style}' ";
    }

    public function table():string {
        return $this->Table;
    }

    public function get_head():string {
        return $this->Head;
    }
    
    public function head_row():string {
        return $this->HeadRow;
    }
    
    public function head_cell():string {
        return $this->HeadCell;
    }
    
    public function body():string {
        return $this->Body;
    }
    
    public function body_row():string {
        return $this->BodyRow;
    }
    
    public function body_cell():string {
        return $this->BodyCell;
    }
    
}


$table = new Table("event", "event");
$table->new_column("one", "two");
$table->col_width(20, 55);
$table->new_row(1, 2);
$table->new_row(2, 2);
$table->new_row(1123123123, 2);
$table->new_row(1, 2123123123);
$table->extend();
$table->new_column("1", "2", "3", "4");
$table->new_row(11, 12, 13, 14);
$table->new_row(21, 22, 23, 24);
$table->new_row(31, 32, 33, 34);
$table->deploy();



//$table->debug();






















// Helpers

function debug(...$debug) {
    $i = 1;
    foreach ($debug as $d) {

        echo("<pre class=\"debug\"><b>Debug Data {$i} is shown below</b>\n");
        print_r($d);
        echo("</pre>");
        $i++;
    }
}

?>